---
title: 2012's Music In Review
description: What music was I listening to in 2012?
date: 2013-01-01T00:00:00
aliases:
- /posts/2013/01/01/2012-music-in-review/
---
In 2012, I listened to 4619.3 minutes (76.99 hours, 3.21 days) of music on Spotify.

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<h2>Breakdown of listening over the months</h2>

<p>Each month, how much music did I listen to? Of that, what was taken up by that month's top 50 songs, or top 10 artists?</p>

<p>What about how much of that month was listening to songs that are in the top 100 songs over the year?</p>

<p>
  <canvas id="months"></canvas>
</p>

<script>
  const months = {"12":{"total":195001332,"songs":{"Sub Focus - Tidal Wave":9741620,"Ron Pope - A Drop In The Ocean":6543246,"Imagine Dragons - Radioactive":5682685,"will.i.am - Scream & Shout":4835021,"Of Monsters And Men - Little Talks":4561638,"Kesha - Die Young":3827800,"Calvin Harris - Sweet Nothing (feat. Florence Welch)":3256542,"Bruno Mars - Locked Out of Heaven":3214211,"Skrillex - Kyoto (feat. Sirah)":2982722,"Disclosure - Latch Featuring Sam Smith":2827361,"Mumford & Sons - The Cave":2810745,"The Lumineers - Slow It Down":2748114,"Sia - Breathe Me":2741863,"Various Artists - Clique":2642517,"Two Door Cinema Club - Sun":2443870,"Gabrielle Aplin - The Power of Love":2361278,"Conor Maynard - Turn Around - feat. Ne-Yo":2323850,"Little Mix - DNA":2150230,"Otto Knows - Million Voices":2147450,"Swedish House Mafia - Don't You Worry Child - Radio Edit":2128620,"The Script - Hall of Fame":2026955,"Gotye - Somebody That I Used To Know":1970550,"Mumford & Sons - I Will Wait":1937040,"Rudimental - Feel the Love":1884931,"Rudimental - Not Giving In":1862994,"fun. - Some Nights":1810404,"Flo Rida - I Cry":1790400,"Ne-Yo - Let Me Love You (Until You Learn To Love Yourself)":1778651,"Maroon 5 - One More Night":1756731,"Justin Bieber - Beauty And A Beat":1641313,"Calvin Harris - We'll Be Coming Back (feat. Example)":1640520,"Wiley - Can You Hear Me? (ayayaya) (feat. Skepta, JME & Ms D)":1628305,"The Script - Six Degrees of Separation":1625771,"Owl City - Good Time":1608783,"McFly - Love Is Easy":1598728,"alt-J - Breezeblocks":1575738,"Nicki Minaj - Va Va Voom":1479256,"Coldplay - Princess of China":1464943,"One Direction - Little Things":1461297,"Ellie Goulding - Anything Could Happen":1446306,"Labrinth - Beneath Your Beautiful":1443430,"Florence + The Machine - Spectrum (Say My Name) - Calvin Harris Remix":1403892,"Taylor Swift - We Are Never Ever Getting Back Together":1350902,"David Guetta - She Wolf (Falling to Pieces) [feat. Sia]":1341776,"Emeli Sandé - Read All About It, Pt. III":1323542,"Olly Murs - Troublemaker (feat. Flo Rida)":1300680,"The Weeknd - Wicked Games":1294932,"The Lumineers - Ho Hey":1293569,"P!nk - Try":1270339,"Calvin Harris - Bounce (feat. Kelis) - Radio Edit":1207043},"artists":{"Sub Focus":9741620,"Calvin Harris":7540341,"Various Artists":6668137,"Ron Pope":6543246,"The Lumineers":6442934,"Mumford & Sons":6440254,"Imagine Dragons":5682685,"will.i.am":4835021,"Of Monsters And Men":4561638,"The Script":4268973},"minsTopSongs":2559.55},"11":{"total":82156809,"songs":{"Of Monsters And Men - Little Talks":4537947,"Calvin Harris - Sweet Nothing (feat. Florence Welch)":4144358,"Various Artists - Continuous Mix 1":2824036,"Gabrielle Aplin - The Power of Love":2211475,"Linkin Park - BURN IT DOWN":2072277,"Two Door Cinema Club - Sun":1827284,"Linkin Park - POWERLESS":1366985,"The Wanted - I Found You":1172420,"Linkin Park - ROADS UNTRAVELED":1149665,"fun. - Some Nights":1114973,"alt-J - Something Good":1091200,"Linkin Park - CASTLE OF GLASS":1064853,"Linkin Park - I'LL BE GONE":1057926,"Alex Clare - Too Close":1026452,"Linkin Park - SKIN TO BONE":1010076,"Linkin Park - UNTIL IT BREAKS":979164,"Bon Iver - Skinny Love":955751,"Various Artists - Clique":938851,"One Direction - Little Things":878560,"Mumford & Sons - I Will Wait":832064,"Linkin Park - LOST IN THE ECHO":829418,"Labrinth - Earthquake":823800,"Skrillex - Kyoto (feat. Sirah)":803144,"Linkin Park - IN MY REMAINS":802772,"P!nk - Blow Me (One Last Kiss)":766758,"BURNS - Lies - Otto Knows Remix":758772,"Various Artists - Just You - Original Mix":736666,"Otto Knows - Million Voices":715012,"I See MONSTAS - Holdin' On - Skrillex & Nero Remix":711723,"The Script - Six Degrees of Separation":696759,"Sam and the Womp - Bom Bom - Radio Edit":696132,"Noisia - Split The Atom":684826,"McFly - Love Is Easy":684437,"Justin Bieber - Beauty And A Beat":683958,"Mumford & Sons - The Cave":660350,"Owl City - Good Time":628585,"Black Sun Empire - Wasteland":625140,"Linkin Park - LIES GREED MISERY":618970,"Black Sun Empire - Hyper Sun":613724,"The Script - Hall of Fame":607599,"Taylor Swift - We Are Never Ever Getting Back Together":581094,"Deaf Havana - Hunstanton Pier":579466,"Emeli Sandé - Read All About It, Pt. III":567412,"Dubsidia - Dont Tell It":565722,"The Lumineers - Stubborn Love":554666,"Ben Howard - Only Love":529099,"Disclosure - Latch Featuring Sam Smith":513880,"Calvin Harris - I Need Your Love (feat. Ellie Goulding)":506301,"D.I.D. - Teenage Daughter":505760,"Two Door Cinema Club - Next Year":502986},"artists":{"Linkin Park":11655790,"Various Artists":5233258,"Calvin Harris":5133444,"Of Monsters And Men":4969376,"Two Door Cinema Club":2980684,"Mumford & Sons":2348614,"Lana Del Rey":2237906,"Gabrielle Aplin":2211475,"alt-J":1915428,"Swedish House Mafia":1903185},"minsTopSongs":1013.2666666666667}}

  const labels = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ]

  var totals = {
    label: '# minutes',
    data: [],
    borderColor: '#519f50',
    fill: false,
  }

  var artists = {
    label: 'Top 10 Artists',
    data: [],
    borderColor: '#fbb829',
    fill: false,
  }

  var songs = {
    label: 'Top 50 Songs',
    data: [],
    borderColor: '#0aaeb3',
    fill: false,
  }

  var top100Songs = {
    label: 'Songs from the annual top 100',
    data: [],
    borderColor: '#ef2f27',
    fill: false,
  }

  for (var month in months) {
    var m = Math.round(months[month].total / 1000 / 60)
    totals.data.push(m)

    let s = 0
    for (var song in months[month].songs) {
      var m = Math.round(months[month].songs[song] / 1000 / 60)
      s += m
    }
    songs.data.push(s)

    s = 0
    for (var artist in months[month].artists) {
      var m = Math.round(months[month].artists[artist] / 1000 / 60)
      s += m
    }
    artists.data.push(s)

    top100Songs.data.push(months[month].minsTopSongs)
  }

  new Chart(document.getElementById('months'),
    {
      type: "line",
      data: {
        labels: labels,
        datasets: [
          totals,
          songs,
          artists,
          top100Songs,
        ],
      },
      options: {
        title: {
          display: true,
          text: 'Month-based listening breakdown',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
  );


</script>


<h2>Most music listened to in a given day</h2>

<p>
  <canvas id="days"></canvas>
</p>

<script>
  new Chart(document.getElementById('days'),
    {
      type: "line",
      data: {"labels":["2012-12-12","2012-12-03","2012-12-05","2012-12-04","2012-12-02","2012-11-25","2012-12-07","2012-11-27","2012-11-24","2012-12-06","Average"],"datasets":[{"label":"Hours listened to per day","data":[5.85,5.72,5.0,4.97,4.93,4.56,4.55,4.44,3.93,3.72,2.08],"borderColor":"#519f50","fill":false}]},
      options: {
        title: {
          display: true,
          text: 'Most music listened to in a given day',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
  );


</script>

<h2>Raw data</h2>

<details>
  <summary>Top 100 songs</summary>
  <table>
    <tr>
      <th>Song Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Sub Focus - Tidal Wave</td>
      <td>169.9</td>
      <td>2.83</td>
    </tr>
    <tr>
      <td>Of Monsters And Men - Little Talks</td>
      <td>151.65</td>
      <td>2.53</td>
    </tr>
    <tr>
      <td>Calvin Harris - Sweet Nothing (feat. Florence Welch)</td>
      <td>123.33</td>
      <td>2.06</td>
    </tr>
    <tr>
      <td>Ron Pope - A Drop In The Ocean</td>
      <td>109.05</td>
      <td>1.82</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Radioactive</td>
      <td>94.73</td>
      <td>1.58</td>
    </tr>
    <tr>
      <td>will.i.am - Scream & Shout</td>
      <td>80.58</td>
      <td>1.34</td>
    </tr>
    <tr>
      <td>Gabrielle Aplin - The Power of Love</td>
      <td>76.2</td>
      <td>1.27</td>
    </tr>
    <tr>
      <td>Two Door Cinema Club - Sun</td>
      <td>71.18</td>
      <td>1.19</td>
    </tr>
    <tr>
      <td>Kesha - Die Young</td>
      <td>63.78</td>
      <td>1.06</td>
    </tr>
    <tr>
      <td>Skrillex - Kyoto (feat. Sirah)</td>
      <td>63.08</td>
      <td>1.05</td>
    </tr>
    <tr>
      <td>Bruno Mars - Locked Out of Heaven</td>
      <td>61.53</td>
      <td>1.03</td>
    </tr>
    <tr>
      <td>Various Artists - Clique</td>
      <td>59.68</td>
      <td>0.99</td>
    </tr>
    <tr>
      <td>Mumford & Sons - The Cave</td>
      <td>57.85</td>
      <td>0.96</td>
    </tr>
    <tr>
      <td>Disclosure - Latch Featuring Sam Smith</td>
      <td>55.68</td>
      <td>0.93</td>
    </tr>
    <tr>
      <td>The Lumineers - Slow It Down</td>
      <td>50.88</td>
      <td>0.85</td>
    </tr>
    <tr>
      <td>Sia - Breathe Me</td>
      <td>50.23</td>
      <td>0.84</td>
    </tr>
    <tr>
      <td>fun. - Some Nights</td>
      <td>48.75</td>
      <td>0.81</td>
    </tr>
    <tr>
      <td>Otto Knows - Million Voices</td>
      <td>47.7</td>
      <td>0.8</td>
    </tr>
    <tr>
      <td>Various Artists - Continuous Mix 1</td>
      <td>47.07</td>
      <td>0.78</td>
    </tr>
    <tr>
      <td>Mumford & Sons - I Will Wait</td>
      <td>46.15</td>
      <td>0.77</td>
    </tr>
    <tr>
      <td>The Script - Hall of Fame</td>
      <td>43.9</td>
      <td>0.73</td>
    </tr>
    <tr>
      <td>Swedish House Mafia - Don't You Worry Child - Radio Edit</td>
      <td>42.85</td>
      <td>0.71</td>
    </tr>
    <tr>
      <td>Conor Maynard - Turn Around - feat. Ne-Yo</td>
      <td>42.6</td>
      <td>0.71</td>
    </tr>
    <tr>
      <td>Linkin Park - BURN IT DOWN</td>
      <td>42.2</td>
      <td>0.7</td>
    </tr>
    <tr>
      <td>Little Mix - DNA</td>
      <td>39.77</td>
      <td>0.66</td>
    </tr>
    <tr>
      <td>The Wanted - I Found You</td>
      <td>39.42</td>
      <td>0.66</td>
    </tr>
    <tr>
      <td>One Direction - Little Things</td>
      <td>38.98</td>
      <td>0.65</td>
    </tr>
    <tr>
      <td>Justin Bieber - Beauty And A Beat</td>
      <td>38.75</td>
      <td>0.65</td>
    </tr>
    <tr>
      <td>The Script - Six Degrees of Separation</td>
      <td>38.7</td>
      <td>0.65</td>
    </tr>
    <tr>
      <td>McFly - Love Is Easy</td>
      <td>38.05</td>
      <td>0.63</td>
    </tr>
    <tr>
      <td>Flo Rida - I Cry</td>
      <td>37.3</td>
      <td>0.62</td>
    </tr>
    <tr>
      <td>Owl City - Good Time</td>
      <td>37.28</td>
      <td>0.62</td>
    </tr>
    <tr>
      <td>Gotye - Somebody That I Used To Know</td>
      <td>36.92</td>
      <td>0.62</td>
    </tr>
    <tr>
      <td>Maroon 5 - One More Night</td>
      <td>36.67</td>
      <td>0.61</td>
    </tr>
    <tr>
      <td>Ne-Yo - Let Me Love You (Until You Learn To Love Yourself)</td>
      <td>33.87</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Nicki Minaj - Va Va Voom</td>
      <td>32.58</td>
      <td>0.54</td>
    </tr>
    <tr>
      <td>Taylor Swift - We Are Never Ever Getting Back Together</td>
      <td>32.18</td>
      <td>0.54</td>
    </tr>
    <tr>
      <td>BURNS - Lies - Otto Knows Remix</td>
      <td>31.6</td>
      <td>0.53</td>
    </tr>
    <tr>
      <td>Emeli Sandé - Read All About It, Pt. III</td>
      <td>31.5</td>
      <td>0.53</td>
    </tr>
    <tr>
      <td>Rudimental - Feel the Love</td>
      <td>31.4</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Calvin Harris - We'll Be Coming Back (feat. Example)</td>
      <td>31.28</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Wiley - Can You Hear Me? (ayayaya) (feat. Skepta, JME & Ms D)</td>
      <td>31.05</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Rudimental - Not Giving In</td>
      <td>31.03</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Florence + The Machine - Spectrum (Say My Name) - Calvin Harris Remix</td>
      <td>30.67</td>
      <td>0.51</td>
    </tr>
    <tr>
      <td>Labrinth - Beneath Your Beautiful</td>
      <td>30.47</td>
      <td>0.51</td>
    </tr>
    <tr>
      <td>Linkin Park - POWERLESS</td>
      <td>30.25</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>alt-J - Breezeblocks</td>
      <td>30.03</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>Alex Clare - Too Close</td>
      <td>29.93</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>David Guetta - She Wolf (Falling to Pieces) [feat. Sia]</td>
      <td>29.78</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>P!nk - Try</td>
      <td>29.52</td>
      <td>0.49</td>
    </tr>
    <tr>
      <td>Ellie Goulding - Anything Could Happen</td>
      <td>28.87</td>
      <td>0.48</td>
    </tr>
    <tr>
      <td>Coldplay - Princess of China</td>
      <td>28.47</td>
      <td>0.47</td>
    </tr>
    <tr>
      <td>The xx - Chained</td>
      <td>28.18</td>
      <td>0.47</td>
    </tr>
    <tr>
      <td>Labrinth - Earthquake</td>
      <td>27.48</td>
      <td>0.46</td>
    </tr>
    <tr>
      <td>Linkin Park - ROADS UNTRAVELED</td>
      <td>26.82</td>
      <td>0.45</td>
    </tr>
    <tr>
      <td>alt-J - Something Good</td>
      <td>25.53</td>
      <td>0.43</td>
    </tr>
    <tr>
      <td>Mumford & Sons - Lover Of The Light</td>
      <td>25.15</td>
      <td>0.42</td>
    </tr>
    <tr>
      <td>Linkin Park - I'LL BE GONE</td>
      <td>24.67</td>
      <td>0.41</td>
    </tr>
    <tr>
      <td>Linkin Park - CASTLE OF GLASS</td>
      <td>24.58</td>
      <td>0.41</td>
    </tr>
    <tr>
      <td>The Lumineers - Ho Hey</td>
      <td>24.4</td>
      <td>0.41</td>
    </tr>
    <tr>
      <td>Deaf Havana - Hunstanton Pier</td>
      <td>24.13</td>
      <td>0.4</td>
    </tr>
    <tr>
      <td>Calvin Harris - Drinking from the Bottle (feat. Tinie Tempah)</td>
      <td>24.03</td>
      <td>0.4</td>
    </tr>
    <tr>
      <td>The Lumineers - Stubborn Love</td>
      <td>23.1</td>
      <td>0.39</td>
    </tr>
    <tr>
      <td>RITA ORA - Shine Ya Light</td>
      <td>22.9</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Ellie Goulding - Figure 8</td>
      <td>22.82</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Linkin Park - SKIN TO BONE</td>
      <td>22.43</td>
      <td>0.37</td>
    </tr>
    <tr>
      <td>Olly Murs - Troublemaker</td>
      <td>21.77</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>Olly Murs - Troublemaker (feat. Flo Rida)</td>
      <td>21.67</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>The Weeknd - Wicked Games</td>
      <td>21.57</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>alt-J - Tessellate</td>
      <td>21.35</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>Naughty Boy - Wonder</td>
      <td>20.75</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>P!nk - Blow Me (One Last Kiss)</td>
      <td>20.37</td>
      <td>0.34</td>
    </tr>
    <tr>
      <td>Linkin Park - UNTIL IT BREAKS</td>
      <td>20.28</td>
      <td>0.34</td>
    </tr>
    <tr>
      <td>Calvin Harris - Bounce (feat. Kelis) - Radio Edit</td>
      <td>20.2</td>
      <td>0.34</td>
    </tr>
    <tr>
      <td>Linkin Park - IN MY REMAINS</td>
      <td>20.07</td>
      <td>0.33</td>
    </tr>
    <tr>
      <td>Bon Iver - Skinny Love</td>
      <td>19.9</td>
      <td>0.33</td>
    </tr>
    <tr>
      <td>Sam and the Womp - Bom Bom - Radio Edit</td>
      <td>19.08</td>
      <td>0.32</td>
    </tr>
    <tr>
      <td>The Pogues - Fairytale of New York (feat. Kirsty MacColl)</td>
      <td>18.75</td>
      <td>0.31</td>
    </tr>
    <tr>
      <td>fun. - We Are Young (feat. Janelle Monáe)</td>
      <td>18.6</td>
      <td>0.31</td>
    </tr>
    <tr>
      <td>One Direction - Kiss You</td>
      <td>17.77</td>
      <td>0.3</td>
    </tr>
    <tr>
      <td>Linkin Park - LOST IN THE ECHO</td>
      <td>17.23</td>
      <td>0.29</td>
    </tr>
    <tr>
      <td>Noisia - Split The Atom</td>
      <td>17.12</td>
      <td>0.29</td>
    </tr>
    <tr>
      <td>The Lumineers - Classy Girls</td>
      <td>16.85</td>
      <td>0.28</td>
    </tr>
    <tr>
      <td>HAIM - Don't Save Me</td>
      <td>16.35</td>
      <td>0.27</td>
    </tr>
    <tr>
      <td>Christina Aguilera - Your Body</td>
      <td>16.28</td>
      <td>0.27</td>
    </tr>
    <tr>
      <td>The Lumineers - Flowers In Your Hair</td>
      <td>16.28</td>
      <td>0.27</td>
    </tr>
    <tr>
      <td>Rihanna - Diamonds</td>
      <td>16.23</td>
      <td>0.27</td>
    </tr>
    <tr>
      <td>Maroon 5 - Payphone</td>
      <td>15.55</td>
      <td>0.26</td>
    </tr>
    <tr>
      <td>Misha B - Do You Think of Me - Radio edit</td>
      <td>15.5</td>
      <td>0.26</td>
    </tr>
    <tr>
      <td>Linkin Park - LIES GREED MISERY</td>
      <td>15.18</td>
      <td>0.25</td>
    </tr>
    <tr>
      <td>Ben Howard - Oats In The Water</td>
      <td>14.98</td>
      <td>0.25</td>
    </tr>
    <tr>
      <td>One Direction - Heart Attack</td>
      <td>14.92</td>
      <td>0.25</td>
    </tr>
    <tr>
      <td>JAY Z - Ni**as In Paris</td>
      <td>14.77</td>
      <td>0.25</td>
    </tr>
    <tr>
      <td>Stooshe - Waterfalls</td>
      <td>14.77</td>
      <td>0.25</td>
    </tr>
    <tr>
      <td>Plan B - Playing with Fire (feat. Labrinth)</td>
      <td>14.65</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>Chase & Status - Big Man</td>
      <td>14.43</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>The xx - Angels</td>
      <td>14.42</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>Wiley - Heatwave (feat. Ms. D)</td>
      <td>14.03</td>
      <td>0.23</td>
    </tr>
    <tr>
      <td>Swedish House Mafia - Greyhound</td>
      <td>13.7</td>
      <td>0.23</td>
    </tr>
    <tr>
      <td>Carly Rae Jepsen - Call Me Maybe</td>
      <td>13.5</td>
      <td>0.23</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 artists</summary>
  <table>
    <tr>
      <th>Artist</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Linkin Park</td>
      <td>259.72</td>
      <td>4.33</td>
    </tr>
    <tr>
      <td>Calvin Harris</td>
      <td>211.22</td>
      <td>3.52</td>
    </tr>
    <tr>
      <td>Various Artists</td>
      <td>198.35</td>
      <td>3.31</td>
    </tr>
    <tr>
      <td>Sub Focus</td>
      <td>169.9</td>
      <td>2.83</td>
    </tr>
    <tr>
      <td>Of Monsters And Men</td>
      <td>158.85</td>
      <td>2.65</td>
    </tr>
    <tr>
      <td>Mumford & Sons</td>
      <td>146.47</td>
      <td>2.44</td>
    </tr>
    <tr>
      <td>The Lumineers</td>
      <td>131.53</td>
      <td>2.19</td>
    </tr>
    <tr>
      <td>Ron Pope</td>
      <td>109.05</td>
      <td>1.82</td>
    </tr>
    <tr>
      <td>Imagine Dragons</td>
      <td>94.73</td>
      <td>1.58</td>
    </tr>
    <tr>
      <td>The Script</td>
      <td>94.52</td>
      <td>1.58</td>
    </tr>
    <tr>
      <td>One Direction</td>
      <td>93.23</td>
      <td>1.55</td>
    </tr>
    <tr>
      <td>Two Door Cinema Club</td>
      <td>90.4</td>
      <td>1.51</td>
    </tr>
    <tr>
      <td>alt-J</td>
      <td>82.05</td>
      <td>1.37</td>
    </tr>
    <tr>
      <td>will.i.am</td>
      <td>80.58</td>
      <td>1.34</td>
    </tr>
    <tr>
      <td>Swedish House Mafia</td>
      <td>80.27</td>
      <td>1.34</td>
    </tr>
    <tr>
      <td>Gabrielle Aplin</td>
      <td>76.2</td>
      <td>1.27</td>
    </tr>
    <tr>
      <td>Ellie Goulding</td>
      <td>72.83</td>
      <td>1.21</td>
    </tr>
    <tr>
      <td>Skrillex</td>
      <td>70.27</td>
      <td>1.17</td>
    </tr>
    <tr>
      <td>fun.</td>
      <td>67.37</td>
      <td>1.12</td>
    </tr>
    <tr>
      <td>Kesha</td>
      <td>63.78</td>
      <td>1.06</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 albums</summary>
  <table>
    <tr>
      <th>Album Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Linkin Park - LIVING THINGS</td>
      <td>259.72</td>
      <td>4.33</td>
    </tr>
    <tr>
      <td>Calvin Harris - 18 Months</td>
      <td>211.22</td>
      <td>3.52</td>
    </tr>
    <tr>
      <td>Sub Focus - Tidal Wave</td>
      <td>169.9</td>
      <td>2.83</td>
    </tr>
    <tr>
      <td>Of Monsters And Men - My Head Is An Animal</td>
      <td>141.25</td>
      <td>2.35</td>
    </tr>
    <tr>
      <td>The Lumineers - The Lumineers</td>
      <td>131.53</td>
      <td>2.19</td>
    </tr>
    <tr>
      <td>Ron Pope - The Bedroom Demos</td>
      <td>109.05</td>
      <td>1.82</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Hear Me EP</td>
      <td>94.73</td>
      <td>1.58</td>
    </tr>
    <tr>
      <td>One Direction - Take Me Home: Yearbook Edition</td>
      <td>93.2</td>
      <td>1.55</td>
    </tr>
    <tr>
      <td>The Script - #3 Deluxe Version</td>
      <td>91.72</td>
      <td>1.53</td>
    </tr>
    <tr>
      <td>Two Door Cinema Club - Beacon</td>
      <td>86.6</td>
      <td>1.44</td>
    </tr>
    <tr>
      <td>alt-J - An Awesome Wave</td>
      <td>82.05</td>
      <td>1.37</td>
    </tr>
    <tr>
      <td>will.i.am - Scream & Shout</td>
      <td>80.58</td>
      <td>1.34</td>
    </tr>
    <tr>
      <td>Gabrielle Aplin - The Power of Love</td>
      <td>76.2</td>
      <td>1.27</td>
    </tr>
    <tr>
      <td>Skrillex - Bangarang EP</td>
      <td>70.27</td>
      <td>1.17</td>
    </tr>
    <tr>
      <td>Mumford & Sons - Sigh No More</td>
      <td>66.15</td>
      <td>1.1</td>
    </tr>
    <tr>
      <td>Bruno Mars - Locked Out Of Heaven</td>
      <td>61.53</td>
      <td>1.03</td>
    </tr>
    <tr>
      <td>Various Artists - Kanye West Presents Good Music Cruel Summer</td>
      <td>59.68</td>
      <td>0.99</td>
    </tr>
    <tr>
      <td>Labrinth - Electronic Earth</td>
      <td>57.97</td>
      <td>0.97</td>
    </tr>
    <tr>
      <td>Disclosure - Latch Featuring Sam Smith</td>
      <td>55.68</td>
      <td>0.93</td>
    </tr>
    <tr>
      <td>Sia - Colour The Small One</td>
      <td>50.23</td>
      <td>0.84</td>
    </tr>
  </table>
</details>
