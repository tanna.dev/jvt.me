---
title: 2015's Music In Review
description: What music was I listening to in 2015?
date: 2016-01-01T00:00:00
aliases:
- /posts/2016/01/01/2015-music-in-review/
---
In 2015, I listened to 19711.92 minutes (328.53 hours, 13.69 days) of music on Spotify.

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<h2>Breakdown of listening over the months</h2>

<p>Each month, how much music did I listen to? Of that, what was taken up by that month's top 50 songs, or top 10 artists?</p>

<p>What about how much of that month was listening to songs that are in the top 100 songs over the year?</p>

<p>
  <canvas id="months"></canvas>
</p>

<script>
  const months = {"12":{"total":385673935,"songs":{"Uppermost - The People":5694331,"Bring Me The Horizon - Blasphemy":5609345,"deadmau5 - Ghosts 'n' Stuff":4314048,"The Prodigy - Breathe (Zeds Dead Remix)":3008044,"Various Artists - Hold Your Breath - Spectrasoul Remix":2978849,"Uppermost - New Moon":2918555,"Fred V & Grafix - Maverick Souls":2825769,"Fred V & Grafix - Forest Fires - Etherwood Remix":2711997,"Fred V & Grafix - Clouds Cross Skies":2646602,"Logistics - Thunder Child":2581455,"Fred V & Grafix - Green Destiny":2509024,"EDEN - Nocturne (Pierce Fulton Remix)":2439298,"Peter Gregson - A Little Chaos":2425028,"Uppermost - Gather":2339124,"Uppermost - Breaking Time":2319929,"Logistics - Triangles":2268546,"Carly Rae Jepsen - Run Away With Me":2261839,"MitiS - Forever - Original Mix":2254209,"Fred V & Grafix - Minor Happy":2162728,"The Prodigy - Breathe (The Glitch Mob Remix)":2132072,"Ed Sheeran - Nina":2022739,"MitiS - Vulnerability - Original Mix":1993818,"Fred V & Grafix - Maverick Souls - Dan Dakota Remix":1970834,"Fred V & Grafix - Here With You":1874708,"Tantrum Desire - Freeze":1867199,"Kiasmos - Looped":1841494,"Chase & Status - Alive":1839536,"Fred V & Grafix - Major Happy - Frederic Robinson Remix":1833118,"Feint - Fall Away":1807693,"Snow Ghosts - And The World Was Gone - Calibre Remix":1748741,"Fred V & Grafix - Major Happy":1726465,"Fred V & Grafix - Hydra":1704384,"Uppermost - Visions":1702196,"Fred V & Grafix - Green Destiny - Subwave Remix":1699181,"Uppermost - Blame It on Love":1654353,"Uppermost - Flashback":1651019,"Uppermost - Outsider":1633560,"Camo & Krooked - All Fall Down - Club Mix":1626089,"Feint - Vagrant":1611418,"Uppermost - Positive":1601689,"Fred V & Grafix - Recognise":1599905,"Fred V & Grafix - 3D Glasses":1590209,"Uppermost - Fly":1579022,"Fred V & Grafix - Let Your Guard Down - Hugh Hardie Remix":1572122,"Fred V & Grafix - Recognise - Emperor Remix":1571512,"Bring Me The Horizon - Oh No":1538725,"Camo & Krooked - Move Around":1496745,"Uppermost - Side Effects":1485807,"Ed Sheeran - I'm a Mess":1482799,"Fred V & Grafix - Forest Fires":1471724},"artists":{"Uppermost":59209016,"Fred V & Grafix":46239961,"Various Artists":28195138,"Tantrum Desire":22662922,"Camo & Krooked":20662719,"Logistics":15312452,"Bring Me The Horizon":14558961,"The Glitch Mob":10556289,"MitiS":9930220,"Feint":9551346},"minsTopSongs":2217.8166666666666},"11":{"total":322678505,"songs":{"EDEN - Nocturne (Pierce Fulton Remix)":3473898,"Various Artists - City Needs Sleep - Original Mix":2406258,"Camo And Krooked - Without You":2288900,"Various Artists - Run Away From Me":2278842,"Tantrum Desire - Underground":2017493,"Various Artists - Green Valleys - Original Mix":1935752,"Uppermost - Fly":1849803,"Bring Me The Horizon - Throne":1843182,"Uppermost - The People":1731151,"Imagine Dragons - Shots - Broiler Remix":1721301,"Tove Lo - Talking Body - Gryffin Remix":1617826,"Uppermost - Side Effects":1610784,"Martin Garrix - Don't Look Down":1561945,"Uppermost - Life Is a Fight":1560004,"Uppermost - Devotion":1519171,"Carly Rae Jepsen - Run Away With Me":1507890,"Logistics - Thunder Child":1473409,"DJ Fresh - Hypercaine":1431983,"Phaeleh - Afterglow":1416575,"Uppermost - Last Codes":1416184,"deadmau5 - Ghosts 'n' Stuff":1354050,"Uppermost - Flashback":1337710,"London Grammar - Strong - High Contrast Remix":1320684,"Fred V & Grafix - Here With You":1308329,"Andreya Triana - Lullaby - Logistics Remix":1301155,"S.P.Y - By Your Side":1300101,"Kove - Searching":1268957,"Uppermost - Always Begin":1121048,"Coasts - Oceans":1113823,"Various Artists - Goodbye For Now My Love - Original Mix":1093688,"Uppermost - Angels":1090482,"Uppermost - Format Funk":1061300,"Fred V & Grafix - Forest Fires":1039961,"Uppermost - Closer to You":1031985,"HNNY - Memory Tape One":1010877,"Madeon - Imperium":1009196,"Sebastian Sollerman - Don't Stop - Sam Heyman & Elliet Mendoza Remix":1004018,"Camo & Krooked - Without You - Original Mix":997542,"Madeon - Icarus - Fred V & Grafix Remix":993653,"Chase & Status - Lost & Not Found":980722,"Various Artists - Voyager":966168,"Miike Snow - Black & Blue - Netsky Remix":949670,"Hybrid Minds - New Orleans - Bcee Remix":948975,"Uppermost - The Core":948942,"Uppermost - Outsider":934272,"The Prototypes - Cascade":922770,"Danny Byrd - Like A Byrd":890520,"Uppermost - Left Unsaid":871428,"ShockOne - Home":866734,"Tez Cadey - Seve - Radio Edit":864487},"artists":{"Various Artists":37338949,"Uppermost":35437584,"Fred V & Grafix":12660749,"High Contrast":7634316,"Madeon":6879510,"Netsky":6822054,"Tantrum Desire":6578832,"Camo & Krooked":5985924,"Sub Focus":4828051,"Hybrid Minds":4687045},"minsTopSongs":1206.95},"10":{"total":360177168,"songs":{"Madeon - Imperium":3350444,"Netsky - I Refuse - Shockone Remix":2980695,"Madeon - Technicolor":2964777,"Skrillex - With You, Friends (Long Drive)":2320238,"Phaeleh - Afterglow":2010212,"Martin Solveig - Ready 2 Go - Single Edit":1952961,"Maduk - Never Again":1887922,"Various Artists - The Trip":1705657,"Various Artists - City Needs Sleep - Original Mix":1622430,"East & Young - In The Sky (Horizon) [feat. David Spekter]":1599785,"Madeon - Pay No Mind (feat. Passion Pit)":1558890,"Sigala - Easy Love":1554593,"Netsky - Prisma":1441048,"Sub Focus - Tidal Wave":1363068,"Coasts - Oceans":1339887,"John Dahlback - Atlantis":1327113,"S.P.Y - By Your Side":1300101,"Netsky - Give & Take":1262718,"Uppermost - Different":1245744,"Martin Solveig - Intoxicated - Radio Edit":1223835,"Camo & Krooked - Vice":1203792,"Pendulum - The Tempest":1181481,"Sub Focus - Turn It Around":1177796,"Fred V & Grafix - Hydra":1173304,"Black Sun Empire - Arrakis":1164692,"Tantrum Desire - Reach VIP":1131851,"Various Artists - Trick Of The Tail":1130988,"Chase & Status - Lost & Not Found":1130594,"Kaskade - Disarm You (feat. Ilsey)":1111449,"Diplo - Boy Oh Boy":1110402,"Various Artists - Coast To Coast":1093964,"High Contrast - The First Note Is Silent":1072866,"Netsky - Smile":1062754,"Camo & Krooked - Aurora":1053371,"Fred V & Grafix - Just a Thought":1040520,"Various Artists - Red Mist - VIP":1037083,"Jakwob - Blinding - Bcee Remix":1025867,"Various Artists - Believe - Original Mix":1016503,"Sub Focus - Follow The Light":1003273,"Fred V & Grafix - Shine":998104,"Black Caviar - Lady (Hear Me Tonight)":983396,"Danny Byrd - Get On It":978948,"Various Artists - Turn to Salt":966924,"Nu:Tone - Say Hello, Wave Goodbye":965748,"Uppermost - Visions":965705,"Imagine Dragons - Shots - Broiler Remix":959761,"Blackmill - In The Night Of Wilderness":958274,"Brookes Brothers - Last Night - Full Version":948102,"Matrix & Futurebound - All I Know - M&F's Rolling Out DJ Mix":904740,"Kove - Searching":893026},"artists":{"Various Artists":40067009,"Netsky":12093535,"Madeon":11472237,"Uppermost":9733547,"Snow Patrol":9445648,"Matrix & Futurebound":9301947,"Sub Focus":7778963,"Dyro":7584025,"Blackmill":6606071,"VA":6451703},"minsTopSongs":914.9},"9":{"total":108832225,"songs":{"Martin Solveig - Intoxicated - Radio Edit":1894474,"The Weeknd - Can't Feel My Face":1714761,"Sigala - Easy Love":1314444,"Various Artists - Written Emotions - Original Mix":1312222,"Justin Bieber - What Do You Mean?":1289658,"Uppermost - Fly":1288898,"Various Artists - Liquid Spirit (Claptone Remix)":1144917,"Carly Rae Jepsen - Run Away With Me":1134974,"Example - Whisky Story":1114252,"Coasts - Oceans":1046710,"Koen Holtkamp - Hoosick":1010777,"Foals - Mountain at My Gates":982081,"Seven Lions - Lose Myself":947607,"Galantis - Peanut Butter Jelly":937253,"Gregory Porter - Liquid Spirit - Claptone Remix Edit":937119,"Noctilucent - Something New":848989,"deadmau5 - Ghosts 'n' Stuff":842861,"Kiasmos - Swayed":807345,"Taylor McFerrin - Degrees of Light":798160,"WALK THE MOON - Shut Up and Dance":797850,"One Direction - Drag Me Down":793982,"Kiasmos - Held":791183,"MitiS - Born - Original Mix":733245,"Uppermost - Different":677026,"Coyote Kisses - Acid Wolfpack - Original Mix":653216,"Rachel Platten - Fight Song":631637,"Various Artists - Beautiful Lies":630804,"Uppermost - Futur":621027,"Calvin Harris - How Deep Is Your Love":603007,"Raw Theory - Meltdown":595687,"Lost Frequencies - Are You With Me - Radio Edit":573832,"MitiS - Innocent Discretion - Original Mix":569310,"Metrik - Freefall - xKore Remix":557251,"Kiasmos - Thrown":548709,"We Are The Ocean - Good For You":544849,"Mark Ronson - Uptown Funk":514687,"Major Lazer - Lean On (feat. MØ & DJ Snake)":508778,"Jack Ü - Where Are Ü Now (with Justin Bieber)":500556,"Skream - Anticipate - Netsky Remix":499899,"Metrik - Out Of The Fire":497070,"Wolf Alice - You’re a Germ":488958,"The Weeknd - The Hills":484360,"Various Artists - Turn to Salt":482952,"Skrillex - Kill EVERYBODY":467297,"Pitbull - Time of Our Lives":458760,"BT - In the Air (Mord Fustang Remix)":458265,"Galantis - Runaway (U & I)":454154,"Ron Pope - A Drop In The Ocean":452592,"Sigma - Glitterball":452553,"Fetty Wap - Trap Queen":444211},"artists":{"Uppermost":7851710,"Various Artists":7056524,"Kiasmos":3576882,"MitiS":2842428,"Skrillex":2632460,"The Weeknd":2199121,"Martin Solveig":2153738,"deadmau5":2142481,"Seven Lions":1615851,"Carly Rae Jepsen":1405577},"minsTopSongs":303.05},"6":{"total":2244254,"songs":{"The Glitch Mob - Fly By Night Only (feat. Yaarrohs)":556303,"deadmau5 - The Veldt":521847,"The Glitch Mob - A Dream Within A Dream":333540,"deadmau5 - Strobe - Radio Edit":214177,"Skrillex - Make It Bun Dem":213654,"Pretty Lights - Finally Moving":202787,"Nosaj Thing - Fog":126657,"Skrillex - Coast Is Clear":43236,"Skrillex - Scatta (feat. Foreign Beggars & Bare Noize)":22225,"Bassnectar - You & Me (feat. W. Darling)":7463,"Feed Me - Love Is All I Got":1428,"Modestep - Sunlight":937,"The Glitch Mob - The Clouds Breathe for You":0,"The Glitch Mob - Mind of a Beast":0,"Various Artists - I Can't Stop":0,"The Glitch Mob - Fly by Night Only - Yaarrohs Cover":0,"Various Artists - Ampersand":0,"Flux Pavilion - Daydreamer (feat. Example) - Radio Edit":0,"NERO - Me And You":0,"The Glitch Mob - Skullclub":0,"Mt. Eden - What's Below":0,"deadmau5 - The Veldt - Radio Edit":0,"Pendulum - Crush":0,"Ronald Jenkees - Guitar Sound":0,"deadmau5 - I Remember":0,"Pendulum - The Island -, Pt. I (Dawn)":0,"Klaypex - Lights":0},"artists":{"The Glitch Mob":889843,"deadmau5":736024,"Skrillex":279115,"Pretty Lights":202787,"Nosaj Thing":126657,"Bassnectar":7463,"Feed Me":1428,"Modestep":937,"Klaypex":0,"Ronald Jenkees":0},"minsTopSongs":0.0},"5":{"total":1692382,"songs":{"Otto Knows - Next to Me - Matisse & Sadko Remix":302314,"Tiësto - Secrets":250905,"Michael Brun - See You Soon":206079,"Armin van Buuren - Another You - Headhunterz Radio Edit":187950,"Lou Van - My Love":182029,"Tough Love - So Freakin' Tight":161305,"OMI - Cheerleader (Felix Jaehn Remix) - Radio Edit":132578,"Mark Ronson - Uptown Funk":97761,"Disclosure - Holding On":85474,"Jess Glynne - Hold My Hand":59640,"Carly Rae Jepsen - I Really Like You":19765,"LunchMoney Lewis - Bills":4230,"SIROJ - Slowly":1670,"The Chemical Brothers - Go":682,"Kygo - Firestone (feat. Conrad Sewell)":0,"Wiz Khalifa - See You Again (feat. Charlie Puth)":0,"Fetty Wap - Trap Queen":0,"Jack Ü - Where Are Ü Now (with Justin Bieber)":0,"Jason Derulo - Want to Want Me":0,"Galantis - Runaway (U & I)":0,"Major Lazer - Lean On (feat. MØ & DJ Snake)":0},"artists":{"Otto Knows":302314,"Tiësto":250905,"Michael Brun":206079,"Armin van Buuren":187950,"Lou Van":182029,"Tough Love":161305,"OMI":132578,"Mark Ronson":97761,"Disclosure":85474,"Jess Glynne":59640},"minsTopSongs":0.0},"4":{"total":1069075,"songs":{"Ten Walls - Walking with Elephants":325520,"Borgeous - Tsunami - Original Mix":236706,"Far East Movement - The Illest - Version With Schoolboy Q":219999,"Hardwell - Call Me A Spaceman - Radio Edit":189375,"Tiësto - Say Something":97475},"artists":{"Ten Walls":325520,"Borgeous":236706,"Far East Movement":219999,"Hardwell":189375,"Tiësto":97475},"minsTopSongs":0.0},"3":{"total":348330,"songs":{"Ellie Goulding - Love Me Like You Do - From \"Fifty Shades Of Grey\"":287097,"Sub Focus - Triple X":23684,"Kelly Clarkson - Heartbeat Song":14305,"Kendrick Lamar - The Blacker The Berry":13792,"Various Artists - Triple X - 12 Inch Version":5758,"Sub Focus - Triple X - 12 Inch Version":3694},"artists":{"Ellie Goulding":287097,"Sub Focus":27378,"Kelly Clarkson":14305,"Kendrick Lamar":13792,"Various Artists":5758},"minsTopSongs":0.0}}

  const labels = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ]

  var totals = {
    label: '# minutes',
    data: [],
    borderColor: '#519f50',
    fill: false,
  }

  var artists = {
    label: 'Top 10 Artists',
    data: [],
    borderColor: '#fbb829',
    fill: false,
  }

  var songs = {
    label: 'Top 50 Songs',
    data: [],
    borderColor: '#0aaeb3',
    fill: false,
  }

  var top100Songs = {
    label: 'Songs from the annual top 100',
    data: [],
    borderColor: '#ef2f27',
    fill: false,
  }

  for (var month in months) {
    var m = Math.round(months[month].total / 1000 / 60)
    totals.data.push(m)

    let s = 0
    for (var song in months[month].songs) {
      var m = Math.round(months[month].songs[song] / 1000 / 60)
      s += m
    }
    songs.data.push(s)

    s = 0
    for (var artist in months[month].artists) {
      var m = Math.round(months[month].artists[artist] / 1000 / 60)
      s += m
    }
    artists.data.push(s)

    top100Songs.data.push(months[month].minsTopSongs)
  }

  new Chart(document.getElementById('months'),
    {
      type: "line",
      data: {
        labels: labels,
        datasets: [
          totals,
          songs,
          artists,
          top100Songs,
        ],
      },
      options: {
        title: {
          display: true,
          text: 'Month-based listening breakdown',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
  );


</script>


<h2>Most music listened to in a given day</h2>

<p>
  <canvas id="days"></canvas>
</p>

<script>
  new Chart(document.getElementById('days'),
    {
      type: "line",
      data: {"labels":["2015-12-11","2015-12-09","2015-11-25","2015-12-06","2015-11-11","2015-12-13","2015-10-28","2015-10-21","2015-10-07","2015-12-10","Average"],"datasets":[{"label":"Hours listened to per day","data":[10.61,10.47,10.37,7.73,7.0,6.75,6.6,6.51,6.27,6.17,2.76],"borderColor":"#519f50","fill":false}]},
      options: {
        title: {
          display: true,
          text: 'Most music listened to in a given day',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
  );


</script>

<h2>Raw data</h2>

<details>
  <summary>Top 100 songs</summary>
  <table>
    <tr>
      <th>Song Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Uppermost - The People</td>
      <td>123.75</td>
      <td>2.06</td>
    </tr>
    <tr>
      <td>deadmau5 - Ghosts 'n' Stuff</td>
      <td>113.98</td>
      <td>1.9</td>
    </tr>
    <tr>
      <td>EDEN - Nocturne (Pierce Fulton Remix)</td>
      <td>106.67</td>
      <td>1.78</td>
    </tr>
    <tr>
      <td>Bring Me The Horizon - Blasphemy</td>
      <td>93.48</td>
      <td>1.56</td>
    </tr>
    <tr>
      <td>Uppermost - Fly</td>
      <td>92.75</td>
      <td>1.55</td>
    </tr>
    <tr>
      <td>Carly Rae Jepsen - Run Away With Me</td>
      <td>85.93</td>
      <td>1.43</td>
    </tr>
    <tr>
      <td>Madeon - Imperium</td>
      <td>79.28</td>
      <td>1.32</td>
    </tr>
    <tr>
      <td>Logistics - Thunder Child</td>
      <td>73.72</td>
      <td>1.23</td>
    </tr>
    <tr>
      <td>Phaeleh - Afterglow</td>
      <td>71.28</td>
      <td>1.19</td>
    </tr>
    <tr>
      <td>Madeon - Technicolor</td>
      <td>68.68</td>
      <td>1.14</td>
    </tr>
    <tr>
      <td>Various Artists - City Needs Sleep - Original Mix</td>
      <td>67.13</td>
      <td>1.12</td>
    </tr>
    <tr>
      <td>Martin Solveig - Intoxicated - Radio Edit</td>
      <td>65.43</td>
      <td>1.09</td>
    </tr>
    <tr>
      <td>Skrillex - With You, Friends (Long Drive)</td>
      <td>64.6</td>
      <td>1.08</td>
    </tr>
    <tr>
      <td>Coasts - Oceans</td>
      <td>62.05</td>
      <td>1.03</td>
    </tr>
    <tr>
      <td>Bring Me The Horizon - Throne</td>
      <td>59.37</td>
      <td>0.99</td>
    </tr>
    <tr>
      <td>Uppermost - Side Effects</td>
      <td>57.1</td>
      <td>0.95</td>
    </tr>
    <tr>
      <td>Chase & Status - Lost & Not Found</td>
      <td>56.48</td>
      <td>0.94</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Forest Fires - Etherwood Remix</td>
      <td>56.38</td>
      <td>0.94</td>
    </tr>
    <tr>
      <td>Various Artists - Hold Your Breath - Spectrasoul Remix</td>
      <td>55.75</td>
      <td>0.93</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Underground</td>
      <td>55.48</td>
      <td>0.92</td>
    </tr>
    <tr>
      <td>The Prodigy - Breathe (Zeds Dead Remix)</td>
      <td>54.73</td>
      <td>0.91</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Clouds Cross Skies</td>
      <td>54.4</td>
      <td>0.91</td>
    </tr>
    <tr>
      <td>Uppermost - Flashback</td>
      <td>54.27</td>
      <td>0.9</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Here With You</td>
      <td>53.05</td>
      <td>0.88</td>
    </tr>
    <tr>
      <td>Uppermost - Visions</td>
      <td>53.03</td>
      <td>0.88</td>
    </tr>
    <tr>
      <td>Maduk - Never Again</td>
      <td>52.43</td>
      <td>0.87</td>
    </tr>
    <tr>
      <td>Uppermost - Breaking Time</td>
      <td>52.12</td>
      <td>0.87</td>
    </tr>
    <tr>
      <td>Sigala - Easy Love</td>
      <td>51.6</td>
      <td>0.86</td>
    </tr>
    <tr>
      <td>Logistics - Triangles</td>
      <td>51.27</td>
      <td>0.85</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Maverick Souls</td>
      <td>51.22</td>
      <td>0.85</td>
    </tr>
    <tr>
      <td>Netsky - I Refuse - Shockone Remix</td>
      <td>49.67</td>
      <td>0.83</td>
    </tr>
    <tr>
      <td>Various Artists - Green Valleys - Original Mix</td>
      <td>49.35</td>
      <td>0.82</td>
    </tr>
    <tr>
      <td>Uppermost - New Moon</td>
      <td>49.25</td>
      <td>0.82</td>
    </tr>
    <tr>
      <td>Camo And Krooked - Without You</td>
      <td>48.25</td>
      <td>0.8</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Hydra</td>
      <td>47.95</td>
      <td>0.8</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Shots - Broiler Remix</td>
      <td>47.87</td>
      <td>0.8</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Minor Happy</td>
      <td>45.05</td>
      <td>0.75</td>
    </tr>
    <tr>
      <td>Uppermost - Last Codes</td>
      <td>44.4</td>
      <td>0.74</td>
    </tr>
    <tr>
      <td>Uppermost - Hidden Poetry</td>
      <td>43.72</td>
      <td>0.73</td>
    </tr>
    <tr>
      <td>Uppermost - Different</td>
      <td>43.5</td>
      <td>0.73</td>
    </tr>
    <tr>
      <td>S.P.Y - By Your Side</td>
      <td>43.33</td>
      <td>0.72</td>
    </tr>
    <tr>
      <td>Uppermost - Outsider</td>
      <td>42.78</td>
      <td>0.71</td>
    </tr>
    <tr>
      <td>Tove Lo - Talking Body - Gryffin Remix</td>
      <td>42.67</td>
      <td>0.71</td>
    </tr>
    <tr>
      <td>Various Artists - Run Away From Me</td>
      <td>42.58</td>
      <td>0.71</td>
    </tr>
    <tr>
      <td>Netsky - Prisma</td>
      <td>42.02</td>
      <td>0.7</td>
    </tr>
    <tr>
      <td>Uppermost - Angels</td>
      <td>41.97</td>
      <td>0.7</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Green Destiny</td>
      <td>41.88</td>
      <td>0.7</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Forest Fires</td>
      <td>41.85</td>
      <td>0.7</td>
    </tr>
    <tr>
      <td>Uppermost - Futur</td>
      <td>41.28</td>
      <td>0.69</td>
    </tr>
    <tr>
      <td>Peter Gregson - A Little Chaos</td>
      <td>40.42</td>
      <td>0.67</td>
    </tr>
    <tr>
      <td>Kove - Searching</td>
      <td>40.25</td>
      <td>0.67</td>
    </tr>
    <tr>
      <td>Sub Focus - Turn It Around</td>
      <td>40.03</td>
      <td>0.67</td>
    </tr>
    <tr>
      <td>Uppermost - Life Is a Fight</td>
      <td>39.77</td>
      <td>0.66</td>
    </tr>
    <tr>
      <td>Uppermost - Blame It on Love</td>
      <td>39.67</td>
      <td>0.66</td>
    </tr>
    <tr>
      <td>Uppermost - Gather</td>
      <td>39.53</td>
      <td>0.66</td>
    </tr>
    <tr>
      <td>Uppermost - Action</td>
      <td>38.93</td>
      <td>0.65</td>
    </tr>
    <tr>
      <td>Feint - The Journey Feat. Veela - Original Mix</td>
      <td>38.67</td>
      <td>0.64</td>
    </tr>
    <tr>
      <td>London Grammar - Strong - High Contrast Remix</td>
      <td>38.52</td>
      <td>0.64</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Shine</td>
      <td>38.45</td>
      <td>0.64</td>
    </tr>
    <tr>
      <td>Bustre - Combine</td>
      <td>37.77</td>
      <td>0.63</td>
    </tr>
    <tr>
      <td>MitiS - Forever - Original Mix</td>
      <td>37.57</td>
      <td>0.63</td>
    </tr>
    <tr>
      <td>Kiasmos - Looped</td>
      <td>36.75</td>
      <td>0.61</td>
    </tr>
    <tr>
      <td>Uppermost - Positive</td>
      <td>36.15</td>
      <td>0.6</td>
    </tr>
    <tr>
      <td>Various Artists - Written Emotions - Original Mix</td>
      <td>36.13</td>
      <td>0.6</td>
    </tr>
    <tr>
      <td>Andreya Triana - Lullaby - Logistics Remix</td>
      <td>36.1</td>
      <td>0.6</td>
    </tr>
    <tr>
      <td>The Weeknd - Can't Feel My Face</td>
      <td>35.93</td>
      <td>0.6</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - 3D Glasses</td>
      <td>35.63</td>
      <td>0.59</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Recognise - Emperor Remix</td>
      <td>35.58</td>
      <td>0.59</td>
    </tr>
    <tr>
      <td>The Prodigy - Breathe (The Glitch Mob Remix)</td>
      <td>35.53</td>
      <td>0.59</td>
    </tr>
    <tr>
      <td>Uppermost - Devotion</td>
      <td>35.25</td>
      <td>0.59</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Major Happy - Frederic Robinson Remix</td>
      <td>35.1</td>
      <td>0.59</td>
    </tr>
    <tr>
      <td>Metrik - Freefall - xKore Remix</td>
      <td>35.03</td>
      <td>0.58</td>
    </tr>
    <tr>
      <td>Various Artists - Just A Thought</td>
      <td>34.68</td>
      <td>0.58</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Just a Thought</td>
      <td>34.68</td>
      <td>0.58</td>
    </tr>
    <tr>
      <td>Uppermost - Left Unsaid</td>
      <td>33.98</td>
      <td>0.57</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Reach VIP</td>
      <td>33.78</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Ed Sheeran - Nina</td>
      <td>33.7</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Uppermost - Countdown</td>
      <td>33.6</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Uppermost - Disco Kids</td>
      <td>33.23</td>
      <td>0.55</td>
    </tr>
    <tr>
      <td>MitiS - Vulnerability - Original Mix</td>
      <td>33.22</td>
      <td>0.55</td>
    </tr>
    <tr>
      <td>High Maintenance - Watching Me</td>
      <td>33.02</td>
      <td>0.55</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Maverick Souls - Dan Dakota Remix</td>
      <td>32.83</td>
      <td>0.55</td>
    </tr>
    <tr>
      <td>Tez Cadey - Seve - Radio Edit</td>
      <td>32.6</td>
      <td>0.54</td>
    </tr>
    <tr>
      <td>Martin Solveig - Ready 2 Go - Single Edit</td>
      <td>32.53</td>
      <td>0.54</td>
    </tr>
    <tr>
      <td>Danny Byrd - Gold Rush</td>
      <td>32.02</td>
      <td>0.53</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Recognise</td>
      <td>31.98</td>
      <td>0.53</td>
    </tr>
    <tr>
      <td>Martin Garrix - Don't Look Down</td>
      <td>31.57</td>
      <td>0.53</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Let Your Guard Down - Hugh Hardie Remix</td>
      <td>31.37</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Seven Lions - Lose Myself</td>
      <td>31.37</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Freeze</td>
      <td>31.12</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Madeon - The City</td>
      <td>31.1</td>
      <td>0.52</td>
    </tr>
    <tr>
      <td>Chase & Status - Alive</td>
      <td>30.65</td>
      <td>0.51</td>
    </tr>
    <tr>
      <td>Sub Focus - Tidal Wave</td>
      <td>30.25</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Guided Rhythm</td>
      <td>30.23</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>Madeon - Pay No Mind (feat. Passion Pit)</td>
      <td>30.13</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>Feint - Fall Away</td>
      <td>30.12</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>High Contrast - The First Note Is Silent</td>
      <td>29.8</td>
      <td>0.5</td>
    </tr>
    <tr>
      <td>Aurix - Source</td>
      <td>29.68</td>
      <td>0.49</td>
    </tr>
    <tr>
      <td>John Dahlback - Atlantis</td>
      <td>29.65</td>
      <td>0.49</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Move Around</td>
      <td>29.48</td>
      <td>0.49</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 artists</summary>
  <table>
    <tr>
      <th>Artist</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Various Artists</td>
      <td>1877.72</td>
      <td>31.3</td>
    </tr>
    <tr>
      <td>Uppermost</td>
      <td>1870.52</td>
      <td>31.18</td>
    </tr>
    <tr>
      <td>Fred V & Grafix</td>
      <td>1045.3</td>
      <td>17.42</td>
    </tr>
    <tr>
      <td>Camo & Krooked</td>
      <td>567.82</td>
      <td>9.46</td>
    </tr>
    <tr>
      <td>Tantrum Desire</td>
      <td>521.27</td>
      <td>8.69</td>
    </tr>
    <tr>
      <td>Logistics</td>
      <td>391.48</td>
      <td>6.52</td>
    </tr>
    <tr>
      <td>Madeon</td>
      <td>365.93</td>
      <td>6.1</td>
    </tr>
    <tr>
      <td>Netsky</td>
      <td>331.17</td>
      <td>5.52</td>
    </tr>
    <tr>
      <td>Bring Me The Horizon</td>
      <td>285.97</td>
      <td>4.77</td>
    </tr>
    <tr>
      <td>Maduk</td>
      <td>242.48</td>
      <td>4.04</td>
    </tr>
    <tr>
      <td>Sub Focus</td>
      <td>230.08</td>
      <td>3.83</td>
    </tr>
    <tr>
      <td>MitiS</td>
      <td>227.42</td>
      <td>3.79</td>
    </tr>
    <tr>
      <td>deadmau5</td>
      <td>211.3</td>
      <td>3.52</td>
    </tr>
    <tr>
      <td>The Glitch Mob</td>
      <td>199.77</td>
      <td>3.33</td>
    </tr>
    <tr>
      <td>High Contrast</td>
      <td>196.85</td>
      <td>3.28</td>
    </tr>
    <tr>
      <td>Feint</td>
      <td>192.75</td>
      <td>3.21</td>
    </tr>
    <tr>
      <td>Chase & Status</td>
      <td>189.95</td>
      <td>3.17</td>
    </tr>
    <tr>
      <td>Matrix & Futurebound</td>
      <td>183.17</td>
      <td>3.05</td>
    </tr>
    <tr>
      <td>Danny Byrd</td>
      <td>169.72</td>
      <td>2.83</td>
    </tr>
    <tr>
      <td>Snow Patrol</td>
      <td>157.42</td>
      <td>2.62</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 albums</summary>
  <table>
    <tr>
      <th>Album Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Fred V & Grafix - Recognise</td>
      <td>419.73</td>
      <td>7.0</td>
    </tr>
    <tr>
      <td>Uppermost - One</td>
      <td>403.38</td>
      <td>6.72</td>
    </tr>
    <tr>
      <td>Uppermost - Evolution</td>
      <td>401.22</td>
      <td>6.69</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Unrecognisable</td>
      <td>312.33</td>
      <td>5.21</td>
    </tr>
    <tr>
      <td>Various Artists - Galaxy of Dreams 2</td>
      <td>253.53</td>
      <td>4.23</td>
    </tr>
    <tr>
      <td>Uppermost - Action</td>
      <td>249.98</td>
      <td>4.17</td>
    </tr>
    <tr>
      <td>Uppermost - Revolution</td>
      <td>245.97</td>
      <td>4.1</td>
    </tr>
    <tr>
      <td>Bring Me The Horizon - That's The Spirit</td>
      <td>226.07</td>
      <td>3.77</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Zeitgeist</td>
      <td>197.92</td>
      <td>3.3</td>
    </tr>
    <tr>
      <td>Madeon - Adventure (Deluxe)</td>
      <td>182.97</td>
      <td>3.05</td>
    </tr>
    <tr>
      <td>Uppermost - New Moon - EP</td>
      <td>172.15</td>
      <td>2.87</td>
    </tr>
    <tr>
      <td>Ed Sheeran - x</td>
      <td>142.77</td>
      <td>2.38</td>
    </tr>
    <tr>
      <td>Logistics - Polyphony</td>
      <td>131.95</td>
      <td>2.2</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Diversified</td>
      <td>129.22</td>
      <td>2.15</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Cross The Line</td>
      <td>128.42</td>
      <td>2.14</td>
    </tr>
    <tr>
      <td>Sub Focus - Torus</td>
      <td>122.18</td>
      <td>2.04</td>
    </tr>
    <tr>
      <td>VA - Escapism 2 - (Liquicity Presents)</td>
      <td>116.68</td>
      <td>1.94</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Between The Lines</td>
      <td>107.07</td>
      <td>1.78</td>
    </tr>
    <tr>
      <td>EDEN - Nocturne (Pierce Fulton Remix)</td>
      <td>106.67</td>
      <td>1.78</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Here With You</td>
      <td>98.1</td>
      <td>1.64</td>
    </tr>
  </table>
</details>
