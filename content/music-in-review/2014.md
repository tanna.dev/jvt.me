---
title: 2014's Music In Review
description: What music was I listening to in 2014?
date: 2015-01-01T00:00:00
aliases:
- /posts/2015/01/01/2014-music-in-review/
---
In 2014, I listened to 871.88 minutes (14.53 hours, 0.61 days) of music on Spotify.

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<h2>Breakdown of listening over the months</h2>

<p>Each month, how much music did I listen to? Of that, what was taken up by that month's top 50 songs, or top 10 artists?</p>

<p>What about how much of that month was listening to songs that are in the top 100 songs over the year?</p>

<p>
  <canvas id="months"></canvas>
</p>

<script>
  const months = {"8":{"total":1286447,"songs":{"Faul & Wad Ad - Changes":344163,"MAGIC! - Rude":224782,"Steve Aoki - Delirious (Boneless) (feat. Kid Ink)":223424,"Katy Perry - Birthday":211501,"Jason Derulo - Wiggle (feat. Snoop Dogg)":193279,"Katy Perry - Walking On Air":50435,"deadmau5 - Avaritia":26618,"Various Artists - Steve Angello - Dance Mega Mix Intro":5511,"Deorro - Rambo - Hardwell Edit":4459,"Tove Lo - Out Of Mind":2275,"Tove Lo - Habits (Stay High)":0,"Tove Lo - Stay High - Habits Remix":0,"Various Artists - Nobody To Love - Radio Edit":0,"Tove Lo - Paradise":0},"artists":{"Faul & Wad Ad":344163,"Katy Perry":261936,"MAGIC!":224782,"Steve Aoki":223424,"Jason Derulo":193279,"deadmau5":26618,"Various Artists":5511,"Deorro":4459,"Tove Lo":2275},"minsTopSongs":21.266666666666666},"7":{"total":412107,"songs":{"Porter Robinson - Sea Of Voices":296494,"G-Eazy - Been On":43954,"Various Artists - For The Love Of Money":27660,"Various Artists - Tequila":18041,"Lonnie Smith - Sweet Honey Wine":8947,"Passion Pit - Sleepyhead":7531,"Roy Ayers - Funk In The Hole":3250,"The 2 Bears - Bear Hug":3199,"Gorgon City - Ready For Your Love":1540,"Tom Waits - The Heart Of Saturday Night":1491,"Aztec Camera - Oblivious":0,"Evelyn \"Champagne\" King - Till I Come off the Road":0,"The Notorious B.I.G. - Hypnotize":0},"artists":{"Porter Robinson":296494,"Various Artists":45701,"G-Eazy":43954,"Lonnie Smith":8947,"Passion Pit":7531,"Roy Ayers":3250,"The 2 Bears":3199,"Gorgon City":1540,"Tom Waits":1491,"Aztec Camera":0},"minsTopSongs":6.133333333333334},"6":{"total":11015063,"songs":{"Jason Derulo - Trumpets":1220715,"Tove Lo - Stay High - Habits Remix":1033288,"David Guetta - Bad (feat. Vassy) - Radio Edit":511659,"Clean Bandit - Rather Be (feat. Jess Glynne)":455624,"George Ezra - Budapest":400414,"CRO - Traum":389692,"A Great Big World - Say Something":345147,"Deorro - Five Hours":335848,"Martin Garrix - Animals - Extended":303777,"One Direction - Story of My Life":292109,"Coldplay - Magic":287535,"John Legend - All of Me":269557,"Coldplay - A Sky Full of Stars":268407,"Tiësto - Red Lights":265399,"Avicii - Hey Brother":258591,"Passenger - Let Her Go":252681,"Kiesza - Hideaway":251950,"John Legend - All of Me (Tiësto's Birthday Treatment Remix) - Radio Edit":251636,"Avicii - Wake Me Up - Radio Edit":247405,"Calvin Harris - I Need Your Love (feat. Ellie Goulding)":235062,"5 Seconds Of Summer - She Looks So Perfect":233199,"Enya - Only Time":223828,"RITA ORA - I Will Never Let You Down":223001,"Naughty Boy - La La La":222223,"Bastille - Pompeii":218576,"Pitbull - Wild Wild Love":215625,"Mr. Probz - Waves - Robin Schulz Radio Edit":209067,"Nico & Vinz - Am I Wrong":203829,"Imagine Dragons - On Top Of The World":189831,"Jason Derulo - Talk Dirty (feat. 2 Chainz)":183481,"Sam Smith - Stay With Me":172747,"Avicii - Addicted To You":148401,"Enrique Iglesias - Bailando - Spanish Version":140326,"Tove Lo - Out Of Mind":93444,"Eminem - The Monster":90797,"Kid Ink - Show Me (feat. Chris Brown)":84823,"Milky Chance - Stolen Dance":75267,"Iggy Azalea - Fancy":61548,"Jason Derulo - Wiggle (feat. Snoop Dogg)":48803,"Daft Punk - Get Lucky (feat. Pharrell Williams & Nile Rodgers) - Radio Edit":37651,"Michael Jackson - Love Never Felt So Good":19093,"Pharrell Williams - Happy - From \"Despicable Me 2\"":9413,"Katy Perry - Birthday":7009,"Enya - Pax Deorum - 2009 Remaster":6968,"Calvin Harris - Summer":6614,"Faul & Wad Ad - Changes":3614,"Various Artists - Turn Down for What":3031,"Aloe Blacc - The Man":1489,"Pitbull - Timber":1488,"Duke Dumont - I Got U":1002},"artists":{"Jason Derulo":1452999,"Tove Lo":1126732,"Avicii":654397,"Coldplay":555942,"John Legend":521193,"David Guetta":511659,"Clean Bandit":455624,"George Ezra":400414,"CRO":389692,"A Great Big World":345147},"minsTopSongs":183.38333333333333},"5":{"total":38611909,"songs":{"John Legend - All of Me (Tiësto's Birthday Treatment Remix) - Radio Edit":3096391,"Nico & Vinz - Am I Wrong":2814524,"Calvin Harris - Summer":2023222,"David Guetta - Bad (feat. Vassy) - Radio Edit":1804967,"Calvin Harris - Under Control (feat. Hurts)":1474165,"Pitbull - Timber":1413225,"Lorde - Team":1310928,"Cash Cash - Take Me Home (feat. Bebe Rexha)":1269154,"John Legend - All of Me":1081000,"Mr. Probz - Waves - Robin Schulz Radio Edit":1061454,"Iggy Azalea - Fancy":1055521,"Röyksopp - Do It Again":1050621,"Clean Bandit - Rather Be (feat. Jess Glynne)":918475,"Kiesza - Hideaway":902228,"Paramore - Ain't It Fun":864720,"Bastille - Pompeii":859270,"Shakira - Can't Remember to Forget You":685485,"CAZZETTE - Sleepless - Radio Edit":665175,"Pharrell Williams - Happy - From \"Despicable Me 2\"":653800,"5 Seconds Of Summer - She Looks So Perfect":589430,"Jason Derulo - The Other Side":549185,"Imagine Dragons - Demons":526425,"John Newman - Love Me Again":487248,"Avicii - Hey Brother":470816,"Michael Jackson - Love Never Felt So Good":465396,"Vance Joy - Riptide":409027,"Imagine Dragons - Radioactive":373614,"Timeflies - All The Way":359296,"Sam Smith - Stay With Me":345494,"Faul & Wad Ad - Changes":344671,"Of Monsters And Men - Little Talks":317400,"Justin Timberlake - Not a Bad Thing":316353,"The Neighbourhood - Sweater Weather":314916,"Kid Ink - Show Me (feat. Chris Brown)":308010,"Coldplay - A Sky Full of Stars":289874,"Disclosure - Latch":283740,"Route 94 - My Love":281568,"Tiësto - Red Lights":263914,"Macklemore & Ryan Lewis - Can't Hold Us - feat. Ray Dalton":258322,"Eminem - The Monster":254671,"will.i.am - Feelin' Myself":254020,"Tove Lo - Stay High - Habits Remix":250666,"Skrillex - Recess":238679,"Bruno Mars - Locked out of Heaven":233455,"Katy Perry - Roar":225965,"One Direction - Story of My Life":221686,"Boyce Avenue - Speed Limit":220969,"Zedd - Stay The Night - Featuring Hayley Williams Of Paramore":217312,"Katy Perry - Birthday":212678,"Ellie Goulding - Burn":204948},"artists":{"John Legend":4177391,"Calvin Harris":3674899,"Nico & Vinz":2814524,"David Guetta":1996365,"Lorde":1549574,"Pitbull":1415216,"Cash Cash":1269154,"Mr. Probz":1061454,"Iggy Azalea":1055521,"Röyksopp":1050621},"minsTopSongs":642.2},"3":{"total":987731,"songs":{"Jason Derulo - Talk Dirty (feat. 2 Chainz)":501928,"CHVRCHES - Recover":225827,"Bastille - Flaws":217834,"Disclosure - F For You":20960,"Lily Allen - Air Balloon":15447,"Ellie Goulding - Goodness Gracious":1596,"Foxes - Let Go for Tonight":1562,"Arctic Monkeys - Arabella":1108,"Imagine Dragons - Demons":1057,"Jake Bugg - A Song About Love":412},"artists":{"Jason Derulo":501928,"CHVRCHES":225827,"Bastille":217834,"Disclosure":20960,"Lily Allen":15447,"Ellie Goulding":1596,"Foxes":1562,"Arctic Monkeys":1108,"Imagine Dragons":1057,"Jake Bugg":412},"minsTopSongs":16.116666666666667}}

  const labels = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ]

  var totals = {
    label: '# minutes',
    data: [],
    borderColor: '#519f50',
    fill: false,
  }

  var artists = {
    label: 'Top 10 Artists',
    data: [],
    borderColor: '#fbb829',
    fill: false,
  }

  var songs = {
    label: 'Top 50 Songs',
    data: [],
    borderColor: '#0aaeb3',
    fill: false,
  }

  var top100Songs = {
    label: 'Songs from the annual top 100',
    data: [],
    borderColor: '#ef2f27',
    fill: false,
  }

  for (var month in months) {
    var m = Math.round(months[month].total / 1000 / 60)
    totals.data.push(m)

    let s = 0
    for (var song in months[month].songs) {
      var m = Math.round(months[month].songs[song] / 1000 / 60)
      s += m
    }
    songs.data.push(s)

    s = 0
    for (var artist in months[month].artists) {
      var m = Math.round(months[month].artists[artist] / 1000 / 60)
      s += m
    }
    artists.data.push(s)

    top100Songs.data.push(months[month].minsTopSongs)
  }

  new Chart(document.getElementById('months'),
    {
      type: "line",
      data: {
        labels: labels,
        datasets: [
          totals,
          songs,
          artists,
          top100Songs,
        ],
      },
      options: {
        title: {
          display: true,
          text: 'Month-based listening breakdown',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
  );


</script>


<h2>Most music listened to in a given day</h2>

<p>
  <canvas id="days"></canvas>
</p>

<script>
  new Chart(document.getElementById('days'),
    {
      type: "line",
      data: {"labels":["2014-05-23","2014-05-27","2014-06-09","2014-05-28","2014-05-24","2014-05-29","2014-05-25","2014-05-26","2014-06-10","2014-05-30","Average"],"datasets":[{"label":"Hours listened to per day","data":[3.51,2.51,2.38,0.96,0.94,0.84,0.79,0.77,0.43,0.4,0.61],"borderColor":"#519f50","fill":false}]},
      options: {
        title: {
          display: true,
          text: 'Most music listened to in a given day',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    },
  );


</script>

<h2>Raw data</h2>

<details>
  <summary>Top 100 songs</summary>
  <table>
    <tr>
      <th>Song Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>John Legend - All of Me (Tiësto's Birthday Treatment Remix) - Radio Edit</td>
      <td>55.8</td>
      <td>0.93</td>
    </tr>
    <tr>
      <td>Nico & Vinz - Am I Wrong</td>
      <td>50.3</td>
      <td>0.84</td>
    </tr>
    <tr>
      <td>David Guetta - Bad (feat. Vassy) - Radio Edit</td>
      <td>38.6</td>
      <td>0.64</td>
    </tr>
    <tr>
      <td>Calvin Harris - Summer</td>
      <td>33.82</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Calvin Harris - Under Control (feat. Hurts)</td>
      <td>24.57</td>
      <td>0.41</td>
    </tr>
    <tr>
      <td>Pitbull - Timber</td>
      <td>23.57</td>
      <td>0.39</td>
    </tr>
    <tr>
      <td>Clean Bandit - Rather Be (feat. Jess Glynne)</td>
      <td>22.9</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>John Legend - All of Me</td>
      <td>22.5</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Lorde - Team</td>
      <td>21.83</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>Tove Lo - Stay High - Habits Remix</td>
      <td>21.38</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>Mr. Probz - Waves - Robin Schulz Radio Edit</td>
      <td>21.17</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>Cash Cash - Take Me Home (feat. Bebe Rexha)</td>
      <td>21.15</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>Jason Derulo - Trumpets</td>
      <td>20.47</td>
      <td>0.34</td>
    </tr>
    <tr>
      <td>Kiesza - Hideaway</td>
      <td>19.23</td>
      <td>0.32</td>
    </tr>
    <tr>
      <td>Iggy Azalea - Fancy</td>
      <td>18.62</td>
      <td>0.31</td>
    </tr>
    <tr>
      <td>Bastille - Pompeii</td>
      <td>17.95</td>
      <td>0.3</td>
    </tr>
    <tr>
      <td>Röyksopp - Do It Again</td>
      <td>17.5</td>
      <td>0.29</td>
    </tr>
    <tr>
      <td>Paramore - Ain't It Fun</td>
      <td>14.4</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>Jason Derulo - Talk Dirty (feat. 2 Chainz)</td>
      <td>14.38</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>5 Seconds Of Summer - She Looks So Perfect</td>
      <td>13.7</td>
      <td>0.23</td>
    </tr>
    <tr>
      <td>Avicii - Hey Brother</td>
      <td>12.15</td>
      <td>0.2</td>
    </tr>
    <tr>
      <td>Faul & Wad Ad - Changes</td>
      <td>11.53</td>
      <td>0.19</td>
    </tr>
    <tr>
      <td>Shakira - Can't Remember to Forget You</td>
      <td>11.42</td>
      <td>0.19</td>
    </tr>
    <tr>
      <td>CAZZETTE - Sleepless - Radio Edit</td>
      <td>11.08</td>
      <td>0.18</td>
    </tr>
    <tr>
      <td>Pharrell Williams - Happy - From "Despicable Me 2"</td>
      <td>11.05</td>
      <td>0.18</td>
    </tr>
    <tr>
      <td>George Ezra - Budapest</td>
      <td>10.0</td>
      <td>0.17</td>
    </tr>
    <tr>
      <td>CRO - Traum</td>
      <td>9.75</td>
      <td>0.16</td>
    </tr>
    <tr>
      <td>Coldplay - A Sky Full of Stars</td>
      <td>9.3</td>
      <td>0.16</td>
    </tr>
    <tr>
      <td>Jason Derulo - The Other Side</td>
      <td>9.15</td>
      <td>0.15</td>
    </tr>
    <tr>
      <td>Tiësto - Red Lights</td>
      <td>8.82</td>
      <td>0.15</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Demons</td>
      <td>8.78</td>
      <td>0.15</td>
    </tr>
    <tr>
      <td>Sam Smith - Stay With Me</td>
      <td>8.63</td>
      <td>0.14</td>
    </tr>
    <tr>
      <td>One Direction - Story of My Life</td>
      <td>8.55</td>
      <td>0.14</td>
    </tr>
    <tr>
      <td>John Newman - Love Me Again</td>
      <td>8.12</td>
      <td>0.14</td>
    </tr>
    <tr>
      <td>Michael Jackson - Love Never Felt So Good</td>
      <td>8.07</td>
      <td>0.13</td>
    </tr>
    <tr>
      <td>A Great Big World - Say Something</td>
      <td>7.75</td>
      <td>0.13</td>
    </tr>
    <tr>
      <td>Katy Perry - Birthday</td>
      <td>7.18</td>
      <td>0.12</td>
    </tr>
    <tr>
      <td>Calvin Harris - I Need Your Love (feat. Ellie Goulding)</td>
      <td>6.87</td>
      <td>0.11</td>
    </tr>
    <tr>
      <td>Jason Derulo - Wiggle (feat. Snoop Dogg)</td>
      <td>6.87</td>
      <td>0.11</td>
    </tr>
    <tr>
      <td>Vance Joy - Riptide</td>
      <td>6.83</td>
      <td>0.11</td>
    </tr>
    <tr>
      <td>Kid Ink - Show Me (feat. Chris Brown)</td>
      <td>6.53</td>
      <td>0.11</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Radioactive</td>
      <td>6.22</td>
      <td>0.1</td>
    </tr>
    <tr>
      <td>Timeflies - All The Way</td>
      <td>5.98</td>
      <td>0.1</td>
    </tr>
    <tr>
      <td>Eminem - The Monster</td>
      <td>5.75</td>
      <td>0.1</td>
    </tr>
    <tr>
      <td>Deorro - Five Hours</td>
      <td>5.58</td>
      <td>0.09</td>
    </tr>
    <tr>
      <td>Of Monsters And Men - Little Talks</td>
      <td>5.28</td>
      <td>0.09</td>
    </tr>
    <tr>
      <td>Justin Timberlake - Not a Bad Thing</td>
      <td>5.27</td>
      <td>0.09</td>
    </tr>
    <tr>
      <td>The Neighbourhood - Sweater Weather</td>
      <td>5.23</td>
      <td>0.09</td>
    </tr>
    <tr>
      <td>Martin Garrix - Animals - Extended</td>
      <td>5.05</td>
      <td>0.08</td>
    </tr>
    <tr>
      <td>Porter Robinson - Sea Of Voices</td>
      <td>4.93</td>
      <td>0.08</td>
    </tr>
    <tr>
      <td>Coldplay - Magic</td>
      <td>4.93</td>
      <td>0.08</td>
    </tr>
    <tr>
      <td>Disclosure - Latch</td>
      <td>4.73</td>
      <td>0.08</td>
    </tr>
    <tr>
      <td>Route 94 - My Love</td>
      <td>4.68</td>
      <td>0.08</td>
    </tr>
    <tr>
      <td>Macklemore & Ryan Lewis - Can't Hold Us - feat. Ray Dalton</td>
      <td>4.3</td>
      <td>0.07</td>
    </tr>
    <tr>
      <td>RITA ORA - I Will Never Let You Down</td>
      <td>4.28</td>
      <td>0.07</td>
    </tr>
    <tr>
      <td>will.i.am - Feelin' Myself</td>
      <td>4.23</td>
      <td>0.07</td>
    </tr>
    <tr>
      <td>Passenger - Let Her Go</td>
      <td>4.2</td>
      <td>0.07</td>
    </tr>
    <tr>
      <td>Avicii - Wake Me Up - Radio Edit</td>
      <td>4.12</td>
      <td>0.07</td>
    </tr>
    <tr>
      <td>Skrillex - Recess</td>
      <td>3.97</td>
      <td>0.07</td>
    </tr>
    <tr>
      <td>Bruno Mars - Locked out of Heaven</td>
      <td>3.88</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Katy Perry - Roar</td>
      <td>3.75</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>CHVRCHES - Recover</td>
      <td>3.75</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>MAGIC! - Rude</td>
      <td>3.73</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Enya - Only Time</td>
      <td>3.72</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Steve Aoki - Delirious (Boneless) (feat. Kid Ink)</td>
      <td>3.72</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Naughty Boy - La La La</td>
      <td>3.7</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Boyce Avenue - Speed Limit</td>
      <td>3.67</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Bastille - Flaws</td>
      <td>3.62</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Pitbull - Wild Wild Love</td>
      <td>3.62</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Zedd - Stay The Night - Featuring Hayley Williams Of Paramore</td>
      <td>3.62</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Ellie Goulding - Burn</td>
      <td>3.4</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Capital Cities - Safe And Sound</td>
      <td>3.3</td>
      <td>0.06</td>
    </tr>
    <tr>
      <td>Imagine Dragons - On Top Of The World</td>
      <td>3.25</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>American Authors - Best Day Of My Life</td>
      <td>3.25</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>David Guetta - Shot Me Down (feat. Skylar Grey) - Radio Edit</td>
      <td>3.18</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>Duke Dumont - I Got U</td>
      <td>3.18</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>CHVRCHES - The Mother We Share</td>
      <td>3.18</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>Avicii - Addicted To You</td>
      <td>3.07</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>The Chainsmokers - #SELFIE</td>
      <td>3.05</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>The Lumineers - Ho Hey</td>
      <td>2.75</td>
      <td>0.05</td>
    </tr>
    <tr>
      <td>Faul & Wad Ad - Changes - Radio Mix</td>
      <td>2.38</td>
      <td>0.04</td>
    </tr>
    <tr>
      <td>Enrique Iglesias - Bailando - Spanish Version</td>
      <td>2.33</td>
      <td>0.04</td>
    </tr>
    <tr>
      <td>OneRepublic - Counting Stars</td>
      <td>2.3</td>
      <td>0.04</td>
    </tr>
    <tr>
      <td>Lorde - Glory And Gore</td>
      <td>2.12</td>
      <td>0.04</td>
    </tr>
    <tr>
      <td>Sam Smith - Money On My Mind</td>
      <td>1.95</td>
      <td>0.03</td>
    </tr>
    <tr>
      <td>Lorde - Royals</td>
      <td>1.77</td>
      <td>0.03</td>
    </tr>
    <tr>
      <td>DJ Snake - Turn Down for What</td>
      <td>1.68</td>
      <td>0.03</td>
    </tr>
    <tr>
      <td>Tove Lo - Out Of Mind</td>
      <td>1.58</td>
      <td>0.03</td>
    </tr>
    <tr>
      <td>Indiana - Solo Dancing</td>
      <td>1.48</td>
      <td>0.02</td>
    </tr>
    <tr>
      <td>Milky Chance - Stolen Dance</td>
      <td>1.28</td>
      <td>0.02</td>
    </tr>
    <tr>
      <td>Katy Perry - Walking On Air</td>
      <td>0.83</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Katy Perry - Dark Horse</td>
      <td>0.75</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Various Artists - Let It Go - From "Frozen"/Soundtrack Version</td>
      <td>0.73</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>G-Eazy - Been On</td>
      <td>0.72</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Various Artists - Let It Go - English Version</td>
      <td>0.62</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Daft Punk - Get Lucky (feat. Pharrell Williams & Nile Rodgers) - Radio Edit</td>
      <td>0.62</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Howard Shore - I See Fire - From "The Hobbit - The Desolation Of Smaug"</td>
      <td>0.52</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Various Artists - For The Love Of Money</td>
      <td>0.45</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>deadmau5 - Avaritia</td>
      <td>0.43</td>
      <td>0.01</td>
    </tr>
    <tr>
      <td>Disclosure - F For You</td>
      <td>0.33</td>
      <td>0.01</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 artists</summary>
  <table>
    <tr>
      <th>Artist</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>John Legend</td>
      <td>78.3</td>
      <td>1.31</td>
    </tr>
    <tr>
      <td>Calvin Harris</td>
      <td>65.27</td>
      <td>1.09</td>
    </tr>
    <tr>
      <td>Jason Derulo</td>
      <td>50.88</td>
      <td>0.85</td>
    </tr>
    <tr>
      <td>Nico & Vinz</td>
      <td>50.3</td>
      <td>0.84</td>
    </tr>
    <tr>
      <td>David Guetta</td>
      <td>41.8</td>
      <td>0.7</td>
    </tr>
    <tr>
      <td>Pitbull</td>
      <td>27.2</td>
      <td>0.45</td>
    </tr>
    <tr>
      <td>Lorde</td>
      <td>25.82</td>
      <td>0.43</td>
    </tr>
    <tr>
      <td>Tove Lo</td>
      <td>22.98</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Clean Bandit</td>
      <td>22.9</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Bastille</td>
      <td>21.62</td>
      <td>0.36</td>
    </tr>
    <tr>
      <td>Mr. Probz</td>
      <td>21.17</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>Cash Cash</td>
      <td>21.15</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>Avicii</td>
      <td>19.35</td>
      <td>0.32</td>
    </tr>
    <tr>
      <td>Kiesza</td>
      <td>19.23</td>
      <td>0.32</td>
    </tr>
    <tr>
      <td>Iggy Azalea</td>
      <td>18.62</td>
      <td>0.31</td>
    </tr>
    <tr>
      <td>Imagine Dragons</td>
      <td>18.27</td>
      <td>0.3</td>
    </tr>
    <tr>
      <td>Röyksopp</td>
      <td>17.5</td>
      <td>0.29</td>
    </tr>
    <tr>
      <td>Paramore</td>
      <td>14.4</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>Coldplay</td>
      <td>14.23</td>
      <td>0.24</td>
    </tr>
    <tr>
      <td>Faul & Wad Ad</td>
      <td>13.93</td>
      <td>0.23</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 albums</summary>
  <table>
    <tr>
      <th>Album Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>John Legend - All of Me (Tiësto's Birthday Treatment Remix)</td>
      <td>55.8</td>
      <td>0.93</td>
    </tr>
    <tr>
      <td>Nico & Vinz - Am I Wrong</td>
      <td>50.3</td>
      <td>0.84</td>
    </tr>
    <tr>
      <td>David Guetta - Bad (feat. Vassy)</td>
      <td>38.6</td>
      <td>0.64</td>
    </tr>
    <tr>
      <td>Calvin Harris - Summer</td>
      <td>33.82</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Jason Derulo - Tattoos</td>
      <td>33.37</td>
      <td>0.56</td>
    </tr>
    <tr>
      <td>Lorde - Pure Heroine</td>
      <td>25.82</td>
      <td>0.43</td>
    </tr>
    <tr>
      <td>Calvin Harris - Under Control (feat. Hurts)</td>
      <td>24.57</td>
      <td>0.41</td>
    </tr>
    <tr>
      <td>Pitbull - Timber</td>
      <td>23.57</td>
      <td>0.39</td>
    </tr>
    <tr>
      <td>Tove Lo - Truth Serum</td>
      <td>22.98</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Clean Bandit - Rather Be (feat. Jess Glynne)</td>
      <td>22.9</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>John Legend - Love In The Future (Expanded Edition)</td>
      <td>22.5</td>
      <td>0.38</td>
    </tr>
    <tr>
      <td>Mr. Probz - Waves</td>
      <td>21.17</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>Cash Cash - Take Me Home (feat. Bebe Rexha)</td>
      <td>21.15</td>
      <td>0.35</td>
    </tr>
    <tr>
      <td>Avicii - True</td>
      <td>19.35</td>
      <td>0.32</td>
    </tr>
    <tr>
      <td>Kiesza - Hideaway</td>
      <td>19.23</td>
      <td>0.32</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Night Visions</td>
      <td>18.27</td>
      <td>0.3</td>
    </tr>
    <tr>
      <td>Bastille - All This Bad Blood</td>
      <td>17.98</td>
      <td>0.3</td>
    </tr>
    <tr>
      <td>Iggy Azalea - Fancy</td>
      <td>17.58</td>
      <td>0.29</td>
    </tr>
    <tr>
      <td>Röyksopp - Do It Again</td>
      <td>17.5</td>
      <td>0.29</td>
    </tr>
    <tr>
      <td>Paramore - Paramore</td>
      <td>14.4</td>
      <td>0.24</td>
    </tr>
  </table>
</details>
