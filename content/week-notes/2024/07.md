---
title: "Week Notes 24#07"
description: "What happened in the week of 2024-02-12?"
date: 2024-02-18T20:47:34+0000
---
A week in the public eye:

- Firstly, the Fedi-drama of Ryan's BlueSky bridge with Bridgy - some very good points, some personal attacks and my own (and many others') access to the Fediverse is [now lost](https://www.jvt.me/mf2/2024/02/cnzvz/)
  - It's given me a chance to start rethinking how I engage with things and how consent should work
  - But it does mean that if you're reading this and follow me through the Fediverse, maybe check you can still see my most recent posts 😅
  - It's been fun trying to work out which servers are now blocking my account, as well as catching up on the conversation across various instances
- My State of Open Con talk [is now live](https://www.youtube.com/watch?v=xHWmuNDxisw)
- I was on [Changelog and Friends](https://changelog.com/friends/31) and [wrote up a little bit about it](https://www.jvt.me/posts/2024/02/17/changelog-friends/)!
  - It was a really great experience, and although I've not yet finished listening to it - it's very weird listening to yourself 😅 - it seemed like I didn't make a fool of myself!
  - Although only a small section of the recording may be videos, I spent some time cleaning up my background to make it just right, and even popped my old XPS - with an excellent covering of stickers - into my Kallax so it's in view
  - Before [posting about my appearance on the podcast](https://www.jvt.me/posts/2024/02/17/changelog-friends/), I spent some time writing about [why you should listen to The Changelog](https://www.jvt.me/posts/2024/02/17/listen-changelog/) as I felt it was the right time to finally get around to writing it
  - I've been getting a lot of views on my website and DMD, which has been good, and hopefully will get some more folks interested in and using the project
  - There's also been some good discussion on Slack, and looking forward to hearing what others think soon
  - Tried to see if my old phones had any other podcast listening history, but unfortunately no data there I could use
  - I've not yet gotten around to watching _Memento_ but hopefully next week 👀

Also:

- Although the new season of _Apex Legends_ has come out, and I'd been planning on getting back into playing it, but days of having the download going and then restarting itself, I've instead just stuck with _TitanFall 2_
- Had a nice catch-up with Dom
- We've had some nice - but muddy! - walks this weekend at Mill Lakes and Colwick with Carol and Juno
- A shut-it-down day on Friday 🥱
  - And unfortunately came with the news that we're no longer going to be getting them going forwards. It's been so nice having a day off a month to look after myself, and although it was originally introduced during COVID, it's been such a nice perk and been very nice for giving me a day to focus on self care, like a monthly massage. Disappointed that we'll only have a few more for the rest of this calendar year, and then it's gone!
  - Had a nice massage to end the day
  - Went to Netherfield M&S to pick up a Valentine's meal but turns out they only sold it until Valentine's not all week 😅 Had nice a Indian meal from there, though
  - Didn't get around to any of the oapi-codegen work I'd hoped to, but glad to have had a chilled day
- Enjoyed watching [Tycho playing live](https://www.youtube.com/watch?v=v6uHSoe5a-k) as I've never seen him and the gang play
- Been putting together plans for a trip to Italy for my 30th
- Cookie's been rather bark-y this week, in evenings, while we're eating, when we're trying to get to sleep and when we're sitting down for lunch 😬
- Even though Cookie had a big walk on Friday, she ended up having some super zoomies in the afternoon which was very funny
- Made the sweet-and-spicy Korean sauce from a few months ago's deep fried KFC and it worked very nicely with Asda's own brand chicken dippers - a much safer way of preparing it, too
- Made some progress on a lil' bit of my life admin this morning
- Made some progress at work towards a proposal for an internal Open Source Dashboard, taking inspiration from Microsoft's (that was mentioned [on CHAOSScast](https://podcast.chaoss.community/77)) - hoping to tweak it further this week, and then propose it for implementation while we're in Prague for the Engineering All Hands
- Excited about an upcoming re-org in Engineering Productivity which hopefully will give some good opportunities for growth and getting more stuck into things
- Spent some time this week cleaning up source-tracked `golangci-lint`s across our repos, to speed up builds + do the needed cleanup
- Had some good chats following Mat's blog post about building Go HTTP services
- Been having some good chats with Ashley from GitHub about dependency-management-data, `dependabot-graph`, and measures to understand dependencies better, as well as hearing about the interesting stuff that GitHub's OSPO are doing

Also hello to all my new followers and readers 👋

Watched:

- _The Bear_
  - Finished Season 2 - very good, and thankfully not so long to wait
- _The Good Witch_
- _Mockingjay (Part 2)_
- _The Marvels_
- _Marvel's What If?_
