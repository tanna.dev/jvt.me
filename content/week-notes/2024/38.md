---
title: "Week Notes 24#38"
description: "What happened in the week of 2024-09-16?"
date: 2024-09-22T21:44:48+0100
---
A busy week ahead of a week off:

- A very busy on-call week, as well as trying to close things off before holiday
  - I ended up getting my first page (at Elastic) and realised that my PagerDuty setup did _not_ have the "do not disturb" bypass on 🫣 Fortunately I was at my desk, in a meeting, and noticed an alert in my emails
  - I'd also frontloaded all my meetings and work-y work, so I could have a clear day on Friday with Carol, and focus on `oapi-codegen`
- One night before bed, Cookie was barking (as she sometimes does) and it turns out she'd gotten herself stuck in my office while trying to steal one of my crocs 🫣
- Did some more tweaks to `renovate-graph` to fix the overriding + detection of repo-level configuration, which appears to have finally fixed it 👏🏼
- A little bit of blogging this week, and mostly written _How I manage my dotfiles_ and _Don't do Agile, be agile_, but not yet gotten either of them finished enough
- Wrote a small-ish post about not pretty-printing JSON response bodies and had some good traction on Hacker News, and it appeared in Golang Weekly, which was cool
- Had a nice time meeting Tushar, Steve, Lewis and Tulio on Tuesday evening
- Had a good catch-up with Abby
- No pizza this week 😱 But we had a nice takeaway from Spice, albeit it took a while (and we were already fairly hungry)
- Been listening to [Totem - Tycho](https://open.spotify.com/track/4LgTZ7yelUcMU6B3uelSop) a lot this week, as it's been a particularly perfect representation of Tycho's feel!
- Was cool to [chat to Kin Lane](https://www.jvt.me/posts/2024/09/19/api-evangelist-conversation/), as another podcast appearance
  - I wrote the post quickly before taking Cookie to the park, and while I was almost at the park, I thought "huh, did I put a date on the post?" and no, I had not 🫣
  - But fortunately could Micropub the post (via Micropublish, on my phone) and add one, so it correctly appeared elsewhere before we got out of the car and started our walk
- With it being WiT on Thursday, I took Cookie for a park walk, so she'd be less needing of a walk for Anna post-WiT
- ... and as I was at Carol's for coworking, but gave her a little walk in the morning before I left
- Had a fun day coworking with Carol on Friday
  - Was nice to hang, and we both had a pretty chill day so that helped
  - Had some very good food
  - Watched a fun spider-on-spider fight over a fly they both wanted to eat
  - Nice to see Mark for the first time in a while
- My Friday was also August + September's Elastic-supported time for `oapi-codegen`, which meant I was able to get on top of some of the backlog, and more importantly, get the next release out
  - The (perceived?) pressure led me to dropping a couple of issues/PRs out of the running, as there was still work required on them, but hopefully can sort them soon
  - Also received a new sponsor 🙌🏼
  - I gave GitHub a nudge - via the OSPO community - and got `oapi-codegen`'s org available as an option for sponsoring, too
- Definitely going to OggCamp now (solo unfortunately, as we can't get Cookie cover), but looking forward to it!
  - Still need to book hotel + travel but hopefully will do that in the next couple of days
- Got an office trip planned the week we're back, alongside my talk at DTX London
- Nice to have Sarah to stay this weekend
- We had thunderstorms last night, and while Anna and Sarah were out at Rakki Rakkas, Cookie came into my office while I was gaming, whimpering cause she was scared 🥺
  - While my game was finishing, she was leaning against my leg (on my standing mat) and I gave her some scritches
  - When I took her downstairs, I couldn't see Morph so was calling to him - in vain, over the sound of the rain - until he walked into the conservatory from the dining room to be like, "huh? I'm already here"
  - Cookie and Morph then came straight into the living room, and both got some cuddles to keep them feeling safe
  - And surprisingly Cookie was OK with Morph, which shows how scaredy she was 🥺
  - But it was nice to have them both cuddly
- Had a nice walk at Gravelley Hollow, but ended up getting a little lost so ended up being a longer walk than we planned 🫣
- Went to Sunday lunch at The Windmill Inn
  - Anna went with Sarah - so I could walk Cookie before leaving her - and so I had a bit of a longer less familiar drive for the first time
  - There was a _significantly_ awful weather, but I managed to drive OK, aside from a slightly sharp turning on a roundabout, and it's a nice confidence boost!
- Not had much time to play _Apex Legends_ but enjoying Knockout, albeit I've been having lots of `VIDEO_TDR_FAILURE` BSODs, every time I start a new game 😬
- There appears to be an extra app that's recently popped into my app list and I've no idea what it is, but things are slightly out of order at the bottom of my list 🤔
- We've managed to have Morph in the living room _a lot_ this week which has been nice
- He's also had a nice afternoon on his tower while we were upstairs, and Cookie was happy about it too
- Looking forward to a few days away in Wells-next-the-sea

Watched:

- _Only Murders in the Building_
- _The US Office_
- _What If?_
- _Seinfeld_
  - (At Carol's)
- _The Perfect Couple_
  - (As Anna's been watching it)
