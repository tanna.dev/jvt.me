---
title: "Week Notes 24#48"
description: "What happened in the week of 2024-11-25?"
date: 2024-12-01T20:54:23+0000
---
A very busy week at work, getting back into Ring Fit and getting on top of my office, ahead of guests staying over the next few weeks

- I've managed to nudge myself into getting back into Ring Fit
  - Did some good workouts Monday-Thursday, took a day off on Friday, and then this weekend's been busy so hadn't had a chance to work out any more
  - Finding the extra focus of not doing the "adventure" mode but trying to just focus on muscle / cardio workouts, which are the more important part of it being much better
  - Felt the burn and enjoying focussing on i.e. abs/core/back rather than it being whatever is most powerful against enemies
- Had a nice catch-up with Abby
- My post about Cobra + OTel was featured in Applied Go 🤓
- Been talking a lot about ADHD at work this last week
  - It's been nice to share some more about it and how it affects me, as well as feeding into my still-to-be-finished Manual Of Me updates
- Built a test harness for testing Renovate `autodiscoverFilter`s (at work) as crafting (and maintaining) the right, complex, regex felt rather risky
  - As ever, super big fan of the way Renovate as an OSS codebase works, and how straightforward I'm finding it to dig around in the Typescript codebase to create wrappers for i.e. extra validation
- Had a few songs on a lot of repeat this week:
  - [Talking to God - Elliot Vast Remix](https://open.spotify.com/track/4Ngxinbt2VmkL2n1VfEDbx)
  - [Timelines - Attom](https://open.spotify.com/track/6MAftl9xe8PJbUs18oaEyk)
- Looking forward to seeing what Spotify Wrapped has to show for the year, but given how I find it doesn't quite line up with the raw data (i.e. [2023's](/music-in-review/2023/))
- Was voted as a winner of the Engineering Awards at Elastic for October 👏🏼 Very appreciative of it, and I'll share some more details (in a separate post)
- Flossed for the first time in a bit, two days in a row, but my teeth felt weird afterwards
- I thought I'd catch up on some conference talk videos instead of listening to podcasts this week, which has been good
- Was reminded of how much I enjoy pate and in particular the Lidl festive pate, so we've had a lot of it this week 😋
  - But Cookie was _not_ happy she didn't get any, she was going wild and wanted to get as close to me while eating it on Friday night that I thought she'd try and get into my jumper if it meant she'd be able to get a chance at a morsel of pate being dropped 😹
- We got our tumble dryer, but didn't get it installed until today as it was a bit persnickety
- We made a very good start on my office - ahead of having folks to stay over Xmas
  - On Saturday morning I made a start with it - somewhat in hyperfocus
  - Then while at Asda Anna started doing a few bits and we spent a lot of the weekend on it - it's a lot better, but still a fair bit to do
- Been enjoying a bit more flexibility this week with showering later in the day - which also means I can do Ring Fit whenever works for me
- Didn't get perfect pizza, but managed to get some substitutes that made it a fairly nice approximation
- We've had a good few meals from Cook, which have been very useful and easy - once we've remembered to turn the oven on
- Back on _Cat Snack Bar_ a little more this weekend, as the Peak Time event is revamped and it was a good chance to play some more of the non-event levels

In dependency-management-data:

- Thanks to my colleague Mario, who was doing a hackathon week and was adding OTel support to a Cobra CLI (thanks to my blog post) noticed that traces on error conditions weren't showing up
  - Pointing me to the right solution, I fixed my own implementation + blogged about it
  - Was funny when Linh noted "how quick" I am with blogging 🫣
- Fixed an issue with `renovate-graph` where a repo's `enabled: false` would be respected, instead of ignored

In `oapi-codegen`:

- This week I did a day of `oapi-codegen` maintenance (thanks Elastic for combined time from October + November!)
  - But only got to spend ~60% of the day on it, as there was quite a lot of stuff going on at work I needed to help with, unfortunately
  - It's wild how much time it takes to maintain the project, and get through the open PRs I'd planned to get in for the next release, and I ended up getting through maybe a handful of things I'd planned to
  - I did do a bit of prep for improving responses (via an automated reply) for common questions / to save me from repeating myself
  - But again, sad to have not had/have the time to support the project as much as I'd want to (but ain't getting paid enough by users)

Watched:

- _Taskmaster_
- _Mr Right_
  - Was not great
- _The Lovebirds_
- _Love Hard_
- _Archer_
- _Saturday Night Live_ (on YouTube)
- Various _Dropout_-y shows (on YouTube)
  - _Um, Actually_
  - _Make Some Noise_
  - _Smartypants Presentation_
