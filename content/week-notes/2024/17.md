---
title: "Week Notes 24#17"
description: "What happened in the week of 2024-04-22?"
date: 2024-04-28T21:30:47+0100
---
- The second week of work being super busy 🥱
  - This coming week is the first week of our new on-call/host-of-the-week, so there's going to be a lot less context switching and (internal) customer support
  - Meant there's been a fair bit less free brain time outside of work as it's been rather intense during the week
- Had a couple of different folks at work reach out about dependency-management-data and whether it could help them solve a query which is cool
- While playing with Cookie, she jumped at my nose - mouth open - and her teeth broke the skin on my nose a fair bit
- I've had [Falling - Cyantific](https://open.spotify.com/track/5SI4qB5eBYg4z8ajWF9fwv?si=426da91b22fa4597) on loop a fair bit this weekend
- Got some Caramilk ice cream, which is confusing - not quite as nice as regular Caramilk
- Started using the [JIRA CLI](https://github.com/ankitpokhrel/jira-cli) at work to make it easier to open our board / create new issues, as I ended up starting to write a script to open up the various boards I need
- Spotted the [Encore x Golang Insiders Hackathon (Launch Week)](https://encore.dev/launchweek) and spent a bit of time at the weekend playing around with it
  - Really liking some of the design and experience that the `encore` CLI and development process has, deffo reminds me of the Architect Framework, but feel it may have a leg up on snazziness
  - Not sure I'm going to be able to get round to doing it, as I'm not sure what an `oapi-codegen` playground would look like, and Go-and-Typescript interop isn't quite there yet, so building the start of the dependency-management-data cloud isn't quite as easy
- Been interesting seeing how many hits I've been receiving on my site from links from the `oapi-codegen` README rewrite
- Spent a good chunk of the weekend writing what will be some very exciting posts for `oapi-codegen` next week 👀 Very excited to share
- Spent some time working on syncing files between GitHub repos, in particular making it a re-usable Action
- No updates yet on the Hackerone report I'd raised, but hopefully soon 🤞
- While implementing `--version` at work on a CLI we own, I noticed that I'd not implemented it correctly with DMD either, and so needed to [fix that too](https://gitlab.com/tanna.dev/dependency-management-data/-/commit/a57f8ee) as well as my article about how to do it
- Had a nice catch-up with Sebastian (for the first time)
- Had fun playing some of the shrines with Anna in _Zelda: Tears of the Kingdom_
- Had a nice coworking day with Carol on Friday, despite both of us having a busy day and not being able to chill quite as much!
- _Cat Snack Bar_ has been fun this week
- Cookie had a good day at daycare on Friday, and so she's been super sleepy and cute this weekend
  - We took her for a walk after Anna got back from a quick trip to Derby and Costco, and she'll be nice and sleepy tomorrow
  - On the way back we got a Rakki Rakkas 😋
- Got some crappy [Substack spam](https://www.jvt.me/mf2/2024/04/pzt7z/)
- Had a good catch up with Giuseppe, in particular learning about `devenv.sh`
- Enjoyed [this inventive new date picker](https://codepen.io/rahul-sahni/pen/MWBJxJV)
- Didn't get around to any _Shogun_ this week as it's been a busy week
- Excited to be playing with dependency-management-data [with Dan Lorenc](https://www.linkedin.com/events/danlearnshowtousedependencymana7189657039315382273/) next week - we booked it in ~2 months ago, and I'm eagerly looking forward to some feedback and to see how he (and the chat) find it 😅

Watched:

- _How I Met Your Mother_
- _Love_
- _Derry Girls_
