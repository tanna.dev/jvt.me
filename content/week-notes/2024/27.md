---
title: "Week Notes 24#27"
description: "What happened in the week of 2024-07-01?"
date: 2024-07-07T21:26:50+0100
---
A _lovely_ and very restorative week off for Anna's birthday

- We didn't get as much done around the house as we'd hoped, but we ended up really switching off and having a lot of nice time on the sofa together with a mix of fur babies
  - Got a lot of _Ratchet and Clank_ and _New Amsterdam_ in
- Finally got around to some work I've been doing with Speakeasy around their SDKs (for Go) which also gave me a lot of ideas for `oapi-codegen`
- Google Home Android app's TV controls have suddenly gone really crap, meaning I can't control it at all 😠
- My work brain was thankfully very switched off - I had one calendar update on my mind for a few days, but thankfully I'd left everything else well handed over and in a good place (and it's only a week being off) so I wasn't super concerned
  - But I am looking forward to getting back to things tomorrow - I feel refreshed, and although I think it'll be tough going back to a busy week, hopefully should be good
- On Monday, wrote two very meta blog posts:
  - A [call to action](https://www.jvt.me/posts/2024/07/01/who-reads-blog/) for readers of my blog - thanks for the insights I've already received, and interested to hear from others!
  - And [a celebration of my 1000th(!!!) blog post](https://www.jvt.me/posts/2024/07/01/1000-posts/) 👏
- Had a lovely overnight stay at Ragdale Hall spa for Anna's birthday
- Plus a super yum and authentic Pizzamisú on Anna's birthday, before I drove us over to the spa
  - My first fast A-roads and was mostly OK - but included a bit of a heart rate jump when a van pulled over suddenly into a lay-by, and coming off the road (at 70) onto a very short slip road with a sharp bend 😬 but made it there and back
- On my second treatment (Clarins Beauty Sleep Massage) I ended up falling asleep a little bit 🫣 Kept spluttering awake, but was reminded it's kinda ideal with that treatment
- It was a good stay, but I'd say that Hoar Cross Hall is my preference
- Definitely having the Mandella Effect around Buzz Blades in Ratchet And Clank - I was sure that they existed in the original games, but turns out the first time I saw them was _Ratchet And Clank (2016)_
- At Bestwood Mill Lakes we saw the crabby cat on the corner house again - which was especially crabby and came swiping for Cookie 😅 In the cat's defence, Cookie always makes faces at them, and that day opened her mouth to start barking at the cat and the cat was not having it 🫣 Luckily managed to get in between them and then move Cookie away before it got spicy 😬
- With Cookie away at Anna's parents, we had a lot of Morph time - he was an absolute living room fiend, running over to the (closed) double doors every time we came in, or went near the room, and we had a lot of lovely cuddles, and reminded ourselves of how nice it used to be with him 🥰 and vice versa
- Was nice to have Cookie back after a few days away, - she'd had a good time and was good for Anna's parents - and she's been mostly sleepy since she's been home, although wants to make sure we've not forgotten how sometimes she can be annoying 😅
- Had a cheeky Nandos 😋
- It was the general election and a very loud 📢 Fuck the Tories 📢 and an encouraging first look at a pretty good Cabinet, with people who know the work
- Had a very good Rakki Rakkas, and even got a handshake from one of the owners when he saw me 😏
- Been enjoying reading `/r/StarWars` a lot this week
- Did do _some_ bits and pieces of house things this week
- Had [Backwaters - polaroit](https://open.spotify.com/track/6IL8bohmnS4HW9NRzX9MxR?si=db782f08a67c4524) on replay _a lot_

In dependency-management-data:

- As someone who thrives under deadline pressure (read: my ADHD sometimes needs a deadline to do anything) I've been rushing over the weekend to get a big release out for dependency-management-data - `v0.100.0` 😱
  - This is actually my 158th overall release!
- As I hit 1000 blog posts, I thought I should also try and have this week be the `0.100.0` release of DMD to line up
- I'm very happy for my integration tests which gave me a lot more confidence in doing a _considerable breaking change_ around how SBOMs are used, but should make a huge quality of life improvement for folks using dependency-management-data with SBOMs, especially if those SBOMs aren't directly linked to the source repo they come from. More on the blog tomorrow
- This is also something I've been kinda working on for ~3 months on-and-off before realising it's hard and pushing it back to another time 😹
- Started the process of - finally - getting a Mastodon bot account to announce releases from
- Looking to get a `CHANGELOG.md` published (including some retrospective data) as it's probably a bit easier than looking through each of the releases between versions

In renovate-graph, found a couple of folks using it:

- https://github.com/LunchTimeCode/renovate-graph-fork
- https://github.com/camptocamp/github-app-geo-project

Played:

- _Apex Legends_
- _Ratchet and Clank: Size Matters_
- _Ratchet and Clank: Tools of Destruction_

Watched:

- _The Acolyte_
- _New Amsterdam_
