---
title: "Week Notes 24#09"
description: "What happened in the week of 2024-02-26?"
date: 2024-03-03T20:44:22+0000
---
This week was Elastic's Engineering All Hands in Prague, which was great but tiring:

- Had a pretty nice journey to Prague - it was an afternoon flight, so was quite a chilled day, although I ended up arriving just as the welcome party was out of food 😅
  - Had a bit of a dangerous Uber from the airport, weaving through _lots_ of traffic at maximum speed, and was glad that I was sharing it with Rory and Mark, not just to meet them but to have eyewitnesses in case anything went wrong 😬
  - Had a giant Yorkshire pudding with roast beef for lunch
  - Had an alright flight, but was quite crammed so couldn't even open my work laptop 😅
  - Lost my Wordle streak 💀
  - Giving myself a deadline of "before I get in the air" to finish my [Job titles are bullshit](https://www.jvt.me/posts/2024/02/26/job-titles-bullshit/) post was good, and helped me get it out of my head and onto paper. Was cool to refresh the page on HN a couple of minutes after posting and see there be more upvotes, and getting to 15 upvotes in ~10 minutes. It maxed out at around 50, and dropped off the front page, but some interesting discussion and points
    - Was funny that it was [issue 1337](https://gitlab.com/tanna.dev/jvt.me/-/issues/1337)
  - As I didn't manage to get any food at the welcome event, I ended up nipping out to try and find a (super)market to get some snacks, and while on my way saw there was a McDonalds, so had that
- I gave myself the permission to not attend a couple of talks, in the morning to regain some spoons so I could attend the 2-hour platform all hands (and reminded myself that I'm definitely no longer able to deal with in-person meetings like that 😅) as well as have some hallway track sessions which were very good
- Was nice to be walking around and having folks coming over to say hey - I don't feel that I'm necessarily making that sort of impact, but it's nice to see it, and was also lovely to meet so many people that I'd only ever seen through Zoom or Slack
- On the team day we had an interesting session on services and ownership, but didn't get too much time to talk about team ways of working, how we can handle our cross-timezone (Australia, US, and a few parts of Europe), etc so we as a team decided to spend some time doing that. I ran some good discussions, as well as digging into some things that weren't in the prepped pieces to talk about, and I'm really looking forward to the ways that we've decided to work together as a team
- Friday was very slow, chilled day, which I'd thought would be spent going to the pool/sauna then maybe doing some sightseeing, but was nice to see Greg and Jas at breakfast, and then I ended up having a bit of a chill, slowly packing, then spent some time on the mezzanine writing up some notes to send Andy about the week's meetings and what we'd discussed and decided as a team, before going for a lil' walk around Prague and a late Kung Pao from one of the shopping centre's food courts
- Friday flight back was good but I was pretty beat so didn't end up wanting to do anything on my laptop, despite having the physical space to do so
  - Ended up getting a bit of a weird situation with my headphones, where they wouldn't increase the volume when using the Android controls - meaning listening to a podcast was almost impossible. Spent a good amount of time trying to dig through settings to see if maybe it was safety settings due to the sound of the plane, but turns out no, I could control it by the physical controls on the earbuds. Very odd
  - Joined the TODO Group meeting - while in the queue to go through security - as there were interesting talks about Scorecards, and got a nice shout-out from Ashley about some stuff I'd suggested recently
  - Nice to be picked up by Anna, and was surprised to see Cookie in the car too, who was also surprised to see me (but was a bit less excited to see me than I would get if I'd have walked through the door)
- Had nice conversations like "is that i3 you're using?" and other things that were made so much easier due to physical proximity
- Has been nice to meet the team over a few days, as well as other folks in EngProd, chatting, having meals together, and playing 2 truths and 1 lie
- ... and some last-minute tweaks to the team, so sad to be no longer working with Ryan, but nice to be working with Jonah and Ellie
- My State of Open Con hoodie was excellent for keeping me warm in chilly Prague
- Some very good food and company, and feeling incredibly revitalised after some quality time with folks, new focus on what we're doing as a team, and lovely people
- Walking into my office on Saturday morning and finding it _very_ cold, despite the radiator being "on" got me to finally get around to bleeding the radiators. I'm sure it hasn't been good for me in the past with it not working at all, and the stark evidence of it, as well as having some spoons to look at it was good, and it's now much warmer in the house (after bleeding all the radiators) 👏
- Cool to see the University of Auckland's SOFTENG 281 course pointing to some of my posts
- Started updating my [Manual of Me](https://manual.jvt.me) off the back of some discussion at work, and the fact that I've not worked with anyone else on the team, and it's a good opportunity to refresh it, as well as consider how I _now_ want to work after ~18 months since I first wrote it
  - Also took the opportunity to trial Atkinson Hyperlegible on the site, as well as considering using it on the dependency-management-data site as well
- Morph had a particularly crabby mid-eating meow while he was eating some of Cookie's biscuits - that she'd left from her dinner - as I lifted him away
- Playing around with Vanna, Ollama and Elasticsearch (my first proper time using ES) and getting a local-only setup for doing LLM-y things with dependency-management-data
- I had my phone connected to work Slack + email this week, which was absolutely the right call given a lot of conversations around "where is everyone" and "who wants breakfast"
  - But it did lead to some annoying things where I'd accidentally added Google Tasks to my work account, not personal, because we don't do Work Profile
- Thanks to an anonymous reader who followed my Gousto referral link on my [Support Me page](/support-me/), as well as a new friend I met this week
- Had a nice chat with my parents to do some more planning for Italy
- Had shortbread and Nutella, a delicacy that I've not considered in years (and maybe not even had before) which was good 😋
- Had a nice walk with Carol and Juno this afternoon
  - Cookie was well muddy, so we gave her a bath when we got home, which she was _not_ happy about 😅 And she hid in the shower after we dried her off which was cute

Watched:

- _The Lost City_
  - One scene reminded me of _The Enigma of Amigara Fault_, which I then had to read to remind myself fully what was kinda stuck in my mind 🫠
- _One Day_ (for a short bit, on in the background)
- _The Good Witch_
- _Modern Family_
- [Inside Love is Blind's brutal conditions on set](https://www.youtube.com/watch?v=DaMOpPBNqbs)
- _The League of Gentlemen_ (a few minutes, on someone's tablet on the plane, with subtitles)
- _Saturday Night Live_ (on YouTube)
