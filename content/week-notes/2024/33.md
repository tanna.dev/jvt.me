---
title: "Week Notes 24#33"
description: "What happened in the week of 2024-08-12?"
date: 2024-08-18T21:21:26+0100
---
A busy week with a few days of work, GopherCon UK, and then a (long) weekend up North with Anna's family:

- Cookie was rather worried by the thunderstorms 🥺 but she was OK in the end
- This week was my first anniversary at Elastic 🎉
- A busy-ish start to the week, ahead of a few days of time off, and Ellie's couple of weeks off, so both trying to get various bits closed off
- Some team changes upcoming
- Got a new laptop charger ahead of GopherCon, and literally the first time I plugged it in, there was an audible crack, but it seems to have been OK 😬
- Had a great time at GopherCon UK
  - And even managed to do my writeup on the train (and about an hour of semi-focussed writing when I got home) which was very nice to have it completely out of the way
  - And posted it on `/r/golang`, cause why not - had some folks visiting and interested!
- This week I've done a few things:
  - [OpenAPI Overlay support for `oapi-codegen`](https://github.com/oapi-codegen/oapi-codegen/pull/1727) (fixing a longstanding issue folks have with modifying their specs manually)
  - Updating Renovate's support for `replacement`s with Go
  - Digging into [a bug with sqls' LSP formatting](https://github.com/sqls-server/sqls/issues/149 )
- Updated my /elsewhere/ page to link to Bridgy Fed's syndicated feed, rather than my separate Bluesky user, and hope that'll be useful for folks wanting to follow me there
- On the train home from GoperCon, someone diagonally opposite me was open mouth coughing which was very grim 😷
- Didn't notice until after the event that CrowdStrike were one of the sponsors 😅
- Had some annoying crashes - on my laptop - when running `golangci-lint`
  - But may be related to a discussion in the `#golangci-lint` channel on the Gopher Slack, related to the newest version 🤔
- Had some time in the bunny field from last year but unfortunately [not nearly as many bunnies as last year](/week-notes/2023/32/)
- Looked after Jasper while Dave and Faye were cooking, and Anna took Cookie, and went surprisngly well - had lots of nice cuddles, Cookie didn't mind too much and a nice sign for us having other dogs
- The dogs have generally been really good, with only a few barky bits and they've got on well considering it's the first dog home Cookie's been into
  - Things have been better with them both on lead, and they've settled well
  - And seemed to be generally OK with the kids, too
- About an hour ago we were in the living room and a cat jumped up onto the window sill, glaring at Cookie 🫣 Anna managed to scare it off before Cookie could see, luckily
- Had a lovely weekend so far

Played:

- _Apex Legends_

Watched:

- (no) _New Amsterdam_
- _Game Changers_ (on YouTube)
- _The Great British Sewing Bee_
- _His Dark Materials_
  - All but the finale of _The Amber Spyglass_, which has been very good
- _Alice's Wonderland Bakery_
