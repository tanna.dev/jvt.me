---
title: "Week Notes 24#47"
description: "What happened in the week of 2024-11-18?"
date: 2024-11-24T21:14:56+0000
---
A busy week in all senses:

- It's been hecking cold this week - we had some _significant_ snow on Monday night that impacted Anna's work trip to Birmingham on Wednesday, and left the week as mostly indoors - although this weekend it's been back to ~15 degrees
- Cookie's done very well not being walked Tue -> Sat, and we're very impressed, but she was very much starting to let us know she was needing more enrichment. When we walked her in the park on Saturday it wasn't quite like a normal walk - not least because of all the dangerous ice still around - but I'm sure she'll be happy for a normal walk tomorrow
- Had a nice catch-up with James and Aash on Tuesday night
  - There was a quiz on at Canalhouse, which was a little distracting from catching up, but also good cause we got 26/~50 points
  - Bit precarious getting in/out of town, and _incredibly cold_ despite being in my thermals and warmest jumper + coat
- Some continued traffic + discussion about my Cobra + OpenTelemetry post
  - But didn't end up getting into the Go newsletters unfortunately
- Noticed that my swap file on my work machine isn't working, and is likely why I've been having such a bad time recently! Need to work out why, but the main thing is that manually `sudo swapon`ing does the trick
- The Monkey Puzzle tree in the front of the house
  - It's been on my TODO list for several months, and has been dying for > 18 months
  - In a bout of executive function in July I managed to put in the planning permission requirements (as we're in a conservation area)
  - But it wasn't until this week I finally texted the tree surgeon we were going to use - generally because I'd meant to do so many a time, but didn't want to message out of working hours (as it was unfair) or when I was i.e. in meetings and unable to speak
  - The team did an excellent job, working incredibly quickly to bring it down with chainsaws on sticks
  - We kinda wish we'd recorded more of it / took some good before/after photos
  - Unfortunately we are now feeling _very_ exposed in the living room
  - Was a rather odd thing to have to get a wad of cash out for the work - naturally the one day I needed large quantities of cash out, and I didn't want to defrost the car, the Co-op's cash machine was out of order, so I then needed to work out where the nearest one was, and spend ~20 minutes defrosting the car 🫣🥶
- Pondering "can I improve my standup writing" I [wrote a Neovim plugin](https://www.jvt.me/posts/2024/11/24/neovim-standup/)
  - Including how [we as a team do standups](https://www.jvt.me/posts/2024/11/24/async-standups/)
  - A nice, low energy and brain capacity project this weekend
- Ordered a tumble dryer, after talking about it for a few months
- Had a good lunch from Boo - the K-Chick was good 😋
- Tried the Gu Sundaes - fairly nice, but think we prefer the regular ones
- A little bit of `oapi-codegen` triage, ahead of doing a full day on it this coming Thursday
- Spoke remotely at Fusion meetup on Thursday about DMD, as some last minute speaker changes
  - Really enjoyed Joe and Mark's talks before mine
    - In particular, thinking about adding more humanity and fun into `dmd`
  - Thought it went reasonably well, but there was a ~2 minute audio drop that threw me off and meant I was a little bit more pressed for time than planned
- Tried debugging the fact that [Android Auto isn't automagically switching to dark mode at night](https://support.google.com/androidauto/thread/178318868/android-auto-maps-do-not-switch-to-night-mode?hl=en) on Waze or Google Maps
  - Tried a few things, including the Android Auto developer settings, and it turns out right now we just need to manually swap modes - unhelpful when it's dark and you don't want to be blinded
- Went to Asda with Anna on Thursday, and picked up perfect pizza for Friday 😋
- Morph's been very cute this week, including being very playful! He enjoyed playing with a loo roll inside that Cookie usually plays with, but don't worry - she stole it back, despite the fact she had one of her own 🫣
- Heard that Instacart are using [my OpenAPI-to-Sorbet tool](https://www.jvt.me/posts/2023/05/31/openapi-sorbet/) and are actively seeing to upstream changes
- Was nice to have a fire on Friday night
- Had a Cook food delivery, which led to a good not-a-pie-but-a-casserole-and-lid pie tonight which was nice, and easy given I've not (yet) finished all the washing up - that's the job after I `git push` these
- Work has also been very busy this week, with lots of context switching

In dependency-management-data:

- Ashley Wolf (of GitHub) [presented](https://lfms24.sched.com/event/1nazk/dependency-risk-management-a-guide-for-ospos-ashley-wolf-github) at the Linux Foundation Member Summit which included a heavy focus on DMD 🥳
- Thanks to Yannic on the Gopher Slack for a suggestion of using `spancheck`
- Need to tweak my setup as Honeycomb rate-limits the DMD example project
- No progress on `renovate-packagedump-diff` but as we get closer to hackathon week at work I _really_ need to get going with it

Watched:

- _Two Broke Girls_
 - A disappointing finale, especially as the last ever
- _The Lincoln Lawyer_
  - A great season
- _Taskmaster_
  - Returned to it after finishing _Two Broke Girls_, and a good end to Season 9
- _The Man from Toronto_
- _Archer_
