---
title: "Week Notes 24#13"
description: "What happened in the week of 2024-03-25?"
date: 2024-03-31T21:49:51+0100
---
A four-day week ahead of the Easter weekend.

- Enjoyed attending the GitHub OSPO Advisory Board, learning about cool stuff being done at GitHub and OSPOs around the world
- On Thursday, went to see James Acaster in Nottingham for his _Heckler's Welcome_ tour, which was very good!
  - We'd forgotten that we'd booked it until a week or so ago
  - Cookie was mostly OK on her own for the evening
  - At the end, James mentioned he'd got through roughly 40% of his planned content, due to tangents and distractions off the back of heckling
  - Some good mix of deep chats (related to therapy and his realisations about his life) as well as funny tangents, zany stories and thought it was a pretty cool that at the end he mentioned that it "doesn't matter if [he] enjoyed the show", and that instead he's here for us, and not to centre himself
- Pizza night was a lot better, as Anna's toppings were all available, but mine wasn't quite, but I had a good backup to substitute to make it a good pizza nonetheless
- Now it's Easter week, we've started having our Easter Eggs 😋
- An interesting (and slightly spicy) thread on `/r/Nottingham` about salary expectations for jobs
- Created `wm` to send webmentions from a given URL (given the `webmention` CLI doesn't seem to make it easy for arbitrary URLs), and then found that I did it just over a month ago, but hadn't pulled the code on my laptop 😹
- One night I went to let Cookie out and a black cat was chased away, incidentally it was not Morph, which I knew because Morph then came in, so not sure if he had a matching friend, or what
- After several months of working towards it, finally was able to get my first mythic type for Cat Snack Bar
- Got back to Ring Fit Adventure, after meaning to definitely since the start of the year
  - I'm up to day 111 on it, and it's a lot harder than I remember - every day is squat day 💪
- Still can't seem to download _Apex Legends_, and am tempted to just uninstall it and then retry
- Very happy that the Gopher Slack is _finally_ getting back to allowing threads in #general, one of the biggest pain points of it, and makes it so incredibly hard to have discussions
- Got round to replying to some messages across various Slacks, which was a big weight on my mind
- Had a very excitable afternoon one day with Cookie, with us both playing hide and seek in the house, and then sneaking in/out of the living room so she couldn't get to me and had to keep running around
- Had a pretty good sandwich shop, and a very good brownie 😋
- The `xz` backdoor
  - It's been very interesting, both the backdoor itself, and the means it's taken to be shipped
  - I took the opportunity to [blog about](https://www.jvt.me/posts/2024/03/29/xz-scorecards/) how dependency-management-data and OpenSSF Scorecard could provide a lil' bit of insight into places that _may_ be affected by a similar issue
    - Especially as it was within a couple of hours of announcement, and I wanted to monopolise on timing, but I know some of the chatter online talked about all the SCA platforms saying "use our tools, it'd help you avoid this" so 😅
  - There's also been some big things in there around OSS sustainability - leading to maintainers that don't burnout and then arbitrarily bring co-maintainers onboard who want to do bad things
- Anna and I had a nice walk around Blidworth woods with Carol and Juno and Cookie
  - There was a dog that was lost from its owner, but luckily they came to find it - Cookie wasn't at all happy when I tried to get the dog's attention to try and get it on lead 😅
- Started the internal team handovers, which went well and had some good discussion
- Continued work towards oapi-codegen's triaging of old issues, as well as working towards the big ol' documentation revamp
- Morph's spent some time in the living room which has been nice
- Sad that Five Guys no longer does Reece's Peanut Butter Cup, but managed to approximate it with a peanut butter and chocolate mix
- Netflix appears to have lost history for most of the week's media, so I've had to go on memory for some of these 😹
- A weekend of 🥱

Watched:

- _The Crown_
- _Friday Night Dinner_
- _Love_
- _Memento_
  - Very good recommendation from [my episode of Changelog & Friends](https://changelog.com/friends/31#t=3346) - no spoilers, but 🔥
- _Taskmaster_
- _Rick and Morty_
