---
title: "Week Notes 25#08"
description: "What happened in the week of 2025-02-24?"
date: 2025-03-02T20:50:03+0000
---
A bit of a busy week:

- Was nice to chat with Tessa Kriesel on Monday, about one of her current clients, but also some more general chat
- Had a nice catch up with the Capital One folks on Wednesday
- My post [_Some useful SQL(ite) tips I've learned_](https://www.jvt.me/posts/2024/12/05/sql-tidbits/) appeared on /r/SQLite and Hacker News this week, unexpectedly, but seems to have been interesting to folks, and appeared in the Hacker Newsletter too
- The weather being nicer towards the end of the week has been nice, and we've had windows open a bit more
  - Morph and Cookie have been enjoying being in the conservatory when it's warm and sunny which is cute
- Had a Saturday trip to Asda, but they only had "deep dish" pizza base which was OK, but prefer our "thin and crispy"
  - Tried the Reece's ice cream, which is alright? Not that amazing, but I'll eat it
- A very exciting Sunday morning for Cookie and Morph, with a dead pigeon in the garden (luckily neither of them tried to get it)
- A number of obligations this week have been taking a _lot_ of mental energy away and being a bit of a drag
- Played a lot of _Apex Legends_ this week
- GopherCon UK's CFP is open, so I've put in a few proposals for different topics
  - Including a little help from Ollama (although mostly ignored)
- Dug into Custom Datasources for Renovate, and how to test them
- I've been taking more advantage of my Ollama setup this last week
  - Got a helpful answer for my JSONata queries at work with Renovate custom datasources
  - Got some good ideas for a Tailwind-powered dashboard structure, while fleshing out what Deps.FYI could look like
  - Gave it a bit of a go for rating my Call for Papers submissions, although I didn't really listen to it much
  - It's been a bit annoying that to do bits of coding - between games of _Apex Legends_ I'm not able to use the GPU (as it's busy gaming) so I miss out on the chance to play with it more
  - I'm fortunate to be able to use [`qwen2.5-coder:32b`](https://ollama.com/library/qwen2.5-coder:32b) on my new GPU which has been fairly good, although if you ask it about someone, it seems _very_ biased towards them being in the Kubernetes community 🫣
  - Also trying to work out if I can use it - with [Open WebUI](https://github.com/open-webui/open-webui/) to take in knowledge / interact with DMD's SQLite database locally
- AMD released the 9700 XT, which is poor timing given my new 7900 XTX - although the 9700 XT isn't comparable
- Had a good chat with Adrian
- Back to listening to some tech podcasts, but still mostly listening to The Delta Flyers (and very much enjoying it)

In dependency-management-data:

- After looking into the painful process of updating the vendored code in `renovate-graph`, discovered a post talking about `pnpm patch` and so migrated over to `pnpm` and then set up the patching to _much more easily_ modify the underlying dependency on `renovate` which so far hasn't caused any issues!

Watched:

- _Life in Pieces_
  - Sad to have finished it
- _Reacher_
- _Rings of Power_
  - Restarting our rewatch from season 1 (so we can watch season 2)
- _Brooklyn 99_
  - As our light watching programme
