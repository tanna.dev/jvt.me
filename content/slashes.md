---
title: /slashes pages
---
Inspired by <span class="h-card"><a class="u-url" href="https://rknight.me">Robb Knight</a></span>'s [Slash Pages](https://slashpages.net/), the following are pages on my website - found at a top level URL - which may be of interest:

- [/salary/: _My Salary History_](/salary/)
- [/now/: _/now_](/now/)
- [/elsewhere/: _Elsewhere_](/elsewhere/)
- [/open-source/: _Open Source Projects I Maintain_](/open-source/)
- [/support-me/: _Support Me_](/support-me/)
- [/popular-posts/: _Popular Posts_](/popular-posts/)
- [/subscribe/: _Follow This Blog_](/subscribe/)
- [/archives/: _Archives_](/archives/)
- [/search/: _Search_](/search/)
- [/blogroll/: _Blogroll_](/blogroll/)
- [/on-call-compensation/: _My On-Call Compensation History_](/on-call-compensation/)
- [/post-frequency/: _Post Frequency_](/post-frequency/)
- [/typography/: _Typography_](/typography/)
- [/rsvps/: _RSVPs_](/rsvps/)
- [/slashes/: _/slashes pages_](/slashes/)

(These are in an order that I feel may be more interesting to you, the reader)
