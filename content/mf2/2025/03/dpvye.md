{
  "date" : "2025-03-07T08:00:10.032479381Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5964" ],
    "start" : [ "2025-03-06T00:00:00Z" ],
    "end" : [ "2025-03-07T00:00:00Z" ],
    "published" : [ "2025-03-07T08:00:10.032479381Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/dpvye",
  "client_id" : "https://www-api.jvt.me/fit"
}
