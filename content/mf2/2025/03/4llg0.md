{
  "date" : "2025-03-01T08:00:17.617939466Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7522" ],
    "start" : [ "2025-02-28T00:00:00Z" ],
    "end" : [ "2025-03-01T00:00:00Z" ],
    "published" : [ "2025-03-01T08:00:17.617939466Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/4llg0",
  "client_id" : "https://www-api.jvt.me/fit"
}
