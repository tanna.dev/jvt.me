{
  "date" : "2025-03-08T08:00:12.513187727Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5386" ],
    "start" : [ "2025-03-07T00:00:00Z" ],
    "end" : [ "2025-03-08T00:00:00Z" ],
    "published" : [ "2025-03-08T08:00:12.513187727Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/mlgom",
  "client_id" : "https://www-api.jvt.me/fit"
}
