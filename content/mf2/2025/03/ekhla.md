{
  "date" : "2025-03-02T20:06:16.758805321Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2025-03-02T20:06:16.758805321Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Folks may be interested to see that I've now got [an RSS feed](https://www.jvt.me/salary.xml) for [my salary page](https://www.jvt.me/salary/), which is also on the Fediverse via @www.jvt.me.salary@rss-parrot.net in case y'all wanna know when there are _any_ updates to it 👀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2025/03/ekhla",
  "client_id" : "https://editor.tanna.dev/"
}
