{
  "date" : "2025-03-03T08:00:10.610702284Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5552" ],
    "start" : [ "2025-03-02T00:00:00Z" ],
    "end" : [ "2025-03-03T00:00:00Z" ],
    "published" : [ "2025-03-03T08:00:10.610702284Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/vzik0",
  "client_id" : "https://www-api.jvt.me/fit"
}
