{
  "date" : "2025-03-09T08:00:10.78757178Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9708" ],
    "start" : [ "2025-03-08T00:00:00Z" ],
    "end" : [ "2025-03-09T00:00:00Z" ],
    "published" : [ "2025-03-09T08:00:10.78757178Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/gbjqi",
  "client_id" : "https://www-api.jvt.me/fit"
}
