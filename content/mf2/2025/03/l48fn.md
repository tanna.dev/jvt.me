{
  "date" : "2025-03-04T08:00:10.203205192Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4994" ],
    "start" : [ "2025-03-03T00:00:00Z" ],
    "end" : [ "2025-03-04T00:00:00Z" ],
    "published" : [ "2025-03-04T08:00:10.203205192Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/l48fn",
  "client_id" : "https://www-api.jvt.me/fit"
}
