{
  "date" : "2025-03-05T08:00:13.696350275Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2485" ],
    "start" : [ "2025-03-04T00:00:00Z" ],
    "end" : [ "2025-03-05T00:00:00Z" ],
    "published" : [ "2025-03-05T08:00:13.696350275Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/03/jckkt",
  "client_id" : "https://www-api.jvt.me/fit"
}
