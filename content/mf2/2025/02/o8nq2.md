{
  "date" : "2025-02-16T08:00:10.798702967Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7345" ],
    "start" : [ "2025-02-15T00:00:00Z" ],
    "end" : [ "2025-02-16T00:00:00Z" ],
    "published" : [ "2025-02-16T08:00:10.798702967Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/o8nq2",
  "client_id" : "https://www-api.jvt.me/fit"
}
