{
  "date" : "2025-02-14T08:00:09.950230409Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3811" ],
    "start" : [ "2025-02-13T00:00:00Z" ],
    "end" : [ "2025-02-14T00:00:00Z" ],
    "published" : [ "2025-02-14T08:00:09.950230409Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/rfjqz",
  "client_id" : "https://www-api.jvt.me/fit"
}
