{
  "date" : "2025-02-07T08:00:12.494377913Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4390" ],
    "start" : [ "2025-02-06T00:00:00Z" ],
    "end" : [ "2025-02-07T00:00:00Z" ],
    "published" : [ "2025-02-07T08:00:12.494377913Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/ijj4z",
  "client_id" : "https://www-api.jvt.me/fit"
}
