{
  "date" : "2025-02-10T11:30:21.965624952Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2025-02-10T11:30:21.965624952Z" ],
    "category" : [ "soocon25", "state-of-open-con" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very exited to see that my talk, _\"I inherited this project, and all I got was all these angry users\"_ from <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> is [now live](https://www.youtube.com/watch?v=PK8CMcePn2A) - thanks for the super speedy publishing, OpenUK!\r\n\r\nI was honoured to kick off the Open Source Software & Security Track with a keynote about my experiences being an Open Source maintainer, and digging into the factors that make maintaining [`oapi-codegen`](https://github.com/oapi-codegen/oapi-codegen/) difficult, and on top of that my own personal experiences which lead to difficulties in maintaining Open Source, especially when it's used actively by companies.\r\n\r\nThere were some great discussions about the maintainer burnout problem over the conference, and I had some great chats with folks in the \"hallway track\".\r\n\r\nI'll be finishing my write-up of the conference in the next week (🤞🏼) but am very excited to see the talks are already up on YouTube, so I can catch up on talks I missed, too"
    } ]
  },
  "kind" : "notes",
  "slug" : "2025/02/lrefz",
  "tags" : [ "soocon25", "state-of-open-con" ],
  "client_id" : "https://editor.tanna.dev/"
}
