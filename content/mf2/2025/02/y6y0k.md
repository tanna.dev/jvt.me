{
  "date" : "2025-02-11T08:00:10.962516198Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3744" ],
    "start" : [ "2025-02-10T00:00:00Z" ],
    "end" : [ "2025-02-11T00:00:00Z" ],
    "published" : [ "2025-02-11T08:00:10.962516198Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/y6y0k",
  "client_id" : "https://www-api.jvt.me/fit"
}
