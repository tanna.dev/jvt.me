{
  "date" : "2025-02-20T08:00:10.857427647Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2675" ],
    "start" : [ "2025-02-19T00:00:00Z" ],
    "end" : [ "2025-02-20T00:00:00Z" ],
    "published" : [ "2025-02-20T08:00:10.857427647Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/vkgck",
  "client_id" : "https://www-api.jvt.me/fit"
}
