{
  "date" : "2025-02-21T08:00:09.959395799Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3986" ],
    "start" : [ "2025-02-20T00:00:00Z" ],
    "end" : [ "2025-02-21T00:00:00Z" ],
    "published" : [ "2025-02-21T08:00:09.959395799Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/cphqu",
  "client_id" : "https://www-api.jvt.me/fit"
}
