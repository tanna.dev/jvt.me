{
  "date" : "2025-02-05T10:25:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A laptop sticker, placed on Jamie's laptop, saying \"make art, not billionaires\" with a smiley face under the \"not\" ",
      "photo" : "https://media.jvt.me/7fa87f6c57.jpeg"
    } ],
    "published" : [ "2025-02-05T10:25:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Absolutely love this new \"make art, not billionaires\" sticker, thanks very much @andypiper@macaw.social 💜"
    } ]
  },
  "kind" : "photos",
  "slug" : "2025/02/nqzee",
  "client_id" : "https://indiepass.app/"
}
