{
  "date" : "2025-02-16T15:45:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-02-16T15:45:00+0000" ],
    "category" : [ "open-source", "state-of-open-con" ],
    "bookmark-of" : [ "https://www.theregister.com/2025/02/16/open_source_maintainers_state_of_open/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2025/02/4getu",
  "tags" : [ "open-source", "state-of-open-con" ],
  "client_id" : "https://indiepass.app/"
}
