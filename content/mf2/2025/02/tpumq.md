{
  "date" : "2025-02-21T19:33:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-02-21T19:33:00+0000" ],
    "category" : [ "entrepreneurship" ],
    "like-of" : [ "https://linear.app/blog/the-profitable-startup" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/02/tpumq",
  "tags" : [ "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
