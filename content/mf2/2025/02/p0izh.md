{
  "date" : "2025-02-18T18:33:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-02-18T18:33:00+0000" ],
    "category" : [ "sqlite", "go" ],
    "like-of" : [ "https://boyter.org/posts/searchcode-bigger-sqlite-than-you/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/02/p0izh",
  "tags" : [ "sqlite", "go" ],
  "client_id" : "https://indiepass.app/"
}
