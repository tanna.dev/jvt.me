{
  "date" : "2025-02-05T11:13:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@borntyping/113947718807558057" ],
    "published" : [ "2025-02-05T11:13:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Oof yeah, that's a difficult one - it's not always \"easy\" to fix something without something else breaking, or [breaking someone's workflow](https://xkcd.com/1172/)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2025/02/2lju8",
  "client_id" : "https://indiepass.app/"
}
