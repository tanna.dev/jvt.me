{
  "date" : "2025-02-04T10:42:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "name" : [ "Why I joined incident.io (and you should too!)" ],
    "published" : [ "2025-02-04T10:42:00+0000" ],
    "like-of" : [ "https://www.linkedin.com/pulse/why-i-joined-incidentio-you-should-too-lawrence-jones-pkfue" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/02/ypcew",
  "client_id" : "https://indiepass.app/"
}
