{
  "date" : "2025-02-04T11:51:53.238141095Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2025-02-04T11:51:53.238141095Z" ],
    "category" : [ "state-of-open-con", "soocon25" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thanks to those of you who were able to find the Hardwick Hub at <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> and came to my talk, [\"I inherited this project, and all I got was all these angry users\"](https://talks.jvt.me/inherited-oss-project/) - slides can be found 👆🏼 or in PDF form [on Sched](https://stateofopencon2025.sched.com/event/1tBIP/i-inherited-this-project-and-all-i-got-was-all-these-angry-users-jamie-tanna-chief-blogger-jvtme) <a href=\"/tags/soocon25/\">#soocon25</a> - would be happy to hear some tired maintainer stories from y'all too!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2025/02/owlqb",
  "tags" : [ "state-of-open-con", "soocon25" ],
  "client_id" : "https://editor.tanna.dev/"
}
