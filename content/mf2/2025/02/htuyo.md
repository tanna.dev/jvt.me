{
  "date" : "2025-02-01T08:00:10.325717288Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4859" ],
    "start" : [ "2025-01-31T00:00:00Z" ],
    "end" : [ "2025-02-01T00:00:00Z" ],
    "published" : [ "2025-02-01T08:00:10.325717288Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/htuyo",
  "client_id" : "https://www-api.jvt.me/fit"
}
