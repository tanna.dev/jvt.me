{
  "date" : "2025-02-07T17:00:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-02-07T17:00:00+0000" ],
    "category" : [ "go", "python" ],
    "like-of" : [ "https://lovable.dev/blog/from-python-to-go" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/02/hwi3q",
  "tags" : [ "go", "python" ],
  "client_id" : "https://indiepass.app/"
}
