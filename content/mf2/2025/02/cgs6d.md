{
  "date" : "2025-02-03T08:00:11.814933685Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8583" ],
    "start" : [ "2025-02-02T00:00:00Z" ],
    "end" : [ "2025-02-03T00:00:00Z" ],
    "published" : [ "2025-02-03T08:00:11.814933685Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/cgs6d",
  "client_id" : "https://www-api.jvt.me/fit"
}
