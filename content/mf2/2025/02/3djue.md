{
  "date" : "2025-02-05T08:00:10.348576618Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10609" ],
    "start" : [ "2025-02-04T00:00:00Z" ],
    "end" : [ "2025-02-05T00:00:00Z" ],
    "published" : [ "2025-02-05T08:00:10.348576618Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/3djue",
  "client_id" : "https://www-api.jvt.me/fit"
}
