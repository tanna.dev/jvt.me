{
  "date" : "2025-02-28T08:00:10.229693932Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5800" ],
    "start" : [ "2025-02-27T00:00:00Z" ],
    "end" : [ "2025-02-28T00:00:00Z" ],
    "published" : [ "2025-02-28T08:00:10.229693932Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/rx9cb",
  "client_id" : "https://www-api.jvt.me/fit"
}
