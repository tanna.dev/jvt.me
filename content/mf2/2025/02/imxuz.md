{
  "date" : "2025-02-14T10:48:24.688339083Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/devoops-athens/events/306031272/" ],
    "published" : [ "2025-02-14T10:48:24.688339083Z" ],
    "category" : [ "elastic", "incident-management" ],
    "post-status" : [ "published" ],
    "rsvp" : [ "interested" ],
    "content" : [ {
      "html" : "",
      "value" : "Having been at the *ahem* epicentre of this incident, this will be a very interesting talk for those of you in Athens!\r\n\r\nAlso looking forward to internal conversations around being able to get a blog post on Elastic's blog about this 👀"
    } ]
  },
  "kind" : "rsvps",
  "slug" : "2025/02/imxuz",
  "tags" : [ "elastic", "incident-management" ],
  "client_id" : "https://editor.tanna.dev/"
}
