{
  "date" : "2025-02-13T21:01:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-02-13T21:01:00+0000" ],
    "category" : [ "architecture" ],
    "like-of" : [ "https://monzo.com/blog/tolerating-full-cloud-outages-with-monzo-stand-in" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/02/6abfc",
  "tags" : [ "architecture" ],
  "client_id" : "https://indiepass.app/"
}
