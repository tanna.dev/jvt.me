{
  "date" : "2025-02-04T08:00:10.181005518Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8681" ],
    "start" : [ "2025-02-03T00:00:00Z" ],
    "end" : [ "2025-02-04T00:00:00Z" ],
    "published" : [ "2025-02-04T08:00:10.181005518Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/mo86j",
  "client_id" : "https://www-api.jvt.me/fit"
}
