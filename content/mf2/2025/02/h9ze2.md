{
  "date" : "2025-02-08T08:00:12.904292466Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4744" ],
    "start" : [ "2025-02-07T00:00:00Z" ],
    "end" : [ "2025-02-08T00:00:00Z" ],
    "published" : [ "2025-02-08T08:00:12.904292466Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/h9ze2",
  "client_id" : "https://www-api.jvt.me/fit"
}
