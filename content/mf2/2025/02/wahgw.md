{
  "date" : "2025-02-10T08:00:10.288880645Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2770" ],
    "start" : [ "2025-02-09T00:00:00Z" ],
    "end" : [ "2025-02-10T00:00:00Z" ],
    "published" : [ "2025-02-10T08:00:10.288880645Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/wahgw",
  "client_id" : "https://www-api.jvt.me/fit"
}
