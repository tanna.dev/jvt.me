{
  "date" : "2025-02-09T18:49:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://gregorlove.com/2025/02/its-a-tron-legacy/" ],
    "published" : [ "2025-02-09T18:49:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "100% one of the best soundtracks of all time, and a number of the songs are on my regular wider rotation"
    } ]
  },
  "kind" : "replies",
  "slug" : "2025/02/g8bwg",
  "client_id" : "https://indiepass.app/"
}
