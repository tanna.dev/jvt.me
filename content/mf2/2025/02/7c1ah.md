{
  "date" : "2025-02-23T08:00:10.29546482Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6834" ],
    "start" : [ "2025-02-22T00:00:00Z" ],
    "end" : [ "2025-02-23T00:00:00Z" ],
    "published" : [ "2025-02-23T08:00:10.29546482Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/7c1ah",
  "client_id" : "https://www-api.jvt.me/fit"
}
