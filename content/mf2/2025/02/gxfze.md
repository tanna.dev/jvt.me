{
  "date" : "2025-02-24T08:00:10.247319591Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3442" ],
    "start" : [ "2025-02-23T00:00:00Z" ],
    "end" : [ "2025-02-24T00:00:00Z" ],
    "published" : [ "2025-02-24T08:00:10.247319591Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/gxfze",
  "client_id" : "https://www-api.jvt.me/fit"
}
