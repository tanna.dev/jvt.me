{
  "date" : "2025-02-19T08:00:10.298847984Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6164" ],
    "start" : [ "2025-02-18T00:00:00Z" ],
    "end" : [ "2025-02-19T00:00:00Z" ],
    "published" : [ "2025-02-19T08:00:10.298847984Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/ygl0g",
  "client_id" : "https://www-api.jvt.me/fit"
}
