{
  "date" : "2025-02-15T08:00:13.204611059Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6884" ],
    "start" : [ "2025-02-14T00:00:00Z" ],
    "end" : [ "2025-02-15T00:00:00Z" ],
    "published" : [ "2025-02-15T08:00:13.204611059Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/jtg5y",
  "client_id" : "https://www-api.jvt.me/fit"
}
