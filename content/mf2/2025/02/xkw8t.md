{
  "date" : "2025-02-21T19:26:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-02-21T19:26:00+0000" ],
    "category" : [ "open-source", "entrepreneurship" ],
    "like-of" : [ "https://boyney123.substack.com/p/why-is-your-product-open-source" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/02/xkw8t",
  "tags" : [ "open-source", "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
