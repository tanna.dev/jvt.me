{
  "date" : "2025-02-26T08:00:09.696547203Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4025" ],
    "start" : [ "2025-02-25T00:00:00Z" ],
    "end" : [ "2025-02-26T00:00:00Z" ],
    "published" : [ "2025-02-26T08:00:09.696547203Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/s9xu5",
  "client_id" : "https://www-api.jvt.me/fit"
}
