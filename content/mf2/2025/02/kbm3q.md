{
  "date" : "2025-02-12T08:00:12.066657491Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3170" ],
    "start" : [ "2025-02-11T00:00:00Z" ],
    "end" : [ "2025-02-12T00:00:00Z" ],
    "published" : [ "2025-02-12T08:00:12.066657491Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/kbm3q",
  "client_id" : "https://www-api.jvt.me/fit"
}
