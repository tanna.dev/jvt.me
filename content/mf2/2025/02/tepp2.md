{
  "date" : "2025-02-09T08:00:09.610289575Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4415" ],
    "start" : [ "2025-02-08T00:00:00Z" ],
    "end" : [ "2025-02-09T00:00:00Z" ],
    "published" : [ "2025-02-09T08:00:09.610289575Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/tepp2",
  "client_id" : "https://www-api.jvt.me/fit"
}
