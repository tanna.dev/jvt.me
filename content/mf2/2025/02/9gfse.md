{
  "date" : "2025-02-17T08:00:11.083199155Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9142" ],
    "start" : [ "2025-02-16T00:00:00Z" ],
    "end" : [ "2025-02-17T00:00:00Z" ],
    "published" : [ "2025-02-17T08:00:11.083199155Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/9gfse",
  "client_id" : "https://www-api.jvt.me/fit"
}
