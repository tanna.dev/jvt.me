{
  "date" : "2025-02-06T08:00:13.822598878Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6975" ],
    "start" : [ "2025-02-05T00:00:00Z" ],
    "end" : [ "2025-02-06T00:00:00Z" ],
    "published" : [ "2025-02-06T08:00:13.822598878Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/qekg5",
  "client_id" : "https://www-api.jvt.me/fit"
}
