{
  "date" : "2025-02-13T08:00:10.227549208Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5429" ],
    "start" : [ "2025-02-12T00:00:00Z" ],
    "end" : [ "2025-02-13T00:00:00Z" ],
    "published" : [ "2025-02-13T08:00:10.227549208Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/c4e4f",
  "client_id" : "https://www-api.jvt.me/fit"
}
