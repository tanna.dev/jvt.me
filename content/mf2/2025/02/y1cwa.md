{
  "date" : "2025-02-18T08:00:10.046494008Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4102" ],
    "start" : [ "2025-02-17T00:00:00Z" ],
    "end" : [ "2025-02-18T00:00:00Z" ],
    "published" : [ "2025-02-18T08:00:10.046494008Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/02/y1cwa",
  "client_id" : "https://www-api.jvt.me/fit"
}
