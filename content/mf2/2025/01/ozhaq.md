{
  "date" : "2025-01-30T08:00:10.300230781Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3864" ],
    "start" : [ "2025-01-29T00:00:00Z" ],
    "end" : [ "2025-01-30T00:00:00Z" ],
    "published" : [ "2025-01-30T08:00:10.300230781Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/ozhaq",
  "client_id" : "https://www-api.jvt.me/fit"
}
