{
  "date" : "2025-01-04T21:44:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://phpc.social/@awoodsnet/113753999422087578" ],
    "published" : [ "2025-01-04T21:44:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I've got a [meta page for all my feeds](https://www.jvt.me/subscribe/) but https://www.jvt.me/kind/articles/feed.xml is probably the main one that folks are interested in!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2025/01/ww8sw",
  "client_id" : "https://indiepass.app/"
}
