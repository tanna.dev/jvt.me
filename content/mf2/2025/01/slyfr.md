{
  "date" : "2025-01-03T08:00:18.098504627Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5272" ],
    "start" : [ "2025-01-02T00:00:00Z" ],
    "end" : [ "2025-01-03T00:00:00Z" ],
    "published" : [ "2025-01-03T08:00:18.098504627Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/slyfr",
  "client_id" : "https://www-api.jvt.me/fit"
}
