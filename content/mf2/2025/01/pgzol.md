{
  "date" : "2025-01-07T08:00:20.119729072Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6842" ],
    "start" : [ "2025-01-06T00:00:00Z" ],
    "end" : [ "2025-01-07T00:00:00Z" ],
    "published" : [ "2025-01-07T08:00:20.119729072Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/pgzol",
  "client_id" : "https://www-api.jvt.me/fit"
}
