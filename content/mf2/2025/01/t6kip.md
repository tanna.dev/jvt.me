{
  "date" : "2025-01-29T13:59:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-01-29T13:59:00+0000" ],
    "category" : [ "api", "versioning" ],
    "like-of" : [ "https://greenhouse.skriptble.press/thought/web-api-versioning-is-insufficient/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/01/t6kip",
  "tags" : [ "api", "versioning" ],
  "client_id" : "https://indiepass.app/"
}
