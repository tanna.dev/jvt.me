{
  "date" : "2025-01-02T08:00:17.270248254Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6815" ],
    "start" : [ "2025-01-01T00:00:00Z" ],
    "end" : [ "2025-01-02T00:00:00Z" ],
    "published" : [ "2025-01-02T08:00:17.270248254Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/0mmn9",
  "client_id" : "https://www-api.jvt.me/fit"
}
