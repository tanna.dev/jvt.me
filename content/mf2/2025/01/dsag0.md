{
  "date" : "2025-01-09T08:00:14.18646117Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3747" ],
    "start" : [ "2025-01-08T00:00:00Z" ],
    "end" : [ "2025-01-09T00:00:00Z" ],
    "published" : [ "2025-01-09T08:00:14.18646117Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/dsag0",
  "client_id" : "https://www-api.jvt.me/fit"
}
