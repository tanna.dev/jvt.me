{
  "date" : "2025-01-18T12:55:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-01-18T12:55:00+0000" ],
    "category" : [ "gov.uk" ],
    "like-of" : [ "https://dafyddvaughan.uk/blog/2025/why-some-dvla-digital-services-dont-work-at-night/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/01/b6ggk",
  "tags" : [ "gov.uk" ],
  "client_id" : "https://indiepass.app/"
}
