{
  "date" : "2025-01-22T08:00:12.11355383Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2770" ],
    "start" : [ "2025-01-21T00:00:00Z" ],
    "end" : [ "2025-01-22T00:00:00Z" ],
    "published" : [ "2025-01-22T08:00:12.11355383Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/i6mfh",
  "client_id" : "https://www-api.jvt.me/fit"
}
