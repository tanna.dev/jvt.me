{
  "date" : "2025-01-01T08:00:18.591843526Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11842" ],
    "start" : [ "2024-12-31T00:00:00Z" ],
    "end" : [ "2025-01-01T00:00:00Z" ],
    "published" : [ "2025-01-01T08:00:18.591843526Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/jjvrw",
  "client_id" : "https://www-api.jvt.me/fit"
}
