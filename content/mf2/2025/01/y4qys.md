{
  "date" : "2025-01-08T08:00:15.610253069Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8654" ],
    "start" : [ "2025-01-07T00:00:00Z" ],
    "end" : [ "2025-01-08T00:00:00Z" ],
    "published" : [ "2025-01-08T08:00:15.610253069Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/y4qys",
  "client_id" : "https://www-api.jvt.me/fit"
}
