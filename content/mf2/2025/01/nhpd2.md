{
  "date" : "2025-01-04T08:00:14.62066931Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4268" ],
    "start" : [ "2025-01-03T00:00:00Z" ],
    "end" : [ "2025-01-04T00:00:00Z" ],
    "published" : [ "2025-01-04T08:00:14.62066931Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/nhpd2",
  "client_id" : "https://www-api.jvt.me/fit"
}
