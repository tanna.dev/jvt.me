{
  "date" : "2025-01-10T08:00:13.114733749Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5499" ],
    "start" : [ "2025-01-09T00:00:00Z" ],
    "end" : [ "2025-01-10T00:00:00Z" ],
    "published" : [ "2025-01-10T08:00:13.114733749Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/ofihr",
  "client_id" : "https://www-api.jvt.me/fit"
}
