{
  "date" : "2025-01-11T08:01:32.769012517Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4719" ],
    "start" : [ "2025-01-10T00:00:00Z" ],
    "end" : [ "2025-01-11T00:00:00Z" ],
    "published" : [ "2025-01-11T08:01:32.769012517Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/kt62t",
  "client_id" : "https://www-api.jvt.me/fit"
}
