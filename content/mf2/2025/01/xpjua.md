{
  "date" : "2025-01-20T16:51:54.45774135Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2025-01-20T16:51:54.45774135Z" ],
    "category" : [ "music" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "A very apt song to start this week off (for my US friends) - 🎶 [Don't Panik - CloZee Remix](https://open.spotify.com/track/4ZFguQEsZQxVyNuSFbUctU) 🎶"
    } ]
  },
  "kind" : "notes",
  "slug" : "2025/01/xpjua",
  "tags" : [ "music" ],
  "client_id" : "https://editor.tanna.dev/"
}
