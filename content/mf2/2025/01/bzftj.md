{
  "date" : "2025-01-03T08:53:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://josh.tel/@josh/113760856967052680" ],
    "published" : [ "2025-01-03T08:53:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Being fortunate and not getting sick during the pandemic (yay social distancing and masking!) and I got sick over the holiday period and _dang_ it sucks to be ill, I can't believe it used to be way more often in the past 😷🤒"
    } ]
  },
  "kind" : "replies",
  "slug" : "2025/01/bzftj",
  "client_id" : "https://indiepass.app/"
}
