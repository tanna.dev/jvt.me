{
  "date" : "2025-01-28T08:00:12.295324806Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9140" ],
    "start" : [ "2025-01-27T00:00:00Z" ],
    "end" : [ "2025-01-28T00:00:00Z" ],
    "published" : [ "2025-01-28T08:00:12.295324806Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/0n5iy",
  "client_id" : "https://www-api.jvt.me/fit"
}
