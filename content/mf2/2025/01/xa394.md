{
  "date" : "2025-01-24T08:00:15.142878624Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4685" ],
    "start" : [ "2025-01-23T00:00:00Z" ],
    "end" : [ "2025-01-24T00:00:00Z" ],
    "published" : [ "2025-01-24T08:00:15.142878624Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/xa394",
  "client_id" : "https://www-api.jvt.me/fit"
}
