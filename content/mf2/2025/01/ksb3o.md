{
  "date" : "2025-01-03T12:21:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-01-03T12:21:00+0000" ],
    "category" : [ "impostor-syndrome" ],
    "like-of" : [ "https://www.linkedin.com/pulse/first-week-2025-dont-panic-tiffany-lawrence-jud1e" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/01/ksb3o",
  "tags" : [ "impostor-syndrome" ],
  "client_id" : "https://indiepass.app/"
}
