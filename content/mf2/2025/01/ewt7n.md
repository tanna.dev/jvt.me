{
  "date" : "2025-01-14T08:00:19.838409053Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6738" ],
    "start" : [ "2025-01-13T00:00:00Z" ],
    "end" : [ "2025-01-14T00:00:00Z" ],
    "published" : [ "2025-01-14T08:00:19.838409053Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/ewt7n",
  "client_id" : "https://www-api.jvt.me/fit"
}
