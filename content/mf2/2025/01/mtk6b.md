{
  "date" : "2025-01-12T08:00:16.12144226Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4228" ],
    "start" : [ "2025-01-11T00:00:00Z" ],
    "end" : [ "2025-01-12T00:00:00Z" ],
    "published" : [ "2025-01-12T08:00:16.12144226Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/mtk6b",
  "client_id" : "https://www-api.jvt.me/fit"
}
