{
  "date" : "2025-01-06T08:00:14.63316291Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8777" ],
    "start" : [ "2025-01-05T00:00:00Z" ],
    "end" : [ "2025-01-06T00:00:00Z" ],
    "published" : [ "2025-01-06T08:00:14.63316291Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/oa8cj",
  "client_id" : "https://www-api.jvt.me/fit"
}
