{
  "date" : "2025-01-03T13:18:28.710264196Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.andycarolan.com/success-stories" ],
    "published" : [ "2025-01-03T13:18:28.710264196Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I thoroughly enjoyed working with Andy to design a project logo for an Open Source project I've been building.\r\n\r\nWanting to work with a real human and get their experience and breadth of ideas, Andy was a great choice, having been recommended by a connection on social media, and helped me unpick what I wanted out of the logo, providing several avenues for design choices before honing in on the final design, which I was super happy with.\r\n\r\nAndy was great to work with, provides a wealth of experience and knowledge and was great to work with. I really enjoyed the usage of both synchronous discussions (over video call) and using Miro for asynchronously describing the design decisions in each iteration of the design, allowing me to hear his thoughts (rather than just read them over email) and respond to them in kind. \r\n\r\nI've recently started a second project with Andy, and can definitely foresee using his services again!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2025/01/9fdno",
  "client_id" : "https://editor.tanna.dev/"
}
