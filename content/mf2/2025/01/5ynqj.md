{
  "date" : "2025-01-17T08:00:18.293318881Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7741" ],
    "start" : [ "2025-01-16T00:00:00Z" ],
    "end" : [ "2025-01-17T00:00:00Z" ],
    "published" : [ "2025-01-17T08:00:18.293318881Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/5ynqj",
  "client_id" : "https://www-api.jvt.me/fit"
}
