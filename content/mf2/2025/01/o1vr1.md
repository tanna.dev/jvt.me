{
  "date" : "2025-01-26T08:00:11.647945579Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "12118" ],
    "start" : [ "2025-01-25T00:00:00Z" ],
    "end" : [ "2025-01-26T00:00:00Z" ],
    "published" : [ "2025-01-26T08:00:11.647945579Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/o1vr1",
  "client_id" : "https://www-api.jvt.me/fit"
}
