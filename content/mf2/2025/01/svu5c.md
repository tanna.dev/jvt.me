{
  "date" : "2025-01-21T08:00:15.175109102Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6560" ],
    "start" : [ "2025-01-20T00:00:00Z" ],
    "end" : [ "2025-01-21T00:00:00Z" ],
    "published" : [ "2025-01-21T08:00:15.175109102Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/svu5c",
  "client_id" : "https://www-api.jvt.me/fit"
}
