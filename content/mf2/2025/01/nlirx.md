{
  "date" : "2025-01-16T08:00:20.089951186Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4390" ],
    "start" : [ "2025-01-15T00:00:00Z" ],
    "end" : [ "2025-01-16T00:00:00Z" ],
    "published" : [ "2025-01-16T08:00:20.089951186Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/nlirx",
  "client_id" : "https://www-api.jvt.me/fit"
}
