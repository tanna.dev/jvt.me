{
  "date" : "2025-01-15T08:00:16.611496608Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3732" ],
    "start" : [ "2025-01-14T00:00:00Z" ],
    "end" : [ "2025-01-15T00:00:00Z" ],
    "published" : [ "2025-01-15T08:00:16.611496608Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/xh3jf",
  "client_id" : "https://www-api.jvt.me/fit"
}
