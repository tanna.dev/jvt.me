{
  "date" : "2025-01-18T08:00:16.600422554Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3453" ],
    "start" : [ "2025-01-17T00:00:00Z" ],
    "end" : [ "2025-01-18T00:00:00Z" ],
    "published" : [ "2025-01-18T08:00:16.600422554Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/jrhgk",
  "client_id" : "https://www-api.jvt.me/fit"
}
