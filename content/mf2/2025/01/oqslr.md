{
  "date" : "2025-01-21T07:27:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@philgyford/113855910855685942" ],
    "published" : [ "2025-01-21T07:27:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very much agree, but also - I've spoken in the past about how [I learned a tonne from overengineering my personal website](https://talks.jvt.me/overengineering-personal-website/) and recommend others do too!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2025/01/oqslr",
  "client_id" : "https://indiepass.app/"
}
