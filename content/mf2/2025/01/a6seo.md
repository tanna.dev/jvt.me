{
  "date" : "2025-01-23T08:00:09.955701287Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5575" ],
    "start" : [ "2025-01-22T00:00:00Z" ],
    "end" : [ "2025-01-23T00:00:00Z" ],
    "published" : [ "2025-01-23T08:00:09.955701287Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/a6seo",
  "client_id" : "https://www-api.jvt.me/fit"
}
