{
  "date" : "2025-01-05T08:00:17.477594233Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4573" ],
    "start" : [ "2025-01-04T00:00:00Z" ],
    "end" : [ "2025-01-05T00:00:00Z" ],
    "published" : [ "2025-01-05T08:00:17.477594233Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/7ju7l",
  "client_id" : "https://www-api.jvt.me/fit"
}
