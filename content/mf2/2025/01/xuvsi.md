{
  "date" : "2025-01-25T08:00:10.681912202Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7316" ],
    "start" : [ "2025-01-24T00:00:00Z" ],
    "end" : [ "2025-01-25T00:00:00Z" ],
    "published" : [ "2025-01-25T08:00:10.681912202Z" ]
  },
  "kind" : "steps",
  "slug" : "2025/01/xuvsi",
  "client_id" : "https://www-api.jvt.me/fit"
}
