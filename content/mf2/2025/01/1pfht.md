{
  "date" : "2025-01-25T10:28:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2025-01-25T10:28:00+0000" ],
    "category" : [ "neovim" ],
    "like-of" : [ "https://www.jonashietala.se/blog/2024/05/26/autocomplete_with_nvim-cmp/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2025/01/1pfht",
  "tags" : [ "neovim" ],
  "client_id" : "https://indiepass.app/"
}
