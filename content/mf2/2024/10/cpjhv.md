{
  "date" : "2024-10-05T08:00:16.331658545Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6531" ],
    "start" : [ "2024-10-04T00:00:00Z" ],
    "end" : [ "2024-10-05T00:00:00Z" ],
    "published" : [ "2024-10-05T08:00:16.331658545Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/cpjhv",
  "client_id" : "https://www-api.jvt.me/fit"
}
