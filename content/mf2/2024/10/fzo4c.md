{
  "date" : "2024-10-18T22:14:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://infosec.exchange/@briankrebs/113330458505175494" ],
    "published" : [ "2024-10-18T22:14:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Maintenance. I write so much about software maintenance, but always add too many `i`s and `a`s"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/10/fzo4c",
  "client_id" : "https://indiepass.app/"
}
