{
  "date" : "2024-10-10T16:34:10.992128034Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-10-10T16:34:10.992128034Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Good idea! I ended up [finding I can use `xdotool` to do it instead](https://www.jvt.me/posts/2023/08/25/caps-lock-linux-rebound/) / also realised I could hold `Shift` while in caps mode to type normally 🫣"
    } ],
    "in-reply-to": [
      "https://hachyderm.io/@simontatham/113266645773839672"
    ]
  },
  "kind" : "replies",
  "slug" : "2024/10/nn3ou",
  "client_id" : "https://editor.tanna.dev/"
}
