{
  "date" : "2024-10-13T08:00:10.194834381Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10129" ],
    "start" : [ "2024-10-12T00:00:00Z" ],
    "end" : [ "2024-10-13T00:00:00Z" ],
    "published" : [ "2024-10-13T08:00:10.194834381Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/ug0ge",
  "client_id" : "https://www-api.jvt.me/fit"
}
