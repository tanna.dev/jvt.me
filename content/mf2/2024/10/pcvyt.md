{
  "date" : "2024-10-19T07:29:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-10-19T07:29:00+0100" ],
    "category" : [ "supply-chain-security" ],
    "like-of" : [ "https://www.kusari.dev/blog/you-cant-fix-issues-if-you-cant-find-them" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/10/pcvyt",
  "tags" : [ "supply-chain-security" ],
  "client_id" : "https://indiepass.app/"
}
