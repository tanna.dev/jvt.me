{
  "date" : "2024-10-22T08:00:13.457012812Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4554" ],
    "start" : [ "2024-10-21T00:00:00Z" ],
    "end" : [ "2024-10-22T00:00:00Z" ],
    "published" : [ "2024-10-22T08:00:13.457012812Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/nixkx",
  "client_id" : "https://www-api.jvt.me/fit"
}
