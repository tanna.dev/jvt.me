{
  "date" : "2024-10-18T08:00:11.12564916Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4603" ],
    "start" : [ "2024-10-17T00:00:00Z" ],
    "end" : [ "2024-10-18T00:00:00Z" ],
    "published" : [ "2024-10-18T08:00:11.12564916Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/4usvo",
  "client_id" : "https://www-api.jvt.me/fit"
}
