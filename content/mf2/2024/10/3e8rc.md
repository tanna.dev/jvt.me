{
  "date" : "2024-10-10T08:00:15.536953733Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3741" ],
    "start" : [ "2024-10-09T00:00:00Z" ],
    "end" : [ "2024-10-10T00:00:00Z" ],
    "published" : [ "2024-10-10T08:00:15.536953733Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/3e8rc",
  "client_id" : "https://www-api.jvt.me/fit"
}
