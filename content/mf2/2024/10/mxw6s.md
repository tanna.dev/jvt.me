{
  "date" : "2024-10-30T08:00:14.378651098Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2690" ],
    "start" : [ "2024-10-29T00:00:00Z" ],
    "end" : [ "2024-10-30T00:00:00Z" ],
    "published" : [ "2024-10-30T08:00:14.378651098Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/mxw6s",
  "client_id" : "https://www-api.jvt.me/fit"
}
