{
  "date" : "2024-10-15T08:00:13.110001459Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6281" ],
    "start" : [ "2024-10-14T00:00:00Z" ],
    "end" : [ "2024-10-15T00:00:00Z" ],
    "published" : [ "2024-10-15T08:00:13.110001459Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/emily",
  "client_id" : "https://www-api.jvt.me/fit"
}
