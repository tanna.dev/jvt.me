{
  "date" : "2024-10-31T08:00:12.828325298Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7300" ],
    "start" : [ "2024-10-30T00:00:00Z" ],
    "end" : [ "2024-10-31T00:00:00Z" ],
    "published" : [ "2024-10-31T08:00:12.828325298Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/k3tfx",
  "client_id" : "https://www-api.jvt.me/fit"
}
