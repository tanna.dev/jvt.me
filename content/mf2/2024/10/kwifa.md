{
  "date" : "2024-10-02T08:00:13.639246234Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4590" ],
    "start" : [ "2024-10-01T00:00:00Z" ],
    "end" : [ "2024-10-02T00:00:00Z" ],
    "published" : [ "2024-10-02T08:00:13.639246234Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/kwifa",
  "client_id" : "https://www-api.jvt.me/fit"
}
