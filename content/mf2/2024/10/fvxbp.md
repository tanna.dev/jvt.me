{
  "date" : "2024-10-26T08:00:12.477496398Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5279" ],
    "start" : [ "2024-10-25T00:00:00Z" ],
    "end" : [ "2024-10-26T00:00:00Z" ],
    "published" : [ "2024-10-26T08:00:12.477496398Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/fvxbp",
  "client_id" : "https://www-api.jvt.me/fit"
}
