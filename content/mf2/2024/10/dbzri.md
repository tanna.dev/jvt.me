{
  "date" : "2024-10-12T15:13:52.370579278Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://joind.in/event/oggcamp-2024/into-the-multiverse-a-parallel-universe-where-neurotypicals-are-the-weird-ones" ],
    "published" : [ "2024-10-12T15:13:52.370579278Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Parul's talk was incredible - her humour and reflection on her experiences of life as a neurodiverse person, through the lense of \"what if neurotypicals were the weird ones\" was really quite powerful.\r\n\r\nI spent the talk very eagerly nodding along and laughing - a little distractedly - at her incredibly relatable take on the role reversal, and bangers like \"Norman received feedback [that his colleagues felt weird about him because] he couldn't fidget\".\r\n\r\nI'm biased as someone who is neurodiverse myself, so related much more heavily to the talk, but feel that non-neurodiverse people in the audience could also understand the parallels.\r\n\r\nI was very strongly reminded - in a great way - of <span class=\"h-card\"><a class=\"u-url\" href=\"https://sparkleclass.com\">Rachel Morgan-Trimmer</a></span>'s [talk from OggCamp 2019](https://joind.in/event/oggcamp-19/the-power-of-change---learning-to-live-as-a-weirdo) which was also a really powerful insight into life as a neurodiverse person, and I really enjoyed Parul's empathy-driven talk.\r\n\r\nI especially loved the ending call to action, asking us all to consider what steps we could take to understand others in our life better.\r\n\r\nMy only complaint was that I wish the talk was recorded!\r\n\r\n(I unfortunately missed the first few minutes)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/10/dbzri",
  "client_id" : "https://editor.tanna.dev/"
}
