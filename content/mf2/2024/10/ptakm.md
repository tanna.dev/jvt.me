{
  "date" : "2024-10-29T08:00:11.638519122Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3226" ],
    "start" : [ "2024-10-28T00:00:00Z" ],
    "end" : [ "2024-10-29T00:00:00Z" ],
    "published" : [ "2024-10-29T08:00:11.638519122Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/ptakm",
  "client_id" : "https://www-api.jvt.me/fit"
}
