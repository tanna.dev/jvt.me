{
  "date" : "2024-10-21T21:58:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-10-21T21:58:00+0100" ],
    "category" : [ "github-actions" ],
    "like-of" : [ "https://hayden.moe/posts/2024-06-01-prettifying-long-github-actions-scripts/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/10/mqlsl",
  "tags" : [ "github-actions" ],
  "client_id" : "https://indiepass.app/"
}
