{
  "date" : "2024-10-20T08:41:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://andypiper.co.uk/2024/10/18/meetup-com-is-so-over/" ],
    "published" : [ "2024-10-20T08:41:00+0100" ],
    "category" : [ "indieweb" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "On my side I stopped using Meetup as heavily when they removed the free API, and post-lockdown, at least nearer to me, there aren't nearly as many in-person meetups.\n\nIn the <a href=\"/tags/indieweb/\">#IndieWeb</a> community, there's [the Meetable project](https://github.com/aaronpk/Meetable), seen at https://events.indieweb.org/ and is powered by IndieWeb tech (not ActivityPub) if you fancied self hosting, but then you miss out on network effect and discoverability of events you're not necessarily organising yourself 🤔"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/10/v2qvd",
  "tags" : [ "indieweb" ],
  "client_id" : "https://indiepass.app/"
}
