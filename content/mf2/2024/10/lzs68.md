{
  "date" : "2024-10-06T08:00:11.960894813Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6256" ],
    "start" : [ "2024-10-05T00:00:00Z" ],
    "end" : [ "2024-10-06T00:00:00Z" ],
    "published" : [ "2024-10-06T08:00:11.960894813Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/lzs68",
  "client_id" : "https://www-api.jvt.me/fit"
}
