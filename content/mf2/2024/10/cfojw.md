{
  "date" : "2024-10-24T08:00:14.120195623Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5136" ],
    "start" : [ "2024-10-23T00:00:00Z" ],
    "end" : [ "2024-10-24T00:00:00Z" ],
    "published" : [ "2024-10-24T08:00:14.120195623Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/cfojw",
  "client_id" : "https://www-api.jvt.me/fit"
}
