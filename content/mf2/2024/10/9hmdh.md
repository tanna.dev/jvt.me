{
  "date" : "2024-10-02T15:57:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@anderseknert/113237704317762049" ],
    "published" : [ "2024-10-02T15:57:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Ohh gotcha I see what you mean - so it's actually that the `map` isn't initialised (still one of the annoying things Go does, you have to `make(map)` to initialise it), the `key` being unused just stops us compiling.\n\nI do agree it'd be useful to have a way to catch uninitialised `map`s earlier!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/10/9hmdh",
  "client_id" : "https://indiepass.app/"
}
