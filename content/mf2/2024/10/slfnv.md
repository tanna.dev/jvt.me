{
  "date" : "2024-10-11T08:00:10.108576528Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3818" ],
    "start" : [ "2024-10-10T00:00:00Z" ],
    "end" : [ "2024-10-11T00:00:00Z" ],
    "published" : [ "2024-10-11T08:00:10.108576528Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/slfnv",
  "client_id" : "https://www-api.jvt.me/fit"
}
