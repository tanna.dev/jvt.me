{
  "date" : "2024-10-01T08:00:12.298701861Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2509" ],
    "start" : [ "2024-09-30T00:00:00Z" ],
    "end" : [ "2024-10-01T00:00:00Z" ],
    "published" : [ "2024-10-01T08:00:12.298701861Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/rvkr0",
  "client_id" : "https://www-api.jvt.me/fit"
}
