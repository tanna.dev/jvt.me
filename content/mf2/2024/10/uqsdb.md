{
  "date" : "2024-10-16T08:00:13.169609708Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6558" ],
    "start" : [ "2024-10-15T00:00:00Z" ],
    "end" : [ "2024-10-16T00:00:00Z" ],
    "published" : [ "2024-10-16T08:00:13.169609708Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/uqsdb",
  "client_id" : "https://www-api.jvt.me/fit"
}
