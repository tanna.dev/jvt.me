{
  "date" : "2024-10-09T08:00:14.398633411Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3458" ],
    "start" : [ "2024-10-08T00:00:00Z" ],
    "end" : [ "2024-10-09T00:00:00Z" ],
    "published" : [ "2024-10-09T08:00:14.398633411Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/bpaiw",
  "client_id" : "https://www-api.jvt.me/fit"
}
