{
  "date" : "2024-10-27T08:00:10.469395902Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10735" ],
    "start" : [ "2024-10-26T00:00:00Z" ],
    "end" : [ "2024-10-27T00:00:00Z" ],
    "published" : [ "2024-10-27T08:00:10.469395902Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/qxqxb",
  "client_id" : "https://www-api.jvt.me/fit"
}
