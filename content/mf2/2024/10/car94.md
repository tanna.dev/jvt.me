{
  "date" : "2024-10-08T08:00:15.745209253Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6188" ],
    "start" : [ "2024-10-07T00:00:00Z" ],
    "end" : [ "2024-10-08T00:00:00Z" ],
    "published" : [ "2024-10-08T08:00:15.745209253Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/car94",
  "client_id" : "https://www-api.jvt.me/fit"
}
