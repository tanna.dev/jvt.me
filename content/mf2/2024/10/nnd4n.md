{
  "date" : "2024-10-03T08:00:11.226243601Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10860" ],
    "start" : [ "2024-10-02T00:00:00Z" ],
    "end" : [ "2024-10-03T00:00:00Z" ],
    "published" : [ "2024-10-03T08:00:11.226243601Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/nnd4n",
  "client_id" : "https://www-api.jvt.me/fit"
}
