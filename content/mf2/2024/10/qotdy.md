{
  "date" : "2024-10-17T08:00:14.974085523Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4222" ],
    "start" : [ "2024-10-16T00:00:00Z" ],
    "end" : [ "2024-10-17T00:00:00Z" ],
    "published" : [ "2024-10-17T08:00:14.974085523Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/qotdy",
  "client_id" : "https://www-api.jvt.me/fit"
}
