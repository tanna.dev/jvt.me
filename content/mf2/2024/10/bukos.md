{
  "date" : "2024-10-19T08:29:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/zemnmez/status/1847352559817937037" ],
    "published" : [ "2024-10-19T08:29:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "👋🏽 Co-maintainer of oapi-codegen here, this is something that can be fixed by migrating the type on the spec side to a named Schema, but will see if we've got a planned feature to autogenerate a type name here instead of an anonymous struct 🤞🏽"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/10/bukos",
  "client_id" : "https://indiepass.app/"
}
