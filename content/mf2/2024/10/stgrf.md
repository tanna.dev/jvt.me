{
  "date" : "2024-10-20T08:00:11.964394594Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7010" ],
    "start" : [ "2024-10-19T00:00:00Z" ],
    "end" : [ "2024-10-20T00:00:00Z" ],
    "published" : [ "2024-10-20T08:00:11.964394594Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/stgrf",
  "client_id" : "https://www-api.jvt.me/fit"
}
