{
  "date" : "2024-10-21T08:00:13.411961011Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7447" ],
    "start" : [ "2024-10-20T00:00:00Z" ],
    "end" : [ "2024-10-21T00:00:00Z" ],
    "published" : [ "2024-10-21T08:00:13.411961011Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/ixjip",
  "client_id" : "https://www-api.jvt.me/fit"
}
