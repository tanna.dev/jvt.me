{
  "date" : "2024-10-19T08:00:15.652551173Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4729" ],
    "start" : [ "2024-10-18T00:00:00Z" ],
    "end" : [ "2024-10-19T00:00:00Z" ],
    "published" : [ "2024-10-19T08:00:15.652551173Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/whxtr",
  "client_id" : "https://www-api.jvt.me/fit"
}
