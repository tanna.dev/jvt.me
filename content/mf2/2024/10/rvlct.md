{
  "date" : "2024-10-28T08:00:10.822186196Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8921" ],
    "start" : [ "2024-10-27T00:00:00Z" ],
    "end" : [ "2024-10-28T00:00:00Z" ],
    "published" : [ "2024-10-28T08:00:10.822186196Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/rvlct",
  "client_id" : "https://www-api.jvt.me/fit"
}
