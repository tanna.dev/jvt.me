{
  "date" : "2024-10-04T08:00:10.849076543Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6107" ],
    "start" : [ "2024-10-03T00:00:00Z" ],
    "end" : [ "2024-10-04T00:00:00Z" ],
    "published" : [ "2024-10-04T08:00:10.849076543Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/y9s0q",
  "client_id" : "https://www-api.jvt.me/fit"
}
