{
  "date" : "2024-10-23T08:00:13.925007844Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5095" ],
    "start" : [ "2024-10-22T00:00:00Z" ],
    "end" : [ "2024-10-23T00:00:00Z" ],
    "published" : [ "2024-10-23T08:00:13.925007844Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/obhjc",
  "client_id" : "https://www-api.jvt.me/fit"
}
