{
  "date" : "2024-10-06T21:11:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A packet of ~30 stickers from Sticker App, showing the Dependency Management Data UFO and text, which look pretty cool and you definitely want one",
      "photo" : "https://media.jvt.me/bd2a2ed30d.jpeg"
    } ],
    "published" : [ "2024-10-06T21:11:00+0100" ],
    "category" : [ "dependency-management-data" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Why yes, <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a> now has stickers 👀"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/10/uwvkl",
  "tags" : [ "dependency-management-data" ],
  "client_id" : "https://indiepass.app/"
}
