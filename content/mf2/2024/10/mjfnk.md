{
  "date" : "2024-10-14T08:00:11.734984683Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7600" ],
    "start" : [ "2024-10-13T00:00:00Z" ],
    "end" : [ "2024-10-14T00:00:00Z" ],
    "published" : [ "2024-10-14T08:00:11.734984683Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/mjfnk",
  "client_id" : "https://www-api.jvt.me/fit"
}
