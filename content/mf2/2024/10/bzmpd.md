{
  "date" : "2024-10-07T08:01:15.818550604Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11806" ],
    "start" : [ "2024-10-06T00:00:00Z" ],
    "end" : [ "2024-10-07T00:00:00Z" ],
    "published" : [ "2024-10-07T08:01:15.818550604Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/bzmpd",
  "client_id" : "https://www-api.jvt.me/fit"
}
