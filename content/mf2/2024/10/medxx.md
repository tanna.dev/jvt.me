{
  "date" : "2024-10-13T11:55:38.314993579Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-10-13T11:55:38.314993579Z" ],
    "category" : [ "oggcamp2024" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thanks for those who came to my talk at <a href=\"/tags/oggcamp2024/\">#OggCamp2024</a>, \"89 things I know about Git commits (abridged version)\" based on [blog post of the same name](https://www.jvt.me/posts/2024/07/12/things-know-commits/) - glad I resisted doing a couple of slide changes in the ~90 seconds before I went up on stage 🫣"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/10/medxx",
  "tags" : [ "oggcamp2024" ],
  "client_id" : "https://editor.tanna.dev/"
}
