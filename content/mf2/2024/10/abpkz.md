{
  "date" : "2024-10-12T08:00:13.899434709Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10979" ],
    "start" : [ "2024-10-11T00:00:00Z" ],
    "end" : [ "2024-10-12T00:00:00Z" ],
    "published" : [ "2024-10-12T08:00:13.899434709Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/10/abpkz",
  "client_id" : "https://www-api.jvt.me/fit"
}
