{
  "date" : "2024-10-28T07:46:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-10-28T07:46:00+0000" ],
    "category" : [ "github-actions", "security", "supply-chain-security" ],
    "like-of" : [ "https://github.com/woodruffw/zizmor" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/10/86vfd",
  "tags" : [ "github-actions", "security", "supply-chain-security" ],
  "client_id" : "https://indiepass.app/"
}
