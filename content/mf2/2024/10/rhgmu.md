{
  "date" : "2024-10-13T16:12:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-10-13T16:12:00+0100" ],
    "category" : [ "oggcamp", "union", "oggcamp2024" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "On the topic of joining a <a href=\"/tags/union/\">#union</a> and the collective bargaining port we as workers should work towards at <a href=\"/tags/oggcamp2024/\">#OggCamp2024</a>, I'd also recommend discussing your salary with your colleagues! \n\nIt doesn't have to be [as public as my /salary page](https://www.jvt.me/salary/), but it's _hugely_ useful to do and helps work out inequity"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/10/rhgmu",
  "tags" : [ "oggcamp", "union", "oggcamp2024" ],
  "client_id" : "https://indiepass.app/"
}
