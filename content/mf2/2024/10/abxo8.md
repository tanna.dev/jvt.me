{
  "date" : "2024-10-12T12:06:57.809785624Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-10-12T12:06:57.809785624Z" ],
    "category" : [ "oggcamp2024" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm talking about - would you believe it - [dependency-management-data](https://dmd.tanna.dev) at <a href=\"/tags/oggcamp2024/\">#OggCamp2024</a> [later this afternoon](https://joind.in/event/oggcamp-2024/quantifying-your-reliance-on-open-source-software) 🤓🛸\r\n\r\nCome to learn about some very interesting things you can learn from your usage of Open Source and internal dependencies 👀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/10/abxo8",
  "tags" : [ "oggcamp2024" ],
  "client_id" : "https://editor.tanna.dev/"
}
