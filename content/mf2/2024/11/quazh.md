{
  "date" : "2024-11-10T20:11:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A screenshot of Jamie's FitBit app, showing the heartrate for a ~90 minute period of time, leading up to the meeting.\n\n\nThe first heartrate spike is at 1450 (up to 114bpm), immediately before the meeting, and as I'm preparing myself with a final runthrough and check that everything's ready.\n\n\nIt relaxes down to 92bpm while other parts of the meeting are going on, a quick spike up to 110bpm as it's noted that I'm going to be presenting later.\n\n\nA few minutes before I talk - at 1530 - my heartrate drops down to 84bpm (as I'm mentally playing \"Moving On - Phaeleh\") and then spikes to 107bpm as I start to speak at 1540.\n\n",
      "photo" : "https://media.jvt.me/b4a5aab47f.png"
    } ],
    "published" : [ "2024-11-10T20:11:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Why yes, on Wednesday I _was_ presenting a very high-profile meeting at work  - why do you ask?\n\n(Sorry that FitBit doesn't make it easier to export a graph more nicely)"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/11/quazh",
  "client_id" : "https://indiepass.app/"
}
