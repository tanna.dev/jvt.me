{
  "date" : "2024-11-07T18:22:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-11-07T18:22:00+0000" ],
    "category" : [ "indieweb", "public-speaking" ],
    "repost-of" : [ "https://social.yakshed.org/@moonglum/113442020207038885" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/11/sle9a",
  "tags" : [ "indieweb", "public-speaking" ],
  "client_id" : "https://indiepass.app/"
}
