{
  "date" : "2024-11-25T08:00:11.390546134Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2967" ],
    "start" : [ "2024-11-24T00:00:00Z" ],
    "end" : [ "2024-11-25T00:00:00Z" ],
    "published" : [ "2024-11-25T08:00:11.390546134Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/qas3v",
  "client_id" : "https://www-api.jvt.me/fit"
}
