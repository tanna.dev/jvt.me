{
  "date" : "2024-11-01T08:00:16.448401962Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5824" ],
    "start" : [ "2024-10-31T00:00:00Z" ],
    "end" : [ "2024-11-01T00:00:00Z" ],
    "published" : [ "2024-11-01T08:00:16.448401962Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/ahsa9",
  "client_id" : "https://www-api.jvt.me/fit"
}
