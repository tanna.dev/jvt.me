{
  "date" : "2024-11-03T16:47:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://lornajane.net/posts/2024/pretty-print-json-with-jq" ],
    "published" : [ "2024-11-03T16:47:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Alternatively:\n\n```sh\njq \".\" posts.json > better.json\n```\n\n(to remove \"useless use of cat\")"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/11/lvgly",
  "client_id" : "https://indiepass.app/"
}
