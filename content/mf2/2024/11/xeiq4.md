{
  "date" : "2024-11-28T08:00:14.254446431Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3980" ],
    "start" : [ "2024-11-27T00:00:00Z" ],
    "end" : [ "2024-11-28T00:00:00Z" ],
    "published" : [ "2024-11-28T08:00:14.254446431Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/xeiq4",
  "client_id" : "https://www-api.jvt.me/fit"
}
