{
  "date" : "2024-11-27T08:00:18.587906101Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3220" ],
    "start" : [ "2024-11-26T00:00:00Z" ],
    "end" : [ "2024-11-27T00:00:00Z" ],
    "published" : [ "2024-11-27T08:00:18.587906101Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/bhd2r",
  "client_id" : "https://www-api.jvt.me/fit"
}
