{
  "date" : "2024-11-11T08:00:13.229747421Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4387" ],
    "start" : [ "2024-11-10T00:00:00Z" ],
    "end" : [ "2024-11-11T00:00:00Z" ],
    "published" : [ "2024-11-11T08:00:13.229747421Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/fzbzf",
  "client_id" : "https://www-api.jvt.me/fit"
}
