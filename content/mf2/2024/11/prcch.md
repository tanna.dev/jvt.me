{
  "date" : "2024-11-05T19:41:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-11-05T19:41:00+0000" ],
    "category" : [ "security", "supply-chain-security", "npm" ],
    "repost-of" : [ "https://tane.codes/@tanepiper/113431618940381373" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/11/prcch",
  "tags" : [ "security", "supply-chain-security", "npm" ],
  "client_id" : "https://indiepass.app/"
}
