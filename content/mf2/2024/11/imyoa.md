{
  "date" : "2024-11-03T08:00:11.128188274Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4300" ],
    "start" : [ "2024-11-02T00:00:00Z" ],
    "end" : [ "2024-11-03T00:00:00Z" ],
    "published" : [ "2024-11-03T08:00:11.128188274Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/imyoa",
  "client_id" : "https://www-api.jvt.me/fit"
}
