{
  "date" : "2024-11-26T22:09:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-11-26T22:09:00+0000" ],
    "category" : [ "adhd" ],
    "like-of" : [ "https://bradfrost.com/blog/post/my-adhd-diagnosis-process/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/11/6hhpa",
  "tags" : [ "adhd" ],
  "client_id" : "https://indiepass.app/"
}
