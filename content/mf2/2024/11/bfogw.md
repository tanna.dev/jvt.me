{
  "date" : "2024-11-19T08:00:17.765428757Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4684" ],
    "start" : [ "2024-11-18T00:00:00Z" ],
    "end" : [ "2024-11-19T00:00:00Z" ],
    "published" : [ "2024-11-19T08:00:17.765428757Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/bfogw",
  "client_id" : "https://www-api.jvt.me/fit"
}
