{
  "date" : "2024-11-18T08:00:14.563920404Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9601" ],
    "start" : [ "2024-11-17T00:00:00Z" ],
    "end" : [ "2024-11-18T00:00:00Z" ],
    "published" : [ "2024-11-18T08:00:14.563920404Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/ugk8m",
  "client_id" : "https://www-api.jvt.me/fit"
}
