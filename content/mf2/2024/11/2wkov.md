{
  "date" : "2024-11-17T08:00:11.698751814Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6752" ],
    "start" : [ "2024-11-16T00:00:00Z" ],
    "end" : [ "2024-11-17T00:00:00Z" ],
    "published" : [ "2024-11-17T08:00:11.698751814Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/2wkov",
  "client_id" : "https://www-api.jvt.me/fit"
}
