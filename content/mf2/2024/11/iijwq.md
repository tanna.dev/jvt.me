{
  "date" : "2024-11-24T13:47:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-11-24T13:47:00+0000" ],
    "category" : [ "neovim", "python" ],
    "like-of" : [ "https://mkaz.blog/code/neovim-plugin-python" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/11/iijwq",
  "tags" : [ "neovim", "python" ],
  "client_id" : "https://indiepass.app/"
}
