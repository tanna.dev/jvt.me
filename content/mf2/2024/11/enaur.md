{
  "date" : "2024-11-28T21:59:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@thisismissem/113562530385426627" ],
    "published" : [ "2024-11-28T21:59:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Not personally used it, but [Vale](https://vale.sh/) is well recommended with technical writers I know (but may require some configuring), ie https://lornajane.net/posts/2024/reviewdog-filter-settings-with-github-actions from @lornajane@indieweb.social may help with using https://github.com/errata-ai/vale-action"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/11/enaur",
  "client_id" : "https://indiepass.app/"
}
