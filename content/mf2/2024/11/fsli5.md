{
  "date" : "2024-11-13T20:59:52.326891027Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-11-13T20:59:52.326891027Z" ],
    "category" : [ "opentelemetry", "sqlite", "sql" ],
    "like-of" : [ "https://github.com/XSAM/otelsql" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/11/fsli5",
  "tags" : [ "opentelemetry", "sqlite", "sql" ],
  "client_id" : "https://editor.tanna.dev/"
}
