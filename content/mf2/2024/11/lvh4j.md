{
  "date" : "2024-11-04T08:00:14.012025033Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7422" ],
    "start" : [ "2024-11-03T00:00:00Z" ],
    "end" : [ "2024-11-04T00:00:00Z" ],
    "published" : [ "2024-11-04T08:00:14.012025033Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/lvh4j",
  "client_id" : "https://www-api.jvt.me/fit"
}
