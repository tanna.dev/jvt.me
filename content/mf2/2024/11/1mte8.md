{
  "date" : "2024-11-20T08:00:13.416274191Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5619" ],
    "start" : [ "2024-11-19T00:00:00Z" ],
    "end" : [ "2024-11-20T00:00:00Z" ],
    "published" : [ "2024-11-20T08:00:13.416274191Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/1mte8",
  "client_id" : "https://www-api.jvt.me/fit"
}
