{
  "date" : "2024-11-26T22:00:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-11-26T22:00:00+0000" ],
    "category" : [ "xkcd", "open-source" ],
    "repost-of" : [ "https://fosstodon.org/@lorenipsum/113551027862943605" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/11/rlsge",
  "tags" : [ "xkcd", "open-source" ],
  "client_id" : "https://indiepass.app/"
}
