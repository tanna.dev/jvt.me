{
  "date" : "2024-11-30T09:54:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-11-30T09:54:00+0000" ],
    "category" : [ "neovim", "adhd" ],
    "like-of" : [ "https://github.com/Hashino/doing.nvim" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/11/souew",
  "tags" : [ "neovim", "adhd" ],
  "client_id" : "https://indiepass.app/"
}
