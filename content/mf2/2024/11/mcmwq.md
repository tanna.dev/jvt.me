{
  "date" : "2024-11-14T08:00:12.67258854Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5032" ],
    "start" : [ "2024-11-13T00:00:00Z" ],
    "end" : [ "2024-11-14T00:00:00Z" ],
    "published" : [ "2024-11-14T08:00:12.67258854Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/mcmwq",
  "client_id" : "https://www-api.jvt.me/fit"
}
