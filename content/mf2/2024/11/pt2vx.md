{
  "date" : "2024-11-22T20:09:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/levelsio/status/1859770850171302339" ],
    "published" : [ "2024-11-22T20:09:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Please don't recommend people use online tools for possibly sensitive data - even \"client side\" should be avoided https://www.jvt.me/posts/2020/09/01/against-online-tooling/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/11/pt2vx",
  "client_id" : "https://indiepass.app/"
}
