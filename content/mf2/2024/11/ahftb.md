{
  "date" : "2024-11-05T18:00:00.000Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://essentialcomm.com/podcast/sorting-and-labeling/" ],
    "published" : [ "2024-11-05T18:00:00.000Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/11/ahftb",
  "client_id" : "https://editor.tanna.dev/"
}
