{
  "date" : "2024-11-30T08:00:12.565995962Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5389" ],
    "start" : [ "2024-11-29T00:00:00Z" ],
    "end" : [ "2024-11-30T00:00:00Z" ],
    "published" : [ "2024-11-30T08:00:12.565995962Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/tlgg9",
  "client_id" : "https://www-api.jvt.me/fit"
}
