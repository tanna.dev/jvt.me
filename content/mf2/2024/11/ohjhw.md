{
  "date" : "2024-11-29T08:00:16.758034743Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3800" ],
    "start" : [ "2024-11-28T00:00:00Z" ],
    "end" : [ "2024-11-29T00:00:00Z" ],
    "published" : [ "2024-11-29T08:00:16.758034743Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/ohjhw",
  "client_id" : "https://www-api.jvt.me/fit"
}
