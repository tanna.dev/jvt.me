{
  "date" : "2024-11-02T08:00:11.529455966Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5820" ],
    "start" : [ "2024-11-01T00:00:00Z" ],
    "end" : [ "2024-11-02T00:00:00Z" ],
    "published" : [ "2024-11-02T08:00:11.529455966Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/mv7f1",
  "client_id" : "https://www-api.jvt.me/fit"
}
