{
  "date" : "2024-11-22T22:19:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A screenshot of the Matomo analytics app, showing a hit from the `poe.com` AI app, pointing to a blog post that doesn't exist, which is rightfully returning a 404",
      "photo" : "https://media.jvt.me/aef0ab9e35.png"
    } ],
    "published" : [ "2024-11-22T22:19:00+0000" ],
    "category" : [ "llm", "ai" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "This is fun - it looks like I've had an <a href=\"/tags/llm/\">#LLM</a> / <a href=\"/tags/ai/\">#AI</a> tool hallucinate a blog post on my site 🤔"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/11/vrlwu",
  "tags" : [ "llm", "ai" ],
  "client_id" : "https://indiepass.app/"
}
