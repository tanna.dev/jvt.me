{
  "date" : "2024-11-05T08:00:11.970276894Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4935" ],
    "start" : [ "2024-11-04T00:00:00Z" ],
    "end" : [ "2024-11-05T00:00:00Z" ],
    "published" : [ "2024-11-05T08:00:11.970276894Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/wm1fq",
  "client_id" : "https://www-api.jvt.me/fit"
}
