{
  "date" : "2024-11-21T08:00:11.498663688Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8024" ],
    "start" : [ "2024-11-20T00:00:00Z" ],
    "end" : [ "2024-11-21T00:00:00Z" ],
    "published" : [ "2024-11-21T08:00:11.498663688Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/pq6fl",
  "client_id" : "https://www-api.jvt.me/fit"
}
