{
  "date" : "2024-11-26T08:00:11.634378825Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4837" ],
    "start" : [ "2024-11-25T00:00:00Z" ],
    "end" : [ "2024-11-26T00:00:00Z" ],
    "published" : [ "2024-11-26T08:00:11.634378825Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/bgcz6",
  "client_id" : "https://www-api.jvt.me/fit"
}
