{
  "date" : "2024-11-22T08:00:10.602208749Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6704" ],
    "start" : [ "2024-11-21T00:00:00Z" ],
    "end" : [ "2024-11-22T00:00:00Z" ],
    "published" : [ "2024-11-22T08:00:10.602208749Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/1j8st",
  "client_id" : "https://www-api.jvt.me/fit"
}
