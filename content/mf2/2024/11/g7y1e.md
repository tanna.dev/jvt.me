{
  "date" : "2024-11-12T08:00:12.972814288Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3771" ],
    "start" : [ "2024-11-11T00:00:00Z" ],
    "end" : [ "2024-11-12T00:00:00Z" ],
    "published" : [ "2024-11-12T08:00:12.972814288Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/g7y1e",
  "client_id" : "https://www-api.jvt.me/fit"
}
