{
  "date" : "2024-11-10T08:00:10.666079445Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8118" ],
    "start" : [ "2024-11-09T00:00:00Z" ],
    "end" : [ "2024-11-10T00:00:00Z" ],
    "published" : [ "2024-11-10T08:00:10.666079445Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/pqdox",
  "client_id" : "https://www-api.jvt.me/fit"
}
