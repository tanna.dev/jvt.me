{
  "date" : "2024-11-13T08:00:11.994668143Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2478" ],
    "start" : [ "2024-11-12T00:00:00Z" ],
    "end" : [ "2024-11-13T00:00:00Z" ],
    "published" : [ "2024-11-13T08:00:11.994668143Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/kmrge",
  "client_id" : "https://www-api.jvt.me/fit"
}
