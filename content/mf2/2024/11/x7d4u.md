{
  "date" : "2024-11-06T08:00:11.325326748Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2728" ],
    "start" : [ "2024-11-05T00:00:00Z" ],
    "end" : [ "2024-11-06T00:00:00Z" ],
    "published" : [ "2024-11-06T08:00:11.325326748Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/x7d4u",
  "client_id" : "https://www-api.jvt.me/fit"
}
