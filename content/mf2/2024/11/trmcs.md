{
  "date" : "2024-11-09T08:00:13.10837649Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2459" ],
    "start" : [ "2024-11-08T00:00:00Z" ],
    "end" : [ "2024-11-09T00:00:00Z" ],
    "published" : [ "2024-11-09T08:00:13.10837649Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/trmcs",
  "client_id" : "https://www-api.jvt.me/fit"
}
