{
  "date" : "2024-11-08T08:00:11.475457169Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4244" ],
    "start" : [ "2024-11-07T00:00:00Z" ],
    "end" : [ "2024-11-08T00:00:00Z" ],
    "published" : [ "2024-11-08T08:00:11.475457169Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/uu2uo",
  "client_id" : "https://www-api.jvt.me/fit"
}
