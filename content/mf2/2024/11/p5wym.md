{
  "date" : "2024-11-07T08:00:12.302846705Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3572" ],
    "start" : [ "2024-11-06T00:00:00Z" ],
    "end" : [ "2024-11-07T00:00:00Z" ],
    "published" : [ "2024-11-07T08:00:12.302846705Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/11/p5wym",
  "client_id" : "https://www-api.jvt.me/fit"
}
