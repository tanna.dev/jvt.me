{
  "date" : "2024-05-10T08:00:11.153113067Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5421" ],
    "start" : [ "2024-05-09T00:00:00Z" ],
    "end" : [ "2024-05-10T00:00:00Z" ],
    "published" : [ "2024-05-10T08:00:11.153113067Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/9h4br",
  "client_id" : "https://www-api.jvt.me/fit"
}
