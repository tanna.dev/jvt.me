{
  "date" : "2024-05-10T07:53:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-05-10T07:53:00+0100" ],
    "in-reply-to" : [ "https://x.com/ChrisShort/status/1788719682645635259?t=HpAaefpMb2-NEXt13lwdnQ&s=09" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Yup, especially as a maintainer, just in the last few days posted https://www.jvt.me/posts/2024/05/09/oapi-codegen-sustainable/ for the most active project I maintain, very much in my free time"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/05/tb3zy",
  "client_id" : "https://indiepass.app/"
}
