{
  "date" : "2024-05-27T09:19:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "name" : [ "The xz Hack Revealed a Looming $8.8 Trillion Infrastructure Disaster - The New Stack" ],
    "published" : [ "2024-05-27T09:19:00+0100" ],
    "category" : [ "open-source" ],
    "bookmark-of" : [ "https://thenewstack.io/the-xz-hack-reveals-a-looming-8-8-trillion-infrastructure-disaster-hidden-in-plain-sight/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2024/05/avjzu",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
