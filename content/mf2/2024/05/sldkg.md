{
  "date" : "2024-05-25T14:00:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-05-25T14:00:00+0100" ],
    "category" : [ "monolith", "microservices" ],
    "like-of" : [ "https://yekta.dev/posts/dont-microservice-do-module/#what-are-microservices" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/05/sldkg",
  "tags" : [ "monolith", "microservices" ],
  "client_id" : "https://indiepass.app/"
}
