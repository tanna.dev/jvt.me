{
  "date" : "2024-05-22T08:00:10.296112754Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4676" ],
    "start" : [ "2024-05-21T00:00:00Z" ],
    "end" : [ "2024-05-22T00:00:00Z" ],
    "published" : [ "2024-05-22T08:00:10.296112754Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/bfuge",
  "client_id" : "https://www-api.jvt.me/fit"
}
