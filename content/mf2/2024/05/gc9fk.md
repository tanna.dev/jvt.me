{
  "date" : "2024-05-05T08:00:09.989797335Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7238" ],
    "start" : [ "2024-05-04T00:00:00Z" ],
    "end" : [ "2024-05-05T00:00:00Z" ],
    "published" : [ "2024-05-05T08:00:09.989797335Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/gc9fk",
  "client_id" : "https://www-api.jvt.me/fit"
}
