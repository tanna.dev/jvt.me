{
  "date" : "2024-05-16T08:00:10.249600912Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6275" ],
    "start" : [ "2024-05-15T00:00:00Z" ],
    "end" : [ "2024-05-16T00:00:00Z" ],
    "published" : [ "2024-05-16T08:00:10.249600912Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/mghce",
  "client_id" : "https://www-api.jvt.me/fit"
}
