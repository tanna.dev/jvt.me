{
  "date" : "2024-05-12T08:00:12.200559648Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6116" ],
    "start" : [ "2024-05-11T00:00:00Z" ],
    "end" : [ "2024-05-12T00:00:00Z" ],
    "published" : [ "2024-05-12T08:00:12.200559648Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/pmju2",
  "client_id" : "https://www-api.jvt.me/fit"
}
