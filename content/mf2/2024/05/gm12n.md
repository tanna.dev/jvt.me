{
  "date" : "2024-05-24T08:01:18.17084387Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8047" ],
    "start" : [ "2024-05-23T00:00:00Z" ],
    "end" : [ "2024-05-24T00:00:00Z" ],
    "published" : [ "2024-05-24T08:01:18.17084387Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/gm12n",
  "client_id" : "https://www-api.jvt.me/fit"
}
