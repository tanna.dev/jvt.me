{
  "date" : "2024-05-20T22:48:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://social.coop/@jsit/112474886908221811" ],
    "published" : [ "2024-05-20T22:48:00+0100" ],
    "category" : [ "indieweb" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "There are a number in the <a href=\"/tags/indieweb/\">#IndieWeb</a> community, generally under https://indieweb.org/Microsub#Clients - and in fact what pointed me to your post to reply to, all via Micropub and Webmentions 😁"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/05/ee6me",
  "tags" : [ "indieweb" ],
  "client_id" : "https://indiepass.app/"
}
