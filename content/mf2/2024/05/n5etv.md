{
  "date" : "2024-05-23T08:00:10.152100987Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3376" ],
    "start" : [ "2024-05-22T00:00:00Z" ],
    "end" : [ "2024-05-23T00:00:00Z" ],
    "published" : [ "2024-05-23T08:00:10.152100987Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/n5etv",
  "client_id" : "https://www-api.jvt.me/fit"
}
