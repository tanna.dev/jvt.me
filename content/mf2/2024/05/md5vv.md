{
  "date" : "2024-05-18T09:06:19.398561072Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://engineering-founders.simplecast.com/episodes/exploring-the-differences-of-hardware-software-startups-jessie-frazelle-zoo-GSP1e1Xe" ],
    "published" : [ "2024-05-18T09:06:19.398561072Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/05/md5vv",
  "client_id" : "https://editor.tanna.dev/"
}
