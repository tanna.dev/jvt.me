{
  "date" : "2024-05-25T08:00:10.532668625Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7159" ],
    "start" : [ "2024-05-24T00:00:00Z" ],
    "end" : [ "2024-05-25T00:00:00Z" ],
    "published" : [ "2024-05-25T08:00:10.532668625Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/ofrdc",
  "client_id" : "https://www-api.jvt.me/fit"
}
