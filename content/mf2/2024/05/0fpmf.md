{
  "date" : "2024-05-27T08:00:09.92890171Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6385" ],
    "start" : [ "2024-05-26T00:00:00Z" ],
    "end" : [ "2024-05-27T00:00:00Z" ],
    "published" : [ "2024-05-27T08:00:09.92890171Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/0fpmf",
  "client_id" : "https://www-api.jvt.me/fit"
}
