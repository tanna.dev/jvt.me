{
  "date" : "2024-05-29T08:00:11.831498094Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6704" ],
    "start" : [ "2024-05-28T00:00:00Z" ],
    "end" : [ "2024-05-29T00:00:00Z" ],
    "published" : [ "2024-05-29T08:00:11.831498094Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/yc3u4",
  "client_id" : "https://www-api.jvt.me/fit"
}
