{
  "date" : "2024-05-15T08:00:10.023716084Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3376" ],
    "start" : [ "2024-05-14T00:00:00Z" ],
    "end" : [ "2024-05-15T00:00:00Z" ],
    "published" : [ "2024-05-15T08:00:10.023716084Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/zouhy",
  "client_id" : "https://www-api.jvt.me/fit"
}
