{
  "date" : "2024-05-17T08:00:10.266506259Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3748" ],
    "start" : [ "2024-05-16T00:00:00Z" ],
    "end" : [ "2024-05-17T00:00:00Z" ],
    "published" : [ "2024-05-17T08:00:10.266506259Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/iicc6",
  "client_id" : "https://www-api.jvt.me/fit"
}
