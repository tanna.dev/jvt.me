{
  "date" : "2024-05-31T08:00:10.536906148Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4756" ],
    "start" : [ "2024-05-30T00:00:00Z" ],
    "end" : [ "2024-05-31T00:00:00Z" ],
    "published" : [ "2024-05-31T08:00:10.536906148Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/sr3xz",
  "client_id" : "https://www-api.jvt.me/fit"
}
