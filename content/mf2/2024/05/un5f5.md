{
  "date" : "2024-05-27T07:55:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-05-27T07:55:00+0100" ],
    "category" : [ "open-source", "curl" ],
    "like-of" : [ "https://daniel.haxx.se/blog/2024/05/27/my-bdfl-guiding-principles/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/05/un5f5",
  "tags" : [ "open-source", "curl" ],
  "client_id" : "https://indiepass.app/"
}
