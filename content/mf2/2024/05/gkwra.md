{
  "date" : "2024-05-21T08:00:10.120266816Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4424" ],
    "start" : [ "2024-05-20T00:00:00Z" ],
    "end" : [ "2024-05-21T00:00:00Z" ],
    "published" : [ "2024-05-21T08:00:10.120266816Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/gkwra",
  "client_id" : "https://www-api.jvt.me/fit"
}
