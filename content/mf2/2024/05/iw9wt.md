{
  "date" : "2024-05-02T06:29:43.465056887Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-05-02T06:29:43.465056887Z" ],
    "category" : [ "gpg", "yubikey" ],
    "like-of" : [ "https://den.dev/blog/signing-github-commits-yubikey/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/05/iw9wt",
  "tags" : [ "gpg", "yubikey" ],
  "client_id" : "https://editor.tanna.dev/"
}
