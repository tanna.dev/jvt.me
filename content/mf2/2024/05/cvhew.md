{
  "date" : "2024-05-08T08:00:10.673608503Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7489" ],
    "start" : [ "2024-05-07T00:00:00Z" ],
    "end" : [ "2024-05-08T00:00:00Z" ],
    "published" : [ "2024-05-08T08:00:10.673608503Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/cvhew",
  "client_id" : "https://www-api.jvt.me/fit"
}
