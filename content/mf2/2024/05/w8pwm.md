{
  "date" : "2024-05-06T08:00:11.437241505Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6850" ],
    "start" : [ "2024-05-05T00:00:00Z" ],
    "end" : [ "2024-05-06T00:00:00Z" ],
    "published" : [ "2024-05-06T08:00:11.437241505Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/w8pwm",
  "client_id" : "https://www-api.jvt.me/fit"
}
