{
  "date" : "2024-05-01T08:00:12.060326168Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4571" ],
    "start" : [ "2024-04-30T00:00:00Z" ],
    "end" : [ "2024-05-01T00:00:00Z" ],
    "published" : [ "2024-05-01T08:00:12.060326168Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/dvsgu",
  "client_id" : "https://www-api.jvt.me/fit"
}
