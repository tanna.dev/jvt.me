{
  "date" : "2024-05-04T08:00:10.337951962Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5260" ],
    "start" : [ "2024-05-03T00:00:00Z" ],
    "end" : [ "2024-05-04T00:00:00Z" ],
    "published" : [ "2024-05-04T08:00:10.337951962Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/6zmtv",
  "client_id" : "https://www-api.jvt.me/fit"
}
