{
  "date" : "2024-05-30T08:00:10.606208368Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7167" ],
    "start" : [ "2024-05-29T00:00:00Z" ],
    "end" : [ "2024-05-30T00:00:00Z" ],
    "published" : [ "2024-05-30T08:00:10.606208368Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/qiqje",
  "client_id" : "https://www-api.jvt.me/fit"
}
