{
  "date" : "2024-05-14T08:00:11.122585127Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4660" ],
    "start" : [ "2024-05-13T00:00:00Z" ],
    "end" : [ "2024-05-14T00:00:00Z" ],
    "published" : [ "2024-05-14T08:00:11.122585127Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/9xmas",
  "client_id" : "https://www-api.jvt.me/fit"
}
