{
  "date" : "2024-05-23T20:04:35.380823195Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-05-23T20:04:35.380823195Z" ],
    "category" : [ "music" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "[Light Cycles - ShockOne](https://open.spotify.com/track/7MA7gioNIfSa6gDdNqsBRX) is a _supremely_ underrated song 🎶"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/05/lppic",
  "tags" : [ "music" ],
  "client_id" : "https://editor.tanna.dev/"
}
