{
  "date" : "2024-05-09T08:00:10.157971996Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3772" ],
    "start" : [ "2024-05-08T00:00:00Z" ],
    "end" : [ "2024-05-09T00:00:00Z" ],
    "published" : [ "2024-05-09T08:00:10.157971996Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/5fydg",
  "client_id" : "https://www-api.jvt.me/fit"
}
