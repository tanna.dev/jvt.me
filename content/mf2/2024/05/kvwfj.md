{
  "date" : "2024-05-07T08:00:10.358278854Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4085" ],
    "start" : [ "2024-05-06T00:00:00Z" ],
    "end" : [ "2024-05-07T00:00:00Z" ],
    "published" : [ "2024-05-07T08:00:10.358278854Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/kvwfj",
  "client_id" : "https://www-api.jvt.me/fit"
}
