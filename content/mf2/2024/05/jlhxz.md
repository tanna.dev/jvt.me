{
  "date" : "2024-05-03T08:00:11.333035669Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9726" ],
    "start" : [ "2024-05-02T00:00:00Z" ],
    "end" : [ "2024-05-03T00:00:00Z" ],
    "published" : [ "2024-05-03T08:00:11.333035669Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/jlhxz",
  "client_id" : "https://www-api.jvt.me/fit"
}
