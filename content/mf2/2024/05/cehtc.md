{
  "date" : "2024-05-06T09:21:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-05-06T09:21:00+0100" ],
    "category" : [ "github" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "GitHub [relaxing the requirement](https://github.blog/changelog/2024-05-01-github-apps-can-now-use-the-client-id-to-fetch-installation-tokens/) of the construction of the GitHub App's JWT (under `private_key_jwt`) is interesting, especially if now you don't need to know the installation ID to auth.\n\nPresumably this also means that on GitHub's side they're still limiting access to repos that an installation has access to, but I'd have assumed that by doing it by installation ID you'd get additional checks\n\n(I'd been lazy in the past and would rarely persist the installation ID, needing me to then go in and find it through the GitHub UI 😅)"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/05/cehtc",
  "tags" : [ "github" ],
  "client_id" : "https://indiepass.app/"
}
