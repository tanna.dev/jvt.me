{
  "date" : "2024-05-09T06:59:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-05-09T06:59:00+0100" ],
    "category" : [ "oapi-codegen", "public-speaking", "openapi" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very excited to be a guest on tomorrow's @cupogo@techhub.social and to chat about [`oapi-codegen`](https://github.com/deepmap/oapi-codegen), <a href=\"/tags/openapi/\">#OpenAPI</a> and Go!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/05/ph3vc",
  "tags" : [ "oapi-codegen", "public-speaking", "openapi" ],
  "client_id" : "https://indiepass.app/"
}
