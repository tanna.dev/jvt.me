{
  "date" : "2024-05-07T11:26:36.456290704Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-05-07T11:26:36.456290704Z" ],
    "category" : [ "open-uk", "github", "maintainer-month" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very excited to announce that @lornajane@indieweb.social and I are running a new @openuk@hachyderm.io Meetup that's digital-only, alongside the other great events being run by the <a href=\"/tags/open-uk/\">#OpenUK</a> organisation.\r\n\r\nOur [first event](https://www.meetup.com/openuk/events/300878789/) will be a tie-in with <a href=\"/tags/github/\">#GitHub</a>'s <a href=\"/tags/maintainer-month/\">#MaintainerMonth</a> and we'll be hearing from a number of excellent maintainers from a variety of projects - stay tuned for more details.\r\n\r\nLooking forward to seeing some of y'all on May 22nd at 1200 noon (UK time) for an interesting webinar! \r\n\r\n(This will be in addition to other OpenUK events still being hybrid where possible)"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/05/4up9p",
  "tags" : [ "open-uk", "github", "maintainer-month" ],
  "client_id" : "https://editor.tanna.dev/"
}
