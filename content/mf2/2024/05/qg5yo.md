{
  "date" : "2024-05-02T22:25:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-05-02T22:25:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://flak.tedunangst.com/post/go-module-bloat" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/05/qg5yo",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
