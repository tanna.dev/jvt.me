{
  "date" : "2024-05-02T09:29:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/bikun_bikun/status/1785923656377598457?t=UqrG4Eik9SHAp-jHqOU7Dw&s=09" ],
    "published" : [ "2024-05-02T09:29:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thanks for the report! No need, I've raised https://github.com/deepmap/oapi-codegen/pull/1585 which should fix it!\n\nIf you're able to try the branch to confirm it that'd be appreciated (instructions in the README) but no worries if not"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/05/ernk7",
  "client_id" : "https://indiepass.app/"
}
