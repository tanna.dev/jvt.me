{
  "date" : "2024-05-02T08:00:11.468818324Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7043" ],
    "start" : [ "2024-05-01T00:00:00Z" ],
    "end" : [ "2024-05-02T00:00:00Z" ],
    "published" : [ "2024-05-02T08:00:11.468818324Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/kqpfk",
  "client_id" : "https://www-api.jvt.me/fit"
}
