{
  "date" : "2024-05-20T08:00:10.030899657Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4693" ],
    "start" : [ "2024-05-19T00:00:00Z" ],
    "end" : [ "2024-05-20T00:00:00Z" ],
    "published" : [ "2024-05-20T08:00:10.030899657Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/0nopy",
  "client_id" : "https://www-api.jvt.me/fit"
}
