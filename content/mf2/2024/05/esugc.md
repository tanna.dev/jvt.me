{
  "date" : "2024-05-11T08:00:10.640740662Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8449" ],
    "start" : [ "2024-05-10T00:00:00Z" ],
    "end" : [ "2024-05-11T00:00:00Z" ],
    "published" : [ "2024-05-11T08:00:10.640740662Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/esugc",
  "client_id" : "https://www-api.jvt.me/fit"
}
