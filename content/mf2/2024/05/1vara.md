{
  "date" : "2024-05-21T16:58:09.624785483Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://podcasters.spotify.com/pod/show/incident-fm/episodes/Why-speed-of-iteration-made-buying-incident-io-the-right-choice-with-John-Paris-of-Skyscanner-e2jtbrt" ],
    "published" : [ "2024-05-21T16:58:09.624785483Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/05/1vara",
  "client_id" : "https://editor.tanna.dev/"
}
