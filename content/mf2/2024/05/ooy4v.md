{
  "date" : "2024-05-01T14:15:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/bikun_bikun/status/1782309431180702053?t=4nEXtyT-nrnvKdA19-oMXA&s=09" ],
    "published" : [ "2024-05-01T14:15:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Have you added the `output-options` configuration item for it? (https://github.com/deepmap/oapi-codegen/?tab=readme-ov-file#generating-nullable-types)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/05/ooy4v",
  "client_id" : "https://indiepass.app/"
}
