{
  "date" : "2024-05-26T08:00:10.383763729Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7547" ],
    "start" : [ "2024-05-25T00:00:00Z" ],
    "end" : [ "2024-05-26T00:00:00Z" ],
    "published" : [ "2024-05-26T08:00:10.383763729Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/05/rzzqg",
  "client_id" : "https://www-api.jvt.me/fit"
}
