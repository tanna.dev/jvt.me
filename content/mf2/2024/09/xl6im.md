{
  "date" : "2024-09-22T08:00:13.327511009Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "12559" ],
    "start" : [ "2024-09-21T00:00:00Z" ],
    "end" : [ "2024-09-22T00:00:00Z" ],
    "published" : [ "2024-09-22T08:00:13.327511009Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/xl6im",
  "client_id" : "https://www-api.jvt.me/fit"
}
