{
  "date" : "2024-09-03T21:53:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@jawnsy/113069997585920818" ],
    "published" : [ "2024-09-03T21:53:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'd say that adding a separate `/x/` or `/exp/` package can also be a good way of testing out new things - very clearly a separate \"experimental\" thing, which can be tested independently\n\nUsing a fork of the module is also a good suggestion and a way to make sure it's not even in the main repo, but gives you mostly the same codebase to be able to test things"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/n4xjb",
  "client_id" : "https://indiepass.app/"
}
