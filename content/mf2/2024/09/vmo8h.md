{
  "date" : "2024-09-28T08:00:10.809715575Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6606" ],
    "start" : [ "2024-09-27T00:00:00Z" ],
    "end" : [ "2024-09-28T00:00:00Z" ],
    "published" : [ "2024-09-28T08:00:10.809715575Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/vmo8h",
  "client_id" : "https://www-api.jvt.me/fit"
}
