{
  "date" : "2024-09-21T09:02:53.110925381Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-09-21T09:02:53.110925381Z" ],
    "category" : [ "ogg-camp" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Who else do I know who's going to <a href=\"/tags/ogg-camp/\">#OggCamp</a>? 👀 Where do folks recommend for a fairly last minute hotel nearby?"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/09/tfhy3",
  "tags" : [ "ogg-camp" ],
  "client_id" : "https://editor.tanna.dev/"
}
