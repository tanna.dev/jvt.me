{
  "date" : "2024-09-03T08:00:10.674875126Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2326" ],
    "start" : [ "2024-09-02T00:00:00Z" ],
    "end" : [ "2024-09-03T00:00:00Z" ],
    "published" : [ "2024-09-03T08:00:10.674875126Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/lbtfr",
  "client_id" : "https://www-api.jvt.me/fit"
}
