{
  "date" : "2024-09-04T21:54:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@grimalkina/113081108010125031" ],
    "published" : [ "2024-09-04T21:54:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Would love to hear about this 💜 as someone who does something somewhat similar, somewhat passively (publicly sharing [my salary history](https://www.jvt.me/salary/)), I can attest to how important it is to chat about it with your friends and colleagues and work to get better 🙌🏽"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/jqwjs",
  "client_id" : "https://indiepass.app/"
}
