{
  "date" : "2024-09-23T10:48:16.688250674Z",
  "deleted" : true,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A 55 inch Sony Bravia TV on top of an IKEA Kallax shelving unit, which is laid horizontally a little awkwardly. Behind the Kallax, you can see the new TV stand which is not wide enough to hold the TV, which annoyingly has legs that are the width of the TV, instead of being in the centre",
      "photo" : "https://media.jvt.me/0d7cf2e79a.jpeg"
    }, {
      "alt" : "A different shot of the 55 inch Sony Bravia TV, but now on the nice wooden TV stand. On the TV you can see a shot from The Acolyte, which looks glorious in 4k, and although it's not super clear, behind the TV we've got a Philips Hue Play light strip which adds great lighting behind the very large telly. In the foreground you can see Cookie who is super cute and sleepy",
      "photo" : "https://media.jvt.me/de8d447780.jpeg"
    } ],
    "published" : [ "2024-09-23T10:48:16.688250674Z" ],
    "content" : [ {
      "html" : "",
      "value" : "So we got a new TV, but naturally I didn't end up measuring the TV and the TV stand quite as closely as you would have expected 🫣"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/09/plr6a",
  "client_id" : "https://gitlab.com/jamietanna/micropub-go/"
}
