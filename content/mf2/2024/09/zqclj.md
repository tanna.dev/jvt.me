{
  "date" : "2024-09-23T10:50:52.516342419Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A speaker banner from the DTX London conference, indicating the talk is on the 2nd October at 1635-1655",
      "photo" : "https://media.jvt.me/7f0f1da7ed.png"
    } ],
    "published" : [ "2024-09-23T10:50:52.516342419Z" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm very excited to be speaking at [DTX London](https://www.dtxevents.io/london) next week, at [DevOps Exchange's talks takeover](https://www.eventbrite.co.uk/e/devops-exchange-london-at-dtx-europe-2024-tickets-1024882279487).\\n\\nI'll be talking about [Quantifying your reliance on Open Source software](https://talks.jvt.me/dmd/) and how you can use [dependency-management-data](https://dmd.tanna.dev) to gain some really interesting insights into your dependency data.\\n\\nHope some of y'all can join me there"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/09/zqclj",
  "client_id" : "https://gitlab.com/jamietanna/micropub-go/"
}
