{
  "date" : "2024-09-16T08:00:11.964185464Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8520" ],
    "start" : [ "2024-09-15T00:00:00Z" ],
    "end" : [ "2024-09-16T00:00:00Z" ],
    "published" : [ "2024-09-16T08:00:11.964185464Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/wxisd",
  "client_id" : "https://www-api.jvt.me/fit"
}
