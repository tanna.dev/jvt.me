{
  "date" : "2024-09-02T08:00:10.975823701Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5813" ],
    "start" : [ "2024-09-01T00:00:00Z" ],
    "end" : [ "2024-09-02T00:00:00Z" ],
    "published" : [ "2024-09-02T08:00:10.975823701Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/rwnwf",
  "client_id" : "https://www-api.jvt.me/fit"
}
