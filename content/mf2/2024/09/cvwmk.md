{
  "date" : "2024-09-17T08:00:14.962930457Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6554" ],
    "start" : [ "2024-09-16T00:00:00Z" ],
    "end" : [ "2024-09-17T00:00:00Z" ],
    "published" : [ "2024-09-17T08:00:14.962930457Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/cvwmk",
  "client_id" : "https://www-api.jvt.me/fit"
}
