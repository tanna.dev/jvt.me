{
  "date" : "2024-09-13T08:00:12.301472398Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8293" ],
    "start" : [ "2024-09-12T00:00:00Z" ],
    "end" : [ "2024-09-13T00:00:00Z" ],
    "published" : [ "2024-09-13T08:00:12.301472398Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/c4xuw",
  "client_id" : "https://www-api.jvt.me/fit"
}
