{
  "date" : "2024-09-11T08:00:10.311490066Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5631" ],
    "start" : [ "2024-09-10T00:00:00Z" ],
    "end" : [ "2024-09-11T00:00:00Z" ],
    "published" : [ "2024-09-11T08:00:10.311490066Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/km3mt",
  "client_id" : "https://www-api.jvt.me/fit"
}
