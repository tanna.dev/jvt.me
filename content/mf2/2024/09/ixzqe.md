{
  "date" : "2024-09-17T10:25:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-09-17T10:25:00+0100" ],
    "category" : [ "wasm", "go" ],
    "like-of" : [ "https://eli.thegreenplace.net/2024/notes-on-running-go-in-the-browser-with-webassembly/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/09/ixzqe",
  "tags" : [ "wasm", "go" ],
  "client_id" : "https://indiepass.app/"
}
