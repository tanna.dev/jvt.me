{
  "date" : "2024-09-24T10:28:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@anderseknert/113191706986003922" ],
    "published" : [ "2024-09-24T10:28:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Yeah not sure where that came from but as you say, you can pin to a tag/commit, and Go's module proxy stops you from having someone re-push the value of a tag, once it's had someone download a dependency.\n\nYou also _only_ pin, as there's no way to do a range, so IMO that makes it nicer and more explicit than other languages / toolchains with respect to pinning"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/ggown",
  "client_id" : "https://indiepass.app/"
}
