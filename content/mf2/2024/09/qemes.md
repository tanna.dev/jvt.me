{
  "date" : "2024-09-20T20:46:01.350550376Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-09-20T20:46:01.350550376Z" ],
    "category" : [ "oapi-codegen", "openapi" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Just put the finishing touches on the latest release of oapi-codegen: v2.4.0 includes:\r\n\r\n\r\n- <a href=\"/tags/openapi/\">#OpenAPI</a> Overlay functionality\r\n\r\n- Improved multi-file OpenAPI spec support\r\n\r\n- Several other features and bug fixes\r\n\r\n\r\nhttps://github.com/oapi-codegen/oapi-codegen/releases/tag/v2.4.0"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/09/qemes",
  "tags" : [ "oapi-codegen", "open-api" ],
  "client_id" : "https://editor.tanna.dev/"
}
