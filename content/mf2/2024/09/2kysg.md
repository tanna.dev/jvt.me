{
  "date" : "2024-09-05T08:00:14.985967863Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7856" ],
    "start" : [ "2024-09-04T00:00:00Z" ],
    "end" : [ "2024-09-05T00:00:00Z" ],
    "published" : [ "2024-09-05T08:00:14.985967863Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/2kysg",
  "client_id" : "https://www-api.jvt.me/fit"
}
