{
  "date" : "2024-09-28T11:44:04.991058315Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-09-28T11:44:04.991058315Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "With the performance management season mostly over, I've done a refresh [on my CV](https://hire.jvt.me/) - this can be your reminder to give yours a refresh. You don't need to be looking to keep it up to date!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/09/msep7",
  "client_id" : "https://editor.tanna.dev/"
}
