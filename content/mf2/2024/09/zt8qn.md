{
  "date" : "2024-09-09T08:00:12.974942889Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3320" ],
    "start" : [ "2024-09-08T00:00:00Z" ],
    "end" : [ "2024-09-09T00:00:00Z" ],
    "published" : [ "2024-09-09T08:00:12.974942889Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/zt8qn",
  "client_id" : "https://www-api.jvt.me/fit"
}
