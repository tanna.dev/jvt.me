{
  "date" : "2024-09-10T19:31:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/ibuildthecloud/status/1833540050086465997" ],
    "published" : [ "2024-09-10T19:31:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Hand writing the spec, then validating (ie via http://gitlab.com/jamietanna/httptest-openapi) is a valid option - keeps it in sync very well from quite a few big Go apps I know using it\n\nOr you could wrap your implementation in https://github.com/oapi-codegen/oapi-codegen/ from a hand rolled spec - we support `net/http` - and that then starts towards the process of being closer to the spec\n\nEither way, you still need to validate the spec and implementation are in sync, and IMO, hand-writing the spec is the only \"right\" solution that makes sure you're _intentionally_ making changes to the API, whereas generating spec from code could lead to just documenting what's in place and ie accidental breaking changes being missed"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/nrwq7",
  "client_id" : "https://indiepass.app/"
}
