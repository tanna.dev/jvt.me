{
  "date" : "2024-09-21T08:00:10.447645399Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10166" ],
    "start" : [ "2024-09-20T00:00:00Z" ],
    "end" : [ "2024-09-21T00:00:00Z" ],
    "published" : [ "2024-09-21T08:00:10.447645399Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/c9y35",
  "client_id" : "https://www-api.jvt.me/fit"
}
