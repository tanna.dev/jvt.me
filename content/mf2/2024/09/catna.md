{
  "date" : "2024-09-17T21:33:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-09-17T21:33:00+0100" ],
    "category" : [ "ethics", "politics", "capitalism" ],
    "repost-of" : [ "https://mastodon.green/@alberto_cottica/113152335455269627" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/09/catna",
  "tags" : [ "ethics", "politics", "capitalism" ],
  "client_id" : "https://indiepass.app/"
}
