{
  "date" : "2024-09-26T08:00:12.35505865Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "16323" ],
    "start" : [ "2024-09-25T00:00:00Z" ],
    "end" : [ "2024-09-26T00:00:00Z" ],
    "published" : [ "2024-09-26T08:00:12.35505865Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/lzqbn",
  "client_id" : "https://www-api.jvt.me/fit"
}
