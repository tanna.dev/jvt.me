{
  "date" : "2024-09-18T08:00:09.988978849Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6527" ],
    "start" : [ "2024-09-17T00:00:00Z" ],
    "end" : [ "2024-09-18T00:00:00Z" ],
    "published" : [ "2024-09-18T08:00:09.988978849Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/nggev",
  "client_id" : "https://www-api.jvt.me/fit"
}
