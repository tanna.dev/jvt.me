{
  "date" : "2024-09-27T08:00:14.225497269Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9104" ],
    "start" : [ "2024-09-26T00:00:00Z" ],
    "end" : [ "2024-09-27T00:00:00Z" ],
    "published" : [ "2024-09-27T08:00:14.225497269Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/i4jpi",
  "client_id" : "https://www-api.jvt.me/fit"
}
