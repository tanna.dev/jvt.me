{
  "date" : "2024-09-06T08:00:13.275145665Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7013" ],
    "start" : [ "2024-09-05T00:00:00Z" ],
    "end" : [ "2024-09-06T00:00:00Z" ],
    "published" : [ "2024-09-06T08:00:13.275145665Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/snphi",
  "client_id" : "https://www-api.jvt.me/fit"
}
