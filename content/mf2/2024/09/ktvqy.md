{
  "date" : "2024-09-23T08:00:10.447008479Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5420" ],
    "start" : [ "2024-09-22T00:00:00Z" ],
    "end" : [ "2024-09-23T00:00:00Z" ],
    "published" : [ "2024-09-23T08:00:10.447008479Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/ktvqy",
  "client_id" : "https://www-api.jvt.me/fit"
}
