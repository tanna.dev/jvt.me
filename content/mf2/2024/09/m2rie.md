{
  "date" : "2024-09-11T08:41:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.dtxevents.io/london-line-up/agenda/#/seminars/devops-exchange-meetup-talk-quantifying-your-reliance-on-open-source-software" ],
    "published" : [ "2024-09-11T08:41:00+0100" ],
    "post-status" : [ "published" ],
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2024/09/m2rie",
  "client_id" : "https://indiepass.app/"
}
