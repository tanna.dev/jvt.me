{
  "date" : "2024-09-04T08:00:10.943096432Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5515" ],
    "start" : [ "2024-09-03T00:00:00Z" ],
    "end" : [ "2024-09-04T00:00:00Z" ],
    "published" : [ "2024-09-04T08:00:10.943096432Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/gg8j2",
  "client_id" : "https://www-api.jvt.me/fit"
}
