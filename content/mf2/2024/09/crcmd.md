{
  "date" : "2024-09-29T08:00:11.162929092Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5673" ],
    "start" : [ "2024-09-28T00:00:00Z" ],
    "end" : [ "2024-09-29T00:00:00Z" ],
    "published" : [ "2024-09-29T08:00:11.162929092Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/crcmd",
  "client_id" : "https://www-api.jvt.me/fit"
}
