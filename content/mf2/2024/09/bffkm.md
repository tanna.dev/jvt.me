{
  "date" : "2024-09-19T08:00:11.460156229Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4076" ],
    "start" : [ "2024-09-18T00:00:00Z" ],
    "end" : [ "2024-09-19T00:00:00Z" ],
    "published" : [ "2024-09-19T08:00:11.460156229Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/bffkm",
  "client_id" : "https://www-api.jvt.me/fit"
}
