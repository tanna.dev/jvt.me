{
  "date" : "2024-09-12T08:00:17.575482265Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6734" ],
    "start" : [ "2024-09-11T00:00:00Z" ],
    "end" : [ "2024-09-12T00:00:00Z" ],
    "published" : [ "2024-09-12T08:00:17.575482265Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/1kevs",
  "client_id" : "https://www-api.jvt.me/fit"
}
