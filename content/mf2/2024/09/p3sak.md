{
  "date" : "2024-09-25T08:00:14.046957882Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "17550" ],
    "start" : [ "2024-09-24T00:00:00Z" ],
    "end" : [ "2024-09-25T00:00:00Z" ],
    "published" : [ "2024-09-25T08:00:14.046957882Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/p3sak",
  "client_id" : "https://www-api.jvt.me/fit"
}
