{
  "date" : "2024-09-10T08:00:12.230285358Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2651" ],
    "start" : [ "2024-09-09T00:00:00Z" ],
    "end" : [ "2024-09-10T00:00:00Z" ],
    "published" : [ "2024-09-10T08:00:12.230285358Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/e4gyp",
  "client_id" : "https://www-api.jvt.me/fit"
}
