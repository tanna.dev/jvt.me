{
  "date" : "2024-09-04T23:06:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@Edent/113078335697769154" ],
    "published" : [ "2024-09-04T23:06:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I used to do this - especially with t-shirts at stake - but since last year I'm not as bothered 🤷🏽‍♂️\n\nNow I try and raise PRs when I can, otherwise I'll forget about them 🙃"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/plvjf",
  "client_id" : "https://indiepass.app/"
}
