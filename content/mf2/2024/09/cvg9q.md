{
  "date" : "2024-09-20T09:26:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A notification from the McDonald's Android app, which reads: \"McRib_Test.notification_16.10.24 [TEST].",
      "photo" : "https://media.jvt.me/dc9e93263d.png"
    } ],
    "published" : [ "2024-09-20T09:26:00+0100" ],
    "category" : [ "mcdonalds" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "So I guess <a href=\"/tags/mcdonalds/\">#Mcdonalds</a> is going to be bringing the McRib back in October, based on this test notification? 👀"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/09/cvg9q",
  "tags" : [ "mcdonalds" ],
  "client_id" : "https://indiepass.app/"
}
