{
  "date" : "2024-09-30T08:00:10.726204492Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7420" ],
    "start" : [ "2024-09-29T00:00:00Z" ],
    "end" : [ "2024-09-30T00:00:00Z" ],
    "published" : [ "2024-09-30T08:00:10.726204492Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/n4ci0",
  "client_id" : "https://www-api.jvt.me/fit"
}
