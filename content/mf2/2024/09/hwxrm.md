{
  "date" : "2024-09-07T08:00:12.208102705Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6144" ],
    "start" : [ "2024-09-06T00:00:00Z" ],
    "end" : [ "2024-09-07T00:00:00Z" ],
    "published" : [ "2024-09-07T08:00:12.208102705Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/hwxrm",
  "client_id" : "https://www-api.jvt.me/fit"
}
