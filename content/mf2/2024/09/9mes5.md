{
  "date" : "2024-09-14T08:00:11.147182066Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9759" ],
    "start" : [ "2024-09-13T00:00:00Z" ],
    "end" : [ "2024-09-14T00:00:00Z" ],
    "published" : [ "2024-09-14T08:00:11.147182066Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/9mes5",
  "client_id" : "https://www-api.jvt.me/fit"
}
