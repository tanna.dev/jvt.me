{
  "date" : "2024-09-03T21:54:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@jawnsy/113070098653692860" ],
    "published" : [ "2024-09-03T21:54:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "They can be useful, but at least my OSS doesn't need them much. I've got some work bits that do have `Must`s for convenience either in `func main` or as you say, in tests"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/cc0ir",
  "client_id" : "https://indiepass.app/"
}
