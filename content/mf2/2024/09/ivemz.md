{
  "date" : "2024-09-15T08:00:11.263392912Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6796" ],
    "start" : [ "2024-09-14T00:00:00Z" ],
    "end" : [ "2024-09-15T00:00:00Z" ],
    "published" : [ "2024-09-15T08:00:11.263392912Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/ivemz",
  "client_id" : "https://www-api.jvt.me/fit"
}
