{
  "date" : "2024-09-24T08:00:11.096052444Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10608" ],
    "start" : [ "2024-09-23T00:00:00Z" ],
    "end" : [ "2024-09-24T00:00:00Z" ],
    "published" : [ "2024-09-24T08:00:11.096052444Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/09/acvec",
  "client_id" : "https://www-api.jvt.me/fit"
}
