{
  "date" : "2024-09-14T22:05:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-09-14T22:05:00+0100" ],
    "category" : [ "open-source", "elastic" ],
    "like-of" : [ "https://socket.dev/blog/developers-burned-by-elasticsearch-license-change-arent-going-back" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/09/jhaes",
  "tags" : [ "open-source", "elastic" ],
  "client_id" : "https://indiepass.app/"
}
