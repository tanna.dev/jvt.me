{
  "date" : "2024-09-24T10:16:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@tomasaschan/113191733277367363" ],
    "published" : [ "2024-09-24T10:16:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Yeah I'm very much looking forward to `// tool` landing as that'll push more folks to using a `tools.go` style approach. \n\nI'm seeing some folks who'll use a `tools.go` in a separate Go module so then it doesn't impact the top-level dependency tree, on top of Go' s inbuilt [module graph pruning](https://go.dev/ref/mod#graph-pruning) \n\nAlso as much as I recommend `tools.go`, there's still some things it can be awkward with ie  `golangci-lint` tracked as a source dependency can lead to issues (dependency version clashes, Go version incompatibility), as well as it not being the recommended use case"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/09/epdtu",
  "client_id" : "https://indiepass.app/"
}
