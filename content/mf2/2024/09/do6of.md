{
  "date" : "2024-09-24T09:22:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-09-24T09:22:00+0100" ],
    "category" : [ "productivity" ],
    "like-of" : [ "https://notes.eatonphil.com/2024-09-23-be-someone-who-does-things.html" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/09/do6of",
  "tags" : [ "productivity" ],
  "client_id" : "https://indiepass.app/"
}
