{
  "date" : "2024-08-15T11:59:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A Gopher plushie (the gopher being the Go programming language's official mascot) with a little bowler hat, following the GopherConUK branding",
      "photo" : "https://media.jvt.me/0f817ad7da.jpeg"
    } ],
    "published" : [ "2024-08-15T11:59:00+0100" ],
    "category" : [ "conference", "gopher-con-uk" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Got myself a very cute and cool Gopher plushie at <a href=\"/tags/gopher-con-uk/\">#GopherConUK</a> today, and been having an interesting day so far 🤓"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/08/gqlch",
  "tags" : [ "conference", "gopher-con-uk" ],
  "client_id" : "https://indiepass.app/"
}
