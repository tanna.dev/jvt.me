{
  "date" : "2024-08-20T08:00:11.165973701Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8018" ],
    "start" : [ "2024-08-19T00:00:00Z" ],
    "end" : [ "2024-08-20T00:00:00Z" ],
    "published" : [ "2024-08-20T08:00:11.165973701Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/0ofl7",
  "client_id" : "https://www-api.jvt.me/fit"
}
