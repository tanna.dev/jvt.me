{
  "date" : "2024-08-14T08:00:09.832263395Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3787" ],
    "start" : [ "2024-08-13T00:00:00Z" ],
    "end" : [ "2024-08-14T00:00:00Z" ],
    "published" : [ "2024-08-14T08:00:09.832263395Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/fhyxu",
  "client_id" : "https://www-api.jvt.me/fit"
}
