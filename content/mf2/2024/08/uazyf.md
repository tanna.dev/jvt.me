{
  "date" : "2024-08-26T14:48:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@Rycaut/113024944852289076" ],
    "published" : [ "2024-08-26T14:48:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "@aaronparecki.com wrote [Meetable](https://github.com/aaronpk/Meetable) - [more info](https://aaronparecki.com/2020/01/14/13/meetable) - which isn't quite a replacement for the platform and it's social aspects, but is used in a few places including https://events.indieweb.org"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/08/uazyf",
  "client_id" : "https://indiepass.app/"
}
