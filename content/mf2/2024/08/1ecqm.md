{
  "date" : "2024-08-30T08:00:13.289718675Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5853" ],
    "start" : [ "2024-08-29T00:00:00Z" ],
    "end" : [ "2024-08-30T00:00:00Z" ],
    "published" : [ "2024-08-30T08:00:13.289718675Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/1ecqm",
  "client_id" : "https://www-api.jvt.me/fit"
}
