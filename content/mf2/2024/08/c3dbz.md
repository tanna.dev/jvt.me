{
  "date" : "2024-08-09T08:00:14.71631602Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4130" ],
    "start" : [ "2024-08-08T00:00:00Z" ],
    "end" : [ "2024-08-09T00:00:00Z" ],
    "published" : [ "2024-08-09T08:00:14.71631602Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/c3dbz",
  "client_id" : "https://www-api.jvt.me/fit"
}
