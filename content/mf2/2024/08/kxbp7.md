{
  "date" : "2024-08-28T08:00:10.151322784Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2865" ],
    "start" : [ "2024-08-27T00:00:00Z" ],
    "end" : [ "2024-08-28T00:00:00Z" ],
    "published" : [ "2024-08-28T08:00:10.151322784Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/kxbp7",
  "client_id" : "https://www-api.jvt.me/fit"
}
