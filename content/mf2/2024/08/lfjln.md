{
  "date" : "2024-08-24T08:00:13.24371812Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6283" ],
    "start" : [ "2024-08-23T00:00:00Z" ],
    "end" : [ "2024-08-24T00:00:00Z" ],
    "published" : [ "2024-08-24T08:00:13.24371812Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/lfjln",
  "client_id" : "https://www-api.jvt.me/fit"
}
