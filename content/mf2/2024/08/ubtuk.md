{
  "date" : "2024-08-06T08:00:12.437781409Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2479" ],
    "start" : [ "2024-08-05T00:00:00Z" ],
    "end" : [ "2024-08-06T00:00:00Z" ],
    "published" : [ "2024-08-06T08:00:12.437781409Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/ubtuk",
  "client_id" : "https://www-api.jvt.me/fit"
}
