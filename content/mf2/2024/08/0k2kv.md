{
  "date" : "2024-08-02T14:36:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-08-02T14:36:00+0100" ],
    "category" : [ "github", "security" ],
    "like-of" : [ "https://trufflesecurity.com/blog/anyone-can-access-deleted-and-private-repo-data-github" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/0k2kv",
  "tags" : [ "github", "security" ],
  "client_id" : "https://indiepass.app/"
}
