{
  "date" : "2024-08-25T22:07:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@anderseknert/113024351835976184" ],
    "published" : [ "2024-08-25T22:07:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "[`go-semantic-release`](https://github.com/go-semantic-release/semantic-release) has this same default but thankfully it's straightforward to set it so you can use 0.x releases.\n\nMaking it the default is IMO a very bad decision, as it teaches people that they should go to 1.x far too early.\n\nMy grumbling will result in a blog post sooner than later 👀"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/08/rfztm",
  "client_id" : "https://indiepass.app/"
}
