{
  "date" : "2024-08-29T08:00:12.258092701Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10231" ],
    "start" : [ "2024-08-28T00:00:00Z" ],
    "end" : [ "2024-08-29T00:00:00Z" ],
    "published" : [ "2024-08-29T08:00:12.258092701Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/gcm3b",
  "client_id" : "https://www-api.jvt.me/fit"
}
