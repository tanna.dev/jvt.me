{
  "date" : "2024-08-17T08:00:13.604638692Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9920" ],
    "start" : [ "2024-08-16T00:00:00Z" ],
    "end" : [ "2024-08-17T00:00:00Z" ],
    "published" : [ "2024-08-17T08:00:13.604638692Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/wgawg",
  "client_id" : "https://www-api.jvt.me/fit"
}
