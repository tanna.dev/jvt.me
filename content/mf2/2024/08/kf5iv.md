{
  "date" : "2024-08-01T08:00:17.684419083Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3076" ],
    "start" : [ "2024-07-31T00:00:00Z" ],
    "end" : [ "2024-08-01T00:00:00Z" ],
    "published" : [ "2024-08-01T08:00:17.684419083Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/kf5iv",
  "client_id" : "https://www-api.jvt.me/fit"
}
