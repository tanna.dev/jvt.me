{
  "date" : "2024-08-21T08:00:10.562603645Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2222" ],
    "start" : [ "2024-08-20T00:00:00Z" ],
    "end" : [ "2024-08-21T00:00:00Z" ],
    "published" : [ "2024-08-21T08:00:10.562603645Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/e5pbu",
  "client_id" : "https://www-api.jvt.me/fit"
}
