{
  "date" : "2024-08-26T08:00:10.251375873Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3353" ],
    "start" : [ "2024-08-25T00:00:00Z" ],
    "end" : [ "2024-08-26T00:00:00Z" ],
    "published" : [ "2024-08-26T08:00:10.251375873Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/n8o3j",
  "client_id" : "https://www-api.jvt.me/fit"
}
