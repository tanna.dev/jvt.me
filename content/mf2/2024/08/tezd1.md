{
  "date" : "2024-08-31T08:42:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-08-31T08:42:00+0100" ],
    "category" : [ "entrepreneurship" ],
    "like-of" : [ "https://writings.founderlabs.io/p/how-to-win-at-micro-saas" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/tezd1",
  "tags" : [ "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
