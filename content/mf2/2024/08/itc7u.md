{
  "date" : "2024-08-12T05:03:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-08-12T05:03:00+0100" ],
    "category" : [ "go", "github-actions" ],
    "like-of" : [ "https://brandur.org/fragments/go-version-matrix" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/itc7u",
  "tags" : [ "go", "github-actions" ],
  "client_id" : "https://indiepass.app/"
}
