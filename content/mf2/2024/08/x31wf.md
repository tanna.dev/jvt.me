{
  "date" : "2024-08-31T08:00:10.908713913Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6654" ],
    "start" : [ "2024-08-30T00:00:00Z" ],
    "end" : [ "2024-08-31T00:00:00Z" ],
    "published" : [ "2024-08-31T08:00:10.908713913Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/x31wf",
  "client_id" : "https://www-api.jvt.me/fit"
}
