{
  "date" : "2024-08-13T08:00:10.263928785Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3376" ],
    "start" : [ "2024-08-12T00:00:00Z" ],
    "end" : [ "2024-08-13T00:00:00Z" ],
    "published" : [ "2024-08-13T08:00:10.263928785Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/xytig",
  "client_id" : "https://www-api.jvt.me/fit"
}
