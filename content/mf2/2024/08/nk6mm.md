{
  "date" : "2024-08-25T19:19:10.510643157Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-08-25T19:19:10.510643157Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm super surprised that `semantic-release` [doesn't support v0.x.y](https://github.com/semantic-release/semantic-release/issues/1507) releases. Pretty disappointing, and reminds me I _really_ need to write a \"you should never release a v1 as your first release\" blog post"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/08/nk6mm",
  "client_id" : "https://editor.tanna.dev/"
}
