{
  "date" : "2024-08-19T08:00:13.400912323Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8820" ],
    "start" : [ "2024-08-18T00:00:00Z" ],
    "end" : [ "2024-08-19T00:00:00Z" ],
    "published" : [ "2024-08-19T08:00:13.400912323Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/8gq8b",
  "client_id" : "https://www-api.jvt.me/fit"
}
