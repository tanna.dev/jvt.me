{
  "date" : "2024-08-08T08:00:11.196572873Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3239" ],
    "start" : [ "2024-08-07T00:00:00Z" ],
    "end" : [ "2024-08-08T00:00:00Z" ],
    "published" : [ "2024-08-08T08:00:11.196572873Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/d7ltz",
  "client_id" : "https://www-api.jvt.me/fit"
}
