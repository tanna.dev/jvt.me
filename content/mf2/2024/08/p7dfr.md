{
  "date" : "2024-08-11T14:01:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-08-11T14:01:00+0100" ],
    "category" : [ "security", "totp" ],
    "like-of" : [ "https://shkspr.mobi/blog/2024/08/is-it-ok-to-share-2fa-secrets/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/p7dfr",
  "tags" : [ "security", "totp" ],
  "client_id" : "https://indiepass.app/"
}
