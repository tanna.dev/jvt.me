{
  "date" : "2024-08-02T07:49:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-08-02T07:49:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://github.com/sourcegraph/conc" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/fz6gv",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
