{
  "date" : "2024-08-03T16:23:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-08-03T16:23:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://josephwoodward.co.uk/2024/08/performance-improvements-unique-package-go-1-23" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/blz84",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
