{
  "date" : "2024-08-03T08:00:13.710895458Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5014" ],
    "start" : [ "2024-08-02T00:00:00Z" ],
    "end" : [ "2024-08-03T00:00:00Z" ],
    "published" : [ "2024-08-03T08:00:13.710895458Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/csvtr",
  "client_id" : "https://www-api.jvt.me/fit"
}
