{
  "date" : "2024-08-13T12:25:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@andreagrandi/112953749614499099" ],
    "published" : [ "2024-08-13T12:25:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "As was mentioned, Tidelift is generally pitched at bigger orgs, so I'd recommend reaching out to see if it's worthwhile. \n\n(I'm a Tidelift maintainer)\n\nThere's also things like https://stackaid.us or https://thanks.dev that may be easier to onboard to"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/08/qbxan",
  "client_id" : "https://indiepass.app/"
}
