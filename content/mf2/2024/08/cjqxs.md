{
  "date" : "2024-08-16T08:00:10.548132292Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5257" ],
    "start" : [ "2024-08-15T00:00:00Z" ],
    "end" : [ "2024-08-16T00:00:00Z" ],
    "published" : [ "2024-08-16T08:00:10.548132292Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/cjqxs",
  "client_id" : "https://www-api.jvt.me/fit"
}
