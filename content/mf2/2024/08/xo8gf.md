{
  "date" : "2024-08-14T07:51:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-08-14T07:51:00+0100" ],
    "category" : [ "bluesky", "bridgy-fed" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Now <a href=\"/tags/bridgy-fed/\">#BridgyFed</a> has support for federating BlueSky, hopefuly y'all should be able to follow [`www.jvt.me.web.brid.gy`](https://bsky.app/profile/www.jvt.me.web.brid.gy) to get my posts straight from the source 👀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/08/xo8gf",
  "tags" : [ "bluesky", "bridgy-fed" ],
  "client_id" : "https://indiepass.app/"
}
