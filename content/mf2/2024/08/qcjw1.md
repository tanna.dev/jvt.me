{
  "date" : "2024-08-02T08:00:14.737187953Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3757" ],
    "start" : [ "2024-08-01T00:00:00Z" ],
    "end" : [ "2024-08-02T00:00:00Z" ],
    "published" : [ "2024-08-02T08:00:14.737187953Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/qcjw1",
  "client_id" : "https://www-api.jvt.me/fit"
}
