{
  "date" : "2024-08-23T08:00:15.533827473Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6729" ],
    "start" : [ "2024-08-22T00:00:00Z" ],
    "end" : [ "2024-08-23T00:00:00Z" ],
    "published" : [ "2024-08-23T08:00:15.533827473Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/ey4fm",
  "client_id" : "https://www-api.jvt.me/fit"
}
