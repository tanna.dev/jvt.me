{
  "date" : "2024-08-18T08:00:11.106387774Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8087" ],
    "start" : [ "2024-08-17T00:00:00Z" ],
    "end" : [ "2024-08-18T00:00:00Z" ],
    "published" : [ "2024-08-18T08:00:11.106387774Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/eswk0",
  "client_id" : "https://www-api.jvt.me/fit"
}
