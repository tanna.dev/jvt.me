{
  "date" : "2024-08-04T16:51:04.723198492Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-08-04T16:51:04.723198492Z" ],
    "category" : [ "neovim" ],
    "like-of" : [ "https://github.com/norcalli/nvim-colorizer.lua" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/08/r1w7v",
  "tags" : [ "neovim" ],
  "client_id" : "https://editor.tanna.dev/"
}
