{
  "date" : "2024-08-26T10:27:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-08-26T10:27:00+0100" ],
    "category" : [ "versioning" ],
    "bookmark-of" : [ "https://twitchard.github.io/posts/2024-08-23-breaking-changes.html" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2024/08/yimtf",
  "tags" : [ "versioning" ],
  "client_id" : "https://indiepass.app/"
}
