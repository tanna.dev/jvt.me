{
  "date" : "2024-08-04T08:00:14.26595532Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6849" ],
    "start" : [ "2024-08-03T00:00:00Z" ],
    "end" : [ "2024-08-04T00:00:00Z" ],
    "published" : [ "2024-08-04T08:00:14.26595532Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/08/y2giz",
  "client_id" : "https://www-api.jvt.me/fit"
}
