{
  "date" : "2024-08-01T15:14:26.015217192Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-08-01T15:14:26.015217192Z" ],
    "category" : [ "open-api" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very excited that in a couple of hours I'm on my first episode of @gotime@changelog.social to chat about [`oapi-codegen`](https://github.com/deepmap/oapi-codegen), <a href=\"/tags/open-api/\">#OpenAPI</a>, Open Source maintenance and Go!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/08/dq74u",
  "tags" : [ "open-api" ],
  "client_id" : "https://editor.tanna.dev/"
}
