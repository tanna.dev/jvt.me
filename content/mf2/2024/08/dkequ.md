{
  "date" : "2024-08-27T12:17:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@anderseknert/113032872416484692" ],
    "published" : [ "2024-08-27T12:17:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thanks! Wondering if it'll get [more traction on Hacker News](https://news.ycombinator.com/item?id=41365048) or whether it's maybe a bit wordy 😝"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/08/dkequ",
  "client_id" : "https://indiepass.app/"
}
