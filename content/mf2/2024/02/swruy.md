{
  "date" : "2024-02-27T11:30:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@lars/112002531907785063" ],
    "published" : [ "2024-02-27T11:30:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Broken link? \n\nBut I would say yes it is IndieWeb, you don't have to own everything down to the bare metal you're using to be indieweb or IndieWeb. It's unhelpful to gatekeep some of these things, especially when it can be hard enough to get folks to chuck stuff onto Netlify let alone learn how to deploy servers"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/02/swruy",
  "client_id" : "https://indiepass.app/"
}
