{
  "date" : "2024-02-16T08:00:10.609515085Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3595" ],
    "start" : [ "2024-02-15T00:00:00Z" ],
    "end" : [ "2024-02-16T00:00:00Z" ],
    "published" : [ "2024-02-16T08:00:10.609515085Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/q7r4h",
  "client_id" : "https://www-api.jvt.me/fit"
}
