{
  "date" : "2024-02-22T08:00:10.574469384Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7984" ],
    "start" : [ "2024-02-21T00:00:00Z" ],
    "end" : [ "2024-02-22T00:00:00Z" ],
    "published" : [ "2024-02-22T08:00:10.574469384Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/llq3p",
  "client_id" : "https://www-api.jvt.me/fit"
}
