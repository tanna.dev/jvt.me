{
  "date" : "2024-02-25T08:00:11.561969304Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3774" ],
    "start" : [ "2024-02-24T00:00:00Z" ],
    "end" : [ "2024-02-25T00:00:00Z" ],
    "published" : [ "2024-02-25T08:00:11.561969304Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/zyora",
  "client_id" : "https://www-api.jvt.me/fit"
}
