{
  "date" : "2024-02-23T14:38:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-23T14:38:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Whenever I see a profile view on LinkedIn from someone who works at a company I used to work at, I always wonder what it means. Like, are you interested in who was in the `git blame`? Did you find something I've done, or heard a story about me and want to know more?\n\nI guess we'll never know 🤷🏽‍♂️"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/tnqeq",
  "client_id" : "https://indiepass.app/"
}
