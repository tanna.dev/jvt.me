{
  "date" : "2024-02-21T08:01:11.444823135Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3095" ],
    "start" : [ "2024-02-20T00:00:00Z" ],
    "end" : [ "2024-02-21T00:00:00Z" ],
    "published" : [ "2024-02-21T08:01:11.444823135Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/vbmrs",
  "client_id" : "https://www-api.jvt.me/fit"
}
