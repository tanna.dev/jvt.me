{
  "date" : "2024-02-26T16:39:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-26T16:39:00+0000" ],
    "category" : [ "go", "command-line" ],
    "like-of" : [ "https://blog.carlana.net/post/2020/go-cli-how-to-and-advice/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/rpmtj",
  "tags" : [ "go", "command-line" ],
  "client_id" : "https://indiepass.app/"
}
