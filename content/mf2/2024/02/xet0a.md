{
  "date" : "2024-02-27T08:00:10.781868191Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9146" ],
    "start" : [ "2024-02-26T00:00:00Z" ],
    "end" : [ "2024-02-27T00:00:00Z" ],
    "published" : [ "2024-02-27T08:00:10.781868191Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/xet0a",
  "client_id" : "https://www-api.jvt.me/fit"
}
