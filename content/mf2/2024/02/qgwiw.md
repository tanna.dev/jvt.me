{
  "date" : "2024-02-02T08:00:10.226251524Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6130" ],
    "start" : [ "2024-02-01T00:00:00Z" ],
    "end" : [ "2024-02-02T00:00:00Z" ],
    "published" : [ "2024-02-02T08:00:10.226251524Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/qgwiw",
  "client_id" : "https://www-api.jvt.me/fit"
}
