{
  "date" : "2024-02-26T16:10:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-26T16:10:00+0000" ],
    "category" : [ "open-source", "soocon24" ],
    "like-of" : [ "https://www.computing.co.uk/news/4171258/open-source-originator-failed" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/6zpkk",
  "tags" : [ "open-source", "soocon24" ],
  "client_id" : "https://indiepass.app/"
}
