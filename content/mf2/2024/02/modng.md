{
  "date" : "2024-02-24T08:00:11.029835787Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7588" ],
    "start" : [ "2024-02-23T00:00:00Z" ],
    "end" : [ "2024-02-24T00:00:00Z" ],
    "published" : [ "2024-02-24T08:00:11.029835787Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/modng",
  "client_id" : "https://www-api.jvt.me/fit"
}
