{
  "date" : "2024-02-19T22:20:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://glyphy.com/n/2024/wn-20240418-fresh-start/" ],
    "published" : [ "2024-02-19T22:20:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Excited to hear how you find the rest of _Loki_ 👀"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/02/hwzbw",
  "client_id" : "https://indiepass.app/"
}
