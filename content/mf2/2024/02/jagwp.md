{
  "date" : "2024-02-18T08:00:11.138369662Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7339" ],
    "start" : [ "2024-02-17T00:00:00Z" ],
    "end" : [ "2024-02-18T00:00:00Z" ],
    "published" : [ "2024-02-18T08:00:11.138369662Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/jagwp",
  "client_id" : "https://www-api.jvt.me/fit"
}
