{
  "date" : "2024-02-27T15:07:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@lars/112003155992795933" ],
    "published" : [ "2024-02-27T15:07:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "That's good they'll cover it - my website ended up getting rather expensive $20-$80/mo for bandwidth and builds so I moved it over to AWS instead 🙃"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/02/ygbj7",
  "client_id" : "https://indiepass.app/"
}
