{
  "date" : "2024-02-19T08:00:11.485284586Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9036" ],
    "start" : [ "2024-02-18T00:00:00Z" ],
    "end" : [ "2024-02-19T00:00:00Z" ],
    "published" : [ "2024-02-19T08:00:11.485284586Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/hcp8n",
  "client_id" : "https://www-api.jvt.me/fit"
}
