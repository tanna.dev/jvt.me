{
  "date" : "2024-02-08T08:00:10.744271331Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8746" ],
    "start" : [ "2024-02-07T00:00:00Z" ],
    "end" : [ "2024-02-08T00:00:00Z" ],
    "published" : [ "2024-02-08T08:00:10.744271331Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/n7y75",
  "client_id" : "https://www-api.jvt.me/fit"
}
