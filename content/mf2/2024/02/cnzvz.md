{
  "date" : "2024-02-15T10:04:19.186174061Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-15T10:04:19.186174061Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "If you're able to see this post (on the Fediverse) yay! That means your admin hasn't blocked [Bridgy Fed](https://fed.brid.gy) which I use to bridge my website with the Fediverse so I can chat to y'all.\r\n\r\nThis is likely due to recent discussion around the upcoming BlueSky bridge and [opt-out being the default decision](http://github.com/snarfed/bridgy-fed/issues/835).\r\n\r\nI don't dispute the freedom or choice to block Bridgy, and am definitely taking some time to think about how I feel about the varying thoughts, but the main thing is that it looks like several admins have blocked Bridgy altogether, resulting in not just the blocking of the upcoming BlueSky bridge (at a separate domain under `brid.gy`) but also classes Bridgy as `Tier 0`:\r\n\r\n> Tier 0 is a combined blocklist of only the worst actors, and it exists to provide one blocklist to which surely no one can object as a baseline for others. It's the perfect starting list for any new mastodon admin.\r\n\r\nSo it could be my time interacting with the Fediverse is going to be cut short, and I'll be screaming into the void very much moreso 😅"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/cnzvz",
  "client_id" : "https://editor.tanna.dev/"
}
