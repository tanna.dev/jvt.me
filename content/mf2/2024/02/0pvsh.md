{
  "date" : "2024-02-04T08:00:11.536372459Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4326" ],
    "start" : [ "2024-02-03T00:00:00Z" ],
    "end" : [ "2024-02-04T00:00:00Z" ],
    "published" : [ "2024-02-04T08:00:11.536372459Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/0pvsh",
  "client_id" : "https://www-api.jvt.me/fit"
}
