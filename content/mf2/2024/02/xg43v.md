{
  "date" : "2024-02-24T17:18:57.190095587Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-24T17:18:57.190095587Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "This is your regularly scheduled reminder to update your CV - you don't need to be looking for a job, and it can be a great way to remind yourself of the great stuff you're doing - a la [brag document](https://jvns.ca/blog/brag-documents/).\r\n\r\nI updated [mine](https://hire.jvt.me) ahead of [appearing on Changelog + Friends](https://changelog.com/friends/31) and I recommend you think about doing yours!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/xg43v",
  "client_id" : "https://editor.tanna.dev/"
}
