{
  "date" : "2024-02-23T08:00:10.056435101Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3911" ],
    "start" : [ "2024-02-22T00:00:00Z" ],
    "end" : [ "2024-02-23T00:00:00Z" ],
    "published" : [ "2024-02-23T08:00:10.056435101Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/kdlqk",
  "client_id" : "https://www-api.jvt.me/fit"
}
