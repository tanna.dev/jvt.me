{
  "date" : "2024-02-20T08:00:10.933154336Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3277" ],
    "start" : [ "2024-02-19T00:00:00Z" ],
    "end" : [ "2024-02-20T00:00:00Z" ],
    "published" : [ "2024-02-20T08:00:10.933154336Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/phioj",
  "client_id" : "https://www-api.jvt.me/fit"
}
