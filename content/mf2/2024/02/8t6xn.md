{
  "date" : "2024-02-19T09:21:53.855429696Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-19T09:21:53.855429696Z" ],
    "category" : [ "nottingham","public-speaking" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "If you're in <a href=\"/tags/nottingham/\">#Nottingham</a> and fancy getting up _super_ early on Wednesday, I'm [talking at Notts TechFast](https://www.meetup.com/notts-techfast/events/299079531/) about [dependency-management-data](https://dmd.tanna.dev) and how you can better understand how much your organisation relies on Open Source and internal dependencies - would be great to see you there!\r\n\r\nAnd for those who are daunted by getting into town at that time (I know I am 😹) there's also a livestream!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/8t6xn",
  "tags" : [ "nottingham","public-speaking" ],
  "client_id" : "https://editor.tanna.dev/"
}
