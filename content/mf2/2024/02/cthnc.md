{
  "date" : "2024-02-18T18:32:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@anderseknert/111847179538423023" ],
    "published" : [ "2024-02-18T18:32:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Sorry for the delay on this, I've also just [spoken about it on a podcast](https://changelog.com/friends/31) - lemme know if you've got any other questions, very happy to chat more!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/02/cthnc",
  "client_id" : "https://indiepass.app/"
}
