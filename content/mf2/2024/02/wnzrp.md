{
  "date" : "2024-02-06T10:05:43.500953108Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-06T10:05:43.500953108Z" ],
    "category" : [ "state-of-open-con", "soocon24" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "If you were interested in what <span class=\"h-card\"><a class=\"u-url\" href=\"https://sarahnovotny.com/\">Sarah Novotny</a></span> mentioned at <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> <a href=\"/tags/soocon24/\">#SOOCon24</a> about understanding how much Open Source you use, as well as some of the risks of using it, y'all may be _very_ interested to come to [my talk](https://stateofopencon2024.sched.com/event/1Xl38/) about [dependency-management-data](https://dmd.tanna.dev) which aims to give you exactly this, with just Free and Open Source and Open APIs 👀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/wnzrp",
  "tags" : [ "state-of-open-con", "soocon24" ],
  "client_id" : "https://editor.tanna.dev/"
}
