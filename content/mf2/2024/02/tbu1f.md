{
  "date" : "2024-02-17T10:30:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-17T10:30:00+0000" ],
    "category" : [ "interviewing", "recruitment", "open-source" ],
    "like-of" : [ "https://fossfox.com/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/tbu1f",
  "tags" : [ "interviewing", "recruitment", "open-source" ],
  "client_id" : "https://indiepass.app/"
}
