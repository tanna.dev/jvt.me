{
  "date" : "2024-02-24T12:20:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-02-24T12:20:00+0000" ],
    "category" : [ "go" ],
    "like-of" : [ "https://github.com/dmarkham/enumer" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/5piyj",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
