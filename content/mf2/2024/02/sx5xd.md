{
  "date" : "2024-02-17T08:00:10.048252812Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7752" ],
    "start" : [ "2024-02-16T00:00:00Z" ],
    "end" : [ "2024-02-17T00:00:00Z" ],
    "published" : [ "2024-02-17T08:00:10.048252812Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/sx5xd",
  "client_id" : "https://www-api.jvt.me/fit"
}
