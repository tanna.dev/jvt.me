{
  "date" : "2024-02-22T13:06:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-22T13:06:00+0000" ],
    "category" : [ "borderlands" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "First found out that the <a href=\"/tags/borderlands/\">#Borderlands</a> games are being made into a film - via [a trailer that's just dropped](https://www.youtube.com/watch?v=lU_NKNZljoQ) - and I'm not as excited as I was before I watched the trailer 😬"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/1axaf",
  "tags" : [ "borderlands" ],
  "client_id" : "https://indiepass.app/"
}
