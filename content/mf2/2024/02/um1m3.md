{
  "date" : "2024-02-17T20:49:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@shortridge/111948693098251737" ],
    "published" : [ "2024-02-17T20:49:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "My biggest nerd thing right now - and slight hyperfixation - is [a way to take your dependency tree and understand more about it](https://dmd.tanna.dev) ie security issues, supply chain hygiene concerns, unmaintained dependencies, as well as being able to ask \"what versions of Terraform modules are we using\" or \"how many [libyears](https://libyear.com) behind are we on updates\""
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/02/um1m3",
  "client_id" : "https://indiepass.app/"
}
