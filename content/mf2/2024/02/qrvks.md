{
  "date" : "2024-02-28T08:00:10.343059007Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8813" ],
    "start" : [ "2024-02-27T00:00:00Z" ],
    "end" : [ "2024-02-28T00:00:00Z" ],
    "published" : [ "2024-02-28T08:00:10.343059007Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/qrvks",
  "client_id" : "https://www-api.jvt.me/fit"
}
