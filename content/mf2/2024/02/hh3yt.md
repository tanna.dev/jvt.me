{
  "date" : "2024-02-26T16:48:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-26T16:48:00+0000" ],
    "category" : [ "open-source", "caddy" ],
    "like-of" : [ "https://caddy.community/t/the-realities-of-being-a-foss-maintainer/2728?u=matt&_escaped_fragment_=" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/hh3yt",
  "tags" : [ "open-source", "caddy" ],
  "client_id" : "https://indiepass.app/"
}
