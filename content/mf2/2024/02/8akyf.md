{
  "date" : "2024-02-01T08:00:09.943909779Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10024" ],
    "start" : [ "2024-01-31T00:00:00Z" ],
    "end" : [ "2024-02-01T00:00:00Z" ],
    "published" : [ "2024-02-01T08:00:09.943909779Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/8akyf",
  "client_id" : "https://www-api.jvt.me/fit"
}
