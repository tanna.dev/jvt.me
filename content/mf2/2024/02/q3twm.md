{
  "date" : "2024-02-26T08:00:10.364822658Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7628" ],
    "start" : [ "2024-02-25T00:00:00Z" ],
    "end" : [ "2024-02-26T00:00:00Z" ],
    "published" : [ "2024-02-26T08:00:10.364822658Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/q3twm",
  "client_id" : "https://www-api.jvt.me/fit"
}
