{
  "date" : "2024-06-18T08:00:09.9790903Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4657" ],
    "start" : [ "2024-06-17T00:00:00Z" ],
    "end" : [ "2024-06-18T00:00:00Z" ],
    "published" : [ "2024-06-18T08:00:09.9790903Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/ozmkm",
  "client_id" : "https://www-api.jvt.me/fit"
}
