{
  "date" : "2024-06-24T08:00:10.188207134Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6738" ],
    "start" : [ "2024-06-23T00:00:00Z" ],
    "end" : [ "2024-06-24T00:00:00Z" ],
    "published" : [ "2024-06-24T08:00:10.188207134Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/c0nm9",
  "client_id" : "https://www-api.jvt.me/fit"
}
