{
  "date" : "2024-06-23T08:00:12.187170608Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6315" ],
    "start" : [ "2024-06-22T00:00:00Z" ],
    "end" : [ "2024-06-23T00:00:00Z" ],
    "published" : [ "2024-06-23T08:00:12.187170608Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/odond",
  "client_id" : "https://www-api.jvt.me/fit"
}
