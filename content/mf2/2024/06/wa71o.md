{
  "date" : "2024-06-04T08:00:10.406996481Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6010" ],
    "start" : [ "2024-06-03T00:00:00Z" ],
    "end" : [ "2024-06-04T00:00:00Z" ],
    "published" : [ "2024-06-04T08:00:10.406996481Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/wa71o",
  "client_id" : "https://www-api.jvt.me/fit"
}
