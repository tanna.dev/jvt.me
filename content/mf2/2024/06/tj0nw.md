{
  "date" : "2024-06-09T08:00:10.144340197Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6722" ],
    "start" : [ "2024-06-08T00:00:00Z" ],
    "end" : [ "2024-06-09T00:00:00Z" ],
    "published" : [ "2024-06-09T08:00:10.144340197Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/tj0nw",
  "client_id" : "https://www-api.jvt.me/fit"
}
