{
  "date" : "2024-06-22T08:00:09.89560465Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4203" ],
    "start" : [ "2024-06-21T00:00:00Z" ],
    "end" : [ "2024-06-22T00:00:00Z" ],
    "published" : [ "2024-06-22T08:00:09.89560465Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/ubsbb",
  "client_id" : "https://www-api.jvt.me/fit"
}
