{
  "date" : "2024-06-12T08:32:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-06-12T08:32:00+0100" ],
    "category" : [ "github", "performance" ],
    "like-of" : [ "https://github.blog/2024-06-11-how-we-improved-push-processing-on-github/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/06/gzrgh",
  "tags" : [ "github", "performance" ],
  "client_id" : "https://indiepass.app/"
}
