{
  "date" : "2024-06-10T09:13:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/devops-notts/events/301554693" ],
    "published" : [ "2024-06-10T09:13:00+0100" ],
    "category" : [ "jenkins" ],
    "post-status" : [ "published" ],
    "rsvp" : [ "no" ],
    "content" : [ {
      "html" : "",
      "value" : "Unfortunately gonna be missing this (due to speaking at Digital Lincoln) but I'm sure this will be interesting. <a href=\"/tags/jenkins/\">#Jenkins</a> is a great piece of software, the problems that many folks see with it are how _their organisation uses it_ not the underlying software that's built by awesome people"
    } ]
  },
  "kind" : "rsvps",
  "slug" : "2024/06/jzgmh",
  "tags" : [ "jenkins" ],
  "client_id" : "https://indiepass.app/"
}
