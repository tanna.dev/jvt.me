{
  "date" : "2024-06-04T09:00:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-06-04T09:00:00+0100" ],
    "category" : [ "go", "openapi", "oapi-codegen" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "New release of <a href=\"/tags/oapi-codegen/\">#oapi-codegen</a> is out 🚀 \r\n\r\nBig changes are:\r\n\r\n- Support for Go 1.22+ `net/http` enhanced routing \r\n- Significant documentation rewrite and adding a JSON schema for configuration file autocomplete/validation\r\n\r\nAnd a whole host of other changes, check out the full release notes at https://github.com/deepmap/oapi-codegen/releases/tag/v2.2.0"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/06/chelp",
  "tags" : [ "go", "openapi", "oapi-codegen" ],
  "client_id" : "https://indiepass.app/"
}
