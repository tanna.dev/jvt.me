{
  "date" : "2024-06-30T12:01:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-06-30T12:01:00+0100" ],
    "category" : [ "product", "entrepreneurship" ],
    "like-of" : [ "https://www.granola.so/blog/how-to-evolve-a-product" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/06/hohpb",
  "tags" : [ "product", "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
