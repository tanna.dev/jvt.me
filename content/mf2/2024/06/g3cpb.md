{
  "date" : "2024-06-04T11:42:11.629191204Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-06-04T11:42:11.629191204Z" ],
    "category" : [ "neovim", "sql" ],
    "like-of" : [ "https://github.com/nanotee/sqls.nvim" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/06/g3cpb",
  "tags" : [ "neovim", "sql" ],
  "client_id" : "https://editor.tanna.dev/"
}
