{
  "date" : "2024-06-19T08:00:10.182893596Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5188" ],
    "start" : [ "2024-06-18T00:00:00Z" ],
    "end" : [ "2024-06-19T00:00:00Z" ],
    "published" : [ "2024-06-19T08:00:10.182893596Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/83kpk",
  "client_id" : "https://www-api.jvt.me/fit"
}
