{
  "date" : "2024-06-28T08:00:12.384750185Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4915" ],
    "start" : [ "2024-06-27T00:00:00Z" ],
    "end" : [ "2024-06-28T00:00:00Z" ],
    "published" : [ "2024-06-28T08:00:12.384750185Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/mft9n",
  "client_id" : "https://www-api.jvt.me/fit"
}
