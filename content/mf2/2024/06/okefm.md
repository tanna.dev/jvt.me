{
  "date" : "2024-06-25T08:00:10.146295684Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "1846" ],
    "start" : [ "2024-06-24T00:00:00Z" ],
    "end" : [ "2024-06-25T00:00:00Z" ],
    "published" : [ "2024-06-25T08:00:10.146295684Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/okefm",
  "client_id" : "https://www-api.jvt.me/fit"
}
