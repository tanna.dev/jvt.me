{
  "date" : "2024-06-11T08:00:10.280342881Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5036" ],
    "start" : [ "2024-06-10T00:00:00Z" ],
    "end" : [ "2024-06-11T00:00:00Z" ],
    "published" : [ "2024-06-11T08:00:10.280342881Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/ge0rn",
  "client_id" : "https://www-api.jvt.me/fit"
}
