{
  "date" : "2024-06-16T08:00:12.574870249Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3088" ],
    "start" : [ "2024-06-15T00:00:00Z" ],
    "end" : [ "2024-06-16T00:00:00Z" ],
    "published" : [ "2024-06-16T08:00:12.574870249Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/plx8k",
  "client_id" : "https://www-api.jvt.me/fit"
}
