{
  "date" : "2024-06-11T17:14:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-06-11T17:14:00+0100" ],
    "category" : [ "entrepreneurship" ],
    "like-of" : [ "https://mbuffett.com/posts/maintaining-motivation/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/06/w9qw2",
  "tags" : [ "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
