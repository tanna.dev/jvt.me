{
  "date" : "2024-06-13T08:00:12.636235641Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8055" ],
    "start" : [ "2024-06-12T00:00:00Z" ],
    "end" : [ "2024-06-13T00:00:00Z" ],
    "published" : [ "2024-06-13T08:00:12.636235641Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/74cbo",
  "client_id" : "https://www-api.jvt.me/fit"
}
