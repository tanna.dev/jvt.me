{
  "date" : "2024-06-12T08:00:10.71857124Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6719" ],
    "start" : [ "2024-06-11T00:00:00Z" ],
    "end" : [ "2024-06-12T00:00:00Z" ],
    "published" : [ "2024-06-12T08:00:10.71857124Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/iuxpf",
  "client_id" : "https://www-api.jvt.me/fit"
}
