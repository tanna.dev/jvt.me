{
  "date" : "2024-06-03T08:00:10.222051822Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4689" ],
    "start" : [ "2024-06-02T00:00:00Z" ],
    "end" : [ "2024-06-03T00:00:00Z" ],
    "published" : [ "2024-06-03T08:00:10.222051822Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/ib42f",
  "client_id" : "https://www-api.jvt.me/fit"
}
