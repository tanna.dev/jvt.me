{
  "date" : "2024-06-30T08:00:12.573200384Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6139" ],
    "start" : [ "2024-06-29T00:00:00Z" ],
    "end" : [ "2024-06-30T00:00:00Z" ],
    "published" : [ "2024-06-30T08:00:12.573200384Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/x1o5c",
  "client_id" : "https://www-api.jvt.me/fit"
}
