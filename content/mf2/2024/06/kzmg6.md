{
  "date" : "2024-06-13T08:08:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@byjp/112607929948489238" ],
    "published" : [ "2024-06-13T08:08:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I remember [hearing about](https://share.transistor.fm/s/f07084ef) that proposal and that it sounded tough to do - sorry you're having to deal with it 😅\n\nhttps://github.com/mholt/archiver came to mind, but looks like it doesn't support the encryption side of things. Can you use one of the options from the proposal's thread?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/06/kzmg6",
  "client_id" : "https://indiepass.app/"
}
