{
  "date" : "2024-06-10T08:00:10.006419235Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8752" ],
    "start" : [ "2024-06-09T00:00:00Z" ],
    "end" : [ "2024-06-10T00:00:00Z" ],
    "published" : [ "2024-06-10T08:00:10.006419235Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/6pxky",
  "client_id" : "https://www-api.jvt.me/fit"
}
