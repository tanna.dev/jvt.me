{
  "date" : "2024-06-15T08:00:12.597249683Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5479" ],
    "start" : [ "2024-06-14T00:00:00Z" ],
    "end" : [ "2024-06-15T00:00:00Z" ],
    "published" : [ "2024-06-15T08:00:12.597249683Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/1hhez",
  "client_id" : "https://www-api.jvt.me/fit"
}
