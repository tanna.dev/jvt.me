{
  "date" : "2024-06-02T10:32:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-06-02T10:32:00+0100" ],
    "category" : [ "go", "architecture" ],
    "like-of" : [ "https://github.com/fe3dback/go-arch-lint" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/06/grmgl",
  "tags" : [ "go", "architecture" ],
  "client_id" : "https://indiepass.app/"
}
