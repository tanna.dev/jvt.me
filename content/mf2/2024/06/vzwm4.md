{
  "date" : "2024-06-14T22:33:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-06-14T22:33:00+0100" ],
    "category" : [ "open-source", "entrepreneurship" ],
    "like-of" : [ "https://yaak.app/blog/why-not-open-source" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/06/vzwm4",
  "tags" : [ "open-source", "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
