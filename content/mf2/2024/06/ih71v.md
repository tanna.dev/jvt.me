{
  "date" : "2024-06-27T08:00:13.011117366Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2675" ],
    "start" : [ "2024-06-26T00:00:00Z" ],
    "end" : [ "2024-06-27T00:00:00Z" ],
    "published" : [ "2024-06-27T08:00:13.011117366Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/ih71v",
  "client_id" : "https://www-api.jvt.me/fit"
}
