{
  "date" : "2024-06-07T08:00:10.253766435Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6743" ],
    "start" : [ "2024-06-06T00:00:00Z" ],
    "end" : [ "2024-06-07T00:00:00Z" ],
    "published" : [ "2024-06-07T08:00:10.253766435Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/opy8b",
  "client_id" : "https://www-api.jvt.me/fit"
}
