{
  "date" : "2024-06-17T08:00:10.222355771Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8661" ],
    "start" : [ "2024-06-16T00:00:00Z" ],
    "end" : [ "2024-06-17T00:00:00Z" ],
    "published" : [ "2024-06-17T08:00:10.222355771Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/dz8uc",
  "client_id" : "https://www-api.jvt.me/fit"
}
