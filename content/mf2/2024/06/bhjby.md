{
  "date" : "2024-06-02T08:00:10.543409339Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "15986" ],
    "start" : [ "2024-06-01T00:00:00Z" ],
    "end" : [ "2024-06-02T00:00:00Z" ],
    "published" : [ "2024-06-02T08:00:10.543409339Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/bhjby",
  "client_id" : "https://www-api.jvt.me/fit"
}
