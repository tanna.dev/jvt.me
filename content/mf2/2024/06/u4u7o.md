{
  "date" : "2024-06-29T21:58:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://k8s.social/@Marcus/112698615621823868" ],
    "published" : [ "2024-06-29T21:58:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Awesome! I've already used it to refactor some gnarly regexes with safety, so it's already been a worthwhile investment 😁"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/06/u4u7o",
  "client_id" : "https://indiepass.app/"
}
