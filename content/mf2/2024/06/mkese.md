{
  "date" : "2024-06-01T08:00:10.040842884Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7322" ],
    "start" : [ "2024-05-31T00:00:00Z" ],
    "end" : [ "2024-06-01T00:00:00Z" ],
    "published" : [ "2024-06-01T08:00:10.040842884Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/mkese",
  "client_id" : "https://www-api.jvt.me/fit"
}
