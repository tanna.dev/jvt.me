{
  "date" : "2024-06-05T08:00:10.376736392Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4860" ],
    "start" : [ "2024-06-04T00:00:00Z" ],
    "end" : [ "2024-06-05T00:00:00Z" ],
    "published" : [ "2024-06-05T08:00:10.376736392Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/06/yby4p",
  "client_id" : "https://www-api.jvt.me/fit"
}
