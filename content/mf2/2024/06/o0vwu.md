{
  "date" : "2024-06-22T10:07:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-06-22T10:07:00+0100" ],
    "category" : [ "writing" ],
    "repost-of" : [ "https://fedi.simonwillison.net/@simon/112657932893424962" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "+1 - in the past I've had it recommended to not inline links and instead include them as footnotes, and I do not like that recommendation 🙃 I want the post to be rich with the context (where appropriate) and it to be linked naturally from what I'm talking about. \n\nSome folks don't like that, that's fine! If it ends up sending someone on a side quest while they're trying to read my post, that's also OK!"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2024/06/o0vwu",
  "tags" : [ "writing" ],
  "client_id" : "https://indiepass.app/"
}
