{
  "date" : "2024-12-05T08:00:14.471053845Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3618" ],
    "start" : [ "2024-12-04T00:00:00Z" ],
    "end" : [ "2024-12-05T00:00:00Z" ],
    "published" : [ "2024-12-05T08:00:14.471053845Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/x1eu8",
  "client_id" : "https://www-api.jvt.me/fit"
}
