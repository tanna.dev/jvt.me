{
  "date" : "2024-12-24T10:32:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-24T10:32:00+0000" ],
    "category" : [ "command-line" ],
    "like-of" : [ "https://www.atlassian.com/blog/it-teams/10-design-principles-for-delightful-clis" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/12/8hu1n",
  "tags" : [ "command-line" ],
  "client_id" : "https://indiepass.app/"
}
