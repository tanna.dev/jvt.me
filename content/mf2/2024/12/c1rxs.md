{
  "date" : "2024-12-06T08:00:15.444327882Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8405" ],
    "start" : [ "2024-12-05T00:00:00Z" ],
    "end" : [ "2024-12-06T00:00:00Z" ],
    "published" : [ "2024-12-06T08:00:15.444327882Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/c1rxs",
  "client_id" : "https://www-api.jvt.me/fit"
}
