{
  "date" : "2024-12-24T16:38:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-24T16:38:00+0000" ],
    "category" : [ "conference", "public-speaking", "call-for-papers" ],
    "repost-of" : [ "https://mastodon.social/@rowdy/113708353070576197" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/12/q6e5u",
  "tags" : [ "conference", "public-speaking", "call-for-papers" ],
  "client_id" : "https://indiepass.app/"
}
