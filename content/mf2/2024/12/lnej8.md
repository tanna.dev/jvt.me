{
  "date" : "2024-12-05T10:30:00.000Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://changelog.com/podcast/620" ],
    "published" : [ "2024-12-05T10:30:00.000Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/12/lnej8",
  "client_id" : "https://editor.tanna.dev/"
}
