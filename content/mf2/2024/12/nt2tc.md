{
  "date" : "2024-12-12T08:00:15.573194833Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10699" ],
    "start" : [ "2024-12-11T00:00:00Z" ],
    "end" : [ "2024-12-12T00:00:00Z" ],
    "published" : [ "2024-12-12T08:00:15.573194833Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/nt2tc",
  "client_id" : "https://www-api.jvt.me/fit"
}
