{
  "date" : "2024-12-17T08:00:17.706904389Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8041" ],
    "start" : [ "2024-12-16T00:00:00Z" ],
    "end" : [ "2024-12-17T00:00:00Z" ],
    "published" : [ "2024-12-17T08:00:17.706904389Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/w2pyy",
  "client_id" : "https://www-api.jvt.me/fit"
}
