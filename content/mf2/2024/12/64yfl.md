{
  "date" : "2024-12-22T08:00:16.532545269Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "13471" ],
    "start" : [ "2024-12-21T00:00:00Z" ],
    "end" : [ "2024-12-22T00:00:00Z" ],
    "published" : [ "2024-12-22T08:00:16.532545269Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/64yfl",
  "client_id" : "https://www-api.jvt.me/fit"
}
