{
  "date" : "2024-12-03T08:00:13.203642443Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2668" ],
    "start" : [ "2024-12-02T00:00:00Z" ],
    "end" : [ "2024-12-03T00:00:00Z" ],
    "published" : [ "2024-12-03T08:00:13.203642443Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/yqb6k",
  "client_id" : "https://www-api.jvt.me/fit"
}
