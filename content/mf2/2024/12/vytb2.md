{
  "date" : "2024-12-02T08:00:17.099158438Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6373" ],
    "start" : [ "2024-12-01T00:00:00Z" ],
    "end" : [ "2024-12-02T00:00:00Z" ],
    "published" : [ "2024-12-02T08:00:17.099158438Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/vytb2",
  "client_id" : "https://www-api.jvt.me/fit"
}
