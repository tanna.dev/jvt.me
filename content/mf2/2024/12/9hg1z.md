{
  "date" : "2024-12-13T08:00:14.885121469Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2740" ],
    "start" : [ "2024-12-12T00:00:00Z" ],
    "end" : [ "2024-12-13T00:00:00Z" ],
    "published" : [ "2024-12-13T08:00:14.885121469Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/9hg1z",
  "client_id" : "https://www-api.jvt.me/fit"
}
