{
  "date" : "2024-12-27T08:00:22.528015806Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10599" ],
    "start" : [ "2024-12-26T00:00:00Z" ],
    "end" : [ "2024-12-27T00:00:00Z" ],
    "published" : [ "2024-12-27T08:00:22.528015806Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/rxqau",
  "client_id" : "https://www-api.jvt.me/fit"
}
