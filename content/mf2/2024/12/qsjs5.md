{
  "date" : "2024-12-23T08:16:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://fedi.simonwillison.net/@simon/113699243504077785" ],
    "published" : [ "2024-12-23T08:16:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'd recommend [Stoplight Elements](https://github.com/stoplightio/elements) for OpenAPI visualisation over the Swagger Editor (for more reasons than it still using the legacy name Swagger)\n\nThe [hosted demo](https://elements-demo.stoplight.io/) doesn't allow providing it a URL, but I've got [a version spun up that renders a spec you upload](https://openapi.tanna.dev/) - the usual caveats about [not trusting online tools](https://www.jvt.me/posts/2020/09/01/against-online-tooling/) apply!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/12/qsjs5",
  "client_id" : "https://indiepass.app/"
}
