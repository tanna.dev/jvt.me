{
  "date" : "2024-12-16T21:48:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-16T21:48:00+0000" ],
    "category" : [ "go" ],
    "bookmark-of" : [ "https://cirw.in/blog/go-tools" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2024/12/mfguf",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
