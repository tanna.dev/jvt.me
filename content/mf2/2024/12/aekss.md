{
  "date" : "2024-12-19T08:00:18.263290762Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9480" ],
    "start" : [ "2024-12-18T00:00:00Z" ],
    "end" : [ "2024-12-19T00:00:00Z" ],
    "published" : [ "2024-12-19T08:00:18.263290762Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/aekss",
  "client_id" : "https://www-api.jvt.me/fit"
}
