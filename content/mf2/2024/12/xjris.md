{
  "date" : "2024-12-24T08:00:17.547255387Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8581" ],
    "start" : [ "2024-12-23T00:00:00Z" ],
    "end" : [ "2024-12-24T00:00:00Z" ],
    "published" : [ "2024-12-24T08:00:17.547255387Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/xjris",
  "client_id" : "https://www-api.jvt.me/fit"
}
