{
  "date" : "2024-12-26T08:00:14.549436492Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6232" ],
    "start" : [ "2024-12-25T00:00:00Z" ],
    "end" : [ "2024-12-26T00:00:00Z" ],
    "published" : [ "2024-12-26T08:00:14.549436492Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/hze2u",
  "client_id" : "https://www-api.jvt.me/fit"
}
