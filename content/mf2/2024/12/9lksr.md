{
  "date" : "2024-12-09T08:00:12.60658491Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7066" ],
    "start" : [ "2024-12-08T00:00:00Z" ],
    "end" : [ "2024-12-09T00:00:00Z" ],
    "published" : [ "2024-12-09T08:00:12.60658491Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/9lksr",
  "client_id" : "https://www-api.jvt.me/fit"
}
