{
  "date" : "2024-12-14T08:00:16.552401215Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3262" ],
    "start" : [ "2024-12-13T00:00:00Z" ],
    "end" : [ "2024-12-14T00:00:00Z" ],
    "published" : [ "2024-12-14T08:00:16.552401215Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/bukqx",
  "client_id" : "https://www-api.jvt.me/fit"
}
