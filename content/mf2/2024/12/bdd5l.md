{
  "date" : "2024-12-07T08:00:13.248641706Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3392" ],
    "start" : [ "2024-12-06T00:00:00Z" ],
    "end" : [ "2024-12-07T00:00:00Z" ],
    "published" : [ "2024-12-07T08:00:13.248641706Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/bdd5l",
  "client_id" : "https://www-api.jvt.me/fit"
}
