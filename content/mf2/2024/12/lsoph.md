{
  "date" : "2024-12-27T09:29:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-27T09:29:00+0000" ],
    "category" : [ "open-source", "mental-health" ],
    "bookmark-of" : [ "https://xuanwo.io/2024/10-a-letter-to-open-source-maintainers/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2024/12/lsoph",
  "tags" : [ "open-source", "mental-health" ],
  "client_id" : "https://indiepass.app/"
}
