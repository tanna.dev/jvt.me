{
  "date" : "2024-12-07T11:01:52.57212226Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-12-07T11:01:52.57212226Z" ],
    "category" : [ "adhd", "manual-of-me" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Something I've been working on on-and-off for a couple of months is some tweaks to [my Manual of Me](https://manual.jvt.me/), prompted by getting a new manager, and wanting to rethink the \"things you should know when working with me\", as well as delving much more into how <a href=\"/tags/adhd/\">#ADHD</a> can affect my work, and some more reflections - and I'm glad that I've finally put the finishing touches on those updates.\r\n\r\nAlso re-reading [my blog post from when I first created it](https://www.jvt.me/posts/2022/02/23/manual-of-me/), it's wild to think that this was the first time I'd started using [Classless.css](https://classless.de/), which is now a staple of my microsites!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/12/ahtrg",
  "tags" : [ "adhd", "manual-of-me" ],
  "client_id" : "https://editor.tanna.dev/"
}
