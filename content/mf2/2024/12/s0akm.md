{
  "date" : "2024-12-01T08:00:16.822389069Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7183" ],
    "start" : [ "2024-11-30T00:00:00Z" ],
    "end" : [ "2024-12-01T00:00:00Z" ],
    "published" : [ "2024-12-01T08:00:16.822389069Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/s0akm",
  "client_id" : "https://www-api.jvt.me/fit"
}
