{
  "date" : "2024-12-07T20:12:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.online/@jpholbrook/113612028021352728" ],
    "published" : [ "2024-12-07T20:12:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thanks very much, that's very nice to hear, especially from a complete stranger 😁\n\nI'll be sharing these updates with my team on Monday, and this deffo fills me with more confidence, thank you!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/12/joxuc",
  "client_id" : "https://indiepass.app/"
}
