{
  "date" : "2024-12-20T09:49:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://shkspr.mobi/blog/2024/12/soft-launching-my-next-big-project-stopping/" ],
    "published" : [ "2024-12-20T09:49:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Incredibly huge congrats, very chuffed for you and hope it's awesome! Look forward to hearing many stories to make us all jealous, and for you both to find out what life is without work. ++ on having a lot of my identity and worth bundled in with work"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/12/02tgj",
  "client_id" : "https://indiepass.app/"
}
