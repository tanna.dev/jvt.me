{
  "date" : "2024-12-27T10:23:04.649155494Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://www.heavybit.com/library/podcasts/open-source-ready/ep-6-the-infinite-nature-of-software-with-adam-jacob/" ],
    "published" : [ "2024-12-27T10:23:04.649155494Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/12/ws46i",
  "client_id" : "https://editor.tanna.dev/"
}
