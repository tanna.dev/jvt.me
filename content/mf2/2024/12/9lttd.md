{
  "date" : "2024-12-04T08:00:14.416609734Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4868" ],
    "start" : [ "2024-12-03T00:00:00Z" ],
    "end" : [ "2024-12-04T00:00:00Z" ],
    "published" : [ "2024-12-04T08:00:14.416609734Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/9lttd",
  "client_id" : "https://www-api.jvt.me/fit"
}
