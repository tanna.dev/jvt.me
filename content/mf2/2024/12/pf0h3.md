{
  "date" : "2024-12-18T08:00:16.784894143Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5232" ],
    "start" : [ "2024-12-17T00:00:00Z" ],
    "end" : [ "2024-12-18T00:00:00Z" ],
    "published" : [ "2024-12-18T08:00:16.784894143Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/pf0h3",
  "client_id" : "https://www-api.jvt.me/fit"
}
