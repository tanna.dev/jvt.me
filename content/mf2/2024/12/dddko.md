{
  "date" : "2024-12-16T08:00:17.631625748Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8284" ],
    "start" : [ "2024-12-15T00:00:00Z" ],
    "end" : [ "2024-12-16T00:00:00Z" ],
    "published" : [ "2024-12-16T08:00:17.631625748Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/dddko",
  "client_id" : "https://www-api.jvt.me/fit"
}
