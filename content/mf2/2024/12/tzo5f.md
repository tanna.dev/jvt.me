{
  "date" : "2024-12-27T12:23:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-27T12:23:00+0000" ],
    "category" : [ "ai", "ethics", "capitalism" ],
    "repost-of" : [ "https://xoxo.zone/@fraying/113721891659638804" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/12/tzo5f",
  "tags" : [ "ai", "ethics", "capitalism" ],
  "client_id" : "https://indiepass.app/"
}
