{
  "date" : "2024-12-29T08:00:17.880307095Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9364" ],
    "start" : [ "2024-12-28T00:00:00Z" ],
    "end" : [ "2024-12-29T00:00:00Z" ],
    "published" : [ "2024-12-29T08:00:17.880307095Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/69lkv",
  "client_id" : "https://www-api.jvt.me/fit"
}
