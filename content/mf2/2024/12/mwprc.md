{
  "date" : "2024-12-28T08:00:17.606475519Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11839" ],
    "start" : [ "2024-12-27T00:00:00Z" ],
    "end" : [ "2024-12-28T00:00:00Z" ],
    "published" : [ "2024-12-28T08:00:17.606475519Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/mwprc",
  "client_id" : "https://www-api.jvt.me/fit"
}
