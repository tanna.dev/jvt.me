{
  "date" : "2024-12-30T08:00:13.257675459Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8589" ],
    "start" : [ "2024-12-29T00:00:00Z" ],
    "end" : [ "2024-12-30T00:00:00Z" ],
    "published" : [ "2024-12-30T08:00:13.257675459Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/npopo",
  "client_id" : "https://www-api.jvt.me/fit"
}
