{
  "date" : "2024-12-31T10:04:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-12-31T10:04:00+0000" ],
    "category" : [ "neovim" ],
    "like-of" : [ "https://github.com/folke/tokyonight.nvim" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/12/uvs8w",
  "tags" : [ "neovim" ],
  "client_id" : "https://indiepass.app/"
}
