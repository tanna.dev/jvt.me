{
  "date" : "2024-12-20T08:00:19.005470252Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5788" ],
    "start" : [ "2024-12-19T00:00:00Z" ],
    "end" : [ "2024-12-20T00:00:00Z" ],
    "published" : [ "2024-12-20T08:00:19.005470252Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/7q5ts",
  "client_id" : "https://www-api.jvt.me/fit"
}
