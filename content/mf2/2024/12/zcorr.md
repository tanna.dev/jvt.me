{
  "date" : "2024-12-23T13:15:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-23T13:15:00+0000" ],
    "category" : [ "bash", "shell" ],
    "like-of" : [ "https://mstdn.io/@pfhllnts/113702126057961040" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/12/zcorr",
  "tags" : [ "bash", "shell" ],
  "client_id" : "https://indiepass.app/"
}
