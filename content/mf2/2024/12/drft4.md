{
  "date" : "2024-12-21T08:00:12.031939352Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9183" ],
    "start" : [ "2024-12-20T00:00:00Z" ],
    "end" : [ "2024-12-21T00:00:00Z" ],
    "published" : [ "2024-12-21T08:00:12.031939352Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/drft4",
  "client_id" : "https://www-api.jvt.me/fit"
}
