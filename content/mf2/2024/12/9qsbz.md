{
  "date" : "2024-12-29T19:49:50.206238233Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A beautiful LEGO set, depicting a Japanese garden, with tea room, stream with koi carp, cherry blossoms, and a beautiful, relaxing garden and scene which was surprisingly relaxing to build. Making that even more amazing, the LEGO set is modified with a lighting set, so the cherry blossoms glow pink, the tea room is lit up, lanterns on the water are glowing a warm orange, and it provides a further three dimensional feel for the gorgeous set",
      "photo" : "https://media.jvt.me/eee5f4276b.jpeg"
    } ],
    "published" : [ "2024-12-29T19:49:50.206238233Z" ],
    "category" : [ "lego", "christmas" ],
    "content" : [ {
      "html" : "",
      "value" : "Got a super cool LEGO set, [Tranquil Garden](https://www.lego.com/en-gb/product/tranquil-garden-10315), for Christmas this year, which in of itself is an absolutely _gorgeous_ set but then on top of that, also got a lighting set which makes it much more magical"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/12/9qsbz",
  "tags" : [ "lego", "christmas" ],
  "client_id" : "https://gitlab.com/jamietanna/micropub-go/"
}
