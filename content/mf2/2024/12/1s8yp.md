{
  "date" : "2024-12-10T08:00:20.058166606Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5017" ],
    "start" : [ "2024-12-09T00:00:00Z" ],
    "end" : [ "2024-12-10T00:00:00Z" ],
    "published" : [ "2024-12-10T08:00:20.058166606Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/1s8yp",
  "client_id" : "https://www-api.jvt.me/fit"
}
