{
  "date" : "2024-12-08T08:00:15.181754044Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4096" ],
    "start" : [ "2024-12-07T00:00:00Z" ],
    "end" : [ "2024-12-08T00:00:00Z" ],
    "published" : [ "2024-12-08T08:00:15.181754044Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/yo0al",
  "client_id" : "https://www-api.jvt.me/fit"
}
