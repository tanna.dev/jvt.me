{
  "date" : "2024-12-23T08:00:18.003984749Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5219" ],
    "start" : [ "2024-12-22T00:00:00Z" ],
    "end" : [ "2024-12-23T00:00:00Z" ],
    "published" : [ "2024-12-23T08:00:18.003984749Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/rr0ym",
  "client_id" : "https://www-api.jvt.me/fit"
}
