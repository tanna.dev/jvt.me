{
  "date" : "2024-12-25T08:00:14.798743624Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6191" ],
    "start" : [ "2024-12-24T00:00:00Z" ],
    "end" : [ "2024-12-25T00:00:00Z" ],
    "published" : [ "2024-12-25T08:00:14.798743624Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/muoi8",
  "client_id" : "https://www-api.jvt.me/fit"
}
