{
  "date" : "2024-12-07T11:35:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "The Spotify Wrapped graphic, showing at the top a graphic of the artist Grafix. Below, my top 5 artists are shown: Grafix, Fred V, Phaeleh, Tycho and Koven. I listened to 73,830 minutes of music, and my top songs were: Paradise - Akacia + HYLO, Circles - Yoe Mase (which interestingly I only came across a month of so ago), Blow the Roof - Louis the Child, Kasbo & EVAN GIIA, Miss a Moment - Grafix & Elipsa, and Falling - Cyantific",
      "photo" : "https://media.jvt.me/213e367765.png"
    } ],
    "published" : [ "2024-12-07T11:35:00+0000" ],
    "category" : [ "spotify-wrapped" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Alas, <a href=\"/tags/spotify-wrapped/\">#SpotifyWrapped</a> is out and is interesting! Did not expect Grafix as my top listen, but given the [top songs of 2024 for me](https://open.spotify.com/playlist/37i9dQZF1FoMwr65taJjxG), it makes sense! \n\nI'll be more interested to see what happens when I get my Spotify data dump at the end of the year and [dig into it as I usually do](https://www.jvt.me/music-in-review/), and I'm looking at tweaking some of the breakdown to possibly graph my listens / top artists month-by-month\n\nThe [NotebookLM AI summary](https://wrappedaipodcast.spotifycdn.com/doxNmkj5If-t-_OuJ7Hqr0VSO-hU7XNyssTkRENaUeg.mp4) of my Wrapped data was a bit uncanny 🤔"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/12/du85p",
  "tags" : [ "spotify-wrapped" ],
  "client_id" : "https://indiepass.app/"
}
