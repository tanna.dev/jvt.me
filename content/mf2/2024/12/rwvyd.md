{
  "date" : "2024-12-20T18:13:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A social sharing card from OpenUK, indicating \"all I want for Christmas is a ticket to State of Open Con\" and indicating that I, Jamie Tanna, the Chief Blogger @ jvt.me is speaking on the Software and Security track. The conference is on the 4th-5th February in London",
      "photo" : "https://media.jvt.me/8193a77fd5.png"
    } ],
    "published" : [ "2024-12-20T18:13:00+0000" ],
    "category" : [ "state-of-open-con", "open-uk" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm very excited to announce that I'll be speaking at <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> in February!\n\nI'll be giving a new talk, _I inherited this project, and all I got was all these angry users_, in particular talking about my experience with maintaining [`oapi-codegen`](https://github.com/oapi-codegen/oapi-codegen/) and some insights into what it's like on the other side of the \"why won't you merge this PR\" and countless \"any update??\" comments 😅\n\nI'd also like to say a bit thanks to @openuk@hachyderm.io for the honour of kicking off the day at the Software and Security track 🤓\n\nHope to see some of you there - [tickets are now available](https://stateofopencon.com/)!\n\n#soocon25 <a href=\"/tags/open-uk/\">#OpenUK</a>"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/12/rwvyd",
  "tags" : [ "state-of-open-con", "open-uk" ],
  "client_id" : "https://indiepass.app/"
}
