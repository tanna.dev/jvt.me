{
  "date" : "2024-12-15T18:25:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-12-15T18:25:00+0000" ],
    "category" : [ "ai", "ethics", "llm", "climate-change" ],
    "repost-of" : [ "https://hachyderm.io/@BenjaminHCCarr/113624116159013960" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/12/w8tt4",
  "tags" : [ "ai", "ethics", "llm", "climate-change" ],
  "client_id" : "https://indiepass.app/"
}
