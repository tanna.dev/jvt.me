{
  "date" : "2024-12-19T17:36:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-12-19T17:36:00+0000" ],
    "category" : [ "dependency-management-data" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "FYI that <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a> [v0.114.0 is out](https://gitlab.com/tanna.dev/dependency-management-data/-/releases/v0.114.0) with an important refactor, but is one to watch out for!\n\nIf you're using the Renovate datasource, the `package_name`s may be different to what they were previously. This now makes them actual package names, rather than the \"pretty\" `depName` but it's likely to catch folks out 👀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/12/jqfzp",
  "tags" : [ "dependency-management-data" ],
  "client_id" : "https://indiepass.app/"
}
