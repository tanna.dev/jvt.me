{
  "date" : "2024-12-31T08:00:17.384311159Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4300" ],
    "start" : [ "2024-12-30T00:00:00Z" ],
    "end" : [ "2024-12-31T00:00:00Z" ],
    "published" : [ "2024-12-31T08:00:17.384311159Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/3b8zq",
  "client_id" : "https://www-api.jvt.me/fit"
}
