{
  "date" : "2024-12-15T08:00:16.059369862Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7242" ],
    "start" : [ "2024-12-14T00:00:00Z" ],
    "end" : [ "2024-12-15T00:00:00Z" ],
    "published" : [ "2024-12-15T08:00:16.059369862Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/12/dgcff",
  "client_id" : "https://www-api.jvt.me/fit"
}
