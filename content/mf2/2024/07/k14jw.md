{
  "date" : "2024-07-23T08:00:13.048271541Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6263" ],
    "start" : [ "2024-07-22T00:00:00Z" ],
    "end" : [ "2024-07-23T00:00:00Z" ],
    "published" : [ "2024-07-23T08:00:13.048271541Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/k14jw",
  "client_id" : "https://www-api.jvt.me/fit"
}
