{
  "date" : "2024-07-10T09:55:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-07-10T09:55:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://github.com/pacedotdev/oto" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/sfhmh",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
