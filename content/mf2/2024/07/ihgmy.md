{
  "date" : "2024-07-21T08:00:09.99647856Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4038" ],
    "start" : [ "2024-07-20T00:00:00Z" ],
    "end" : [ "2024-07-21T00:00:00Z" ],
    "published" : [ "2024-07-21T08:00:09.99647856Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/ihgmy",
  "client_id" : "https://www-api.jvt.me/fit"
}
