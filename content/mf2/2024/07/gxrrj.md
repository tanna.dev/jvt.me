{
  "date" : "2024-07-16T08:00:12.494730863Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2818" ],
    "start" : [ "2024-07-15T00:00:00Z" ],
    "end" : [ "2024-07-16T00:00:00Z" ],
    "published" : [ "2024-07-16T08:00:12.494730863Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/gxrrj",
  "client_id" : "https://www-api.jvt.me/fit"
}
