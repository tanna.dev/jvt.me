{
  "date" : "2024-07-01T08:00:09.809482275Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4330" ],
    "start" : [ "2024-06-30T00:00:00Z" ],
    "end" : [ "2024-07-01T00:00:00Z" ],
    "published" : [ "2024-07-01T08:00:09.809482275Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/ot1b6",
  "client_id" : "https://www-api.jvt.me/fit"
}
