{
  "date" : "2024-07-09T08:00:09.755656747Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6532" ],
    "start" : [ "2024-07-08T00:00:00Z" ],
    "end" : [ "2024-07-09T00:00:00Z" ],
    "published" : [ "2024-07-09T08:00:09.755656747Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/7a4pb",
  "client_id" : "https://www-api.jvt.me/fit"
}
