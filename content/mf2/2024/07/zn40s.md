{
  "date" : "2024-07-05T08:01:04.040433284Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "12111" ],
    "start" : [ "2024-07-04T00:00:00Z" ],
    "end" : [ "2024-07-05T00:00:00Z" ],
    "published" : [ "2024-07-05T08:01:04.040433284Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/zn40s",
  "client_id" : "https://www-api.jvt.me/fit"
}
