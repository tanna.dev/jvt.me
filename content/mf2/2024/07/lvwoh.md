{
  "date" : "2024-07-25T08:00:10.711349048Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5230" ],
    "start" : [ "2024-07-24T00:00:00Z" ],
    "end" : [ "2024-07-25T00:00:00Z" ],
    "published" : [ "2024-07-25T08:00:10.711349048Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/lvwoh",
  "client_id" : "https://www-api.jvt.me/fit"
}
