{
  "date" : "2024-07-03T08:00:10.030996475Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3275" ],
    "start" : [ "2024-07-02T00:00:00Z" ],
    "end" : [ "2024-07-03T00:00:00Z" ],
    "published" : [ "2024-07-03T08:00:10.030996475Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/otkcv",
  "client_id" : "https://www-api.jvt.me/fit"
}
