{
  "date" : "2024-07-29T08:00:10.897346377Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "12024" ],
    "start" : [ "2024-07-28T00:00:00Z" ],
    "end" : [ "2024-07-29T00:00:00Z" ],
    "published" : [ "2024-07-29T08:00:10.897346377Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/k75kn",
  "client_id" : "https://www-api.jvt.me/fit"
}
