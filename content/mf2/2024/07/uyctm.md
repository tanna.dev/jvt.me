{
  "date" : "2024-07-13T15:14:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-13T15:14:00+0100" ],
    "category" : [ "burnout" ],
    "like-of" : [ "https://sollecitom.github.io/software-product-development-blog/posts/2023/2023-11-06-at-the-heart-of-the-burnout-epidemic/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/uyctm",
  "tags" : [ "burnout" ],
  "client_id" : "https://indiepass.app/"
}
