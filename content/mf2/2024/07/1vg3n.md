{
  "date" : "2024-07-14T12:58:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-14T12:58:00+0100" ],
    "category" : [ "platform-engineering" ],
    "like-of" : [ "https://newsletter.rarefiederr.io/archive/building-a-cli-for-your-platform-part-1/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/1vg3n",
  "tags" : [ "platform-engineering" ],
  "client_id" : "https://indiepass.app/"
}
