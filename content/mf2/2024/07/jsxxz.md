{
  "date" : "2024-07-01T12:49:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-01T12:49:00+0100" ],
    "category" : [ "elastic", "architecture" ],
    "like-of" : [ "https://www.elastic.co/search-labs/blog/building-elastic-cloud-serverless" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/jsxxz",
  "tags" : [ "elastic", "architecture" ],
  "client_id" : "https://indiepass.app/"
}
