{
  "date" : "2024-07-22T08:00:10.152825773Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5757" ],
    "start" : [ "2024-07-21T00:00:00Z" ],
    "end" : [ "2024-07-22T00:00:00Z" ],
    "published" : [ "2024-07-22T08:00:10.152825773Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/auz3c",
  "client_id" : "https://www-api.jvt.me/fit"
}
