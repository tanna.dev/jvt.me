{
  "date" : "2024-07-12T08:00:12.373805057Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3218" ],
    "start" : [ "2024-07-11T00:00:00Z" ],
    "end" : [ "2024-07-12T00:00:00Z" ],
    "published" : [ "2024-07-12T08:00:12.373805057Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/rqt4q",
  "client_id" : "https://www-api.jvt.me/fit"
}
