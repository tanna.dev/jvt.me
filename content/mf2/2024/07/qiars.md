{
  "date" : "2024-07-29T07:50:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-07-29T07:50:00+0100" ],
    "category" : [ "go", "github" ],
    "like-of" : [ "https://github.com/gofri/go-github-ratelimit" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/qiars",
  "tags" : [ "go", "github" ],
  "client_id" : "https://indiepass.app/"
}
