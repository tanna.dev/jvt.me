{
  "date" : "2024-07-30T08:00:11.425511892Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4047" ],
    "start" : [ "2024-07-29T00:00:00Z" ],
    "end" : [ "2024-07-30T00:00:00Z" ],
    "published" : [ "2024-07-30T08:00:11.425511892Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/iqfsx",
  "client_id" : "https://www-api.jvt.me/fit"
}
