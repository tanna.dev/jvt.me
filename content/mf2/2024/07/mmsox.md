{
  "date" : "2024-07-14T08:00:09.978115762Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5589" ],
    "start" : [ "2024-07-13T00:00:00Z" ],
    "end" : [ "2024-07-14T00:00:00Z" ],
    "published" : [ "2024-07-14T08:00:09.978115762Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/mmsox",
  "client_id" : "https://www-api.jvt.me/fit"
}
