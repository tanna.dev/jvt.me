{
  "date" : "2024-07-05T09:01:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-05T09:01:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://eli.thegreenplace.net/2024/locally-patching-dependencies-in-go/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/twhlz",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
