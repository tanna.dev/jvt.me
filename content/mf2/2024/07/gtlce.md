{
  "date" : "2024-07-31T08:00:15.423351177Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3377" ],
    "start" : [ "2024-07-30T00:00:00Z" ],
    "end" : [ "2024-07-31T00:00:00Z" ],
    "published" : [ "2024-07-31T08:00:15.423351177Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/gtlce",
  "client_id" : "https://www-api.jvt.me/fit"
}
