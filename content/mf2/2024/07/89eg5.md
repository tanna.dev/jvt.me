{
  "date" : "2024-07-19T08:00:10.573935134Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3394" ],
    "start" : [ "2024-07-18T00:00:00Z" ],
    "end" : [ "2024-07-19T00:00:00Z" ],
    "published" : [ "2024-07-19T08:00:10.573935134Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/89eg5",
  "client_id" : "https://www-api.jvt.me/fit"
}
