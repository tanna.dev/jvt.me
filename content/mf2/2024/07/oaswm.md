{
  "date" : "2024-07-13T07:43:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-13T07:43:00+0100" ],
    "category" : [ "refactoring", "practices" ],
    "like-of" : [ "https://blog.probablyfine.co.uk/2024/07/11/making-time-to-make-improvements.html" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/oaswm",
  "tags" : [ "refactoring", "practices" ],
  "client_id" : "https://indiepass.app/"
}
