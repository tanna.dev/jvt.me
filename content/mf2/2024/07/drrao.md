{
  "date" : "2024-07-24T08:00:10.645149423Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4775" ],
    "start" : [ "2024-07-23T00:00:00Z" ],
    "end" : [ "2024-07-24T00:00:00Z" ],
    "published" : [ "2024-07-24T08:00:10.645149423Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/drrao",
  "client_id" : "https://www-api.jvt.me/fit"
}
