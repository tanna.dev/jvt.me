{
  "date" : "2024-07-20T17:13:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-07-20T17:13:00+0100" ],
    "category" : [ "linkedin" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Strong dislike that <a href=\"/tags/linkedin/\">#Linkedin</a>'s native Web view no longer allows you to copy the URL, or open it in other browsers, and generally making it very hostile to folks who want to ie share the link with someone else, or move it to a read-it-later app"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/07/huah8",
  "tags" : [ "linkedin" ],
  "client_id" : "https://indiepass.app/"
}
