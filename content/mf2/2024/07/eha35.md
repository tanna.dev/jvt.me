{
  "date" : "2024-07-31T11:15:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-31T11:15:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://eli.thegreenplace.net/2024/building-static-binaries-with-go-on-linux/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/eha35",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
