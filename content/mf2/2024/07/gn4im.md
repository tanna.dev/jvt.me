{
  "date" : "2024-07-22T16:51:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-22T16:51:00+0100" ],
    "category" : [ "vim", "neovim", "tree-sitter" ],
    "like-of" : [ "https://relistan.com/parsing-protobuf-files-with-treesitter" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/gn4im",
  "tags" : [ "vim", "neovim", "tree-sitter" ],
  "client_id" : "https://indiepass.app/"
}
