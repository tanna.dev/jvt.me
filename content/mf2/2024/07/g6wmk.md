{
  "date" : "2024-07-29T07:50:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-07-29T07:50:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://github.com/google/go-github" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/g6wmk",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
