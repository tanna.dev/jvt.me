{
  "date" : "2024-07-27T08:00:11.871684234Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7202" ],
    "start" : [ "2024-07-26T00:00:00Z" ],
    "end" : [ "2024-07-27T00:00:00Z" ],
    "published" : [ "2024-07-27T08:00:11.871684234Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/plldw",
  "client_id" : "https://www-api.jvt.me/fit"
}
