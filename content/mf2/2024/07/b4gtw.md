{
  "date" : "2024-07-07T08:00:10.125996662Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5131" ],
    "start" : [ "2024-07-06T00:00:00Z" ],
    "end" : [ "2024-07-07T00:00:00Z" ],
    "published" : [ "2024-07-07T08:00:10.125996662Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/b4gtw",
  "client_id" : "https://www-api.jvt.me/fit"
}
