{
  "date" : "2024-07-12T07:16:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-12T07:16:00+0100" ],
    "category" : [ "docker" ],
    "like-of" : [ "https://ochagavia.nl/blog/using-s3-as-a-container-registry/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/rrpse",
  "tags" : [ "docker" ],
  "client_id" : "https://indiepass.app/"
}
