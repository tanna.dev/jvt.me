{
  "date" : "2024-07-30T09:06:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@Edent/112874082886856803" ],
    "published" : [ "2024-07-30T09:06:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "We've had an [Oxo Easy-Clean Compost Bin](https://www.oxouk.com/products/easy-clean-compost-bin-charcoal-2-83-l/) for years - after my parents have had one for years and rate it - and it's pretty good. A good size, dishwasher safe, fits a fair bit. Would recommend!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/07/mbyl0",
  "client_id" : "https://indiepass.app/"
}
