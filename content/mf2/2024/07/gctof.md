{
  "date" : "2024-07-29T20:43:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-29T20:43:00+0100" ],
    "category" : [ "open-source", "burnout" ],
    "repost-of" : [ "https://tacobelllabs.net/@astraluma/112869984809442568" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/07/gctof",
  "tags" : [ "open-source", "burnout" ],
  "client_id" : "https://indiepass.app/"
}
