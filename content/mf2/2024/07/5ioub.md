{
  "date" : "2024-07-12T08:24:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-07-12T08:24:00+0100" ],
    "category" : [ "firefox-nightly" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Anyone know if there's a way of tweaking the new <a href=\"/tags/firefox-nightly/\">#FirefoxNightly</a> Android layout? \n\nNot a fan of the two row format they've now got\n\nReally hate that the address bar only shows the domain, not the full URL, until you tap into it 😕"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/07/5ioub",
  "tags" : [ "firefox-nightly" ],
  "client_id" : "https://indiepass.app/"
}
