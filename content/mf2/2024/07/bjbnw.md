{
  "date" : "2024-07-28T08:00:16.868914215Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5344" ],
    "start" : [ "2024-07-27T00:00:00Z" ],
    "end" : [ "2024-07-28T00:00:00Z" ],
    "published" : [ "2024-07-28T08:00:16.868914215Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/bjbnw",
  "client_id" : "https://www-api.jvt.me/fit"
}
