{
  "date" : "2024-07-20T08:00:12.927440505Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5482" ],
    "start" : [ "2024-07-19T00:00:00Z" ],
    "end" : [ "2024-07-20T00:00:00Z" ],
    "published" : [ "2024-07-20T08:00:12.927440505Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/c26rt",
  "client_id" : "https://www-api.jvt.me/fit"
}
