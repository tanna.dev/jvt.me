{
  "date" : "2024-07-14T19:50:53.751809256Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-07-14T19:50:53.751809256Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I really need to upgrade my version of Hugo - seeing un-syntax-highlighted code ain't great for readers! ([via](https://www.jvt.me/posts/2024/07/14/dmd-opa-eol/))"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/07/o4dcf",
  "client_id" : "https://editor.tanna.dev/"
}
