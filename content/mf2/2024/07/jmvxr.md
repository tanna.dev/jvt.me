{
  "date" : "2024-07-29T12:27:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-07-29T12:27:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "[Memories - A Little Sound, Gray](https://open.spotify.com/track/2cArx8KSzFqLBrJLjWuai0) is such a good high energy bop ⚡🎧"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/07/jmvxr",
  "client_id" : "https://indiepass.app/"
}
