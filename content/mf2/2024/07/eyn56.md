{
  "date" : "2024-07-13T09:19:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-07-13T09:19:00+0100" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://opensource.microsoft.com/blog/2024/06/27/5-things-we-learned-from-sponsoring-a-sampling-of-our-open-source-dependencies/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/eyn56",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
