{
  "date" : "2024-07-08T08:00:10.199315018Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5584" ],
    "start" : [ "2024-07-07T00:00:00Z" ],
    "end" : [ "2024-07-08T00:00:00Z" ],
    "published" : [ "2024-07-08T08:00:10.199315018Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/sq4li",
  "client_id" : "https://www-api.jvt.me/fit"
}
