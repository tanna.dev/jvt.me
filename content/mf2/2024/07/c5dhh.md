{
  "date" : "2024-07-04T08:00:12.923074579Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6855" ],
    "start" : [ "2024-07-03T00:00:00Z" ],
    "end" : [ "2024-07-04T00:00:00Z" ],
    "published" : [ "2024-07-04T08:00:12.923074579Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/c5dhh",
  "client_id" : "https://www-api.jvt.me/fit"
}
