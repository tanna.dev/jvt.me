{
  "date" : "2024-07-11T08:00:12.499607608Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5168" ],
    "start" : [ "2024-07-10T00:00:00Z" ],
    "end" : [ "2024-07-11T00:00:00Z" ],
    "published" : [ "2024-07-11T08:00:12.499607608Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/kdydt",
  "client_id" : "https://www-api.jvt.me/fit"
}
