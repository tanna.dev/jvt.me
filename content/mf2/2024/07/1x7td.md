{
  "date" : "2024-07-08T07:32:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://k8s.social/@Marcus/112749403448475024" ],
    "published" : [ "2024-07-08T07:32:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Glad to hear, hope it's of use. We're looking at packaging some of it up so teams can be writing tests for regexes in their own repos, without needing to set up the whole framework themselves 🤞🏽 \n\nOh no that's a typo! Will fix that now"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/07/1x7td",
  "client_id" : "https://indiepass.app/"
}
