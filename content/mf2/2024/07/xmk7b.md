{
  "date" : "2024-07-14T10:18:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-07-14T10:18:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://github.com/danicat/testquery" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/07/xmk7b",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
