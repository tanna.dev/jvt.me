{
  "date" : "2024-07-16T09:50:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-07-16T09:50:00+0100" ],
    "category" : [ "php" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Friends in the <a href=\"/tags/php/\">#PHP</a> community, would you mind sharing examples of some packages that have been officially deprecated, as well as some that have been deprecated by the community ie due to lack of maintenance or security issues?"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/07/rtb3j",
  "tags" : [ "php" ],
  "client_id" : "https://indiepass.app/"
}
