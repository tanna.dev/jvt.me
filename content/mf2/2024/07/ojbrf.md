{
  "date" : "2024-07-15T08:00:10.31169332Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8913" ],
    "start" : [ "2024-07-14T00:00:00Z" ],
    "end" : [ "2024-07-15T00:00:00Z" ],
    "published" : [ "2024-07-15T08:00:10.31169332Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/ojbrf",
  "client_id" : "https://www-api.jvt.me/fit"
}
