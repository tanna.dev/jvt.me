{
  "date" : "2024-07-02T08:00:12.897436446Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5913" ],
    "start" : [ "2024-07-01T00:00:00Z" ],
    "end" : [ "2024-07-02T00:00:00Z" ],
    "published" : [ "2024-07-02T08:00:12.897436446Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/3dpau",
  "client_id" : "https://www-api.jvt.me/fit"
}
