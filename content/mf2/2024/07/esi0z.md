{
  "date" : "2024-07-10T08:00:09.875152613Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2005" ],
    "start" : [ "2024-07-09T00:00:00Z" ],
    "end" : [ "2024-07-10T00:00:00Z" ],
    "published" : [ "2024-07-10T08:00:09.875152613Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/esi0z",
  "client_id" : "https://www-api.jvt.me/fit"
}
