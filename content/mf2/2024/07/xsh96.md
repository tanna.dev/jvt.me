{
  "date" : "2024-07-26T08:00:13.074931578Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4784" ],
    "start" : [ "2024-07-25T00:00:00Z" ],
    "end" : [ "2024-07-26T00:00:00Z" ],
    "published" : [ "2024-07-26T08:00:13.074931578Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/07/xsh96",
  "client_id" : "https://www-api.jvt.me/fit"
}
