{
  "date" : "2024-01-02T08:00:10.184783026Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10620" ],
    "start" : [ "2024-01-01T00:00:00Z" ],
    "end" : [ "2024-01-02T00:00:00Z" ],
    "published" : [ "2024-01-02T08:00:10.184783026Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/ybxg6",
  "client_id" : "https://www-api.jvt.me/fit"
}
