{
  "date" : "2024-01-16T17:08:13.835245413Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-16T17:08:13.835245413Z" ],
    "category" : [ "open-source", "supply-chain-security" ],
    "like-of" : [ "https://www.softwaremaxims.com/blog/not-a-supplier" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/dq1vy",
  "tags" : [ "open-source", "supply-chain-security" ],
  "client_id" : "https://editor.tanna.dev/"
}
