{
  "date" : "2024-01-09T11:15:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-09T11:15:00+0000" ],
    "category" : [ "ethics", "capitalism", "ai" ],
    "repost-of" : [ "https://mstdn.ca/@AlisonCreekside/111722168326766043" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/01/gsg4n",
  "tags" : [ "ethics", "capitalism", "ai" ],
  "client_id" : "https://indiepass.app/"
}
