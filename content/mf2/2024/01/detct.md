{
  "date" : "2024-01-10T13:12:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-10T13:12:00+0000" ],
    "category" : [ "capitalism" ],
    "repost-of" : [ "https://fediscience.org/@ct_bergstrom/111729403840221417" ],
    "post-status" : [ "published" ]
  },
  "kind" : "reposts",
  "slug" : "2024/01/detct",
  "tags" : [ "capitalism" ],
  "client_id" : "https://indiepass.app/"
}
