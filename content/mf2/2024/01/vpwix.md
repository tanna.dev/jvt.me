{
  "date" : "2024-01-03T08:00:10.135717825Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4314" ],
    "start" : [ "2024-01-02T00:00:00Z" ],
    "end" : [ "2024-01-03T00:00:00Z" ],
    "published" : [ "2024-01-03T08:00:10.135717825Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/vpwix",
  "client_id" : "https://www-api.jvt.me/fit"
}
