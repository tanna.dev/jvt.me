{
  "date" : "2024-01-09T08:00:10.190451685Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5461" ],
    "start" : [ "2024-01-08T00:00:00Z" ],
    "end" : [ "2024-01-09T00:00:00Z" ],
    "published" : [ "2024-01-09T08:00:10.190451685Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/pvaqj",
  "client_id" : "https://www-api.jvt.me/fit"
}
