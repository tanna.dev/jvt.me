{
  "date" : "2024-01-13T08:00:09.969594879Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8557" ],
    "start" : [ "2024-01-12T00:00:00Z" ],
    "end" : [ "2024-01-13T00:00:00Z" ],
    "published" : [ "2024-01-13T08:00:09.969594879Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/dm6cz",
  "client_id" : "https://www-api.jvt.me/fit"
}
