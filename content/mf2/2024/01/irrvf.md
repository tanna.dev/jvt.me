{
  "date" : "2024-01-04T08:00:09.999806175Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6515" ],
    "start" : [ "2024-01-03T00:00:00Z" ],
    "end" : [ "2024-01-04T00:00:00Z" ],
    "published" : [ "2024-01-04T08:00:09.999806175Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/irrvf",
  "client_id" : "https://www-api.jvt.me/fit"
}
