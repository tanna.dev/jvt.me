{
  "date" : "2024-01-28T08:00:10.816482607Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9496" ],
    "start" : [ "2024-01-27T00:00:00Z" ],
    "end" : [ "2024-01-28T00:00:00Z" ],
    "published" : [ "2024-01-28T08:00:10.816482607Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/sbwky",
  "client_id" : "https://www-api.jvt.me/fit"
}
