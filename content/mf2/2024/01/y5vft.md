{
  "date" : "2024-01-02T12:11:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-02T12:11:00+0000" ],
    "category" : [ "open-source", "github" ],
    "like-of" : [ "https://hynek.me/articles/pull-requests-branch/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/y5vft",
  "tags" : [ "open-source", "github" ],
  "client_id" : "https://indiepass.app/"
}
