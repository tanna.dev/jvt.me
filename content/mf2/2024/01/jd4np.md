{
  "date" : "2024-01-20T12:34:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-20T12:34:00+0000" ],
    "category" : [ "go" ],
    "like-of" : [ "https://michael.stapelberg.ch/posts/2017-08-19-golang_favorite/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/jd4np",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
