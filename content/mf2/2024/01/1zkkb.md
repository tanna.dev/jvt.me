{
  "date" : "2024-01-12T08:00:10.068764267Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6372" ],
    "start" : [ "2024-01-11T00:00:00Z" ],
    "end" : [ "2024-01-12T00:00:00Z" ],
    "published" : [ "2024-01-12T08:00:10.068764267Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/1zkkb",
  "client_id" : "https://www-api.jvt.me/fit"
}
