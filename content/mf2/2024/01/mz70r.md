{
  "date" : "2024-01-24T08:00:09.956711052Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5677" ],
    "start" : [ "2024-01-23T00:00:00Z" ],
    "end" : [ "2024-01-24T00:00:00Z" ],
    "published" : [ "2024-01-24T08:00:09.956711052Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/mz70r",
  "client_id" : "https://www-api.jvt.me/fit"
}
