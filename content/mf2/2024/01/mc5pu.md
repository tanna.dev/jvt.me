{
  "date" : "2024-01-23T15:33:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-23T15:33:00+0000" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://sethmlarson.dev/removing-maintainers-from-open-source-projects" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/mc5pu",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
