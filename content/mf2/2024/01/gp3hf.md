{
  "date" : "2024-01-26T08:00:09.763468229Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8291" ],
    "start" : [ "2024-01-25T00:00:00Z" ],
    "end" : [ "2024-01-26T00:00:00Z" ],
    "published" : [ "2024-01-26T08:00:09.763468229Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/gp3hf",
  "client_id" : "https://www-api.jvt.me/fit"
}
