{
  "date" : "2024-01-05T16:34:41.243242412Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-05T16:34:41.243242412Z" ],
    "category" : [ "open-source" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very cool to have received my first payout from <span class=\"h-card\"><a class=\"u-url\" href=\"https://tidelift.com/\">Tidelift</a></span>, from a company using [one of the Open Source projects that I maintain](https://www.jvt.me/open-source/) 💸 Thanks very much to whoever it was, and looking forward to the income working towards me getting some longer-term financial support to continue maintaining the projects I do 🚀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/fxgvd",
  "tags" : [ "open-source" ],
  "client_id" : "https://editor.tanna.dev/"
}
