{
  "date" : "2024-01-21T21:05:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-21T21:05:00+0000" ],
    "category" : [ "dependency-management-data" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "If you've been hearing me talking about <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a> and are wondering about some real world scenarios it's been useful, check out [the new Case Studies section on the site](https://dmd.tanna.dev/case-studies/) 👀\n\nAlso looking for more examples of where it's been useful!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/vfnkv",
  "tags" : [ "dependency-management-data" ],
  "client_id" : "https://indiepass.app/"
}
