{
  "date" : "2024-01-31T08:00:10.336558681Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4726" ],
    "start" : [ "2024-01-30T00:00:00Z" ],
    "end" : [ "2024-01-31T00:00:00Z" ],
    "published" : [ "2024-01-31T08:00:10.336558681Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/n8fqa",
  "client_id" : "https://www-api.jvt.me/fit"
}
