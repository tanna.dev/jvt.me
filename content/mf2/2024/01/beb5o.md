{
  "date" : "2024-01-01T08:00:11.034672664Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9734" ],
    "start" : [ "2023-12-31T00:00:00Z" ],
    "end" : [ "2024-01-01T00:00:00Z" ],
    "published" : [ "2024-01-01T08:00:11.034672664Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/beb5o",
  "client_id" : "https://www-api.jvt.me/fit"
}
