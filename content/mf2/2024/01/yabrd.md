{
  "date" : "2024-01-21T21:48:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "a screenshot of the Slack interface on mobile, with two messages from the \"DMD Announcements\" Slack app, showing two recent releases of dependency-management-data, one which indicates there are breaking changes in the release and one that does not",
      "photo" : "https://media.jvt.me/97d8728042.png"
    } ],
    "published" : [ "2024-01-21T21:48:00+0000" ],
    "category" : [ "dependency-management-data" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I was pretty chuffed with adding these Slack notifications (via Goreleaser and go-semantic-release) for releases to <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a> which flag when there are breaking changes in the release! Makes it much easier to see at a glance, especially as there's a lot of changes going into it 🤓"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/01/yabrd",
  "tags" : [ "dependency-management-data" ],
  "client_id" : "https://indiepass.app/"
}
