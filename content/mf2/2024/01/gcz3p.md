{
  "date" : "2024-01-22T22:31:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-22T22:31:00+0000" ],
    "category" : [ "music" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "[🎶 Home is where I brush my teeth 🎶](https://open.spotify.com/track/377AoKp62nOZLYwAEaU9yE)"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/gcz3p",
  "tags" : [ "music" ],
  "client_id" : "https://indiepass.app/"
}
