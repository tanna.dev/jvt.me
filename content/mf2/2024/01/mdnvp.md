{
  "date" : "2024-01-29T15:37:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-01-29T15:37:00+0000" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://github.com/pi0/tired-maintainer" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/mdnvp",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
