{
  "date" : "2024-01-31T15:40:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://tenforward.social/@0xdariaj/111851218713689588" ],
    "published" : [ "2024-01-31T15:40:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Chatting with one of the recruiters I know, the answers are yes we do, and no, it's not a b2b contract. I can put you in touch with them to get more concrete answers?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/01/wl6qm",
  "client_id" : "https://indiepass.app/"
}
