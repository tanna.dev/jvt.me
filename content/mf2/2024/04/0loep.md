{
  "date" : "2024-04-14T06:39:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-04-14T06:39:00+0100" ],
    "category" : [ "renovate", "sql" ],
    "like-of" : [ "https://docs.mergestat.com/blog/2023/02/16/querying-renovate-bot-across-repos" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/04/0loep",
  "tags" : [ "renovate", "sql" ],
  "client_id" : "https://indiepass.app/"
}
