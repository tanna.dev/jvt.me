{
  "date" : "2024-04-03T21:29:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/zeeg/status/1775620683600630192?t=CiPTgGoQYhQEhI5WoY8J9Q&s=09" ],
    "published" : [ "2024-04-03T21:29:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "There's `.Get` or `.MustGet` (not ideal outside of tests or CLIs) that you can use to work out whether the value is there, but given you may want to check `IsSpecified` and `IsNull`, that's why there's no way to get it back as a pointer"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/mjpki",
  "client_id" : "https://indiepass.app/"
}
