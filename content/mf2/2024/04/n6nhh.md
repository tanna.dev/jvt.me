{
  "date" : "2024-04-15T08:18:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-04-15T08:18:00+0100" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://www.fastly.com/blog/what-can-you-actually-do-to-reduce-the-threat-of-hacks-like-xz" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/04/n6nhh",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
