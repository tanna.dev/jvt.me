{
  "date" : "2024-04-03T21:15:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/zeeg/status/1775616597954281504" ],
    "published" : [ "2024-04-03T21:15:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "You may want to give https://github.com/oapi-codegen/nullable a go for this instead - we wrote it because /none/ of the options out there worked for all the cases you need to handle - more info at https://www.jvt.me/posts/2024/01/09/go-json-nullable/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/rpllh",
  "client_id" : "https://indiepass.app/"
}
