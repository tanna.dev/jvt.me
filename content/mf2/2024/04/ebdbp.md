{
  "date" : "2024-04-26T08:00:10.460813701Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8034" ],
    "start" : [ "2024-04-25T00:00:00Z" ],
    "end" : [ "2024-04-26T00:00:00Z" ],
    "published" : [ "2024-04-26T08:00:10.460813701Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/ebdbp",
  "client_id" : "https://www-api.jvt.me/fit"
}
