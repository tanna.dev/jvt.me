{
  "date" : "2024-04-12T16:44:21.018685671Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-12T16:44:21.018685671Z" ],
    "category" : [ "www.jvt.me" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Followers of my blog - you can now subscribe to _just_ blog posts for certain tags, for instance if you want to read all my [articles about Go](https://www.jvt.me/tags/go/) but _only_ my articles, and not be annoyed by all the other stuff tagged `go`, you can now add `https://www.jvt.me/tags/go/feed.articles.xml` to your feed reader of choice.\r\n\r\nAnd of course, this is [discoverable via RSS discovery](https://www.jvt.me/posts/2021/11/10/rss-discovery/) so you can just point your feed reader at i.e. `https://www.jvt.me/tags/go/` and it should prompt you the different options."
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/6p0jk",
  "tags" : [ "www.jvt.me" ],
  "client_id" : "https://editor.tanna.dev/"
}
