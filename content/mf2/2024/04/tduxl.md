{
  "date" : "2024-04-21T13:15:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@neilgmacy/112303262596586016" ],
    "published" : [ "2024-04-21T13:15:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I remember on Twitter there was an account that mentioned (folks with a /now page) every couple of months to nudge them to update it. That's also a fun nudge for me to update mine 🙃"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/tduxl",
  "client_id" : "https://indiepass.app/"
}
