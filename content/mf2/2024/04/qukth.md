{
  "date" : "2024-04-02T08:00:10.116275665Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6942" ],
    "start" : [ "2024-04-01T00:00:00Z" ],
    "end" : [ "2024-04-02T00:00:00Z" ],
    "published" : [ "2024-04-02T08:00:10.116275665Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/qukth",
  "client_id" : "https://www-api.jvt.me/fit"
}
