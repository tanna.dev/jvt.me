{
  "date" : "2024-04-03T08:00:10.345935256Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3457" ],
    "start" : [ "2024-04-02T00:00:00Z" ],
    "end" : [ "2024-04-03T00:00:00Z" ],
    "published" : [ "2024-04-03T08:00:10.345935256Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/nu4ox",
  "client_id" : "https://www-api.jvt.me/fit"
}
