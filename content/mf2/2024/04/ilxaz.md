{
  "date" : "2024-04-14T08:00:11.0042652Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "15027" ],
    "start" : [ "2024-04-13T00:00:00Z" ],
    "end" : [ "2024-04-14T00:00:00Z" ],
    "published" : [ "2024-04-14T08:00:11.0042652Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/ilxaz",
  "client_id" : "https://www-api.jvt.me/fit"
}
