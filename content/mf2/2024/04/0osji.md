{
  "date" : "2024-04-27T13:19:30.510869752Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-27T13:19:30.510869752Z" ],
    "category" : [ "the-expanse", "music" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Listening to [Tulips - Minotaur Shock Remix](https://open.spotify.com/track/5YsaAcQzmmstCDoDv3DrQb?si=492224552e96449d) is forever going to remind me of the last few chapters of _Leviathan Falls_. It happened to be what I was listening to at the time, and the lyrics seemed to fit so perfectly with the grand finale, and listening to it just now brought that all back, including all the feels around the events.\r\n\r\nDeffo need to re-read <a href=\"/tags/the-expanse/\">#TheExpanse</a> series, what a great series.\r\n\r\nSee also: [previous thoughts](https://www.jvt.me/posts/2019/11/16/music-associations/) on the way <a href=\"/tags/music/\">#music</a> can remind you of things."
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/0osji",
  "tags" : [ "the-expanse", "music" ],
  "client_id" : "https://editor.tanna.dev/"
}
