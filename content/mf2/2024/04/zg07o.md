{
  "date" : "2024-04-10T10:06:25.534625877Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-10T10:06:25.534625877Z" ],
    "category" : [ "platform-engineering", "developer-experience", "lead-dev" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "It's just under an hour until I'll be speaking at the @TheLeadDev@hachyderm.io webinar [_Does your org need platform engineering?_](https://leaddev.com/events/does-your-org-need-platform-engineering)! Looking forward to my first webinar as a panellist - alongside some excellent people - and to share some of my experiences with <a href=\"/tags/platform-engineering/\">#PlatformEngineering</a> and <a href=\"/tags/developer-experience/\">#DeveloperExperience</a> for the lovely <a href=\"/tags/lead-dev/\">#LeadDev</a> audience - it's not too late to sign up to join!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/zg07o",
  "tags" : [ "platform-engineering", "developer-experience", "lead-dev" ],
  "client_id" : "https://editor.tanna.dev/"
}
