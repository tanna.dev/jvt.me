{
  "date" : "2024-04-09T16:57:47.799533061Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://podcasters.spotify.com/pod/show/incident-fm/episodes/Why-building-a-strong-culture-of-engineering-is-worth-the-effort-e2i54p1" ],
    "published" : [ "2024-04-09T16:57:47.799533061Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/04/bkutn",
  "client_id" : "https://editor.tanna.dev/"
}
