{
  "date" : "2024-04-28T19:42:22.490811784Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-28T19:42:22.490811784Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Has anyone else started getting spam from a Substack they definitely didn't subscribe to? It's for with an email I wouldn't have signed up to, and it's a language I don't know (Spanish)\r\n\r\nI've now unsubscribed and marked it as spam - I didn't seem to get a \"are you sure you want to sign up\", but I did get a \"thanks for subscribing\" post (in Spanish)"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/pzt7z",
  "client_id" : "https://editor.tanna.dev/"
}
