{
  "date" : "2024-04-13T08:00:10.144469626Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6621" ],
    "start" : [ "2024-04-12T00:00:00Z" ],
    "end" : [ "2024-04-13T00:00:00Z" ],
    "published" : [ "2024-04-13T08:00:10.144469626Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/jsbtq",
  "client_id" : "https://www-api.jvt.me/fit"
}
