{
  "date" : "2024-04-07T13:51:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://k8s.social/@Marcus/112229092904610138" ],
    "published" : [ "2024-04-07T13:51:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "First time I've seen someone say Playlist Radio is gone, I assume they've maybe tried to push the \"smart shuffle\" as an alternate way of doing the same thing? I'm glad Song Radio is still there as I use that 🙃"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/3c1if",
  "client_id" : "https://indiepass.app/"
}
