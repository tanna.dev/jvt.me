{
  "date" : "2024-04-16T08:00:10.074165834Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4427" ],
    "start" : [ "2024-04-15T00:00:00Z" ],
    "end" : [ "2024-04-16T00:00:00Z" ],
    "published" : [ "2024-04-16T08:00:10.074165834Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/d7wbh",
  "client_id" : "https://www-api.jvt.me/fit"
}
