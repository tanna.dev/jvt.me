{
  "date" : "2024-04-20T08:01:05.582679924Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6903" ],
    "start" : [ "2024-04-19T00:00:00Z" ],
    "end" : [ "2024-04-20T00:00:00Z" ],
    "published" : [ "2024-04-20T08:01:05.582679924Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/rhp6n",
  "client_id" : "https://www-api.jvt.me/fit"
}
