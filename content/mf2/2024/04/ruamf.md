{
  "date" : "2024-04-28T08:00:09.947385002Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8052" ],
    "start" : [ "2024-04-27T00:00:00Z" ],
    "end" : [ "2024-04-28T00:00:00Z" ],
    "published" : [ "2024-04-28T08:00:09.947385002Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/ruamf",
  "client_id" : "https://www-api.jvt.me/fit"
}
