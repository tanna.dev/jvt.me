{
  "date" : "2024-04-09T08:00:10.233092403Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4315" ],
    "start" : [ "2024-04-08T00:00:00Z" ],
    "end" : [ "2024-04-09T00:00:00Z" ],
    "published" : [ "2024-04-09T08:00:10.233092403Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/3c3sh",
  "client_id" : "https://www-api.jvt.me/fit"
}
