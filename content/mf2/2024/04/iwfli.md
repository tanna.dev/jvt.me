{
  "date" : "2024-04-18T07:29:19.618220721Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://podcasters.spotify.com/pod/show/techleadjournal/episodes/161---The-7-Dimensions-of-Highly-Creative-Programmers---Wouter-Groeneveld-e2fb59c" ],
    "published" : [ "2024-04-18T07:29:19.618220721Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/04/iwfli",
  "client_id" : "https://editor.tanna.dev/"
}
