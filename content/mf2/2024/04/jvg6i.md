{
  "date" : "2024-04-27T19:09:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-27T19:09:00+0100" ],
    "category" : [ "datasette" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Does anyone know if there's a good way of getting a historical storage of queries that users put into <a href=\"/tags/datasette/\">#Datasette</a>? Trying to get some stats around common queries and usage, couldn't see a plugin for it, but not sure if my searching just missed it"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/jvg6i",
  "tags" : [ "datasette" ],
  "client_id" : "https://indiepass.app/"
}
