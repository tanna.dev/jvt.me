{
  "date" : "2024-04-03T21:18:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/zeeg/status/1775618444303761660" ],
    "published" : [ "2024-04-03T21:18:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "That library doesn't handle \"field not set\" vs \"field explicitly set to null\" - https://github.com/guregu/null/issues/39#issuecomment-1895386392"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/4jb5r",
  "client_id" : "https://indiepass.app/"
}
