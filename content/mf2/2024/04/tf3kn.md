{
  "date" : "2024-04-07T08:00:10.277887065Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8422" ],
    "start" : [ "2024-04-06T00:00:00Z" ],
    "end" : [ "2024-04-07T00:00:00Z" ],
    "published" : [ "2024-04-07T08:00:10.277887065Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/tf3kn",
  "client_id" : "https://www-api.jvt.me/fit"
}
