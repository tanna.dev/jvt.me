{
  "date" : "2024-04-12T18:04:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@byjp/112259323458884666" ],
    "published" : [ "2024-04-12T18:04:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Been meaning to do it for a while, and then [this PR](https://github.com/tomasurbanec/planetgolang.dev/pull/24) helped convince me to do so. Hopefully should allow folks' feeds to be a little quieter if they want only my long-form writing"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/x5twq",
  "client_id" : "https://indiepass.app/"
}
