{
  "date" : "2024-04-03T21:40:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/zeeg/status/1775621994568175723?t=gG6CfpzL2zGm2PvyUp8Ymw&s=09" ],
    "published" : [ "2024-04-03T21:40:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Hmm, will have a think about it - this may come down to a bit of verbosity and explicit checking in Go. I think I'd prefer to see it be more explicit when constructing the entity in terms of what happens when it's set, but I get your point"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/dcnq8",
  "client_id" : "https://indiepass.app/"
}
