{
  "date" : "2024-04-10T08:00:10.216546841Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4468" ],
    "start" : [ "2024-04-09T00:00:00Z" ],
    "end" : [ "2024-04-10T00:00:00Z" ],
    "published" : [ "2024-04-10T08:00:10.216546841Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/trxpq",
  "client_id" : "https://www-api.jvt.me/fit"
}
