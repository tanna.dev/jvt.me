{
  "date" : "2024-04-18T08:39:08.879316507Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-04-18T08:39:08.879316507Z" ],
    "category" : [ "tmux" ],
    "like-of" : [ "https://github.com/tmux/tmux/issues/3836#issuecomment-1941801438" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/04/cbw34",
  "tags" : [ "tmux" ],
  "client_id" : "https://editor.tanna.dev/"
}
