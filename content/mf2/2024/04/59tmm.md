{
  "date" : "2024-04-27T08:00:10.26740448Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7581" ],
    "start" : [ "2024-04-26T00:00:00Z" ],
    "end" : [ "2024-04-27T00:00:00Z" ],
    "published" : [ "2024-04-27T08:00:10.26740448Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/59tmm",
  "client_id" : "https://www-api.jvt.me/fit"
}
