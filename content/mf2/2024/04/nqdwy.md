{
  "date" : "2024-04-27T15:19:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://x.com/itsthatladydev/status/1783600119956353405?t=MOxD4CY0_ThqNL8-zWCm1Q&s=09" ],
    "published" : [ "2024-04-27T15:19:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I'd definitely be up for it if you fancy either https://github.com/deepmap/oapi-codegen (used in quite a few big name companies!) or https://dmd.tanna.dev (used by GitHub's OSPO)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/nqdwy",
  "client_id" : "https://indiepass.app/"
}
