{
  "date" : "2024-04-22T08:00:10.930901696Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6538" ],
    "start" : [ "2024-04-21T00:00:00Z" ],
    "end" : [ "2024-04-22T00:00:00Z" ],
    "published" : [ "2024-04-22T08:00:10.930901696Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/xfwrw",
  "client_id" : "https://www-api.jvt.me/fit"
}
