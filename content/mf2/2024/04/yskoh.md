{
  "date" : "2024-04-07T14:07:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://k8s.social/@Marcus/112230075535882101" ],
    "published" : [ "2024-04-07T14:07:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "That's fair, sorry to hear! I mostly listen to my Discover Weekly and Release Radar which help introduce some new things, and sometimes will do a song radio if there's a particular thing I like, and that usually leads to a few more, but that's just what works for me. Hope you find something good for you 🤞🏽"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/yskoh",
  "client_id" : "https://indiepass.app/"
}
