{
  "date" : "2024-04-21T08:00:10.620099172Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4900" ],
    "start" : [ "2024-04-20T00:00:00Z" ],
    "end" : [ "2024-04-21T00:00:00Z" ],
    "published" : [ "2024-04-21T08:00:10.620099172Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/th0x0",
  "client_id" : "https://www-api.jvt.me/fit"
}
