{
  "date" : "2024-04-08T08:00:10.387395432Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7168" ],
    "start" : [ "2024-04-07T00:00:00Z" ],
    "end" : [ "2024-04-08T00:00:00Z" ],
    "published" : [ "2024-04-08T08:00:10.387395432Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/yphs9",
  "client_id" : "https://www-api.jvt.me/fit"
}
