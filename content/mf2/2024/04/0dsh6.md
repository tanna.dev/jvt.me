{
  "date" : "2024-04-15T08:00:10.742984773Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8626" ],
    "start" : [ "2024-04-14T00:00:00Z" ],
    "end" : [ "2024-04-15T00:00:00Z" ],
    "published" : [ "2024-04-15T08:00:10.742984773Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/0dsh6",
  "client_id" : "https://www-api.jvt.me/fit"
}
