{
  "date" : "2024-04-10T11:45:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://werd.io/2024/writing-a-web-first-resum%C3%A9" ],
    "published" : [ "2024-04-10T11:45:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Ooh looks great! I did mine [a few years back](https://www.jvt.me/posts/2021/05/25/microformats-resume/) and have really enjoyed having it on the Web, always generally up-to-date, and with print-specific tweaks"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/j6zmy",
  "client_id" : "https://indiepass.app/"
}
