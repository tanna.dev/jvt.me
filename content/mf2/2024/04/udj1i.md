{
  "date" : "2024-04-30T08:00:11.089210409Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4851" ],
    "start" : [ "2024-04-29T00:00:00Z" ],
    "end" : [ "2024-04-30T00:00:00Z" ],
    "published" : [ "2024-04-30T08:00:11.089210409Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/udj1i",
  "client_id" : "https://www-api.jvt.me/fit"
}
