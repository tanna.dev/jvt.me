{
  "date" : "2024-04-18T08:01:06.756277617Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5397" ],
    "start" : [ "2024-04-17T00:00:00Z" ],
    "end" : [ "2024-04-18T00:00:00Z" ],
    "published" : [ "2024-04-18T08:01:06.756277617Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/nxsra",
  "client_id" : "https://www-api.jvt.me/fit"
}
