{
  "date" : "2024-04-18T16:57:32.08982099Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://tidelift.com/webinar/how-to-reduce-your-organizations-reliance-on-bad-open-source-packages" ],
    "published" : [ "2024-04-18T16:57:32.08982099Z" ],
    "post-status" : [ "published" ],
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2024/04/j9f6u",
  "client_id" : "https://editor.tanna.dev/"
}
