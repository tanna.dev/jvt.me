{
  "date" : "2024-04-03T19:08:55.590424686Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-03T19:08:55.590424686Z" ],
    "category" : [ "slack" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Looks like <a href=\"/tags/slack/\">#Slack</a> v4.36.140 (or some recent version) appears to have removed the ability to use the sidebar workspace switcher, and now you're stuck with the crappy new design?"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/6uibv",
  "tags" : [ "slack" ],
  "client_id" : "https://editor.tanna.dev/"
}
