{
  "date" : "2024-04-28T12:00:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-28T12:00:00+0100" ],
    "category" : [ "sboms" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Friends and folks working with <a href=\"/tags/sboms/\">#SBOMs</a> - how do you conceptually think about them in terms of ingesting them into tools? \n\nI.e. I like to think of an SBOM having a source repository or component it relates to, but sometimes you don't know that up front, and all you have is the result of a scan, which could be the source repo, a container image, or a built binary.\n\nConsidering whether:\n\n- I try to guess what repo/component it is based on the filename\n- Just store the filename in the database and allow querying with that (and leave repo info optional) \n- Retrieve metadata from the SBOM that known tools use to define this\n- Some 4th option? \n\nTrying to [tweak how Dependency Management Data works](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/530) with SBOMs and trying to find how other folks do it and consider them"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/mpg6b",
  "tags" : [ "sboms" ],
  "client_id" : "https://indiepass.app/"
}
