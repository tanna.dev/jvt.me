{
  "date" : "2024-04-01T09:24:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-04-01T09:24:00+0100" ],
    "category" : [ "open-source" ],
    "like-of" : [ "https://robmensching.com/blog/posts/2024/03/30/a-microcosm-of-the-interactions-in-open-source-projects/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/04/5unr4",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
