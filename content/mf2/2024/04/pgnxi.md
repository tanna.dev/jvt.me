{
  "date" : "2024-04-17T08:00:10.292964525Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5643" ],
    "start" : [ "2024-04-16T00:00:00Z" ],
    "end" : [ "2024-04-17T00:00:00Z" ],
    "published" : [ "2024-04-17T08:00:10.292964525Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/pgnxi",
  "client_id" : "https://www-api.jvt.me/fit"
}
