{
  "date" : "2024-04-12T08:00:10.057709623Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6164" ],
    "start" : [ "2024-04-11T00:00:00Z" ],
    "end" : [ "2024-04-12T00:00:00Z" ],
    "published" : [ "2024-04-12T08:00:10.057709623Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/8anx6",
  "client_id" : "https://www-api.jvt.me/fit"
}
