{
  "date" : "2024-04-12T07:37:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://social.lol/@carol/112256766264166763" ],
    "published" : [ "2024-04-12T07:37:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "We enjoyed it. Deffo want to rewatch with the whole film in mind. Bit confusing at times (as intended) but thought it was well done, and hearing that they all learned how to speak backwards was pretty impressive!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/x1ewm",
  "client_id" : "https://indiepass.app/"
}
