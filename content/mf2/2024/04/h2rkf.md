{
  "date" : "2024-04-01T08:00:11.083696172Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11079" ],
    "start" : [ "2024-03-31T00:00:00Z" ],
    "end" : [ "2024-04-01T00:00:00Z" ],
    "published" : [ "2024-04-01T08:00:11.083696172Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/h2rkf",
  "client_id" : "https://www-api.jvt.me/fit"
}
