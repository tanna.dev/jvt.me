{
  "date" : "2024-04-06T08:00:09.954738794Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7173" ],
    "start" : [ "2024-04-05T00:00:00Z" ],
    "end" : [ "2024-04-06T00:00:00Z" ],
    "published" : [ "2024-04-06T08:00:09.954738794Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/l7zj5",
  "client_id" : "https://www-api.jvt.me/fit"
}
