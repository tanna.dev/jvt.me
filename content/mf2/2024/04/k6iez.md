{
  "date" : "2024-04-04T08:00:11.336913136Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2837" ],
    "start" : [ "2024-04-03T00:00:00Z" ],
    "end" : [ "2024-04-04T00:00:00Z" ],
    "published" : [ "2024-04-04T08:00:11.336913136Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/k6iez",
  "client_id" : "https://www-api.jvt.me/fit"
}
