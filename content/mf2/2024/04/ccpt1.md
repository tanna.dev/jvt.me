{
  "date" : "2024-04-23T09:43:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://social.crablab.uk/@hugh/112316440598619126" ],
    "published" : [ "2024-04-23T09:43:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "[Learning this](https://www.jvt.me/posts/2019/01/10/git-commit-fixup/) was a huge booster to my productivity, no longer needing to remember where things went. Pair that with `git rebase --autosq` (or `git rebase --autosq -i` on older Git versions) and 🤌"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/ccpt1",
  "client_id" : "https://indiepass.app/"
}
