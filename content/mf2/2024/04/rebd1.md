{
  "date" : "2024-04-11T08:00:10.449013897Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6590" ],
    "start" : [ "2024-04-10T00:00:00Z" ],
    "end" : [ "2024-04-11T00:00:00Z" ],
    "published" : [ "2024-04-11T08:00:10.449013897Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/rebd1",
  "client_id" : "https://www-api.jvt.me/fit"
}
