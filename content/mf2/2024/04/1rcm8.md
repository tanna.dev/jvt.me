{
  "date" : "2024-04-14T18:33:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-04-14T18:33:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Apropos of [Why I recommend Renovate over any other dependency update tools](https://www.jvt.me/posts/2024/04/12/use-renovate/) having [12+ hours on the front page of Hacker News](https://news.ycombinator.com/item?id=40011111), y'all may be interested in a post from last year, [what happens when a post goes viral](https://www.jvt.me/posts/2022/10/14/blog-viral/)"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/04/1rcm8",
  "client_id" : "https://indiepass.app/"
}
