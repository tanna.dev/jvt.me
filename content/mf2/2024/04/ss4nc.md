{
  "date" : "2024-04-23T08:00:10.16420089Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3132" ],
    "start" : [ "2024-04-22T00:00:00Z" ],
    "end" : [ "2024-04-23T00:00:00Z" ],
    "published" : [ "2024-04-23T08:00:10.16420089Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/04/ss4nc",
  "client_id" : "https://www-api.jvt.me/fit"
}
