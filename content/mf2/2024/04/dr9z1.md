{
  "date" : "2024-04-13T17:20:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-04-13T17:20:00+0100" ],
    "category" : [ "go" ],
    "like-of" : [ "https://benhoyt.com/writings/go-version-performance-2024/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/04/dr9z1",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
