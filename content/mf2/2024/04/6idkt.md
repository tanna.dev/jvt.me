{
  "date" : "2024-04-11T19:44:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://hachyderm.io/@ChrisShort/112253706902229085" ],
    "published" : [ "2024-04-11T19:44:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Last year - while I was still working there - I know some engineers used https://github.com/deliveroo/deliveroo.engineering/blob/gh-pages/Dockerfile to run our Jekyll site locally, 🤞🏽 that may help"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/04/6idkt",
  "client_id" : "https://indiepass.app/"
}
