{
  "date" : "2024-03-19T20:40:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.eventbrite.com/e/sustainoss-foundations-and-fiscal-hosting-for-open-source-communities-tickets-866704676017" ],
    "published" : [ "2024-03-19T20:40:00+0000" ],
    "post-status" : [ "published" ],
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2024/03/58c5v",
  "client_id" : "https://indiepass.app/"
}
