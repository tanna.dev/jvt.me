{
  "date" : "2024-03-19T07:15:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-03-19T07:15:00+0100" ],
    "category" : [ "sqlite", "indieauth" ],
    "like-of" : [ "https://github.com/simonw/datasette-indieauth" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/crquu",
  "tags" : [ "sqlite", "indieauth" ],
  "client_id" : "https://indiepass.app/"
}
