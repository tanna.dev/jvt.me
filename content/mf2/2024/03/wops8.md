{
  "date" : "2024-03-19T10:59:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@purpleidea/112121719016649984" ],
    "published" : [ "2024-03-19T10:59:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "So I think `go get -u ./...` should get you most of the way? I'd usually reach for [Renovate](https://docs.renovatebot.com) and would create a config file that allows grouping all the dependencies in a single branch, then you can check tests etc pass before merging. But you can also apply those changes purely locally - am travelling today but can shoot over an example tomorrow?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/03/wops8",
  "client_id" : "https://indiepass.app/"
}
