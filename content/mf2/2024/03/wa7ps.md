{
  "date" : "2024-03-26T08:00:10.27646778Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6802" ],
    "start" : [ "2024-03-25T00:00:00Z" ],
    "end" : [ "2024-03-26T00:00:00Z" ],
    "published" : [ "2024-03-26T08:00:10.27646778Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/wa7ps",
  "client_id" : "https://www-api.jvt.me/fit"
}
