{
  "date" : "2024-03-16T08:01:12.912894328Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "20390" ],
    "start" : [ "2024-03-15T00:00:00Z" ],
    "end" : [ "2024-03-16T00:00:00Z" ],
    "published" : [ "2024-03-16T08:01:12.912894328Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/rql9z",
  "client_id" : "https://www-api.jvt.me/fit"
}
