{
  "date" : "2024-03-05T08:00:10.298616259Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6276" ],
    "start" : [ "2024-03-04T00:00:00Z" ],
    "end" : [ "2024-03-05T00:00:00Z" ],
    "published" : [ "2024-03-05T08:00:10.298616259Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/em3zv",
  "client_id" : "https://www-api.jvt.me/fit"
}
