{
  "date" : "2024-03-20T20:24:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-20T20:24:00+0000" ],
    "like-of" : [ "https://openssf.org/blog/2024/03/05/openssf-scorecard-evaluating-and-improving-the-health-of-critical-oss-projects/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/aeqzf",
  "client_id" : "https://indiepass.app/"
}
