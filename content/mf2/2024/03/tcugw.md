{
  "date" : "2024-03-17T21:45:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-03-17T21:45:00+0100" ],
    "category" : [ "week-notes" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "No <a href=\"/tags/week-notes/\">#WeekNotes</a> tonight as I'm celebrating my 30th birthday in Rome 🎂🥂🍝🍷\n\nIf you wanted to do something nice to honour it, you could [support my work](/support-me/) on the [Open Source projects I maintain](/open-source/) as well as the content on my blog. But I'd also love to see y'all pay it forward to other creators or maintainers for the stuff you use, and work with your companies to pay to support the Open Source you so heavily rely on!\n\nI'll be posting my Week Notes some time next week, when I get to relive the lovely ~10 days we've been having 🥰"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/03/tcugw",
  "tags" : [ "week-notes" ],
  "client_id" : "https://indiepass.app/"
}
