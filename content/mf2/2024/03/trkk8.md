{
  "date" : "2024-03-13T10:03:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-03-13T10:03:00+0100" ],
    "category" : [ "git", "command-line", "zsh" ],
    "like-of" : [ "https://github.com/chrisgrieser/zsh-magic-dashboard" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/trkk8",
  "tags" : [ "git", "command-line", "zsh" ],
  "client_id" : "https://indiepass.app/"
}
