{
  "date" : "2024-03-10T08:00:10.287966153Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8184" ],
    "start" : [ "2024-03-09T00:00:00Z" ],
    "end" : [ "2024-03-10T00:00:00Z" ],
    "published" : [ "2024-03-10T08:00:10.287966153Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/54nth",
  "client_id" : "https://www-api.jvt.me/fit"
}
