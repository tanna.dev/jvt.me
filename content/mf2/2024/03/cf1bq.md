{
  "date" : "2024-03-05T21:53:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-05T21:53:00+0000" ],
    "category" : [ "github" ],
    "like-of" : [ "https://github.blog/2024-03-04-keeping-repository-maintainer-information-accurate/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/cf1bq",
  "tags" : [ "github" ],
  "client_id" : "https://indiepass.app/"
}
