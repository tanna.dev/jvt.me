{
  "date" : "2024-03-09T09:32:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://pony.social/@cadey/112063008860397953" ],
    "published" : [ "2024-03-09T09:32:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "I've been considering following the work that @shkspr.mobi has done with [improving Atkinson Hyperlegible](https://shkspr.mobi/blog/2022/08/an-update-to-the-atkinson-hyperlegible-font/) and although I've not _yet_ deployed it across my stuff, I feel it's a pretty great choice"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/03/j95sd",
  "client_id" : "https://indiepass.app/"
}
