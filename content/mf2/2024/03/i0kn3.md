{
  "date" : "2024-03-02T08:01:16.341331444Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11462" ],
    "start" : [ "2024-03-01T00:00:00Z" ],
    "end" : [ "2024-03-02T00:00:00Z" ],
    "published" : [ "2024-03-02T08:01:16.341331444Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/i0kn3",
  "client_id" : "https://www-api.jvt.me/fit"
}
