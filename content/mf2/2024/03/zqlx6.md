{
  "date" : "2024-03-02T18:47:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-03-02T18:47:00+0000" ],
    "category" : [ "music", "adhd" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "<a href=\"/tags/adhd/\">#ADHD</a> friends - what are your favourite brain itching songs? Like, what do you end up listening to on end because it scratches that spot in your brain _just right_?"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/03/zqlx6",
  "tags" : [ "music", "adhd" ],
  "client_id" : "https://indiepass.app/"
}
