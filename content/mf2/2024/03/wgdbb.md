{
  "date" : "2024-03-18T08:00:11.156477999Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "14689" ],
    "start" : [ "2024-03-17T00:00:00Z" ],
    "end" : [ "2024-03-18T00:00:00Z" ],
    "published" : [ "2024-03-18T08:00:11.156477999Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/wgdbb",
  "client_id" : "https://www-api.jvt.me/fit"
}
