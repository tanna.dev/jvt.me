{
  "date" : "2024-03-07T08:08:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-07T08:08:00+0000" ],
    "category" : [ "git", "git-worktree" ],
    "like-of" : [ "https://notes.billmill.org/blog/2024/03/How_I_use_git_worktrees.html" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/qaisp",
  "tags" : [ "git", "git-worktree" ],
  "client_id" : "https://indiepass.app/"
}
