{
  "date" : "2024-03-19T10:31:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://mastodon.social/@purpleidea/112118223318844868" ],
    "published" : [ "2024-03-19T10:31:00+0100" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Not Lukáš, but wondering what the use case is? Are you looking to get a way to easily bump dependencies? Trying to work out where you may be able to `go mod tidy` down duplicates/unnecessary deps?"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/03/rxalh",
  "client_id" : "https://indiepass.app/"
}
