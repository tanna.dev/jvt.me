{
  "date" : "2024-03-01T08:00:10.480837003Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11875" ],
    "start" : [ "2024-02-29T00:00:00Z" ],
    "end" : [ "2024-03-01T00:00:00Z" ],
    "published" : [ "2024-03-01T08:00:10.480837003Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/m4u0t",
  "client_id" : "https://www-api.jvt.me/fit"
}
