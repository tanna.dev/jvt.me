{
  "date" : "2024-03-19T20:47:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-03-19T20:47:00+0000" ],
    "category" : [ "platform-engineering", "developer-experience" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very excited to be speaking at the @TheLeadDev@hachyderm.io  webinar [_Does your org need platform engineering?_](https://leaddev.com/events/does-your-org-need-platform-engineering) in a few weeks! Hope to share some of my experiences with <a href=\"/tags/platform-engineering/\">#PlatformEngineering</a> and <a href=\"/tags/developer-experience/\">#DeveloperExperience</a>"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/03/x5zrf",
  "tags" : [ "platform-engineering", "developer-experience" ],
  "client_id" : "https://indiepass.app/"
}
