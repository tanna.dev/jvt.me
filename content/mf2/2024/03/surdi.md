{
  "date" : "2024-03-06T16:11:29.807649722Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-03-06T16:11:29.807649722Z" ],
    "category" : [ "slack" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Anyone else noticed <a href=\"/tags/slack/\">#Slack</a> has recently broken links to GitHub? Today I've had so many popups saying \"Double-check this link\", despite saying \"Don't show this again\" 🥴"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/03/surdi",
  "tags" : [ "slack" ],
  "client_id" : "https://editor.tanna.dev/"
}
