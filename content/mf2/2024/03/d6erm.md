{
  "date" : "2024-03-26T11:30:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-26T11:30:00+0000" ],
    "category" : [ "dark-mode", "css" ],
    "like-of" : [ "https://ar.al/2021/08/24/implementing-dark-mode-in-a-handful-of-lines-of-css-with-css-filters/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/d6erm",
  "tags" : [ "dark-mode", "css" ],
  "client_id" : "https://indiepass.app/"
}
