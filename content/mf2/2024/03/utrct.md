{
  "date" : "2024-03-28T11:58:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-03-28T11:58:00+0000" ],
    "category" : [ "micropub" ],
    "like-of" : [ "https://github.com/benjifs/sparkles" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/utrct",
  "tags" : [ "micropub" ],
  "client_id" : "https://indiepass.app/"
}
