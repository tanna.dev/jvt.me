{
  "date" : "2024-03-19T08:00:10.553868556Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "15758" ],
    "start" : [ "2024-03-18T00:00:00Z" ],
    "end" : [ "2024-03-19T00:00:00Z" ],
    "published" : [ "2024-03-19T08:00:10.553868556Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/igt6d",
  "client_id" : "https://www-api.jvt.me/fit"
}
