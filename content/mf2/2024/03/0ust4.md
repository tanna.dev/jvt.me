{
  "date" : "2024-03-09T08:00:10.425309975Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5090" ],
    "start" : [ "2024-03-08T00:00:00Z" ],
    "end" : [ "2024-03-09T00:00:00Z" ],
    "published" : [ "2024-03-09T08:00:10.425309975Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/0ust4",
  "client_id" : "https://www-api.jvt.me/fit"
}
