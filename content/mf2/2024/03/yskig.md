{
  "date" : "2024-03-19T20:45:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://leaddev.com/events/does-your-org-need-platform-engineering" ],
    "published" : [ "2024-03-19T20:45:00+0000" ],
    "post-status" : [ "published" ],
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2024/03/yskig",
  "client_id" : "https://indiepass.app/"
}
