{
  "date" : "2024-03-25T08:00:10.084169627Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7167" ],
    "start" : [ "2024-03-24T00:00:00Z" ],
    "end" : [ "2024-03-25T00:00:00Z" ],
    "published" : [ "2024-03-25T08:00:10.084169627Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/nzjr4",
  "client_id" : "https://www-api.jvt.me/fit"
}
