{
  "date" : "2024-03-30T12:36:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://tane.codes/@tanepiper/112184636628114138" ],
    "published" : [ "2024-03-30T12:36:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Awesome! A substandard SBOM is better than none, and a highly detailed SBOM is better than that 🤓 then plugging it into something like [dependency-management-data](https://dmd.tanna.dev) or [guac](https://guac.sh) to understand more about your software estate is a great next step. Making sure the runtime environment is safer is a great shout too - recently found out about [OpenSSF's S2C2F](https://openssf.org/projects/s2c2f/) which has some good stuff in there around reducing supply chain security risks too"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/03/l1jlx",
  "client_id" : "https://indiepass.app/"
}
