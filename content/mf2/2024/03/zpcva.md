{
  "date" : "2024-03-15T08:00:11.041644239Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "17770" ],
    "start" : [ "2024-03-14T00:00:00Z" ],
    "end" : [ "2024-03-15T00:00:00Z" ],
    "published" : [ "2024-03-15T08:00:11.041644239Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/zpcva",
  "client_id" : "https://www-api.jvt.me/fit"
}
