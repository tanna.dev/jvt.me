{
  "date" : "2024-03-01T19:30:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-01T19:30:00+0100" ],
    "category" : [ "git" ],
    "like-of" : [ "https://adrg.se/blog/dotfiles-digest-git" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/pllfk",
  "tags" : [ "git" ],
  "client_id" : "https://indiepass.app/"
}
