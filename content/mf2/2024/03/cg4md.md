{
  "date" : "2024-03-01T19:25:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-01T19:25:00+0100" ],
    "category" : [ "open-source", "entrepreneurship" ],
    "like-of" : [ "https://www.openhealthnews.com/story/2019-11-01/product-vs-project-open-source" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/cg4md",
  "tags" : [ "open-source", "entrepreneurship" ],
  "client_id" : "https://indiepass.app/"
}
