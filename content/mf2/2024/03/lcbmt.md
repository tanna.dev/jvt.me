{
  "date" : "2024-03-28T10:45:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-28T10:45:00+0000" ],
    "category" : [ "supply-chain-security" ],
    "like-of" : [ "https://lucumr.pocoo.org/2024/3/26/rust-cdo/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/lcbmt",
  "tags" : [ "supply-chain-security" ],
  "client_id" : "https://indiepass.app/"
}
