{
  "date" : "2024-03-13T08:00:10.127383547Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11498" ],
    "start" : [ "2024-03-12T00:00:00Z" ],
    "end" : [ "2024-03-13T00:00:00Z" ],
    "published" : [ "2024-03-13T08:00:10.127383547Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/dvfju",
  "client_id" : "https://www-api.jvt.me/fit"
}
