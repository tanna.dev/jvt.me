{
  "date" : "2024-03-05T18:35:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-03-05T18:35:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Does anyone know of/use an HTTP caching proxy, which can read/write cached responses to disk? Trying to reduce the overhead on an external service (during CI/CD) and allowing caching between runs"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/03/edq4n",
  "client_id" : "https://indiepass.app/"
}
