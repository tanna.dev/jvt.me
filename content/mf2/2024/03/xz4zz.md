{
  "date" : "2024-03-18T22:32:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-03-18T22:32:00+0100" ],
    "category" : [ "supply-chain-security" ],
    "like-of" : [ "https://www.chainguard.dev/unchained/software-supply-chain-security-broader-than-solarwinds-and-log4j" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/xz4zz",
  "tags" : [ "supply-chain-security" ],
  "client_id" : "https://indiepass.app/"
}
