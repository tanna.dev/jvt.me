{
  "date" : "2024-03-03T08:00:11.133847173Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6193" ],
    "start" : [ "2024-03-02T00:00:00Z" ],
    "end" : [ "2024-03-03T00:00:00Z" ],
    "published" : [ "2024-03-03T08:00:11.133847173Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/va8o3",
  "client_id" : "https://www-api.jvt.me/fit"
}
