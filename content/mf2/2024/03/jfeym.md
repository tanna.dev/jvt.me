{
  "date" : "2024-03-07T08:00:10.811437107Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5344" ],
    "start" : [ "2024-03-06T00:00:00Z" ],
    "end" : [ "2024-03-07T00:00:00Z" ],
    "published" : [ "2024-03-07T08:00:10.811437107Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/jfeym",
  "client_id" : "https://www-api.jvt.me/fit"
}
