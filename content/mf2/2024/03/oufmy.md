{
  "date" : "2024-03-05T15:12:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-03-05T15:12:00+0000" ],
    "category" : [ "analytics", "www.jvt.me" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Not sure what's caused it - or if it's related to recent discussions about Google Search being a bit naff recently - but according to Google, this last month I had ~19k clicks, whereas this time last year I had ~33k"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/03/oufmy",
  "tags" : [ "analytics", "www.jvt.me" ],
  "client_id" : "https://indiepass.app/"
}
