{
  "date" : "2024-03-12T08:00:10.150741369Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "13710" ],
    "start" : [ "2024-03-11T00:00:00Z" ],
    "end" : [ "2024-03-12T00:00:00Z" ],
    "published" : [ "2024-03-12T08:00:10.150741369Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/m68hi",
  "client_id" : "https://www-api.jvt.me/fit"
}
