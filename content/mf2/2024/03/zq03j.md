{
  "date" : "2024-03-06T08:00:10.31184884Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7159" ],
    "start" : [ "2024-03-05T00:00:00Z" ],
    "end" : [ "2024-03-06T00:00:00Z" ],
    "published" : [ "2024-03-06T08:00:10.31184884Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/zq03j",
  "client_id" : "https://www-api.jvt.me/fit"
}
