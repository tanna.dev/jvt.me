{
  "date" : "2024-03-24T08:00:11.3267387Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6304" ],
    "start" : [ "2024-03-23T00:00:00Z" ],
    "end" : [ "2024-03-24T00:00:00Z" ],
    "published" : [ "2024-03-24T08:00:11.3267387Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/rpqwa",
  "client_id" : "https://www-api.jvt.me/fit"
}
