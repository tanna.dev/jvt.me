{
  "date" : "2024-03-17T08:00:11.218364108Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "18304" ],
    "start" : [ "2024-03-16T00:00:00Z" ],
    "end" : [ "2024-03-17T00:00:00Z" ],
    "published" : [ "2024-03-17T08:00:11.218364108Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/g81ta",
  "client_id" : "https://www-api.jvt.me/fit"
}
