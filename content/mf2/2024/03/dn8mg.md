{
  "date" : "2024-03-30T08:00:11.818386401Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10018" ],
    "start" : [ "2024-03-29T00:00:00Z" ],
    "end" : [ "2024-03-30T00:00:00Z" ],
    "published" : [ "2024-03-30T08:00:11.818386401Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/dn8mg",
  "client_id" : "https://www-api.jvt.me/fit"
}
