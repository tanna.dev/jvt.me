{
  "date" : "2024-03-22T08:00:10.588620289Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8220" ],
    "start" : [ "2024-03-21T00:00:00Z" ],
    "end" : [ "2024-03-22T00:00:00Z" ],
    "published" : [ "2024-03-22T08:00:10.588620289Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/igvtk",
  "client_id" : "https://www-api.jvt.me/fit"
}
