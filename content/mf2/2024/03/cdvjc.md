{
  "date" : "2024-03-13T09:47:00+0100",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-03-13T09:47:00+0100" ],
    "category" : [ "go", "sqlite" ],
    "like-of" : [ "https://github.com/maragudk/goqite" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/cdvjc",
  "tags" : [ "go", "sqlite" ],
  "client_id" : "https://indiepass.app/"
}
