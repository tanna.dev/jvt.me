{
  "date" : "2024-03-21T14:46:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://666.glitchwit.ch/@kf/112134173173204852" ],
    "published" : [ "2024-03-21T14:46:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Ooh very cool! We've got [this wobbler bowl(https://www.petsandfriends.co.uk/products/lickimat-wobble-green-dog-treat-bowl-16cm) that our (2 year old) doggo uses and that slows her down a bit, but have found the [K9Connectables](https://k9connectables.com/en-uk) have been the best for slowing down food - but screw top on the Wobbl looks very good!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/03/1tcki",
  "client_id" : "https://indiepass.app/"
}
