{
  "date" : "2024-03-06T11:04:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-03-06T11:04:00+0000" ],
    "category" : [ "vim" ],
    "like-of" : [ "https://github.com/bfrg/vim-jqplay" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/03/smjxk",
  "tags" : [ "vim" ],
  "client_id" : "https://indiepass.app/"
}
