{
  "date" : "2024-03-08T08:00:10.171660361Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9847" ],
    "start" : [ "2024-03-07T00:00:00Z" ],
    "end" : [ "2024-03-08T00:00:00Z" ],
    "published" : [ "2024-03-08T08:00:10.171660361Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/rjvur",
  "client_id" : "https://www-api.jvt.me/fit"
}
