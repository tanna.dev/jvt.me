{
  "date" : "2024-03-23T08:00:09.971280339Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3300" ],
    "start" : [ "2024-03-22T00:00:00Z" ],
    "end" : [ "2024-03-23T00:00:00Z" ],
    "published" : [ "2024-03-23T08:00:09.971280339Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/fdcef",
  "client_id" : "https://www-api.jvt.me/fit"
}
