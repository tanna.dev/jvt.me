{
  "date" : "2024-03-20T08:00:11.664228068Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11363" ],
    "start" : [ "2024-03-19T00:00:00Z" ],
    "end" : [ "2024-03-20T00:00:00Z" ],
    "published" : [ "2024-03-20T08:00:11.664228068Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/li7id",
  "client_id" : "https://www-api.jvt.me/fit"
}
