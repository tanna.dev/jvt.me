{
  "date" : "2024-03-22T20:59:57.161592741Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://social.coop/@eb/112131777763689815" ],
    "published" : [ "2024-03-22T20:59:57.161592741Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thank you for the shame-driven reminder 😅\r\n\r\nI accidentally signed up for it over 4 years ago (and [have been meaning to add the links](https://gitlab.com/tanna.dev/jvt.me/-/issues/538)) but as Jan mentions, I've still been part of the ring, but now I've finally gotten around to adding them!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/03/pnftp",
  "client_id" : "https://editor.tanna.dev/"
}
