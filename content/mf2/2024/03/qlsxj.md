{
  "date" : "2024-03-31T08:00:11.181471845Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3447" ],
    "start" : [ "2024-03-30T00:00:00Z" ],
    "end" : [ "2024-03-31T00:00:00Z" ],
    "published" : [ "2024-03-31T08:00:11.181471845Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/03/qlsxj",
  "client_id" : "https://www-api.jvt.me/fit"
}
