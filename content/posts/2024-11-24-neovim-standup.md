---
title: "Creating a Neovim plugin for my standup updates"
description: "Documenting some of my thoughts about writing a (Python) Neovim plugin."
tags:
- blogumentation
- neovim
- agile
- python
date: 2024-11-24T20:34:42+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: neovim-standup
image: https://media.jvt.me/335b69f67f.png
---
As I noted earlier today, I love [the way my team does standups](https://www.jvt.me/posts/2024/11/24/async-standups/) in an asynchronous, and always-updating manner.

As I've started to keep a single comment on GitHub Discussions up-to-date - be that for my daily team standup updates, or my on-call handover notes - I've been spending a lot of time re-opening the Discussion page, finding my last comment, and updating it.

On Friday evening I had a thought - what if I could automate some of that?

Between bits of _Apex Legends_, I've ended up hacking a new project, [`standup.nvim`](https://gitlab.com/tanna.dev/hacking/standup.nvim) which makes it possible for me to speed up my updating of my standups.

This works for me, and I'm interested to see if it'll pay off as a useful way of keeping my notes up-to-date with a new <kbd>Super</kbd>+<kbd>Shift</kbd>+<kbd>s</kbd> keybinding.

## Building a Lua-based Neovim plugin

My first stab at this was to create a Lua-only Neovim plugin, as I didn't want to bother with Vimscript.

I found [some](https://miguelcrespo.co/posts/how-to-write-a-neovim-plugin-in-lua) [useful](https://m4xshen.dev/posts/develop-a-neovim-plugin-in-lua) posts to give me the basis of how to get started, but as I was looking into how I'd parse the URL from config, and realised that [regexes](https://stackoverflow.com/a/6192354) aren't implemented in Lua, and nor is an HTTP client.

## Building a (Python) Neovim plugin

So I then, reluctantly, - as I don't really enjoy Python - started writing a Python-based plugin, following [this post](https://mkaz.blog/code/neovim-plugin-python) as a starting point.

It took a bit of searching around to find the right incantations I needed to get it wired in "properly", and to work ut why I needed to run `:UpdateRemotePlugins`.

I then found the `pynvim` [Remote (new-style) plugins](https://pynvim.readthedocs.io/en/latest/usage/remote-plugins.html) docs which were a lot more helpful as a starting point, and then once I set up the right type annotations in the code, I managed to get a somewhat reasonable feedback loop.

It took a while to work out how to call `nvim_buf_set_lines` - via `nvim.funcs.nvim_buf_set_lines` - and some of the other Neovim-isms, but I got there in the end.

## Overall experience

Overall, it's been a slightly frustrating experience of generally flailing about while trying to work out what to do, refresh the basics of Python, provide myself a little bit of a means to validate changes + add debug printing while seeing what Neovim + Python would let me do, and I've collected dozens of tabs with questions!

But I'm glad I gave it a go, and opted for Python as it was easier to do a number of things than it would've been for Lua.

## Usage

What does it look like?

{{< asciicast id="neovim-standup" src="/casts/neovim-standup.json" >}}

You'll notice some rough edges:

- a slight flicker when writing the file, as it updates upstream, then re-modifies the local file
- that the buffer is marked as modified, regardless of whether it's been updated

I'm not super bothered right now about these, as it's just me using it, but maybe I'll try and fix them up in the future.
