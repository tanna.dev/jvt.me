---
title: "Why is `set -eu` not working?"
description: "Why you may be finding `set -u` in a shell script not exiting when `set -e` is also present."
date: 2024-01-05T12:02:26+0000
tags:
- "blogumentation"
- "command-line"
- bash
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: set-u-no-fail
---
If you write shell scripts, you may be familiar with the following header (or some variation) in your script:

```sh
set -euo pipefail
```

As noted in [Use Bash Strict Mode (Unless You Love Debugging)](http://redsymbol.net/articles/unofficial-bash-strict-mode/) this allows us to:

- `set -e`: exit the script on errors
- `set -u`: error when undefined variables are used
- `set -o pipefail`: fail when piped commands fail

I recently hit an issue where this didn't work where I expected it to, which was very surprising 😅

For a minimal reproduction, let's use:

```sh
#!/bin/bash
set -euo pipefail

export HI=$(echo $HELLO)
echo $HI

echo "END"
```

When we run this, we should expect to see an error and `END` not being printed, right? Well no, we actually see:

```
a.bash: line 4: HELLO: unbound variable

END
```

It turns out that this is a documented issue, and has a [Shellcheck warning, "Declare and assign separately to avoid masking return values"](https://www.shellcheck.net/wiki/SC2155) for this (which I have actually seen before, but haven't ever addressed 🫣).

Instead, if we make the following change to make sure we `export` separately to defining the variable:

```diff
diff --git a.bash new
index dd353d3..bae1789 100644
--- a
+++ b
@@ -1,7 +1,8 @@
 #!/bin/bash
 set -euo pipefail

-export HI=$(echo $HELLO)
+HI=$(echo $HELLO)
+export $HI
 echo $HI

 echo "END"
```

We now get the correct result (and an exit code):

```console
a.bash: line 4: HELLO: unbound variable
```
