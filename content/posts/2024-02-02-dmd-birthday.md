---
title: "Celebrating dependency-management-data's first birthday"
description: "Reflecting on the last year of the project."
tags:
- dependency-management-data
date: 2024-02-02T21:54:21+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: dmd-birthday
image: https://media.jvt.me/17033d02fd.png
series: dmd-birthday
---
It's officially been 1 year since the [first commit](https://gitlab.com/tanna.dev/dependency-management-data/-/commit/73a99614a2af6fa9f66508bab8541ed65e18ed66) to [dependency-management-data (DMD)](https://dmd.tanna.dev), from the humble beginnings of trying to answer ["what Open Source contributions should we do for Hacktoberfest"](https://www.jvt.me/posts/2022/09/29/roo-hacktoberfest-dependency-analysis/).

For those who aren't aware, I describe dependency-management-data as:

> Dependency Management Data (DMD) is a set of tooling to get a better understanding of the use of dependencies across your organisation.
> The project aims to give you a queryable interface into how Open Source and internal dependencies are used, so you can target changes across your projects and organisation more appropriately.

At the most minimal level - how it was originally developed - the project makes it possible to ingest a list of dependencies and allow you to query that data with SQL.

But the benefits of using DMD is that you can also, for instance, determine where you're using software that is deprecated/unmaintained. Not only does this take in data from the wider Open Source world, including community-sourced data, but you can add your own input, which allows you to flag up uses of `internal-spring-boot-library` as unmaintained, or "don't use <= v1.2.3 of `security-library` as it doesn't actually validate input". Some of these features, and the ability to bring your own data is better than any of the offerings I've seen in any of the major competing enterprise SCA platforms.

I'd also say that it has honestly been a game changer for me as I've been able to answer questions and solve issues that before I couldn't ever easily do before. The visibility the data gives allows asking and answering some pretty useful questions - don't believe me? Check out [some of the case studies](https://dmd.tanna.dev/case-studies/) - and even led to a lil' pivot by one of the platform teams at Deliveroo, who could suddenly work on a piece of enablement that just wasn't feasible without understanding how Terraform modules were used across the org.

But enough of the sale-y pitch, I wanted to reflect on what this last year has been like.

The first thing to say is that I've really enjoyed working on it - it's absolutely been a passion project, and has not only kept my attention (despite my ADHD) but it's been super rewarding.

I've loved seeing the growth in the project, by changes I've introduced based on a colleague saying "hey, what if ..." or "I wish we could ...", and feeling like I've been fairly product-led. I've focussed on the issues that have been immediately requested, rather than trying to just work on any old issue on the backlog, including waiting several months before I come to implement them to give me time to hone my understanding of how we can best solve it.

It's also been a really great long-running project to really get stuck into, and there's been so many new features to add, as well as giving me a chance to play around with [sqlc](https://sqlc.dev/) and SQLite in much more depth, not to mention working with concurrency in Go, some Typescript (with [renovate-graph](https://www.jvt.me/posts/2022/11/01/renovate-dependency-graph/)) as well as seeing first-hand the mistakes I've made and having to introduce breaking changes to fix incorrect assumptions or poor naming schemes.

I've also worked on better implementing technical documentation - of which there is still a way to go, but I'm feeling so much happier with the current documentation than even where it was a few months ago, let alone at the beginning! - and the way that I've started moving towards a [Diátaxis](https://diataxis.fr/) style approach. Working across multiple companies with the project and data has also been useful for different perspectives, as well as folks with differing levels of SQL knowledge and comfort.

Speaking of SQL, I've learned a lot more than I would've expected I'd have been able to in the last year, including learning that you can do if statements in SQL (via `CASE`) and I've had a fair share of learnings about how to get an SQL statement to work with a standard SQLite browser _and_ sqlc, which doesn't always support all of the things.

One thing I've noticed is that I've definitely been blogging a bit less this last year, and I definitely attribute it to the fact that a lot of my spare time in evenings/weekends has been spent on working on DMD, and shipping new features or things that I could then use the next day at work.

I've also enjoyed being able to do some work on DMD on work time, where appropriate, making it a nice blend between a personal and work project.

It's also been interesting trying to grow the project, thinking about a bit about outreach, community building and how to explain the project to people as well as make it usable. It's been fun seeing how hard this can be both as a stranger on the Internet saying "use my project!" as well as being inside a company and saying "I'm your colleague, and you should use my project!"

I thought it may be interesting to look at some stats from the last year, too:

<table>
<tr>
<td>
Number of commits
</td>
<td>
781
</td>
</tr>

<tr>
<td>
Number of releases
</td>
<td>
121
</td>
</tr>

<tr>
<td>
Most commits
</td>
<td>
on 23rd September 2023, with 24 commits!
</td>
</tr>

<tr>
<td>
Most releases
</td>
<td>
a tie between the 1st January 2024 and 23rd September, with 4 releases
</td>
</tr>

<tr>
<td>
Number of companies (definitely) using it
</td>
<td>
4
</td>
</tr>

<tr>
<td>
Number of meetups spoken at
</td>
<td>
1
</td>
</tr>

<tr>
<td>
Number of conferences spoken at
</td>
<td>
2
</td>
</tr>

<tr>
<td>
Number of upcoming talks
</td>
<td>
2
</td>
</tr>

<tr>
<td>
Number of issues closed
</td>
<td>
193
</td>
</tr>

<tr>
<td>
Number of currently open issues
</td>
<td>
258
</td>
</tr>

<tr>
<td>
Number of external contributions
</td>
<td>
1
</td>
</tr>

<tr>
<td>
Number of job offers, in particular due to my work on DMD
</td>
<td>
1
</td>
</tr>

<tr>
<td>
Number of days in the year with commits to DMD
</td>
<td>
133
</td>
</tr>

</table>

I'd also love to say I had numbers of how many repos and dependencies I've scanned, but unfortunately don't have that to hand.

And finally, I've been considering what it could look like if it were to be something I would make a paid offering around this. There's definitely some use-cases where a hosted service or paid enterprise-y features would be worthwhile, as well as consulting, and I'll be thinking about whether it's something to pursue, or if it should stay as Open Source forever - especially as there are a number of other bigger projects (backed by companies) who are doing similar things.

I've been very impressed and happy with how I've got on over the last year, and am looking forward to seeing where things go in the next year 🚀
