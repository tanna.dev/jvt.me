---
title: "The joy of an asynchronous, always updating standup"
description: "Describing how my team does standups across a globally distributed team."
tags:
- agile
- scrum
- elastic
- adhd
date: 2024-11-24T18:35:14+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: async-standups
image: https://media.jvt.me/f2182c17db.png
---
There are several things I really love about working in my team at <span class="h-card"><a class="u-url" href="https://elastic.co">Elastic</a></span>, but one of the things I really enjoy is how we run our standups in my team.

Working in a globally distributed, asynchronous-first team with team members across three continents, and ~8 timezones is that, as noted in [_Making it easier to schedule cross-timezones, with the `tz` CLI_](https://www.jvt.me/posts/2024/05/15/tz/), it's hard to try and find times that a good percentage of the team will be able to attend, and impossible to find any friendly time that'll work for everyone.

As a company, Elastic is heavily built around the culture and ways of working asynchronous-first, and so there are several approaches as an organisation as to how to handle previously synchronous events like a standup.

The first team I joined at Elastic used a daily Slack app to ask the generic "what you did yesterday", "what are you working on today" and "are there any blockers". It was _fine_ but didn't really seem to gel super well.

In March, there was a reorg in Engineering Productivity, which meant that we were mixed up with new folks, and given a chance to rethink how we wanted to work as a team and collaborate better, with a new set of folks. It was here that during our Engineering All Hands meetup in Prague, we started considering what "ways of working" approaches we wanted to take as a newly formed team.

One of the teams that we merged in from had some good practices around their standups - an asynchronous standup that's situated in GitHub Discussions - and this seemed to work really well for them, so we thought we'd try it as a new team.

With this approach, we have a GitHub Discussion per month, and then there'll be a top-level entry for each day, under which team member give their update(s):

![A view of a GitHub Discussion, where there's a top-level message "November 24th" with two sections - "News", in which Jamie is letting the team know about some upcoming time he won't be available for - and "Action requested from other team members" to have specific calls to action. Below this, there are two threaded comments from Jamie and a fake account (for this contrived example) where they are sharing differing levels of detail for their standup updates. It makes it clear that the team is not one homogeneous blob, but are different people with different styles](https://media.jvt.me/f2182c17db.png)

This has continued to be a great choice, and has been working very well for the team. The idea is that our JIRA tickets are still up-to-date - for external stakeholders / our manager - but that the internal standup log can be in as much or as little depth as it needs to be.

As can be expected, I really enjoy the opportunity to be verbose, and over time have been thoroughly enjoying the asynchronous nature of it, being able to add updates throughout my day rather than being bound by a point-in-time view, and make it much more content rich than the previous standup updates could be through the Slack app.

As we're using GitHub Discussions for this, we can take advantage of auto-linking to JIRA, prettified links to GitHub Issues/PRs, and even drop in GIFs or use GitHub Flavoured Markdown improvements like [further emphasis in blockquotes](https://github.com/orgs/community/discussions/16925), or [using the `<details>` tag](https://www.jvt.me/posts/2019/05/19/html-toggle/) to hide extra verbose context.

You'll notice that this is _not_ like a common standup, where it's meant to be very focussed - and this is one I really enjoy about it. I'm a big fan of a focussed synchronous standup, where the right updates are being given, it's clear whether there are projects or goals at risk, and that it's an expensive meeting that we want to keep focussed. But when you're able to expand on your updates in a way like this, it's really freeing

One problem is that providing the right signal-to-noise ratio can be difficult and at odds with the verbosity, so is something we're still working on.

Because it can be less clear how to see blockers, in the updates (unless more explicitly called out) it's important to make sure that any blockers are raised early and often, and that you escalate problems either through the standup discussion, or more likely, separately through other mediums.

As someone with ADHD, I've honestly found this standup process freeing, as it allows me to brain dump into GitHub, periodically, and then clean it up afterwards in a way that can then be usable, improving my focus and allowing me to focus on a high-level update and an in-depth.

A nice thing you'll notice is that different team members have their own ways of writing their updates, and on Friday a new team member had asked about how folks approach their standup updates. I'd noticed that previously I've been adding fresh comments every time I came to do another update, but after feedback from team member Isaac, I've started to keep a single comment updated. Some of the team will only post their updates at the end of the day, and some like me will do them up-front and add more updates.

It's a great pattern that seems to be working well across the team, and is a good source for each of us to see what others are doing, as well as making sure that our manager gets the insights they need.

This is also something we use within our on call rotation, to use as a means to perform our asynchronous shift handovers. For example:

![A slightly different GitHub discussion view, where (a contrived example) is that Jamie's shift handover provides context for "ongoing cases" (using an "important" blockquote to highlight these are being handed over), "support log" for all work done during the shift, and then an area for giving suggestions to improve the on-call experience](https://media.jvt.me/49d6506944.png)

I'm not sure if this is something that'll work for every company, but I love this as a practice, and recommend you give it a go - especially as an option to go hand-in-hand with a [meandering sync](https://www.honeycomb.io/blog/standup-meetings-are-dead).
