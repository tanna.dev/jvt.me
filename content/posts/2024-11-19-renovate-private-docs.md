---
title: "Creating a private mirror of the Renovate docs"
description: "How to set up a copy of Renovate's documentation, pinned to an older version, for your organisation."
date: 2024-11-19T15:37:46+0000
tags:
- "blogumentation"
- "renovate"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/6657d4ff89.jpeg"
slug: renovate-private-docs
---
If you're self-hosting Renovate, you're generally going to be out-of-date compared to the upstream project's releases.

This isn't just because it takes time to update, but because Renovate as a project is _constantly shipping_ 🚀🚀🚀

I've found it's more noticeable when you're running Mend Renovate Community/Enterprise Edition, which purposefully pin to a more stable version of the OSS project, and then updates regularly, which means there's generally a bit more of a lag between updates.

(You _could_ pull `@latest`, but why live life on the edge?)

So one thing you may encounter is that - depending on how far behind you are on updates - you're reading documentation on [the public docs site](https://docs.renovatebot.com/) that doesn't match your version of Renovate.

Hitting this a few times recently, I decided to set up a (private) mirror of the Renovate docs for the organisation, so we had an internal mirror that would tie _directly_ to the version that we're using.

You may ask why a private repo / mirror of the docs? Well the first thing is to avoid impacting the OSS project's Search Engine Optimisation (SEO) by avoiding a set of duplicate content on the public Web.

A second reason is that when attempting to fork the repo "properly" and enabling GitHub Pages we see the following error:

![A screenshot of a subset of the GitHub Pages settings for a fork of the Renovate docs project's repo, noting that "Pages on this forked repository is disabled due to a policy enforced by the owner of the parent repository."](https://media.jvt.me/daf6f32e28.png)

(There's discussion on the Renovate Slack to see if that's intended)

As I set this up yesterday, and it required a little work, I thought I'd [write it up as a form of blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/).

## Determining the revision

First you want to determine the version of Renovate you are running, and work out what commit in the [Renovate docs site](https://github.com/renovatebot/renovatebot.github.io) this corresponds to.

Because the docs site changes over time, we're not necessarily going to be able to take the current version of the docs site's build tooling and use it against an old version of the Renovate project's docs.

As a somewhat arbitrary version, let's say that we're using [Renovate 37.129.0](https://github.com/renovatebot/renovate/releases/tag/37.129.0) internally.

We would need to find which commit SHA the Renovate docs site was at when it bumped to that version of Renovate's docs - or at least, as close as possible.

This is needed to make sure that the build tooling and all its dependencies are aligned with what worked at the time.

In this case, the docs site didn't _directly_ upgrade to Renovate 37.129.0, but bumped from [`0b3b56a` aka 37.128.4](https://github.com/renovatebot/renovate/commit/0b3b56a) to `5210010` (a non-existent commit upstream) to [`fc85d65` aka 37.129.1](https://github.com/renovatebot/renovate/commit/fc85d654) (skipping 37.129.0).

We can see this when viewing `git log -- deps/renovate` in the docs site, and correlating these commit hashes with commits in the Renovate repo:

```
commit eafcade47a39a17333d09af0cd48b81a6349d36f
Author: renovate[bot] <29139614+renovate[bot]@users.noreply.github.com>
Date:   Fri Jan 12 08:20:57 2024 +0000

    chore(docs): update deps/renovate digest to fc85d65

commit af85dd913198c1c70d45176bdb4e667fd72afb68
Author: renovate[bot] <29139614+renovate[bot]@users.noreply.github.com>
Date:   Fri Jan 12 02:00:11 2024 +0000

    chore(docs): update deps/renovate digest to 5210010

commit 789f1965a4a228697f29364917e9bf930e08e658
Author: renovate[bot] <29139614+renovate[bot]@users.noreply.github.com>
Date:   Thu Jan 11 10:41:22 2024 +0000

    chore(docs): update deps/renovate digest to 0b3b56a
```

If you had picked a version that aligned with a commit exactly, you'd select that commit directly i.e. if 37.129.1 is the version you wanted, you'd use the commit hash `fc85d65`.

However, in this case, we should select commit `af85dd913198c1c70d45176bdb4e667fd72afb68`, as the earlier commit, which means the repo is likely closer in configuration.

## Create our "fork"

Next, we need to create our "fork" of the repo.

(The usage of "scare quotes" around "fork" is because this isn't a fork according to GitHub, but it is a technical fork of the repository)

I'd recommend creating this (private) repo first, via ClickOps or the `gh` CLI.

Then:

```sh
# from above
export RENOVATE_DOCS_SHA=af85dd913198c1c70d45176bdb4e667fd72afb68
git clone https://github.com/renovatebot/renovatebot.github.io internal-renovate-docs
# make sure we're down to the version of the docs site we want
git reset --hard $RENOVATE_DOCS_SHA
```

Next, you want to push this up to your new repo.

## GitHub repo settings

Before we can call our docs complete, we need to need to make sure that the repository is correctly set up:

- Enable GitHub Actions
  - From the `/actions` page by clicking `Enable Actions on this repository`
- Enable GitHub Pages
  - From the `/settings/pages` page
  - Set the `Source` to `Deploy from a branch`
  - Set the `Branch` to `gh-pages`
    - This likely won't be set until the first deploy has pushed the branch
  - Set the `Folder` to `/ (root)`

## Code changes

Additionally, there are a few changes that you need to make in the docs site's code:

- Delete the `src/CNAME`
  - So GitHub Pages doesn't try deploying to the official site
- Disable Renovate managing updates (`renovate.json`)
  - As we don't want to keep things up-to-date (because we're pinned to a version of Renovate)
- Updating the banner (`overrides/main.html`)
  - I.e. to provide additional context for your users to indicate it's an internal mirror, or the Renovate version that the docs are pointing to
- Updating MKDocs configuration (`mkdocs.yml`, not really necessary)
  - To update `repo_name`, `repo_url` and `site_url`, so embedded URLs i.e. to "edit this page" work

## Push

Once you've completed these changes, you can now push the changes, and that should trigger a GitHub Actions build that will then trigger GitHub Pages.

You may need to go in and update the GitHub Pages settings to publish from the `gh-pages` branch at this point, now it's pushed.

You'll now have your own internal copy of the documentation that aligns with your Renovate version and you can update as-and-when you're ready to 🚀
