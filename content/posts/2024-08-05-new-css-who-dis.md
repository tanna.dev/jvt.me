---
title: "New CSS, Who Dis? (2024 edition)"
description: "Announcing my new site design."
tags:
- www.jvt.me
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
date: 2024-08-05T18:12:43+0100
slug: "new-css-who-dis"
image: https://media.jvt.me/d423310675.png
---
It's been just over four years since [my last significant site redesign](https://www.jvt.me/posts/2020/06/11/new-css-who-dis/), and today I'm releasing a new site update.

This isn't as significant an update, and retains the layout that I've been using since then, but replaces a lot of the theming.

In recent years I've been deprioritising my personal website (and the many [IndieWeb](https://indieweb.org)-y things I want to do with it), both because I have [far too many other projects](/open-source/) as well as life stuff, and far too many hours in the day.

This also hasn't been helped by the fact that my site takes _an age_ to build. Over the weekend I did a significant upgrade from an ~8 year old SSD to a new M.2 SSD, and my site _still_ takes ~104 seconds to build. Hugo's known to be blazing fast, so if even that struggles with my site, I know I'm in trouble.

<details>

<summary><code>hugo</code> output for building my site</summary>

```
% ./hugo
Start building sites …
hugo v0.100.1-0afb4866e345d31cbbcbab4349e43f1d36122806+extended linux/amd64 BuildDate=2022-06-01T10:11:48Z VendorInfo=gohugoio

                   |  EN
-------------------+--------
  Pages            | 84049
  Paginator pages  |  2769
  Non-page files   |     2
  Static files     |    45
  Processed images |     0
  Aliases          |  2674
  Sitemaps         |     1
  Cleaned          |     0

Total in 103442 ms
./hugo  241.77s user 15.77s system 248% cpu 1:43.67 total
```

</details>

I've thought over the last couple of years that "I won't invest any time in my personal site, as I'll rebuild it soon", as I've wanted to migrate from a static site to a dynamic site. But that's never made it up the very long TODO list, so I thought that I should make some minor tweaks over the weekend, which are live as of this post landing.

Appearing on [Go Time](https://changelog.com/gotime) tomorrow provided a bit of a nudge towards getting the site redesign finished, after starting it ~6 weeks ago.

## `CHANGELOG`

The new site involves a few changes:

- Migrating to the colour scheme I'm using in all my other tools
- Re-introducing code block segments that fit the width of the page, but the code still sits within a readable portion (which I internally refer to as "max-width'd")
- Using the Atkinson Hyperlegible font
- Cleaning up some _wildly_ inconsistent `font-size`s for `<h1>`/etc elements
- Rethinking the colours used by each part of my site (+ using CSS variables)

## Colour scheme

I've been officially migrated to using the [Srcery](https://srcery.sh) theme in my Neovim and terminal setups since [January](https://gitlab.com/jamietanna/dotfiles-arch/-/commit/a1106be758601d4799419b25a3d6b4044e77f845), and I'd been running it for a few weeks leading up until that point. I really like my site being consistent look + feel to what I run locally, and so it's been bugging me for _months_ that it's inconsistent with what I see in my terminal. In recent months I've modified [my CV](https://hire.jvt.me/) and the [dependency-management-data](https://dmd.tanna.dev) website to use the colour scheme, and I'm glad it's now finally consistent here.

## Code blocks

Something I've missed from my site (pre-2020 redesign) was the way that code blocks introduced a visual break in the view, which you can see more clearly below.

Before:

![A before view of the website, with the Gruvbox colour scheme, and a mix of colours, with the `<h2>` element the same size as the main body text](https://media.jvt.me/635de4b790.png)

After:

![An after view of the website, with the Srcery colour scheme, and a mix of colours, with the `<h2>` element a considerably different size than the text, and code blocks extending to the width of the screen](https://media.jvt.me/d423310675.png)

It's been noted that it may not be the most accessible at times, so I'll have to take that under advisement.

## Extended Atkinson Hyperlegible

Despite personally preferring to read monospace fonts, I realise that's not everyone's view, so I'm also using this as an opportunity to migrate to a nice sans-serif font.

I've been a fan of the Atkinson Hyperlegible font since <span class="h-card"><a class="u-url" href="https://shkspr.mobi">Terence Eden</a></span> first mentioned it in his post, [_An update to the Atkinson Hyperlegible font_](https://shkspr.mobi/blog/2022/08/an-update-to-the-atkinson-hyperlegible-font/).

I've added it to my CV in the past, and as part of this release took the opportunity to use it for all non-code content.

## Feedback

Like it? Hate it? Ambivalent? Found something buggy? Lemme know.
