---
title: "Introducing insight into your dependencies' health in dependency-management-data"
description: "How you can use the new dependency health functionality to better understand your dependencies."
tags:
- dependency-management-data
date: 2024-01-27T21:02:50+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: dmd-dependency-health
image: https://media.jvt.me/0ce655e6df.png
---
In the last couple of days I've been working on providing more metadata about dependencies into dependency-management-data, so you can make more intentional decisions around how you think about your dependency tree.

This came out of an interest in getting some insight into gauging how maintained dependencies are, as well as determining if there are gaps in managing supply chain security risks.

As part of [the v0.76.0 release](https://gitlab.com/tanna.dev/dependency-management-data/-/releases/v0.76.0) of dependency-management-data, it's now possible to get insight into:

- Metadata around the repo that underpins the dependency, with information about the repo that the package is maintained at, such as the last push to the default branch by a contributor, and how recently any releases have been shipped, via the excellent [Ecosyste.ms](https://ecosyste.ms)
- [OpenSSF Security Scorecards](https://securityscorecards.dev/) data

With this, any dependencies that have a Maintained Scorecard score of 0, have an archived repo backing the package, or that are marked as deprecated in their package manager will now be marked as `UNMAINTAINED` advisories in dependency-management-data, making it easier to surface.

And you can now [use that data in Policies](https://dmd.tanna.dev/cookbooks/custom-advisories-opa/#dependency-health-data), too, to codify your organisational risk policies, and flag cases where you aren't as comfortable with dependencies, i.e. "we don't want to use any dependencies that don't have signed releases".

I'm looking forward to improve the data that dependency health can surface, including sourcing [which of your packages are looking for funding](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/235).

I'm also very appreciative of the work that <span class="h-card"><a class="u-url" href="https://nesbitt.io/">Andrew Nesbitt</a></span> has done on Ecosytems!
