---
title: "Utilising Renovate's `local` platform to test more easily"
description: "How to use Renovate's `local` platform for validating configuration changes more easily."
date: 2024-09-16T20:18:06+0100
tags:
- "blogumentation"
- "renovate"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/6657d4ff89.jpeg"
slug: renovate-local
---
Last year I wrote about how [you can use Renovate's `local` platform to make `renovate-graph` more efficient](https://www.jvt.me/posts/2023/10/13/renovate-graph-local/), which has been very useful for work I've been doing with [dependency-management-data](https://dmd.tanna.dev).

What I _didn't_ do was also write about how you're able to use Renovate's[`local` platform](https://docs.renovatebot.com/modules/platform/local/) to test out new Renovate configuration.

As I've recently been doing a fair bit of it, I thought I'd make sure I had my workflow [written up as a form of blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/).

When you're testing new Renovate rules or configuration against an existing repo, one thing that can be a little noisy is that you'll have a lot of other dependencies being detected and updates being checked for.

One of the nice things about the `local` platform is that it doesn't need to run against your massive monorepo (or even a small repo), but you can actually create a fresh directory to test things in, and purely focus on the file(s) you care about.

You can of course do this with plain Renovate, i.e. running against `--platform github` or `--platform gitlab` by modifying the [`includePaths`](https://docs.renovatebot.com/configuration-options/#includepaths) or similar, but it's a bit more work compared to a completely fresh directory.

For instance, let's say that we want to [use Renovate to manage updates to `golangci-lint` versions](https://www.jvt.me/posts/2022/12/15/renovate-golangci-lint/), where we have the file `build/Makefile` which has a snippet such as:

```make
$(GOBIN)/golangci-lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(GOBIN) v1.50.1
```

What we can do to test this with Renovate's `local` platform is:

```sh
cd /path/to/repo

# create a new temporary directory for us to test Renovate in
mkdir tmp
cd tmp

# Optional, but recommended: make a temporary Git repo for this testing, as otherwise Renovate could detect the parent repo's configuration
# Alternatively, move this to a location that doesn't have a parent Git repository (which is an exercise for the reader)
git init

# make sure our repo's copy of the Renovate configuration is used
# NOTE that this may be in another location i.e. `.gitlab/renovate.json`
# NOTE that this should also remove any `local` extends and replace that with the relevant platform you host on, if that config is needed
ln -s ../renovate.json

# copy in `build/Makefile`
mkdir build
# this is relative to the directory we're in, so needs to be double-layered
ln -s ../../build/Makefile build

# Optional, but recommended: make a temporary Git repo for this testing, as otherwise Renovate could detect the parent repo's configuration
# NOTE that if you're doing this, you'll then need to `git add` all files that you want Renovate to look for
git add .
git commit -m "init" # or anything you want, https://www.jvt.me/posts/2024/07/12/things-know-commits/ probably doesn't take effect here
```

Now we've set things up, we can run:

```sh
# NOTE that ideally you'd pin this to a specific version of Renovate
env LOG_LEVEL=debug npx renovate@38 --platform=local
```

This will then output a fair bit on the `DEBUG` log level, but the most important pieces are:

```
 INFO: Dependency extraction complete (repository=local)
       "stats": {
         "managers": {"regex": {"fileCount": 1, "depCount": 1}},
         "total": {"fileCount": 1, "depCount": 1}
       }
...
DEBUG: packageFiles with updates (repository=local)
       "config": {
         "regex": [
           {
             "deps": [
               {
                 "depName": "github.com/golangci/golangci-lint",
                 "currentValue": "v1.50.1",
                 "datasource": "go",
                 "replaceString": " sh -s -- -b $(GOBIN) v1.50.1\n",
                 "updates": [
                   {
                     "bucket": "non-major",
                     "newVersion": "v1.61.0",
                     "newValue": "v1.61.0",
                     "releaseTimestamp": "2024-09-09T14:33:19.000Z",
                     "newMajor": 1,
                     "newMinor": 61,
                     "newPatch": 0,
                     "updateType": "minor",
                     "branchName": "renovate/github.com-golangci-golangci-lint-1.x"
                   }
                 ],
                 "packageName": "github.com/golangci/golangci-lint",
                 "versioning": "semver",
                 "warnings": [],
                 "sourceUrl": "https://github.com/golangci/golangci-lint",
                 "currentVersion": "v1.50.1",
                 "currentVersionTimestamp": "2022-10-22T10:48:48.000Z",
                 "isSingleVersion": true,
                 "fixedVersion": "v1.50.1"
               }
             ],
             "matchStrings": [
               "curl .*https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- .* (?<currentValue>.*?)\\n"
             ],
             "depNameTemplate": "github.com/golangci/golangci-lint",
             "datasourceTemplate": "go",
             "packageFile": "build/Makefile"
           }
         ]
       }
...
 INFO: Repository finished (repository=local)
       "cloned": undefined,
       "durationMs": 1490
```

From here, we can see that Renovate's correctly detected the `golangci-lint` version, and noticed that there's a minor update that's necessary to update.

As we want to iterate over the rules, we'll be able to modify the `renovate.json` and then re-run `npx renovate` to re-validate the detected dependencies (and relevant updates) are correct, which is much quicker than needing to test against a GitHub/GitLab repo!
