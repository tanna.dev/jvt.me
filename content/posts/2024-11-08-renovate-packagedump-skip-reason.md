---
title: "Summarising the `skipReason`s for Renovate data exports"
description: "How to work out what `skipReason`s you have for your Renovate package data."
date: 2024-11-08T18:22:48+0000
tags:
- "blogumentation"
- "renovate"
- dependency-management-data
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/6657d4ff89.jpeg"
slug: renovate-packagedump-skip-reason
---
When working with package data dumps from Renovate - regardless of the source - it can be interesting to see which packages haven't found updates.

This can be worked out using the `skipReason` JSON field, for instance with the below `renovate-graph` output:

```json
{
  "metadata": {
    "renovate": {
      "major": 38,
      "platform": "github",
      "version": "38.130.2"
    }
  },
  "organisation": "renovatebot",
  "packageData": {
    "github-actions": [
      {
        "deps": [
          {
            "autoReplaceStringTemplate": "{{depName}}-{{newValue}}",
            "currentValue": "latest",
            "datasource": "github-runners",
            "depName": "ubuntu",
            "depType": "github-runner",
            "packageName": "ubuntu",
            "replaceString": "ubuntu-latest",
            "skipReason": "invalid-version",
            "updates": [
            ]
          }
        ],
        "packageFile": ".github/workflows/build.yml"
      }
    ]
  }
}

```

It can be useful to view at-a-glance which of your dependencies are not being updated, for whatever reason.

To do this, we could use the following JQ script:

```sh
.packageData |
    to_entries[] |
    .key as $k |
    .value[] |
    .packageFile as $pf |
    .deps[] |
    select(.skipReason != null) |
    "\(.skipReason): \($pf) \($k)/\(.packageName)@\(.currentValue)"
```

I'm not particularly `jq`-savvy, so this was a fun one to try and work out. Recommendations for improvements are very welcome!

This outputs:

```sh
# against https://gitlab.com/tanna.dev/dependency-management-data-example/-/blob/main/renovate/github-renovatebot-renovate.json
jq -r '.packageData | to_entries[] | .key as $k | .value[] | .packageFile as $pf | .deps[] | select(.skipReason != null) | "\(.skipReason): \($pf) \($k)/\(.packageName)@\(.currentValue)" ' renovate/github-renovatebot-renovate.json
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/build.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/codeql-analysis.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/dependency-review.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/devcontainer.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/label-actions.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/lock.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/mend-slack.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/scorecard.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/trivy.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/undesirable-test-additions.yaml github-actions/ubuntu@latest
invalid-version: .github/workflows/update-data.yml github-actions/ubuntu@latest
invalid-version: .github/workflows/ws_scan.yaml github-actions/ubuntu@latest
file: package.json npm/@renovate/eslint-plugin@file:tools/eslint
invalid-value: package.json npm/ignoredOptionalDependencies@null
invalid-value: docs/usage/examples/opentelemetry.md regex/renovate/renovate@latest
invalid-value: docs/usage/release-notes-for-major-versions.md regex/renovate/renovate@latest
```

This is something that I may end up constructing into a separate CLI, `renovate-packagedump-summary`, when finished with  `renovate-packagedump-diff`, as this - and other key metadata - can be useful to know.

I'll also want to report this in a slightly nicer way, as well as group duplicate entries by file or by package name.
