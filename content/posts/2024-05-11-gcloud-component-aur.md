---
title: "Installing Google Cloud CLI components on Arch Linux"
description: "How to install `gcloud` components using `pacman`, instead of using the officially supported method."
tags:
- blogumentation
- arch-linux
- google-cloud
- serverless
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
date: 2024-05-11T17:13:03+0100
slug: gcloud-component-aur
image: https://media.jvt.me/e72d135c21.png
---
I've recently been working with Google Cloud more, and so while developing a Cloud Function locally, I naturally tried to follow [the official local development (with Pub/Sub) guide](https://cloud.google.com/functions/docs/local-development).

As I'm ([btw](https://knowyourmeme.com/memes/btw-i-use-arch)) an Arch user, I've set up the `gcloud` CLI using [the `google-cloud-cli` AUR package](https://aur.archlinux.org/packages/google-cloud-cli) so when I went to run:

```sh
gcloud beta emulators pubsub start \
    --project=abc \
    --host-port='localhost:8043'
```

I was unfortunately hit with the following error:

```
ERROR: (gcloud.components.install) You cannot perform this action because this Google Cloud CLI installation is managed by an external package manager.
Please consider using a separate installation of the Google Cloud CLI created through the default mechanism described at: https://cloud.google.com/sdk/
```

It's a reasonable decision by Google not to bundle _all the possible things_ you may want into the CLI installation, so they use [Components](https://cloud.google.com/sdk/docs/components) as a way to install additional functionality separately.

However, it was not immediately obvious how to handle this - in particular, as I didn't want to use their non-package-manager-based installation, and got me rather frustrated 😅

But through a fair bit of trial and error, and inspiration from [other packages such as `google-cloud-sdk-datastore-emulator`](https://github.com/sudoforge/pkgbuilds/blob/9ebdf9bdc55c1b804949ec752fa5eb54047cf6bf/google-cloud-sdk-datastore-emulator/PKGBUILD) and the [`google-cloud-cli-firestore-emulator` package](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=google-cloud-cli-firestore-emulator), I was able to determine that I was looking for the file:

```
https://dl.google.com/dl/cloudsdk/release/downloads/for_packagers/linux/google-cloud-cli-pubsub-emulator_474.0.0.orig.tar.gz
```

Inside this archive is:

```
google-cloud-sdk/.install/.download/
google-cloud-sdk/.install/pubsub-emulator.manifest
google-cloud-sdk/.install/pubsub-emulator.snapshot.json
google-cloud-sdk/platform/pubsub-emulator/bin/cloud-pubsub-emulator
google-cloud-sdk/platform/pubsub-emulator/bin/cloud-pubsub-emulator.bat
google-cloud-sdk/platform/pubsub-emulator/lib/cloud-pubsub-emulator-0.8.14-all.jar
```

Therefore, following the examples of other component package installs, I was able to craft the following `PKGBUILD` that installs the Pub/Sub emulator:

```sh
# Adapted from https://github.com/sudoforge/pkgbuilds/blob/9ebdf9bdc55c1b804949ec752fa5eb54047cf6bf/google-cloud-sdk-datastore-emulator/PKGBUILD andhttps://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=google-cloud-cli-firestore-emulator
pkgname=google-cloud-cli-pubsub-emulator
# NOTE you'll need to keep this version bumped alongside the `gcloud` CLI version
pkgver=474.0.0
pkgrel=1
# TODO: this should probably have a better description
pkgdesc='TODO Pubsub'
arch=('x86_64')
# TODO: this should have a URL
url='TODO'
license=('Apache-2.0')
depends=('google-cloud-cli' 'java-runtime')
options=('!strip')
source=("https://dl.google.com/dl/cloudsdk/release/downloads/for_packagers/linux/${pkgname}_${pkgver}.orig.tar.gz")
sha256sums=('SKIP')

package() {
    cd "$srcdir/google-cloud-sdk"  # Does not match naming convention google-cloud-cli

    # Install component manifest and snapshot
    manifests=(
        "pubsub-emulator.manifest"
        "pubsub-emulator.snapshot.json"
    )
    for item in ${manifests[@]}; do
        install -Dm644 \
            ".install/${item}" \
            "${pkgdir}/opt/google-cloud-cli/.install/${item}"
    done

    # note the trailing, un-quoted `/*` to ensure we glob
    cp -r "${srcdir}/google-cloud-sdk"/* "${pkgdir}/opt/google-cloud-cli"

    # Remove unneeded files
    rm "${pkgdir}/opt/google-cloud-cli/platform/pubsub-emulator/bin/cloud-pubsub-emulator.bat"
}
```

You'll notice that - for my personal usage - I'm not particularly bothered by some of the package metadata.

This then allows me to invoke:

```sh
gcloud beta emulators pubsub start \
    --project=abc \
    --host-port='localhost:8043'
```

And I'll get a running Pub/Sub emulator 👏
