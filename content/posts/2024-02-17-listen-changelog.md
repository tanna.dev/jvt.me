---
title: "You should listen to The Changelog"
description: "Why you should really be adding The Changelog (and its network of podcasts) to your rotation of tech podcasts."
date: 2024-02-17T17:58:12+0000
tags:
- podcast
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: listen-changelog
image: https://media.jvt.me/3f34a50dc2.png
---
This is a post I've been planning on writing for a while now, and with [my first appearance on a podcast in Changelog & Friends](https://changelog.com/friends/31), I thought it was as good a time as anyway.

(But note that this is not at all sponsored, just my own thoughts!)

As noted in the episode, the first time I listened to The Changelog was [in 2016](/mf2/2016/08/unc6l), at least according to the records I have from my old phone's Podcast Addict backup. I've been listening to it over the years since, and absolutely loving the quality and breadth of topics, giving me a chance to keep engaged and up-to-date with happenings in the tech industry.

For those that aren't aware, [The Changelog](https://changelog.com/) is a network of podcasts which has grown out from the original podcast, [Changelog Interviews](https://changelog.com/podcast). As part of the network, there are a number of other podcasts they make, and in recent years, almost every episode of Changelog Interviews, Changelog and Friends, Ship It! and Go Time being part of my core rotation of podcasts to listen to, every week.

As noted in my episode, I'm part of [the IndieWeb community](https://indieweb.org/why) and as part of this, one of the things I do is track the history of podcasts I listen to, which means I can then look at the following breakdown of my listening history:

<table>
<tr>
<th>
Podcast
</th>
<th>
Number of episode listens
</th>
</tr>
  <tr>
  <td>
  <a href=https://changelog.com/podcast>Changelog Interviews</a>
  </td>
  <td>
  128
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/gotime>Go Time</a>
  </td>
  <td>
  114
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/shipit>Ship It!</a>
  </td>
  <td>
  53
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/friends>Changelog and Friends</a>
  </td>
  <td>
  26
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/jsparty>JS Party</a>
  </td>
  <td>
  23
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/founderstalk>Founders Talk</a>
  </td>
  <td>
  4
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/rfc>Request For Commits</a>
  </td>
  <td>
  3
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/practicalai>Practical AI</a>
  </td>
  <td>
  2
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/backstage>Backstage</a>

  </td>
  <td>
  1
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/brainscience>Brain Science</a>
  </td>
  <td>
  1
  </td>
  </tr>
  <tr>
  <td>
  <a href=https://changelog.com/spotlight>Changelog Spotlight</a>
  </td>
  <td>
  1
  </td>
  </tr>
</table>

While starting to write this post, I looked for any references to Changelog in my posts, and noticed that it turns out that I have Changelog to credit for [me starting to write blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/) for [an interview](https://changelog.com/podcast/249) they did with <span class="h-card"><a class="u-url" href="https://www.hanselman.com/">Scott Hanselman</a></span>. This has honestly been such a pivotal thing for my career, as well as me [as a neurodiverse person](https://www.jvt.me/posts/2023/10/04/blogging-neurodiversity/), and over the years, some of the discussions or ads from sponsors have pointed me towards some really interesting tech and avenues to explore.

In late 2022 I [joined the subscriber program, Changelog++](https://www.jvt.me/mf2/2022/11/5egq4/) which as the ancient scriptures say, _it's better!_ - and I do agree with it. Not only is it nice to be able to support something I listened to for a total of 5 days and 19 hours in 2023, but it's also a chance to get access to some extra content, as well as removing the non-invasive ads, and I appreciate being able to support the really great work everyone does to make the episodes as awesome as they are.

On the topic of ads, there was a recent discussion about the great work that <span class="h-card"><a class="u-url" href="https://changelog.com/person/jerodsanto">Jerod</a></span> and <span class="h-card"><a class="u-url" href="https://changelog.com/person/adamstac">Adam</a></span> work hard to get the right sponsors, which I really have to agree with - they're always things that Jerod and Adam believe in and are relevant to the audience. The discussion was interesting because it was coming from someone who subscribes to Changelog++ but still wants to hear the ads to learn about interesting new tech or companies - when people are able to skip the ads but still want to listen to them, you know they're good!

The Changelog's breadth of topics with their Interviews, as well as the different levels of depth that each of the different shows can go into makes it a great listen, and as [I started to learn Go](https://www.jvt.me/posts/2022/08/12/learning-new-language-go/) I found that Go Time was great and - alongside the non-Changelog podcast, [Cup o' Go](https://cupogo.dev/) - is a way for me to keep up-to-date with the Go community at large.

I really like how Changelog tweaks and improves (in particular, their technical self-improvement through [Kaizen](https://changelog.com/topic/kaizen)), and new shows like Changelog & Friends makes it possible to have conversations with folks that may not make sense in an interview - like me! Changelog's got some real big names in tech on the podcasts, as well as some names you may not recognise, but have some really insightful stuff to share, and Adam and Jerod's great interviewing style helps tease out some really interesting points.

Those of you who know the Changelog may have noticed that in the above listen history, I'd not mentioned Changelog News. I did used to listen to News most weeks, but as I listen to podcasts on 1.7x speed, I've found that News is just a little too quick, and often I want to follow through on some of the topics, so instead I've found that I'll often just read the newsletter, but every so often will listen to it as a podcast too. News has been great to get insights into some of the posts or projects I may have missed over the week, and having been featured on it, is also a great opportunity to [go viral](https://www.jvt.me/posts/2022/10/14/blog-viral/).

I guess the TL;DR of this post - which maybe should've been at the top of the post? but I appreciate you getting this far! - is that the Changelog has a wealth of topics, some great interviewing, brilliant and diverse guests, and some [sick beats](https://changelog.com/beats). Give them a listen, cause I'm sure you'll find some stuff you love.
