---
title: "Manually triggering a Buildkite pipeline for a fork"
description: "How to trigger a Buildkite pipeline to run on a fork, if you have access to trigger a build."
date: 2024-07-16T18:46:06+0100
tags:
- "blogumentation"
- "buildkite"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: buildkite-trigger-fork
---
If you're building on [Buildkite](https://buildkite.com/), sometimes you may encounter a time where you're contributing from a fork, but CI isn't running (often by design).

So how do you get your build to start running, without pushing it to a branch instead?

If you don't have access to the ability to click the `New Build` button in the Buildkite UI, you're unfortunately out of luck!

However if you _do_ have the ability to trigger a build, then we can take advantage of the way that GitHub prepares a Git ref ([more info](https://www.jvt.me/posts/2019/01/19/git-ref-github-pull-requests/)) for each Pull Request, which means we can now trigger a build of the given PR.

I.e. if we had PR 12, we'd trigger a build of `refs/pull/12/head`:

![A screenshot of the "New Build" dialog in Buildkite, showing the "Branch" set to `refs/pull/12/head`, indicating the HEAD of PR 12 on the repo, and leaving the "Commit" as the default `HEAD`](https://media.jvt.me/b109a02cdb.png)

Thanks to my colleague Isaac for the tip!
