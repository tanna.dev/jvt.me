---
title: "I'm on API Evangelist Conversation"
description: "Announcing a podcast appearance with Kin Lane about API Versioning."
date: "2024-09-19T17:31:16Z"
tags:
- "podcast"
- "api"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/22f5614a79.jpeg"
slug: "api-evangelist-conversation"
aliases:
- /posts/1/01/01/api-evangelist-conversation/
---
This afternoon, I had a great conversation with <span class="h-card"><a class="u-url" href="https://apievangelist.com/">Kin Lane</a></span>, aka the API Evangelist, about API Versioning on [the API Evangelist Conversation podcast](https://apievangelist.com/2024/09/19/api-evangelist-conversation-with-jamie-tanna-engineering-productivity-at-elastic/).

I've followed Kin's blog for years now - who manages to put out almost double the content I'm able to, which is a good goal to strive for! - and have learned a tonne from Kin's work over the years.

So I was very excited when Kin reached out the other day to discuss [my recent post about versioning](https://www.jvt.me/posts/2024/08/26/v0/).

It was great to chat with Kin, and we had some good discussions in this bite-sized podcast - I really enjoyed it as a short-but-not-too-short session, and hope you enjoy [the episode](https://apievangelist.com/2024/09/19/api-evangelist-conversation-with-jamie-tanna-engineering-productivity-at-elastic/) too.
