---
title: "I'm on Changelog and Friends!"
description: "Announcing my first podcast appearance on Changelog and Friends, talking about salary history, the IndieWeb, ADHD and dependency-management-data, among other things."
date: 2024-02-17T17:58:12+0000
tags:
- podcast
- adhd
- salary
- indieweb
- public-speaking
- dependency-management-data
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: https://media.jvt.me/08d552b07b.png
slug: changelog-friends
---
I'm very excited to be on my first ever podcast, and it happens to be on the ever excellent [the Changelog](https://changelog.com), in their [Changelog & Friends podcast](https://changelog.com/friends/).

As noted in the episode, the first (recorded) time I listened to The Changelog was in 2016, and I've been an avid listener for years, to the point of [joining the Changelog++ subscribers in 2022](https://www.jvt.me/mf2/2022/11/5egq4/).

I've [written a little bit more](https://www.jvt.me/posts/2024/02/17/listen-changelog/) in depth about why I love The Changelog and think you should be listening to it too, which is something I've been meaning to write for a few months now.

This was a lot of fun, and it was really great to chat with <span class="h-card"><a class="u-url" href="https://changelog.com/person/jerodsanto">Jerod</a></span> and <span class="h-card"><a class="u-url" href="https://changelog.com/person/adamstac">Adam</a></span>, especially as I've been hearing their voices and thoughts for a good chunk of the last decade!

We chatted about [my salary history](/salary/) and what I've learned since doing it, the [IndieWeb community](https://indieweb.org/why), [my ADHD](/posts/2022/10/04/adhd/) and then on to my work with [dependency-management-data](https://dmd.tanna.dev) and maybe turning it into a business 👀

There were some really interesting threads of conversation I'd not taken before with other folks, and it was really insightful hearing Jerod and Adam's points of views.

I'd very much like to shout out <span class="h-card"><a class="u-url" href="https://www.mooreds.com/">Dan Moore</a></span> who requested me as a guest, as well as an anonymous member of the Changelog community who requested me in the past.

I had a lot of fun, interesting to see the behind-the-scenes of how the great podcasts are made, and it was really great talking to Jerod and Adam 💜

Check it out and let me know what you think!

<audio data-theme="night" data-src="https://changelog.com/friends/31/embed" src="https://op3.dev/e/https://cdn.changelog.com/uploads/friends/31/changelog--friends-31.mp3" preload="none" class="changelog-episode" controls></audio><p><a href="https://changelog.com/friends/31">Changelog & Friends 31: Yeeting stuff into public</a> – Listen on <a href="https://changelog.com/">Changelog.com</a></p><script async src="//cdn.changelog.com/embed.js"></script>

PS - there's a good Changelog++ section, too! Changelog++, as the ancient scriptures say, _is better_ 🙌

PPS - love the title of the episode 😹
