---
title: 89 things I know about Git commits
description: "Some of the things I've learned over a decade of Git usage, and working on writing good commit messages."
tags:
- git
date: 2024-07-12T20:01:09+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: things-know-commits
image: https://media.jvt.me/53239026de.png
---
This is an article that's been swimming around in my head for ~5 weeks now, and may become a "living post" that I keep updated over time.

In no particular order, some things I've learned about Git commits and commit history, over the last 12 years. This is a mix of experience in companies with teams of 2-12 people, as well as in Open Source codebases with a vast range of contributors.

1. Git has different uses - a collaboration tool, a backup tool, a documentation tool
1. Git commit messages are excellent
1. I've never met anyone who likes reading commit messages as much as me
1. Finding out why a change was made is easier sorting through commits than it is through an issue/bug tracker
1. It's better to have a commit that says "Various fixes. DEV-123" than it is to have "Various fixes"
1. It's worse to have a commit that says "Various fixes. DEV-123" when the issue itself has no useful information
1. Rebase-merging is my preference. Then squash-merge, then merge
1. If you don't learn how to rebase, you're missing out on a good skill
1. People who say "just delete the repo" when things go wrong really frustrate me
1. Learn how to use `git reflog`, and it'll save you deleting a repo and work that was recoverable
1. Learn how to use `git reflog`, and you'll be able to save yourself from mistakes that aren't that bad
1. No amount of learning fancy tools and commands saves you from fucking up every once in a while
1. My most recent botched rebase was last week, and I needed `git reflog` to help un-fuck it
1. Learn how to [undo a force push](https://www.jvt.me/posts/2021/10/23/undo-force-push/) and then how to [more safely force push](https://www.jvt.me/posts/2018/09/18/safely-force-git-push/) (remember the `=ref`!!)
1. Squashing is a waste of well-written atomic commits
1. Squashing is better than 100 crap commits
1. Squashing, and writing a good commit message at the time of merge is good
1. Squashing, and then not re-editing the message is the worst
1. Squashing, when you have 100 crap commits, and then not re-editing the message is __a crime__
1. Squashing, and then not re-editing the message is worse than a merge commit from a branch with 100 crap commits
1. Writing a well documented PR/MR description but not using that to inform the squash-merge message is a waste of time
1. Writing commit messages have helped me pick up on missing test cases, missing documentation, or invalid thought processes, as it helps me rewrite _why_ the changes are being made
1. Using your `git log` as an indication for standup updates is valid
1. I can't be bothered to sign my commits (unless I'm forced to)
1. If I have to sign my commits, SSH key signing makes it almost not awful
1. If you're moving files between repos, you need to keep the history intact, [using `git subtree`](https://www.jvt.me/posts/2018/06/01/git-subtree-monorepo/)
1. Commits should be atomic - all the code and tests, and configuration changes should all be in there
1. I spend a good chunk of time ensuring that each commit passes CI checks atomically
1. Some people do horrific things, like split their implementation and test code from each other
1. It's OK to put documentation in a separate commit - we don't have to have a whole end-to-end feature delivery in a single commit
1. Repos that use squash-merges suck
1. As a maintainer of Open Source projects, I like squash-merge, so I can rewrite contributors' commit messages
1. Sometimes it's not worth coaching how to write a given commit message
1. The people around you shape the way you write commits
1. Do the work up front to make your history atomic
1. It's much more painful to split a mega-commit into atomic commits after the fact
1. Splitting work atomically can be good for improving your reward drive - you can get many more things done
1. Atomic commits work really well with [prefactoring](https://www.jvt.me/posts/2022/04/12/prefactor/)
1. Sometimes prefactor commits can go into separate PRs (especially if squash-merge is used)
1. Writing commit messages can take longer than the implementation
1. The commit message can be an order of magnitude larger than the number of lines changed in a commit
1. If you end up writing a lot of "and"s or "also" in a commit message, you may be trying to do too many things
1. Trawling through Git commit history in the past has helped unlock a number of cases where I could then understand the _why_ without the original authors there to answer my questions
1. Commit messages are a great point to reflect on not just what you've done, but __why__
1. Why is more important than what - anyone can look at the diff and generally work out what changes were made, but the intent behind it is the special sauce
1. If you only write what changed, you're annoying, and I dislike you
1. A commit that explains what is better than a commit that just has "fixes"
1. <span class="h-card"><a class="u-url" href="https://cbea.ms/">Chris Beams</a></span>' article, [_How to Write a Git Commit Message_](https://cbea.ms/git-commit/) is still an excellent post and a great place to start, just under 10 years later!
1. Commits are a point-in-time explanation of the assumptions and the state of the world for the committer. Don't be too hard on them
1. I don't want to read AI/LLM rewrites of your changes - either write it yourself, or call it `Various fixes`
1. There needs to be a way to add an annotation (maybe using `git notes`) to a previous message to correct assumptions
1. I won't write perfect commit messages up front - they'll sometimes be as much as `rew! add support for SBOMs` or `sq`, or [use `git commit --fixup`](https://www.jvt.me/posts/2019/01/10/git-commit-fixup/)
1. I will, generally, break off very good atomic chunks of work into commits
1. I will split atomic commits into multiple commits, sometimes
1. Make sure you [review your own code changes](https://www.jvt.me/posts/2019/01/12/self-code-review/) before you send it out for review to your collaborators
1. Reviewing your commit messages should be as important as reviewing the code changes
1. Getting all your contributors to invest the same amount of care into the commit history is a losing battle
1. Trying to police commit history is going to be painful
1. Trying to mandate reviews of commit messages as part of code review is going to be painful
1. Trying to police commit history does lead to a greater level of documentation and consideration around changes int he codebase
1. Making implicit assumptions explicit is really useful
1. Introducing `commitlint` can be useful, but also frustrating
1. It's nicer to have your collaborators want to write good commit messages than you having to force them
1. Some people don't write, and that's alright
1. Writing is a skill
1. I'm not perfect at writing (commit messages)
1. Sometimes I can't be arsed to write the perfect message
1. Sometimes I write some really great commit messages, and impress myself
1. Using a [template for your Git commit messages](https://www.jvt.me/posts/2017/04/17/commit-templates/) is a good nudge to doing it right
1. [`fixup` commits](https://www.jvt.me/posts/2019/01/10/git-commit-fixup/) and `git rebase --autosquash` has been one of the best Git tips I've learned
1. I value working on a team with a diverse set of perspectives, skills and approaches to work
1. But I also really value having a team who writes atomic commits with well-written commit messages
1. Commit message writing is as useful as writing well-refined user stories/tickets
1. `git commit -m sq` is probably my most-run command
1. Using `git add -p` and `git commit -p` are hugely important for atomic commits
1. Never use `git add -u` or `git add .`
1. Learn when you can use `git add -u` or `git add .`
1. I really need to look into tools like Graphite, `git-branchless` and other means to provide a stacked PR setup
1. Using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) with [semantic-release](https://github.com/semantic-release/semantic-release) or [go-semantic-release](https://github.com/go-semantic-release/semantic-release) can make a huge difference when wanting to release automagically and often
1. Using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) as a framework for your commits can be really useful
1. Using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/), as someone with ADHD, reduces the need for thinking at times and can allow you to focus more on what the changes are
1. Using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) helps you work out when you're trying to do too much in a commit
1. I [think through writing](https://www.jvt.me/posts/2023/10/04/blogging-neurodiversity/) so commit messages help understand [why I did the thing](https://www.youtube.com/watch?v=1qdyNoe3q2A)
1. It can be better to write a good commit message than a piece of documentation, stored elsewhere
1. It can be better to write a good commit message than a code comment
1. Give people space to learn
1. Give people space to fail
1. Remember you weren't so great at one point in time
1. Documentation is rad. Do more of it
