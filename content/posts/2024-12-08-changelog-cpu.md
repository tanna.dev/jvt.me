---
title: "Some mixed thoughts on The Changelog Podcast Universe"
description: "Some reflections on the bittersweet news about the The Changelog Podcast Universe."
date: 2024-12-08T21:35:05+0000
tags:
- podcast
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: changelog-cpu
image: https://media.jvt.me/50e5e64a93.png
---
As I wrote about in [_You should listen to The Changelog_](https://www.jvt.me/posts/2024/02/17/listen-changelog/) I've been listening to The Changelog for ~8 years. I love the overarching podcast, _Changelog Interviews_, and I've really enjoyed the new _Changelog and Friends_ podcast.

But more than that, I've thoroughly enjoyed the quality of the network as a whole, including podcasts I regularly listen to like _Go Time_ and _Ship It!_, with some intermittent listens to _JS Party_. One of the great thing of the network is that I've had access to a tonne of excellent podcasts, of very similar high quality, all from a single feed.

On Friday I saw [the news that Changelog are focussing on focussing on Changelog News, Interviews and Friends](https://changelog.com/posts/a-new-era-for-the-changelog-podcast-universe), and that the non-core podcasts will be split into their own podcasts.

It's positive that they'll be part of the upcoming Changelog Podcast Universe (CPU) - a name to rival the Marvel Cinematic Universe - and I hope that there'll be some other great new shows in the CPU too.

I can't fault Adam and Jerod for making the decision, and am looking forward to hearing more about it on the upcoming final few episodes for each of the affected shows. It's not unexpected, hearing Adam and Jerod saying many a time over the years that "[you should] keep the main thing, the main thing".

I'm also glad to see that immediately, Go Time and Ship It! have a planned fork/successor podcast, and I'll be hoping that the quality of the content and editing that we're used to, as well as maybe giving a little bit more flexibility in the content.

I will say that I'm a little gutted that the Changelog++ won't be applying to the CPU, at least right now, especially just after custom feeds functionality dropped, which made it possible to get all the right episodes auto-downloading as I wanted, and now I'm back to separate feeds for all the podcasts I want to listen to.

(this isn't exactly a huge problem)

I think on the whole this will be positive, but I'm a little averse to change, so am a little bit put off by the news, but as Andrew mentioned on Zulip:

> Oh, that's an ominous title after a year of "A Message to Our Community about the Future of [Corporate Open Source Project We All Rely On But Don't Pay For]" news...

So thankfully we're still in business 🚀

I'd also like to say a huge thank you to Adam and Jerod for running The Changelog network all these years, and the very many awesome hosts and guests that have made the wide network of podcasts awesome - I look forward to listening for the next 8 years 🤓
