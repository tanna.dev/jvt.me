---
title: "Migrating Renovate bots, while keeping existing PRs updated"
description: "How to migrate between two Renovate bot accounts, in the case you want to do a 'big bang rollout'."
tags:
- blogumentation
- renovate
date: 2024-07-18T14:05:31+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: renovate-migrate-self-host
image: "https://media.jvt.me/6657d4ff89.jpeg"
---
In the process of migrating from the [Mend-hosted SAAS platform](https://developer.mend.io) to a Self-Hosted deployment of [Mend Renovate Community Edition](https://github.com/mend/renovate-ce-ee/), we've noticed that PRs raised by the SAAS platform, were not being updated by the Self-Hosted app.

This is down to the way that Renovate tries to not overwrite any changes that (usually a human) has pushed to one of its PRs, and that it naturally can't know that you're in the process of a migration between bots.

Our Dependency Dashboards were full of:

> Edited/Blocked
> These updates have been manually edited so Renovate will no longer make changes. To discard all commits and start over, click on a checkbox.

This is down to our new Renovate bot `internal-renovate[bot]` not wanting to step on the toes of the existing bot, `renovate[bot]`.

We didn't want to have to close (+ rename) all the PRs + delete branches to force Renovate to re-run, so started digging through documentation to find an alternative.

Fortunately, we can use the [`gitIgnoredAuthors`](https://docs.renovatebot.com/configuration-options/#gitignoredauthors) configuration to exclude the SAAS GitHub App, by adding the following to our `config.js`:

```javascript
module.exports = {
  gitIgnoredAuthors: [
    // the App ID + GitHub email address, via https://www.jvt.me/posts/2023/04/20/github-app-email-address/
    // alternatively, via a commit on an existing repository
    "29139614+renovate[bot]@users.noreply.github.com"
  ],
}
```

Once this was in place, Renovate was seeing the branches and was happy starting to modify them. However, we then hit:

```json
{
  "level": 20,
  "branch": "renovate/github.com-urfave-cli-v2-2.x",
  "err": {
    "name": "HTTPError",
    "code": "ERR_NON_2XX_3XX_RESPONSE",
    "message": "Response code 422 (Unprocessable Entity)",
    "stack": "HTTPError: Response code 422 (Unprocessable Entity)\n ..."
  },
  "response": {
    "statusCode": 422,
    "statusMessage": "Unprocessable Entity",
    "body": {
    "message": "Validation Failed",
    "errors": [
      {
      "resource": "PullRequest",
      "code": "custom",
      "message": "A pull request already exists for ...:renovate/github.com-urfave-cli-v2-2.x"
      }
    ],
    "documentation_url": "https://docs.github.com/rest/pulls/pulls#create-a-pull-request",
    "status": "422"
    },
    "headers": {
    ...
    },
    "httpVersion": "1.1",
    "retryCount": 0
  }
```

As expected, there's already a PR from that branch, it just happened to be created by the SAAS bot.

To resolve this, I managed to find [this GitHub Discussion](https://github.com/renovatebot/renovate/discussions/14013) which pointed towards [`ignorePrAuthor`](https://docs.renovatebot.com/configuration-options/#ignoreprauthor), which is used to support the migration between repos, by not performing the pre-optimised search to only find PRs raised by the authenticated bot.

Note that this is likely to cause performance degradation, especially on large repos, as Renovate will look through _all_ PRs in the repo for ones that are Renovate-related.

Therefore, the final `config.js` you want is:

```javascript
module.exports = {
  gitIgnoredAuthors: [
    // the App ID + GitHub email address, via https://www.jvt.me/posts/2023/04/20/github-app-email-address/
    // alternatively, via a commit on an existing repository
    "29139614+renovate[bot]@users.noreply.github.com"
  ],

  ignorePrAuthor: true,
}
```

And that should mean Renovate ✨ just works ✨
