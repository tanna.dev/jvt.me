---
title: "Forcing `pacman` to perform post-upgrade tasks if it's failed part way through"
description: "How I ended up recovering from my Arch Linux upgrade crashing part way through the post-upgrade hooks."
date: 2024-09-09T09:30:39+0100
tags:
- "blogumentation"
- "arch-linux"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: pacman-force-post-upgrade-hooks
---
You know it's not a great start to your week when, after a long weekend, you set your work laptop running through updates and it crashes part-way through the install process. After a reboot, it ends up not even _detecting_ Arch's install, and only allows you to reboot into the BIOS 😅

So I started frantically searching around "how to recover a failed `pacman` install" (and other words to that effect) while getting my installation medium booted as a recovery disk, at which point I found ["Fixing an unbootable system caused by an interrupted upgrade"](https://wiki.archlinux.org/title/Pacman#Fixing_an_unbootable_system_caused_by_an_interrupted_upgrade) on the Arch Wiki, as well as some Arch Linux forum posts to the same effect.

Unfortunately these didn't seem to work, and I couldn't see a full list of the packages that `pacman` had upgraded as part of this morning's view - at least in a super straightforward means - but clicked through to [the "talk" page](https://wiki.archlinux.org/title/Talk:Pacman#More_details_in_pacman_crashes_during_an_update) which noted [github.com/edu4rdshl/archlinux-pkgrecover](https://github.com/edu4rdshl/archlinux-pkgrecover).

Fortunately, this is what I was able to use to resolve my local 👏🏼

To do this, I needed to:

Look for the "last known good" upgrade in `/var/log/pacman.log`, before today's date:

```
...
[2024-09-03T11:16:49+0100] [PACMAN] Running 'pacman -D -q --asexplicit --config /etc/pacman.conf -- xdo'
[2024-09-09T08:03:23+0100] [PACMAN] Running 'pacman -S -y --config /etc/pacman.conf --'
...
```

In this case, we can see that `2024-09-03` was the last upgrade.

Then, I ran `archlinux-pkgrecover` like so:

```
bash ./pkgrecover.sh --pacman-db 2024-09-03 ../pacman.log-fail-boot --dry-run
```

This ended up doing what I needed (despite the `--dry-run` flag) and re-installed every package that had been updated since 2024-09-03, and then ran through the post-upgrade hooks.

Once done, I was able to successfully reboot into my install.

Note that you should make sure you have at a minimum your `/` and `/boot` (or EFI) partitions mounted, depending on how your system is set up.

(Aside: Arch Linux is continually incredibly stable for me and it's largely this specific device which has known issues...)
