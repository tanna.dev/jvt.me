---
title: "Nested teams in GitHub don't give you the control you think they do"
description: "Some pain points when using nested teams on GitHub."
tags:
- blogumentation
- github
date: 2024-09-12T21:32:42+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: github-authz-teams
image: https://media.jvt.me/36fd7d2a48.png
---
GitHub has the concept of [nested teams](https://docs.github.com/en/organizations/organizing-members-into-teams/about-teams#nested-teams) to make it possible to say manage hierarchical access in your organisation.

This can be really useful for indicating cases where i.e. you would expect all of your engineering team to be able to access specific repositories, instead of providing read-only access to all organisational members, or to be able to say "all of the Platform organisation can write to this repository, and everyone else can read".

However, it comes with a number of rough edges, and using it for authorization in your repository settings is almost never _actually_ what you want to do.

It's a conversation I've had before, including today at work, so I thought it was worthwhile recording the reasoning so I could more easily refer back to it in the future.

This can be useful for general access but this doesn't work for a few cases:

## More granular authorization

This makes sense, but it's worth calling out that you'll then need to add any child teams with any elevated access separately

## Specifying `CODEOWNERS`

Even if everyone in the nested team has `Write` access to the repository, you will not have a valid `CODEOWNERS` file, as each _individual team_ needs to be added as a collaborator, with `Write` access.

## Requesting reviews

Similar to the above, you cannot request the review of a child team, unless that team is explicitly added as a collaborator, with `Write` access.
