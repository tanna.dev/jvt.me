---
title: Dependency Management Data can now use sql-studio for database browsing
description: "Announcing the availability of the `sql-studio` database browser for dependency-management-data's web application."
tags:
- dependency-management-data
- sql
date: 2024-06-28T22:29:01+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: dmd-sql-studio
image: https://media.jvt.me/0ce655e6df.png
---
As noted in [_Dependency Management Data's web application can now be deployed as a single static binary_](https://www.jvt.me/posts/2024/06/16/dmd-web-embedded/), Dependency Management Data's web application has recently added a lightweight embedded web browser to enable running as a single static binary.

Last week I learned about [`sql-studio`](https://github.com/frectonz/sql-studio/) from Laravel News on my Google News feed and have really been enjoying using it, and enjoying the UI design as well as the database visualisation and the fact it can be deployed as a single static binary.

I've been working with upstream to [add support for running it behind a reverse proxy](https://github.com/frectonz/sql-studio/issues/16) with the intent of being able to run it with dependency-management-data.

As of today's [v0.98.0 release](https://gitlab.com/tanna.dev/dependency-management-data/-/releases/v0.98.0), it's now possible to do that 🚀

For instance, this is what the overview page looks like against the example database:

![The `sql-studio` overview page, showing some insights into the database - 23 tables, 23 indexes, 0 views, 0 triggers, ~160K rows in one of the tables, with the next largest table being ~30K rows](https://media.jvt.me/d33db5fe45.png)

I'm working with upstream in terms of [running on `musl`-based systems like Alpine Linux](https://github.com/frectonz/sql-studio/issues/31) and [in distroless images](https://github.com/frectonz/sql-studio/issues/33), so until those are complete, the example web app won't have an example of `sql-studio`.

I'm looking forward to using it, and hopefully making it available as a good option for lightweight deployments, although note that it's a bit heavier on the frontend, requiring client-side Javascript, so may not suit every deployment model.
