---
title: Why has Firefox 133 lost all my tabs?
description: Recounting an issue I'm seeing with Firefox 133 where it creates a new profile, and how to resolve it.
tags:
- blogumentation
- firefox
- adhd
date: 2024-10-16T08:10:21+0100
image: https://media.jvt.me/0a211cdb4c.png
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: firefox-profile-133
---
Over the last week, I've upgraded my various (Arch btw) Linux machines' versions of Firefox Nightly to their latest - largely due to the [announced zero-day](https://www.bleepingcomputer.com/news/security/mozilla-fixes-firefox-zero-day-actively-exploited-in-attacks/) - and have noticed that on _each and every device_ it's ended up losing all my settings and open tabs.

As [someone with ADHD](https://www.jvt.me/posts/2022/10/04/adhd/) I (like to think that I?) manage my active TODO list with browser tabs/windows, and found it _deeply unsettling_ when I lost my active windows, and it really did put me on edge as a coping mechanism I've got for managing things to do was no longer there, and I'd likely have lost a lot of important things I should look into.

The first time I hit this was on Thursday night, where I was getting my laptop ready before a weekend away in Manchester at [OggCamp](https://www.jvt.me/posts/2024/10/13/oggcamp/), and I was too high on adrenaline trying to get at least Bitwarden and a few of my services re-authenticated and getting my settings synced that I didn't look into what had happened and if I could recover it.

But on Monday I hit the same issue on my work machine, and after a bit of time trying to recreate all my settings, I ended up starting to look into whether there was a way to recover this. It was particularly important on my work machine, as I heavily curate my open windows as my TODO list, and losing that meant there were a number of documents to review that I'd now have to re-discover, as well as open PRs/investigations into things I'd lose.

I'm not sure _why_ but what has happened, is that at least with Firefox Nightly 133.0a1, there is a new profile that's been created and set as default, instead of re-using my old profile.

I can't quite remember how I got there - after frantically searching around online - but I found that going to `about:profiles` showed that there was a new profile in use:

![The Firefox `About Profiles` page. On it, there are three profiles shown - `default-nightly`, `default`, and `default-nightly-1` which is currently in use. It is assumed that `default-nightly` is my original profile, and `default-nightly-1` was created, for some reason, by Firefox 133](https://media.jvt.me/6e16084728.png)

It looks like `default-nightly` is my original profile, and `default-nightly-1` was created, for some reason, by Firefox 133.

By hitting `Launch profile in new browser` button on `default-nightly`, it re-launches my original profile, with my many windows and logged in services 🚀

I've also made sure to `Set as default profile`, so I can start back with that.

(Aside: it was nice my desktop's Firefox loading _so speedily_ as it didn't have 100s of tabs to load, so maybe I'll look at doing another clean up of windows/tabs to speed up my boot)
