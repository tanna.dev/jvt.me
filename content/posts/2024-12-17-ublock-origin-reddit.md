---
title: "Blocking 'Similar' and 'Because you like ...' suggestions on reddit with uBlock Origin"
description: "How to use uBlock Origin to remove reddit's suggestions in your home feed."
date: 2024-12-17T12:31:31+0000
tags:
- "blogumentation"
- "reddit"
- "ublock-origin"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/70a050dcbb.png"
slug: ublock-origin-reddit
---
Since reddit attempted to destroy all goodwill in the community with their [changes to the API](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy), I've been using the mobile web view, rather than their crappy mobile app.

(I did stop using reddit a lot, especially around the API changes, but unfortunately still use it a little, and moreso after leaving Twitter)

Recently, I've noticed that they've been "pushing" more recommended subreddits / posts like so:

![A screenshot of a post in the reddit home feed, showing a suggestion for `/r/raimimemes` because it's "similar to `/r/BatmanArkham`", and a somewhat funny meme](https://media.jvt.me/c7ae98fdc8.png)

(This is a bad example, as it _is_ actually a subreddit I'm interested in, and I have found some interesting subreddits through this, but that's beside the point right now)

More heinous are posts like this, i.e. this random one from `/r/blackpool`:

![A screenshot of a post in the reddit home feed, showing a suggestion for `/r/blackpool` because it's allegedly "popular near you", despite the fact that I'm in Nottingham, and reddit is likely very aware of that and the fact Blackpool is 3 hours away](https://media.jvt.me/206647e626.png)

So, I set about seeing how I'd be able to get the trusty [uBlock Origin](https://github.com/gorhill/uBlock) to do this for me.

These - unfortunately- don't appear with a CSS class i.e. `.display-annoying-suggestion-to-user`, so I found that I needed to work based on the text that's shown, and then use `:upward` to find the parent and delete it, which results in:

```
###-post-rtjson-content > p:has-text('Because '):upward(article)
###-post-rtjson-content > p:has-text('Similar to '):upward(article)
###-post-rtjson-content > p:has-text('Popular on Reddit'):upward(article)
###-post-rtjson-content > p:has-text('Popular near you'):upward(article)
```

This is based on [this StackOverflow question](https://stackoverflow.com/questions/57271328/write-a-ublock-filter-rule-for-a-parent-element-based-on-content-of-a-child-elem) and [this reddit post](https://www.reddit.com/r/uBlockOrigin/comments/1123ys6/block_a_parent_element_with_wildcard_that_has_a/).
