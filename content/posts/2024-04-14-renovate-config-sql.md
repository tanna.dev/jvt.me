---
title: "Querying your organisation's Renovate configuration using SQL(ite)"
description: "A new tool, `renovate-config-sqlite` to pull Renovate configuration into an SQLite database."
tags:
- blogumentation
- renovate
- sqlite
date: 2024-04-14T10:58:32+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: renovate-config-sql
image: "https://media.jvt.me/6657d4ff89.jpeg"
---
In what will seem very topical on this blog (after my post [_Why I recommend Renovate over any other dependency update tools_](https://www.jvt.me/posts/2024/04/12/why-renovate/) the other day) I've got another post about Renovate.

Over the last few years I've worked a lot with Renovate, and at the last two companies I've been largely focussed on enabling teams to use Renovate better.

However, something recently came up at work where I needed an easy way to list which teams were using a configuration value, so we could make some follow-up changes.

As we have well over 2000 repos, I wanted to search across all repos for relevant config settings, and then get PRs raised for the relevant work associated for those teams. As the lowest barrier to entry, I used the GitHub search for it, raised my PRs and went about my day.

About an hour later I got a message to say "hey, you're missing this repo, and this one, oh and this one". For whatever reason, the GitHub search didn't surface quite a few repos, which was a little frustrating as I hoped I had everything sorted.

To avoid this, as well as to give me a more structured interface for querying the data using my new favourite thing, SQLite, and its [JSON querying](https://www.jvt.me/posts/2023/03/13/sqlite-json/) I've decided to build a smallish Go command-line tool which helps sync Renovate's JSON/JSON5 configuration to an SQLite database for further querying.

This is now live as the Go command-line tool [`renovate-config-sqlite`](https://gitlab.com/tanna.dev/renovate-config-sqlite/).

Right now I've got support for GitHub.com - as that's where I'm using it at work - but I'm open to adding support for GitLab, as it's where my personal projects are hosted, and I'm sure it'll be useful for folks in their companies too.

(aside: since building this tool, I've found _even more_ cases of repos with Renovate configuration that need the mentioned changes applied to them)

(additional aside: after writing this I looked around for anyone else doing similar and found [this blog post from MergeStat](https://docs.mergestat.com/blog/2023/02/16/querying-renovate-bot-across-repos) which seemed cool!)

## Example

For a bit of an idea of how this tool could be useful, let's look at a few examples of what this data can be used for.

In the below example, I've pulled in public repos from the [`oapi-codegen` org](https://github.com/oapi-codegen), [Giant Swarm org](https://github.com/giantswarm) and [the `oapi-codegen` project](https://github.com/deepmap/oapi-codegen/).

For instance, we can look at "who is using the `oapi-codegen` presets" we could write the following query:

```sql
select
  distinct
  organisation,
  repo
from
  renovate_configs,
  json_each(
    json_extract(renovate_configs.config, '$.extends')
  )
  where json_each.value LIKE '%oapi-codegen/renovate-config%'
```

Which results in:

| organisation |        repo        |
|--------------|--------------------|
| oapi-codegen | echo-middleware    |
| oapi-codegen | fiber-middleware   |
| oapi-codegen | gin-middleware     |
| oapi-codegen | iris-middleware    |
| oapi-codegen | nethttp-middleware |
| oapi-codegen | nullable           |
| oapi-codegen | runtime            |
| oapi-codegen | testutil           |
| deepmap      | oapi-codegen       |

Alternatively, to look at which repos have defined the most `packageRules`:

```sql
select
  distinct organisation,
  repo,
  json_array_length(
    json_extract(renovate_configs.config, '$.packageRules')
  ) as num_package_rules
from
  renovate_configs
where
  json_extract(renovate_configs.config, '$.packageRules') is not null
order by
  num_package_rules desc
```

Which results in:


| organisation |                   repo                   | num_package_rules |
|--------------|------------------------------------------|-------------------|
| giantswarm   | crossplane-upstream                      | 8                 |
| giantswarm   | athena                                   | 7                 |
| giantswarm   | cloud-director-cli                       | 7                 |
| giantswarm   | cluster-api-cleaner-cloud-director       | 7                 |
| giantswarm   | cluster-api-cleaner-vsphere              | 7                 |
| giantswarm   | cluster-api-ipam-provider-in-cluster-app | 7                 |
| giantswarm   | dex-operator                             | 7                 |
| giantswarm   | pss-operator                             | 7                 |
| giantswarm   | renovate-playground                      | 7                 |
| giantswarm   | static-cms                               | 7                 |
| giantswarm   | backstage-fork                           | 4                 |
| giantswarm   | loki-upstream                            | 2                 |
| giantswarm   | mimir                                    | 2                 |
| giantswarm   | mimir-upstream                           | 2                 |
| giantswarm   | app-build-suite                          | 1                 |
| giantswarm   | cloudnative-pg-upstream                  | 1                 |
| giantswarm   | step-exec-lib                            | 1                 |

Hopefully this gives an idea of what you can use this to query, and even if it's not useful to others, I'll be using it, even if it's a way to force me to remember how SQLite's JSON operations work.
