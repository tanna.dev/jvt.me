---
title: "Responsible Disclosure: Using GitHub Search (without logging in using SSO) still allows searching"
description: "Reporting on a HackerOne responsible disclosure that I found in GitHub, where you could exfiltrate data without signing in to GitHub SSO."
tags:
- responsible-disclosure
- github
- sso
date: 2024-05-09T22:07:41+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: hackerone-github-sso
image: https://media.jvt.me/4695321dd9.png
---
I've responsibly disclosed my first security vulnerability 👏 Not only that, but it was actually a problem, _and_ it was fixed very quickly, _and_ I've ended up getting a payout for it! Not bad for my first, lucky, discovery 😄

## Report

Below is the report that I shared with GitHub on HackerOne.

This is my first report, so any feedback would be appreciated!

### Classification

- Proposed CSSS V3 Calculation: `High 8.3`
- Accepted severity: `Medium`
- Reward: `$4000` plus a GitHub Pro (individual) subscription for life, and 40 points of swag in the GitHub Bounty Swag Store

### Description:

It appears that GitHub Search does not correctly perform authorization checks when a user is set up with GitHub SSO.

A user who is not logged into GitHub SSO (i.e. on a different machine, or they've been logged out of GitHub SSO, but not GitHub) can search for content that is in non-public repos.

Note that this only works for Internal repos, not Private repos.

The user must already be added to the organisation, so some level of trust is required.

However, this could i.e. allow exfiltration of data onto unsecured machines.

### Steps To Reproduce:

1. A user is added to an organisation that requires GitHub SSO
1. The user does not log in via SSO
1. Use GitHub search to look for a string that exists in an Internal repo, such as `org:Elastic dmd.tanna.dev`

See attached example of searching for a known string in the Elastic organisation.

First image (search results) is using my personal machine, which is not logged in with GitHub SSO.

![A screenshot of the GitHub search page for the search query `dmd.tanna.dev`. A number of the repositories' results are minimised to highlight the code snippet that can be seen in the elastic/dependency-management-data repository. The code snippet is not very sensitive (hence being safe to share here) and just shows that there is a Dockerfile that's installing the `dmd-web` command-line tool](https://media.jvt.me/49ea3f57b0.png)

Second image (repo view) is using my work machine, which _is_ logged in with GitHub SSO.

![A screenshot of the GitHub repository header showing the repo name dependency-management-data and an "Internal" label, indicating it's a repository with Internal privacy settings](https://media.jvt.me/879a2276b5.jpeg)

### Impact

This allows an attacker - who has compromised a GitHub account of an employee, but who does not have access to log in via GitHub SSO - to exfiltrate data from Internal repos, which could be onto unsecured machines, or as a means to gather intelligence about the organisation.

## Timeline

- 2024-04-14: Issue discovered and reported to GItHub via HackerOne
- 2024-04-15: Issue acknowledged by GitHub
- 2024-04-22: Issue fixed by GitHub
- 2024-05-09: Issue scored
- 2024-05-09: Blog post published
  - As it was noted that it's now fine to publicly post about this

## Learnings

As mentioned, this was my first security finding of an external tool, and it was very exciting. I spent a bit of time trying to confirm the behaviour I was seeing, to avoid logging a false finding, and narrowed down whether it was just _this_ repo I was testing with, as well as a few other Internal and Private repos that I had access to, inside the Elastic organisation and others I'm part of.

I appreciate the engineers at GitHub for their timely responses, detailed replies where possible, and their patience with my excitement to find out whether it a) was a valid finding and b) how it'd be scored.

I will say that knowing that GitHub has an [excellent Bug Bounty program](https://bounty.github.com/) and with [some very modest rewards](https://bounty.github.com/#rewards) led me to spend some time considering which of the reward tiers this may sit within.

It was my first time doing a CVSS rating, so although I feel I rated it fairly, I wasn't sure if I was maybe overblowing it when it came out as a High rating. I appreciate that GitHub have rated it as a Medium based on their own scoring, and am very appreciative and excited by my first finding being a big one!

It's not at all my first time reporting a security vulnerability, but I'm generally used to it being at the company I'm currently working at (for instance [around dangerous things in logs](https://www.jvt.me/posts/2022/02/03/common-dangerous-logs/) or access control being a bit funky), so it was cool to see the process elsewhere.

And also a big thank you to <span class="h-card"><a class="u-url" href="https://shkspr.mobi/">Terence Eden</a></span> whose [responsible disclosure writeups](https://shkspr.mobi/blog/tag/responsible-disclosure/) have been very interesting to read in the past, and I appreciate your public posts post-disclosure.
