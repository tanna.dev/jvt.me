---
title: "How to use Dependency Management Data to discover which dependencies are participating in Hacktoberfest"
description: "Detailing how you could use dependency-management-data to gain insight into which dependencies you use are participating in Hacktoberfest."
date: 2024-09-03T21:24:20+0100
tags:
- open-source
- hacktoberfest
- dependency-management-data
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
image: https://media.jvt.me/8abcea0af4.png
slug: dmd-hacktoberfest
---
As I've [mentioned before](https://www.jvt.me/mf2/2022/08/d2r6m/), the fact that it's September means that it's almost October, and October primarily means one thing for me: [Hacktoberfest](https://hacktoberfest.com) 🎃👕👚🎽

Two years ago, the precursor to [dependency-management-data](https://dmd.tanna.dev) was created as part of the blog post [_Analysing our dependency trees to determine where we should send Open Source contributions for Hacktoberfest_](https://www.jvt.me/posts/2022/09/29/roo-hacktoberfest-dependency-analysis/), which has a more fleshed out [history of inception](https://www.jvt.me/posts/2024/02/06/dmd-talk-sooc/#how-did-it-come-to-be) if you're interested.

As the first full year since the project was started, following [its official birthday in February](https://www.jvt.me/posts/2024/02/02/dmd-birthday/), I wanted to take this opportunity to consider - how would I do the same thing in 2024, given I have a _much_ better understanding about how I use Open Source, thanks to dependency-management-data, and the data it understands.

I'd hoped to finish this for September 1st, but I didn't end up doing so (as you can see from the publish date), and in the last few hours I noticed that the new Hacktoberfest website and branding is live, so this is a _perfect_ time to ride on the coattails of hype and get this post out.

With dependency-management-data, one of the key things is that once you've [ingested your dependency data](https://dmd.tanna.dev/cookbooks/getting-started/) you can then start querying it, for instance using [pre-built "reports"](https://dmd.tanna.dev/concepts/report/).

To make the ability to query for repositories participating in Hacktoberfest, there's now a new report in dependency-management-data v0.106.0 which allows you to run a report such as:

```sh
# linebreaks added for readability
dmd report hacktoberfest --db dmd.db \
  --perform-external-lookup \
  --platform gitlab \
  --organisation tanna.dev \
  --repo ghprstats
```

This then provides you a view of which dependencies - if any - are participating in Hacktoberfest by using the `hacktoberfest` topic on their GitHub or GitLab repos.

Notice that when calling the report, we need to explicitly mention the [Repo Key](https://dmd.tanna.dev/concepts/repo-key/) to specifically query a given repository in our database.

This is because there's a few external lookups:

- discover the URL for the repository, via [Ecosystems](https://packages.ecosyste.ms)
- if it's a GitLab repo, call out to `gitlab.com`'s APIs to check the repository topics
- if a `GITHUB_TOKEN` environment variable is set, call out to `github.com`'s APIs to check the repository topics
- otherwise (if there's no `GITHUB_TOKEN` or if a non-GitHub or non-GitLab repo) we use the data straight from `packages.ecosyste.ms`

To avoid a _significant_ set of outbound traffic, I've made the choice to - right now - only support looking up one repo at a time, which should reduce the overhead, as a couple of datasets I've queried this with have tens of thousands of dependencies to look up, which is quite significant outbound traffic 🫣

So what does this look like?

For instance, on [my `ghprstats` project](https://gitlab.com/tanna.dev/ghprstats/), which does have some repos participating:

{{< asciicast id="hacktoberfest-ghprstats" src="/casts/dmd-hacktoberfest/ghprstats.json" >}}

And for a repository that doesn't have any dependencies that are participating, like [my `readme-generator` project](https://gitlab.com/jamietanna/readme-generator):

{{< asciicast id="hacktoberfest-readme-generator" src="/casts/dmd-hacktoberfest/readme-generator.json" >}}

This will hopefully be useful for folks who are looking for take their existing pre-built dataset of dependencies and work out where they can find dependencies that are of high value to the organisation (either used in a large number of repositories across the org, or that is a high percentage of the dependencies in use at the org) and be a good opportunity to give back to the community 👏🏼

What other information do you think would be useful to add here? Should I add dependency-management-data to projects participating in Hacktoberfest?
