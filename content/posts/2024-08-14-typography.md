---
title: "Creating a /typography page"
description: "Creating a page for viewing different types of content and how they display on my site."
tags:
- www.jvt.me
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
date: 2024-08-14T19:49:59+0100
slug: typography
---
As you may have noticed - unless you're an RSS-only reader, in which case, you may want to click through to see this page - I recently [revamped my site design](https://www.jvt.me/posts/2024/08/05/new-css-who-dis/).

As part of this, one of the things I needed to do was to make sure that the new design worked on the various pages and blog posts I have on my site. Given I've got well over [1000 blog posts](https://www.jvt.me/posts/2024/07/01/1000-posts/), that's not a small undertaking, and so I needed to find a good subset of pages that each did something interesting.

(Aside: I _do_ have an issue on my site's backlog to track down a list of "good blog posts" to review after a redesign, that would feed nicely into this, but I can't seem to find it)

This last week, on [the Hacker News discussion](https://news.ycombinator.com/item?id=41223902) of [Go is my hammer, and everything is a nail](https://www.maragu.dev/blog/go-is-my-hammer-and-everything-is-a-nail), I'd seen that <span class="h-card"><a class="u-url" href="https://www.maragu.dev">Markus</a></span> was touting his new [/typography](https://www.maragu.dev/typography) page.

I really like this as a concept, especially from someone who's just tweaked a number of things and would find it good to have a single place to look, and am sure I've seen others do this, but can't seem to immediately find anything in the [IndieWeb wiki](https://indieweb.org) as my first port of call. Do you know of other examples? Let me know!

While on the train down to London to [GopherCon UK](https://www.gophercon.co.uk/), I thought I'd have a stab at making my own /typography page, given for me, it's a case of "just" showing some common elements you'd want on a page, and is a nice throwback to all those NOT -> STP journies I used to make and hack on my site.

This is now live with my own [/typography/](/typography/) page 🎉

It's got some basic stuff in there, and will be useful for me in the future to test other human-facing changes, as and when I make changes.

While doing this, I've picked up on a couple of bugs:

- I'd completely lost any styling for `<blockquotes>`
- I'd not `max-width`'d the Asciicasts, so they displayed as a full width, full height embed which was generally rather large. They're now more manageable

Even if it's not interesting for y'all, it'll be useful for me in the future.
