---
title: Job titles are bullshit
description: When is a Senior Engineer not a Senior Engineer, no standardisation across the industry, and other reasons job titles are frustrating.
tags:
- career
date: 2024-02-26T14:23:30+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: job-titles-bullshit
image: https://media.jvt.me/6ee3a1ec8d.jpeg
---
I've had several versions of this post half-written in my head over the last year or so, but I'm finally getting around to writing it. This is a post that's informed by [the companies I've worked at](https://hire.jvt.me) alongside [recent job hunts](https://www.jvt.me/posts/2022/05/02/lessons-learned-job-hunt/) and from discussion with friends at different companies and who've been going through the job market in recent years.

Note that this isn't a scathing review of any particular company or me trying to throw shade, just some thoughts on things I've seen. This is largely with the view inside Software Development and Quality Engineering, and may apply to other roles.

I first saw the inequity of job titles within my first few years at Capital One.

I remember getting my first promotion at Capital One, which made me a Software Development Engineer 2 and a Senior Associate according to Capital One's internal levelling. This was a great achievement, and made me a "mid-level engineer" on the vague scale of engineering levels that I could use to compare myself with other folks I knew, but who didn't necessarily work at the same company as me.

I remember updating my LinkedIn to note that I was a Software Development Engineer 2, and noticing that some folks who'd been promoted at a similar time were noting that they were a Senior Software Engineer. This was a little annoying because we were the same level but it looked like they were now more senior, but I tried to let it go, as I knew that, at least internally, we were the same level, and to try not to let the perceived judgement of external folks matter.

This worked for a bit, until it came to my promotion to Software Development Engineer 3 (Principal Associate). I was very proud of this, and it meant that I'd reached a level where I could realistically call myself a Senior Engineer, although officially on LinkedIn I was a Software Development Engineer 3.

The problem at this time is that I'd worked hard to become a "real" Senior Engineer, but at this time, I was seeing a number of folks who'd been promoted to Software Development Engineer 2 calling themselves a "Senior Engineer" on LinkedIn, and as happy as I was for them, it did take a bit of the shine off my own promotion as it seemed like I was doing the same work as them.

This may seem petty, but it didn't really sit right as folks were indicating they were at a level they weren't at. And yes, if they went through a job interview it would be clear that they weren't a "Senior Engineer", but I still wasn't super happy with the way that the levels didn't seem to matter. But I wasn't unhappy enough to actually say something directly, just have this blog post bubbling in the back of my mind 😅

And at least for folks who are at Capital One, I can't _really_ fault them, as a couple of years in, there were some changes to tweak the job titles:

<table>
<tr>
<th>Level</th>
<th>Previous Job Title</th>
<th>New Job Title</th>
<th>"Standard" Level</th>
</tr>

<tr>
<td>Associate</td>
<td>Software Development Engineer 1 (SDE1)</td>
<td>Software Engineer</td>
<td>Junior</td>
</tr>

<tr>
<td>Senior Associate</td>
<td>Software Development Engineer 2 (SDE2)</td>
<td>Senior Software Engineer</td>
<td>Mid-level</td>
</tr>

<tr>
<td>Principal Associate</td>
<td>Software Development Engineer 3 (SDE3)</td>
<td>Principal Software Engineer</td>
<td>Senior</td>
</tr>

<tr>
<td>Manager</td>
<td>Senior Software Development Engineer (SSDE)</td>
<td>Master Software Engineer</td>
<td>Staff</td>
</tr>
</table>

Notice that now it's _even more_ unclear. What used to be a Staff-ish role used to be called Senior, and is now Master, and what was a mid-level role is now having the official title of Senior.

As someone who felt that this was mis-selling the levels and roles, I didn't follow this, but I can absolutely see why folks would now set their job title like so on LinkedIn because that's what their company says they should set it to.

But this goes further to the point of what does Senior Engineer even mean? Above I've indicated the "Standard" level but there isn't a definition across the industry of what different levels and expectations are, nor how many years of experience you need, or the sort of experiences you will have needed to be deemed Senior.

For instance, Monzo have a very high bar for their Senior Engineer level, and I've heard that may be getting an increase. Compare this to an early stage startup who may give everyone the title "Senior Engineer", whether they'd been a Staff/Principal engineer elsewhere, or not.

There are companies like Amazon or Netflix who deem their Senior role to be someone who's got 5+ years of experience, and some companies who require even more years to be deemed a Senior.

I've worked with engineers who've been hired at a higher level than I believe they are due to negotiating their compensating package higher, which is frustrating too, as you can then expect more of them, when they don't necessarily have all the skills.

It's also unclear whether being a Senior means you will have experienced leading feature delivery for a team, is "just" writing code or will also include being a Tech Lead, having experience improving your team and their ways of working, solving deeply technical problems or trying to address social problems in the organisation, and more.

I think it'd be ideal if there was an industry-wise acceptance of levels, so things like [levels.fyi](https://levels.fyi) isn't necessarily needed to determine how a role compares between companies, even if it's a fairly loose definition to make it easier to work out what junior/mid-level/senior/staff+ means.

As a neurospicy person, I quite like knowing where folks sit on the org chart compared to me. This isn't so I can feel superior to others, but just so I can understand a bit of social expectations, and for instance, being able to (sometimes) assume a bit more of knowledge and experience. Although [I prefer to give context where possible](https://www.jvt.me/posts/2018/08/16/context-is-king/) it can be nice having a shorthand at times.

But on the flipside, is it even worthwhile trying to get some consensus across the industry? Can we even meaningfully compare what i.e. Senior means companies? Sometimes it can be hard enough comparing the achievements of two engineers _in the same org_, let alone across organisation boundaries where there are different guiding principles, expectations and priorities for organisations' definitions of what good engineering means.

I realise this post is also pretty nice coming from someone privileged with the title Senior, and although her thread was about something slightly different, [Taylor Barnett-Torabi's point](https://hachyderm.io/@taylor_atx/111303914380380584) thoughts are still very important:

> Anyone who tells you titles don't matter are probably on the upper end of the power structure. Ignore them. They struggle to look beyond their power.
> I need to turn this into a blog post.

I'm coming at this from someone who's got over that hump, and can now - fairly reasonably - call themselves a Senior Engineer, and I absolutely don't want to and gatekeep others from reaching the next level, just that maybe we try and call things as they are instead of inflating roles to make everyone feel better.

I don't have a solution per se, but I think it's something we should be talking about - just as [pay equity](https://www.jvt.me/posts/2022/09/21/year-later-salary-history/) - and trying to understand where we can be doing things to make it easier for folks to understand how they fit into their organisation as well as the industry at large.
