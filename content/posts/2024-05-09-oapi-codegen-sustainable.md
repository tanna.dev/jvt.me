---
title: "Creating a more sustainable model for `oapi-codegen` in the future"
description: "Announcing a request for sponsorship to continue to allow allocating more time to `oapi-codegen` as well as to make more ambitious changes to the project."
tags:
- open-source
- oapi-codegen
date: 2024-05-09T08:58:35+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: oapi-codegen-sustainable
image: https://media.jvt.me/b41202acf7.png
---
Note that this is a copy of [the announcement on GitHub](https://github.com/deepmap/oapi-codegen/discussions/1606).

> [!NOTE]
> TL;DR: Maintenance of `oapi-codegen` is largely done for free, on the maintainers' personal time. We'd like to move to a more sustainable model by getting companies who use us to sponsor us.
>
> At this time, Jamie, one of our maintainers, would like to start working 1 day a week on Open Source, primarily focussed on `oapi-codegen`. Consider sponsoring him on [GitHub Sponsors](https://github.com/sponsors/jamietanna/) to make this possible, and for the project to be able to grow more sustainably over the coming years.

> [!IMPORTANT]
> The below is written by Jamie, who generally writes in the "royal we" and it is only planned that Jamie will be working on the project in a paid capacity.
>
> Sponsoring the project itself will, for the foreseeable future, be the same as directly paying Jamie.

Hey folks!

For those that celebrate, happy [Maintainer Month](https://maintainermonth.github.com/)!

As I'm sure a lot of you may relate to, there never feels like enough hours in the day.

Since [my first PR](https://github.com/deepmap/oapi-codegen/pull/576) to `oapi-codegen` in May 2022, ahead of me officially starting to get involved in maintaining the library in July, I've thoroughly enjoyed working with the project and the community, learning how different folks are using it and working to better the project for everyone.

I've been involved with, and a strong proponent of, OpenAPI for years now, and as I joined the Go ecosystem a couple of years ago, have loved working with `oapi-codegen`.

I'm ever grateful to Marcin for starting the project, and continuing to maintain it over the years, and I'm really glad I was able to get involved and help out. If you'd like to read a bit more about the history of the project, we've detailed it more [in this post](https://www.jvt.me/posts/2024/05/09/oapi-codegen-org/).

I love being able to contribute to, and maintain, Open Source projects - of which [I maintain a few](https://www.jvt.me/open-source/) 😅 - but this is always a trade-off against other priorities in my life, which sometimes means Open Source doesn't win. You may remember that there was _six months_ between the [v1.12.4](https://github.com/deepmap/oapi-codegen/releases/tag/v1.12.4) and [v1.13.0](https://github.com/deepmap/oapi-codegen/releases/tag/v1.13.0) releases, in which both Marcin and I were super busy life-wise.

Trying to balance an Open Source project - especially one as widely used as ours - is a difficult one, where I've been personally juggling a full-time, a puppy (who's now ~2.5 years old), going through a difficult redundancies period at a previous company, a new job and the associated ramp up of work towards that, not least to mention my personal blog and Open Source endeavours (in particular [dependency-management-data](https://dmd.tanna.dev)), to name a few things!

Open Source is great because as a community, we get to work towards solving common problems in a way that everyone can benefit. _However_ it's common that [the tech industry thrives on free work](https://www.jvt.me/posts/2022/10/22/tech-industry-free-labour/) and although "free software" often means "free as in beer", there is often [an unpaid developer holding up large businesses](https://xkcd.com/2347/).

![A tower of blocks is shown. The upper half consists of many tiny blocks balanced on top of one another to form smaller towers, labeled:, "All modern digital infrastructure". The blocks rest on larger blocks lower down in the image, finally on a single large block. This is balanced on top of a set of blocks on the left, and on the right, a single tiny block placed on its side. This one is labeled "A project some random person in Nebraska has been thanklessly maintaining since 2003"](https://imgs.xkcd.com/comics/dependency.png)

So, long preamble over, the discussion title notes that [@mromaszewicz](https://github.com/mromaszewicz) and I would like to work towards a more sustainable model for the project. As I've been thinking about the amount of work I do as a maintainer both on this project and a few others, I've considered that to make it truly sustainable for me and my time, it needs to become time that's carved out and more effectively prioritised.

If you were to analyse the data around when I most often respond to issues, it's _commonly_ in the ~30 minutes or so before I head to bed in the evening, or after I've had a direct ping on an issue, both of which are usually driven by a little guilt.

I do aim to keep on top of things, but we have a vibrant community, and like a hydra, every time I close off a few issues and then sit back for a minute, several more issues or PRs land that need attention. I love that, and am very appreciative of the continued use and collaboration from the community, but there's only so much we can do with the limited _free_ (in every sense of the word) time.

I'm a very strong Free Software and Open Source believer, and I am very glad that this project was started and has grown to have great usage. However, it is used by a number of very large companies, some of whom give back contributions, but none of whom actively contribute towards financial sustainability.

To work towards a more sustainable model for supporting the project, the best way is for (me) to be able to dedicate more time to it. That time has to come from somewhere, and so the best option I present is that we as maintainers should get paid to work on the project. This would make it possible to be able to move towards taking a day off a month, or maybe every couple of weeks, towards Open Source maintenance, and then moving to a 4-day week, where the day I am not working becomes Open Source maintenance day.

Long term, I'd like to [follow in the steps](https://words.filippo.io/full-time-maintainer/) of Filippo Valsorado and be paid full time to work on Open Source software (which would include a number of projects that I support), but before I can make this possible, I'd like to start getting there through companies who use `oapi-codegen`.

Therefore, this is a request to the community to [sponsor me on GitHub Sponsors](https://github.com/sponsors/jamietanna/). I also have [other options listed on my website](https://www.jvt.me/support-me/).

> [!NOTE]
> If you'd prefer to sponsor the project, rather than an individual developer, that's also possible - we're in the process of setting up GitHub Sponsors for `oapi-codegen`, which will be possible [after we move to the new organisation](https://www.jvt.me/posts/2024/05/09/oapi-codegen-org/) - note that this will still be going to Jamie, who will be doing the paid work.

I would also strongly recommend you talk to your employer about using a platform such as [Tidelift](https://tidelift.com) to [pay mainainers](https://blog.tidelift.com/paying-maintainers-the-howto) for software you use.

We've not yet finalised what the different sponsorship tiers could look like, but are at least for now considering that by sponsoring a recurring $150/mo payment entitles you to be listed on the project README as a sponsor.

Additionally, we welcome being paid for bug/feature bounties, which will be paid on an hourly basis. Most recently, the [v2.1.0 release](https://github.com/deepmap/oapi-codegen/releases/tag/v2.1.0) included a considerable chunk of work which was sponsored by [Ejendomstorvet](https://www.ejendomstorvet.dk/).

> [!IMPORTANT]
> Some of the below are examples of things that we **could** work towards. This is not a guarantee, and purely a few areas that we're considering.
>
> We'll work with the community to determine which make most sense.

There are a number of other areas we're considering that this time and money can be put towards:

- Continue to work on community management, triaging and addressing issues, feature requests and discussions
  - Additional time to focus on the community will also feed into being able to use [insights from OpenSauced](https://app.opensauced.pizza/workspaces/96b4c78f-73b3-45cc-90ff-3a94cdacab87/repository-insights/744/dashboard) as well as [CHAOSS metrics](https://chaoss.community/)
- Build out a proper documentation site, similar to [Ogen's](https://ogen.dev/)
- Build a playground, similar to [play.sqlc.dev](https://play.sqlc.dev), for oapi-codegen
- ~~Better plan out our roadmap~~ Actually have a roadmap that we work towards
- Further improving the stability of `oapi-codegen` by working with companies and developers to better understand their usage - for instance on a video call - which would then feed into our existing test suites
- Improve tooling and libraries that sit alongside oapi-codegen
- Work providing support for OpenAPI 3.1, by [taking the libopenapi support](https://github.com/deepmap/oapi-codegen/pull/1388) and fully integrating it
  - This is a bigger piece of work, and we're hesitant to merge it right now due to the additional work to take it on and maintain it
  - If your company is struggling without OpenAPI 3.1 support, please consider sponsoring to get the work in!
- Work towards bug fixes + feature improvements
- Provide a (monthly?) planned release cycle that can be depended on
- Making prebuilt binaries
  - Although this isn't the [recommended route](https://github.com/deepmap/oapi-codegen?tab=readme-ov-file#install) it's something that many folks ask about
  - See also: [Open Source Doesn't Require Providing Builds](https://codeengineered.com/blog/2024/open-source-not-builds/)
- Paying it forward - taking money we receive and sponsoring projects we heavily rely upon
- A [next-gen ability to customise the generated code](https://github.com/deepmap/oapi-codegen/issues/1594) which should make it much easier for specifications that you don't control
- Supporting backports to older versions, when customers require them

I'd love to help make the project more sustainable, but for me that requires getting something back financially from the community to help make it a more appropriate priority! If I don't receive sponsors, I **won't** stop my work on oapi-codegen, it just may mean that we're unable to achieve many of 👆

But if I _am_ able to make a considerable way towards my goals, I will look to work a four-day week and spend my day off on Open Source 🚀

My final call-to-action is to please consider how much [we as an industry thrive on free work](https://www.jvt.me/posts/2022/10/22/tech-industry-free-labour/), and that there are real human beings doing a lot of thankless work.

At a minimum, please say something nice! We'd love to hear stories of how it's helped you ship faster, avoid a load of work that's not worth doing by hand, or generally just been good for you.

But really, we'd love to get paid to continue to work on the project.

If you're a developer using `oapi-codegen`, we'd appreciate any donations you can give. But really, we need companies to support us, as it's more likely to get sustainable income from them, at the level we need.

Please talk to your manager, CTO, Open Source Program Office (if you have one), supplier management team and anyone who'll listen around getting some money towards the various projects we build and maintain.

Note that I've personally been separately discussing with my current employer, Elastic, about sponsoring some on-work time, as we've got a number of production services using the project. They've already been positive around me supporting initiatives that we have which depend on `oapi-codegen`, for instance the Nullable support added into v2.1.0, and this would enable me to also work on general maintenance tasks or non-Elastic features on work time, too.

Looking to work out how much your company uses the project? Check out [one of my other projects' insights into dependency usage](https://dmd.tanna.dev/case-studies/library-spread-oapi-codegen/).

Got any thoughts? Marcin and I would love to hear them [on GitHub](https://github.com/deepmap/oapi-codegen/discussions/1605).
