---
title: Dependency Management Data's now on Mastodon!
description: "Announcing the dependency-management-data Mastodon account for automated release announcements (and more?)."
tags:
- dependency-management-data
- mastodon
date: 2024-07-14T20:55:38+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: dmd-mastodon
image: https://media.jvt.me/0ce655e6df.png
---
With thanks to `botsin.space`, I've now got <span class="h-card"><a class="u-url" href="https://botsin.space/@DependencyManagementData">a Mastodon account for dependency-management-data</a></span>!

I'm still planning on having most of the interesting blogging and discussing going via my blog, but I may also reblog myself ([MFW](https://knowyourmeme.com/memes/obama-awards-obama-a-medal)) for anyone following the bot.

For the most part, this is going to be an alternative to [the fancy Slack notifications](https://www.jvt.me/mf2/2024/01/yabrd/) that folks on the DMD slack get, but as a public-facing alternative.

Aside: not on the dependency-management-data Slack but want an invite? Let me know!

You can see the [first announcement here](https://botsin.space/@DependencyManagementData/112786596143373914), and soon to be many many more! I'm excited to make my next breaking change to see what the message looks like 🙌🏼
