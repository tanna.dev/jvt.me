---
title: "You can now resolve remote presets when using Renovate's `local` platform in `renovate-graph`"
description: "Announcing a new release of `renovate-graph`, which can now follow `github>` and `local>` presets."
date: 2024-12-12T10:42:21+0000
tags:
- "renovate"
- dependency-management-data
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/6657d4ff89.jpeg"
slug: renovate-graph-local-presets
---
In [_Utilising Renovate's `local` platform to make `renovate-graph` more efficient_](https://www.jvt.me/posts/2023/10/13/renovate-graph-local/) I mentioned that you can take advantage of Renovate's [`local` platform](https://docs.renovatebot.com/modules/platform/local/) to run against a local directory.

During a hackathon week at work, I've recently been using `renovate-graph --platform local` alongside [my new `renovate-packagedata-diff` tool](https://www.jvt.me/posts/2024/12/08/renovate-packagedata-diff/) to provide a diff of dependencies in a PR, to present relevant information to the user, which has been working very well so far 🙌🏼

One wrinkle in the plan is that as an organisation we take advantage of [shared config presets](https://www.jvt.me/posts/2024/04/12/use-renovate/#centralised-shareable-presets) to centralise some common `customManagers` to extract common internal package files, or have other tweaks to different things, which is super useful.

However, there's a [long-standing issue in `renovate-graph`](https://gitlab.com/tanna.dev/renovate-graph/-/issues/38), inherited from [Renovate's known limitations in the `local` platform](https://docs.renovatebot.com/modules/platform/local/#limitations) where Renovate _rightfully so_ can't determine how to resolve a `local>` or `github>` preset, as in `local` mode, it doesn't have any way to know how to look these up.

This means that if in your repository, you had a `renovate.json`:

```json
{
  "extends": [
    "github>abc/foo:xyz.json5",
    "local>abc/foo:xyz"
  ]
}
```

This would then result in i.e.:

```
 INFO: Throwing preset error
       "validationError": "Preset caused unexpected error (local>elastic/renovate-config:platform-devflow)"
ERROR: Failed to read repository information for "local": Error: config-validation
       "error": {
         "name": "Error",
         "message": "config-validation",
```

However, today I've managed to add support to `renovate-graph` to add the ability to look up presets, even when running in `--platform local` 🚀

This is only possible in `renovate-graph`, due to additional metadata that gets as part of running with `--platform local` i.e.:

```sh
# linebreaks for readability
env RG_LOCAL_PLATFORM=github
    RG_LOCAL_ORGANISATION=elastic
    RG_LOCAL_REPO=kibana
    npx @jamietanna/renovate-graph@latest --platform local
```

In this case, because we're explicitly telling `renovate-graph` that we're running against GitHub, `renovate-graph` will assume that this is

It doesn't support self-hosted setups or non-GitHub hosts (for now), but does at least allow now resolving any presets in a [`config.js`](https://docs.renovatebot.com/self-hosted-configuration/) as well as a repo-level `renovate.json` 👏🏼

If you'd prefer not to follow presets, one option I've found useful is to use [`ignorePresets`](https://docs.renovatebot.com/configuration-options/#ignorepresets):

```sh
env RENOVATE_IGNORE_PRESETS='github>.../renovate-config:some-preset,local>.../renovate-config:another-preset' renovate-graph --platform local
```
