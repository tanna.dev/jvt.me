---
title: "Creating `renovate-packagedata-diff` to diff Renovate package data dumps"
description: "Announcing the release of `renovate-packagedata-diff` which makes it possible to provide a semantic diff between different Renovate package data dumps."
date: 2024-12-08T17:47:20+0000
tags:
- "renovate"
- dependency-management-data
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/6657d4ff89.jpeg"
slug: renovate-packagedata-diff
---
Over the last couple of years, I've been working with Renovate's package data dumps, as part of [`renovate-graph`](https://www.jvt.me/posts/2022/11/01/renovate-dependency-graph/) and work towards [dependency-management-data](https://dmd.tanna.dev).

These data dumps are either via `renovate-graph`, or from [the debug logs](https://dmd.tanna.dev/cookbooks/consuming-renovate-debug-logs/), or using the experimental [`reportType`s](https://docs.renovatebot.com/self-hosted-configuration/#reporttype) and consist of a (large) JSON blob that contains information about the packages detected for a given repository.

These blobs are _super_ important for the operation of dependency-management-data, and anyone wanting to programmatically work out the dependencies they have in a given repository, for instance using Renovate maintainer <span class="h-card"><a class="u-url" href="https://secustor.dev/">Sebastian Poxhofer</a></span>'s [work on a Backstage plugin](https://www.youtube.com/watch?v=dWuAn0x-yAk).

At least for usage with dependency-management-data, the recommended use of these blobs is to be committed to source code (un-prettified), and then i.e. periodically rebuilding the dependency-management-data database.

One problem with them being large JSON blobs, however, is that they're _pretty unwieldy_ to look at.

They're purposefully stored as un-prettified JSON, to avoid ever-so-slight but unnecessary storage space, but that means that showing a `git diff` is super unhelpful, at least out-of-the-box.

Additionally, there's no way at-a-glance to see whether some of the diff between files is useful, without doing a diff of the pretty-printed before/after, and then you the user _knowing_ which fields are important and are not.

You could use a fancy diff tool i.e. [`icdiff`](https://github.com/jeffkaufman/icdiff) to do the diff, which results in i.e.:

{{< asciicast id="icdiff" src="/casts/renovate-packagedata-diff/icdiff.json" >}}

But as noted, which of these fields are _actually_ important?

(also note that `icdiff` takes a while to compute the diff here, given this is a file is ~8300 lines prettified, or 196K of un-prettified text!)

For instance, for some package ecosystems, Renovate will indicate the `currentVersionAgeInDays`:

```json
{
  "autoReplaceStringTemplate": "{{depName}}/restore@{{#if newDigest}}{{newDigest}}{{#if newValue}} # {{newValue}}{{/if}}{{/if}}{{#unless newDigest}}{{newValue}}{{/unless}}",
  "commitMessageTopic": "{{{depName}}} action",
  "currentDigest": "6849a6489940f00c2f30c0fb92c6274307ccb58a",
  "currentValue": "v4.1.2",
  "currentVersion": "v4.1.2",
  "currentVersionAgeInDays": 43,
  "currentVersionTimestamp": "2024-10-22T12:33:17.000Z",
  "datasource": "github-tags",
  "depName": "actions/cache",
  "depType": "action",
  "fixedVersion": "v4.1.2",
  "packageName": "actions/cache",
  "registryUrl": "https://github.com",
  "replaceString": "actions/cache/restore@6849a6489940f00c2f30c0fb92c6274307ccb58a # v4.1.2",
  "sourceUrl": "https://github.com/actions/cache",
  "updates": [

  ],
  "versioning": "docker",
  "warnings": [

  ]
},
```

This isn't _as important_ as knowing that the version actually in use has changed, or that a dependency was deleted, and can make the diff far too noisy.

Because I spend a surprising amount of my time looking at these diffs, and because I want to start having a means to perform diffs of package data in CI, I set about building something to provide a human-readable diff of these data dumps, roughly 7 weeks ago (of on and off work).

With this, I've now released a new CLI in the dependency-management-data ecosystem, [`renovate-packagedata-diff`](https://dmd.tanna.dev/commands/renovate-packagedata-diff/) which aims to do this for you.

Now, instead of seeing a horribly unhelpful diff like so:

{{< asciicast id="before" src="/casts/renovate-packagedata-diff/before.json" >}}

You will now get a much prettier - and human-readable - diff, indicating what's been added/modified/removed:

{{< asciicast id="after" src="/casts/renovate-packagedata-diff/after.json" >}}

This is a _huge_ quality-of-life improvement, and I've found this to already be thoroughly useful.

This _should_ work between different types of data exports, and do not currently support diffing Renovate reports as they're a little bit more complex, but they are [supported by the full dependency-management-data tooling](https://dmd.tanna.dev/cookbooks/consuming-renovate-report/).

As written about in [the docs](https://dmd.tanna.dev/renovate-packagedata-diff/), this is something you can wire in via your local Git config and `.gitattributes`, and then have it on by default (_ish_).

I say _ish_ because Git requires you add the `--ext-diff` flag to allow calling the external command i.e. `git log -p --ext-diff`.

In the future I'll finalise the changes to expose this for CI purposes (with a `--json` flag) and fix [a bug](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/626) where the same dependency (with multiple version) in the same `package_file` results in incorrect diffs, but for now, I'm really happy with the result 🚀
