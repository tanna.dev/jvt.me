---
title: "Gotcha: SXHKD doesn't like comments (in bindings)"
description: "Why your SXHKD bindings may be silently ignored"
date: 2024-08-15T11:23:27+0100
tags:
- "blogumentation"
- "sxhkd"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: sxhkd-comments
---
I've written before about my usage of the [BSPWM](https://github.com/baskerville/bspwm) tiling window manager, which goes hand in hand with [Simple X HotKey Daemon (SXHKD)](https://github.com/baskerville/sxhkd).

I noticed earlier that while trying to set a SXHKD binding, I suddenly stopped being able to trigger the creation of a window via the keybinding.

For instance, with the following binding:

```
super + shift + d
	# Next float via https://www.jvt.me/posts/2024/08/15/bspwm-next-float/
	bspc rule -a '*' -o state=floating; kitty
```

I was finding that SXHKD silently didn't respond to the keypress, nor could I see anything when running the `sxhkd` daemon interactively.

It appears that it was due to the location of the comment, and the following diff fixed it:

```diff
+# Next float via https://www.jvt.me/posts/2024/08/15/bspwm-next-float/
 super + shift + d
-	# Next float via https://www.jvt.me/posts/2024/08/15/bspwm-next-float/
 	bspc rule -a '*' -o state=floating; kitty
```

Once this was done, it was able to run correctly. This is another subtle reminder for me not to treat it as a shell script being executed!
