---
title: "Getting access to rich-text data from the clipboard (on Linux)"
description: "How to use `xclip` to retrieve rich-text output from the clipboard."
date: 2025-01-26T20:19:18+0000
tags:
- "blogumentation"
- "linux"
- "command-line"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: linux-html-clipboard
---
I've recently noticed that i.e. on Slack, if I right click a channel and click "Copy name", and then paste that into a Slack message, I get the name of the channel, but if I paste it into GitHub or Google Docs, I get a rich text link, with the name of the channel, and a link to the channel.

This is _really_ nice behaviour, but with [the way my team does standups](https://www.jvt.me/posts/2024/11/24/async-standups/) and [my Neovim plugin for standup updates](https://www.jvt.me/posts/2024/11/24/neovim-standup/), I wanted to make sure that I'm able to get the same functionality myself.

Digging into this [via StackOverflow](https://stackoverflow.com/a/16960316), it looks like the `xclip` command has the `--target` flag:

```sh
% xclip -o --target text/html
<meta http-equiv="content-type" content="text/html; charset=utf-8"><a href="https://github.com/deepmap/oapi-codegen">oapi-codegen</a>%
```

This is returned as an alternative to the default plain-text output:

```
% xclip -o
oapi-codegen
```

You can also list the options available for outputs with `-t TARGETS`:

```
% xclip -o -t TARGETS
TIMESTAMP
TARGETS
MULTIPLE
text/html
text/_moz_htmlcontext
text/_moz_htmlinfo
UTF8_STRING
COMPOUND_TEXT
TEXT
STRING
text/plain;charset=utf-8
text/plain
text/x-moz-url-priv
```
