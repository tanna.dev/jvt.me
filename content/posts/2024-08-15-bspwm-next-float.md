---
title: "How to get the next window created set to floating in BSPWM"
description: "How to tell BSPWM that the next window created should be floating."
date: 2024-08-15T11:16:10+0100
tags:
- "blogumentation"
- "bspwm"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/8370d97494.png"
slug: bspwm-next-float
---
I've written before about my usage of the [BSPWM](https://github.com/baskerville/bspwm) tiling window manager.

One thing I've recently been looking at doing setting up a "scratch pad" style setup to launch [my local SQL workflow](https://www.jvt.me/posts/2024/06/07/sql-workflow/) so I can query my local instance of data from [dependency-management-data](https://dmd.tanna.dev), especially for ad-hoc requests at work.

To do this, I wanted to:

- Create a new terminal
- Open up a pre-written base for SQL queries
- Make that terminal window floating over all other windows

Via [this comment on Reddit](https://www.reddit.com/r/bspwm/comments/w6k6ue/comment/ihebl43/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button) I found that you can create a `bspc rule` for the next created window only, via:

```sh
bspc rule -a '*' # ... arguments here
```

So in this case, as we want to make sure that we create the next window as a floating window, we would run:

```sh
bspc rule -a '*' -o state=floating
```

Then, the next created window would be floating.
