---
title: "I'm a recipient of the 2024 October Engineering Monthly Award Recognition at Elastic"
description: "Announcing winning the 2024 October Engineering Monthly Award Recognition at Elastic."
date: 2024-11-29T16:46:00Z
tags:
- "job"
- career
- "elastic"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: elastic-october-award
image: https://media.jvt.me/5e45b3ba99.jpeg
---
I was chuffed today to hear that I was one of the winners of October's round of the Engineering Monthly Award Recognition programme at Elastic 🚀

This is a monthly process where engineering leads across the organisation vote based on nominations, and through a mix of nominations in recent months as well as in October, I'm a winner this month.

This is a pretty great accolade at Elastic, and provides good visibility and evidence of my impact at Elastic in recent months.

You can read the summary of why I was awarded this below:

> Jamie Tanna from the Platform Engineering Productivity team consistently goes above and beyond to support his colleagues and drive meaningful improvements across the organization. Jamie regularly identifies bottlenecks and inefficiencies, and proactively drives to solve them.  Known for his collaborative mindset, communication skills and working well across teams, Jamie has been instrumental in addressing complex challenges and enabling smoother workflows. His positive attitude and dedication have made him an invaluable member of the team.

I think this is a really great summary of some of the work I've been doing in recent months, and am very happy to be a recipient for the recognition, and the ~~$1000~~ £500 that comes along with it!
