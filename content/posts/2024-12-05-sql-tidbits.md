---
title: "Some useful SQL(ite) tips I've learned"
description: "A collection of SQLite snippets I've picked up recently to improve my queries."
date: 2024-12-05T09:57:16+0000
tags:
- blogumentation
- sqlite
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: sql-tidbits
image: https://media.jvt.me/0cc01860d8.png
syndication:
- https://news.ycombinator.com/item?id=43168172
- https://www.reddit.com/r/sqlite/comments/1ix5kka/some_useful_sqlite_tips_ive_learned/
---
I've recently spent some time working towards presenting some of the findings from [dependency-management-data](https://dmd.tanna.dev) at work, which has required writing some fun queries, and then using Google Sheets to better visualise that data.

Not only have I taken advantage of some fairly straightforward queries, written off-the-cuff, or relied upon some pre-written queries in the DMD codebase, I've also found myself needing to branch out and write some much more complex SQL, as I didn't want to add any logic into the Google Sheet and have that only as a presentation layer, which could then be consumed by a Google Slides presentation.

In particular, because I haven't looked at presenting this data directly via Google Sheets visualisations, I eneded up searching for things like `sqlite buckets` or `how to make rows columns vice versa`, to mixed luck.

Instead of this being a spatter of separate posts, I thought I'd collect them here for lower discoverability, but higher density.

## Splitting data into buckets

This is a big one that took me _many_ attempts to find an article / StackOverflow post that would explain quite right for what I wanted to do.

For instance, let's say we have the following data (limited for brevity):

<table class="rows-and-columns">
    <thead>
        <tr>
            <th class="col-platform" scope="col">platform</th><th class="col-organisation" scope="col">organisation</th><th class="col-repo" scope="col">repo</th><th class="col-package_name" scope="col">package_name</th><th class="col-version" scope="col">version</th><th class="col-current_version" scope="col">current_version</th><th class="col-package_manager" scope="col">package_manager</th><th class="col-package_file_path" scope="col">package_file_path</th><th class="col-dep_types" scope="col">dep_types</th><th class="col-level" scope="col">level</th><th class="col-advisory_type" scope="col">advisory_type</th><th class="col-description" scope="col">description</th><th class="col-supported_until" scope="col">supported_until</th><th class="col-eol_from" scope="col">eol_from</th>
        </tr>
    </thead>
<tbody>

<tr>

<td class="col-platform">github</td>

<td class="col-organisation">stretchr</td>

<td class="col-repo">testify</td>

<td class="col-package_name">go</td>

<td class="col-version">1.11</td>

<td class="col-current_version">1.23.3</td>

<td class="col-package_manager">gomod</td>

<td class="col-package_file_path">_codegen/go.mod</td>

<td class="col-dep_types">["golang"]</td>

<td class="col-level">ERROR</td>

<td class="col-advisory_type">UNMAINTAINED</td>

<td class="col-description">go 1.11 has been End-of-Life for 1897 days</td>

<td class="col-supported_until">&nbsp;</td>

<td class="col-eol_from">2019-09-03</td>

</tr>

<tr>

<td class="col-platform">github</td>

<td class="col-organisation">wiremock</td>

<td class="col-repo">wiremock</td>

<td class="col-package_name">gradle</td>

<td class="col-version">4.5.1</td>

<td class="col-current_version">4.5.1</td>

<td class="col-package_manager">gradle-wrapper</td>

<td class="col-package_file_path">perf-test/gradle/wrapper/gradle-wrapper.properties</td>

<td class="col-dep_types">[]</td>

<td class="col-level">ERROR</td>

<td class="col-advisory_type">UNMAINTAINED</td>

<td class="col-description">gradle 4 has been End-of-Life for 2178 days</td>

<td class="col-supported_until">2018-11-26</td>

<td class="col-eol_from">2018-11-26</td>

</tr>

<tr>

<td class="col-platform">github</td>

<td class="col-organisation">wiremock</td>

<td class="col-repo">wiremock-resilience-examples</td>

<td class="col-package_name">gradle</td>

<td class="col-version">4.5.1</td>

<td class="col-current_version">4.5.1</td>

<td class="col-package_manager">gradle-wrapper</td>

<td class="col-package_file_path">gradle/wrapper/gradle-wrapper.properties</td>

<td class="col-dep_types">[]</td>

<td class="col-level">ERROR</td>

<td class="col-advisory_type">UNMAINTAINED</td>

<td class="col-description">gradle 4 has been End-of-Life for 2178 days</td>

<td class="col-supported_until">2018-11-26</td>

<td class="col-eol_from">2018-11-26</td>

</tr>

<tr>

<td class="col-platform">gitlab</td>

<td class="col-organisation">gitlab-org</td>

<td class="col-repo">gitlab</td>

<td class="col-package_name">node</td>

<td class="col-version">4.2.2</td>

<td class="col-current_version">4.2.2</td>

<td class="col-package_manager">gitlabci</td>

<td class="col-package_file_path">lib/gitlab/ci/templates/Pages/Metalsmith.gitlab-ci.yml</td>

<td class="col-dep_types">["image"]</td>

<td class="col-level">ERROR</td>

<td class="col-advisory_type">UNMAINTAINED</td>

<td class="col-description">nodejs 4 has been End-of-Life for 2388 days</td>

<td class="col-supported_until">2017-04-01</td>

<td class="col-eol_from">2018-04-30</td>

</tr>

</tbody>
</table>

If we wanted to get a high-level view of what packages (via the `package_name`) were in the state of being `UNMAINTAINED` or `DEPRECATED`, how would we do that, so we can i.e. get the following output?

<table class="rows-and-columns">
    <thead>
        <tr>
            <th class="col-package_name" scope="col">package_name</th><th class="col-bucket" scope="col">bucket</th><th class="col-count-8257ab" scope="col">count(*)</th>
        </tr>
    </thead>
<tbody>

<tr>

<td class="col-package_name">go</td>

<td class="col-bucket">unmaintained</td>

<td class="col-count-8257ab">1</td>

</tr>

<tr>

<td class="col-package_name">gradle</td>

<td class="col-bucket">unmaintained</td>

<td class="col-count-8257ab">2</td>

</tr>

<tr>

<td class="col-package_name">node</td>

<td class="col-bucket">deprecated</td>

<td class="col-count-8257ab">2</td>

</tr>

<tr>

<td class="col-package_name">node</td>

<td class="col-bucket">unmaintained</td>

<td class="col-count-8257ab">10</td>

</tr>

<tr>

<td class="col-package_name">python</td>

<td class="col-bucket">deprecated</td>

<td class="col-count-8257ab">1</td>

</tr>

<tr>

<td class="col-package_name">rails</td>

<td class="col-bucket">unmaintained</td>

<td class="col-count-8257ab">2</td>

</tr>

<tr>

<td class="col-package_name">ruby</td>

<td class="col-bucket">unmaintained</td>

<td class="col-count-8257ab">1</td>

</tr>

</tbody>
</table>

To do this, we can use a `case` / `when` statement to group each type of data into a named bucket, like so:

```sql
select
package_name,
(
	case
	when abs(cast ((julianday(eol_from) - julianday('now')) as integer)) > (365 * 5)
		then 'unmaintained'
	when abs(cast ((julianday(supported_until) - julianday('now')) as integer)) > (365 * 5)
		then 'deprecated'
	else
		'THIS SHOULD NOT BE HIT'
	end
) as bucket,
count(*)
from advisories
where
  (
    abs(cast ((julianday(eol_from) - julianday('now')) as integer)) > (365 * 5)
  )
  or
  (
    abs(cast ((julianday(supported_until) - julianday('now')) as integer)) > (365 * 5)
  )
group by package_name, bucket
order by package_name, bucket
```

This is _very_ useful when trying to then graph the resulting data, and I've ended up using this bucketing quite a few times after finding out how to do it 🙌🏼

## `ORDER BY` doesn't need to reference a column

Something interesting is the fact that it's possible to use an `ORDER BY` with an arbitrary statement, for instance:

```sql
select
  visibility,
  count(*)
from
  repository_metadata
group by
  visibility
order by
  (
    case
      visibility
      when 'PUBLIC' then 0
      when 'INTERNAL' then 1
      when 'PRIVATE' then 2
      else 3
    end
  )
```

Previously, I had been creating a separate column called `ord` and then was using this, but this is much simpler.

## Querying JSON

I've written about this more [in a separate post](https://www.jvt.me/posts/2023/03/13/sqlite-json/) but one thing to note that I've recently been doing is relying on the shorthand "arrow functions" that are inbuilt to SQLite, and are familiar if you're coming from Postgres.

For instance, let's say we have the following:

```sql
select
  id,
  json_extract(data.json, '$.name') name
from
  data
```

We can actually further simplify this with an arrow operator, `->>`:

```diff
 select
   id,
-  json_extract(data.json, '$.name') name
+  data.json ->> '$.name' name
 from
   data
```

## Concatenating strings

Something I've done a bit before, but for posterity:

```sql
  -- ...
  datasource || ': ' || update_type as label,
  --         ^^ this concatenates between multiple strings
  -- ...
```

Very useful when trying to add useful labels for graphs.

## Providing default values for `NULL`s

In the case that we're retrieving a nullable column from database, but want to provide a default value, we can use `coalesce`.

For instance:

```sql
select
  coalesce(current_version, version) as ver
from
  renovate
```

Or:

```sql
select
  coalesce(metadata, '{}') as m
-- ...
```

## Date calculations

Something I've been doing since very early on with DMD is calculating "days between" dates, i.e. to given an indication of "you've been running Node 12.x for 900 days past its End-of-Life date".

For instance, if we want to take a column like:

<table class="rows-and-columns">
    <thead>
        <tr>
            <th class="col-supported_until" scope="col">supported_until</th>
        </tr>
    </thead>
    <tbody>

<tr>

<td class="col-supported_until">2024-10-22</td>

</tr>

</tbody>
</table>

And work out how many days it's been since that date, we could write a query using `julianday`, such as:

```sql
select
  supported_until,
  -- it's more useful to show the absolute (aka always positive) value to humans
  abs(cast(julianday(supported_until) - julianday('now') as integer)) as since
from
  advisories
where
  supported_until is not null
  and
  -- this number will be negative if it's in the past
  cast(julianday(supported_until) - julianday('now') as integer) < 0
```

## Using subqueries as a way to produce column-focussed data (for presenting in Google Sheets)

Within the DMD codebase, I'm using the excellent [`sqlc`](https://sqlc.dev) for my database queries.

One thing it doesn't really support - which is fair - is heavily nested queries, including those with subqueries, so I don't generally lean on them as it's hard to use them with `sqlc`.

However, when preparing this presentation, I found that as I _wasn't_ tied to `sqlc` while preparing the data, I could write the most cursed and nested subqueries I wanted 😈

This allowed me to think about how to handle getting the right columns in the resulting output, at the cost of it generally being a less efficient query, but working for what I needed.

For instance, the below query:

```sql
select
(
	select
	count(*)
	from
	(
		select
		distinct package_manager, package_name
		from
		renovate
	)
) as num_deps,
(
	select
	count(*)
	from advisories
	where advisory_type = 'DEPRECATED'
) as total_deprecated,
(
	select
	count(*)
	from advisories
	where advisory_type = 'UNMAINTAINED'
) as total_unmaintained
```

This _isn't_ the most complex example I could share, but out of an abundance of shame I won't go into it too much 🫣
