---
title: "What level of access do I have on that GitHub repository?"
description: "How to check, via the GitHub API, what access a given user has on a repository."
tags:
- blogumentation
- github
date: 2025-01-02T17:18:54+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: what-access-github
image: https://media.jvt.me/36fd7d2a48.png
---
Earlier today, I was trying to work out if I had sufficient privileges to make some changes on a repository.

Without access to the `Settings` panel as a non-`Admin` on the repo made it a little difficult, so I went looking for API access, and it turns out there are some good APIs for checking collaborator access, such as [Get repository permissions for a user](https://docs.github.com/en/rest/collaborators/collaborators?apiVersion=2022-11-28#get-repository-permissions-for-a-user) as well as [Check if a user is a repository collaborator](https://docs.github.com/en/rest/collaborators/collaborators?apiVersion=2022-11-28#check-if-a-user-is-a-repository-collaborator).

We can use this to check i.e. if I personally have access to `oapi-codegen` repo:

```
$ gh api /repos/oapi-codegen/oapi-codegen/collaborators/jamietanna/permission
{
  "permission": "admin",
  "role_name": "admin",
  "user": {
    "login": "jamietanna",
    "permissions": {
      "admin": true,
      "maintain": true,
      "pull": true,
      "push": true,
      "triage": true
    },
    "role_name": "admin",
  }
}
```

(slightly omitted for brevity)

Alternatively, for a user that'll only have read-only access:

```
$ gh api /repos/oapi-codegen/oapi-codegen/collaborators/jamietanna-bot/permission
{
  "permission": "read",
  "role_name": "read",
  "user": {
    "login": "jamietanna-bot",
    "permissions": {
      "admin": false,
      "maintain": false,
      "pull": true,
      "push": false,
      "triage": false
    },
    "role_name": "read",
  }
}
```


This only works when you have at least `push` access to a repository:

```
% gh api /repos/wiremock/wiremock.org/collaborators/jamietanna/permission
gh: Must have push access to view collaborator permission. (HTTP 403)
{
  "message": "Must have push access to view collaborator permission.",
  "documentation_url": "https://docs.github.com/rest/collaborators/collaborators#get-repository-permissions-for-a-user",
  "status": "403"
}
```

This also only works for a GitHub _User_ not an App.
