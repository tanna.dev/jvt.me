---
title: "Modifying the response body of an `httputil.ReverseProxy` response"
description: "How to modify the response from a `httputil.ReverseProxy` before it goes back to the caller."
tags:
- blogumentation
- go
date: 2024-06-25T21:26:31+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: modify-go-reverseproxy-response
image: https://media.jvt.me/b41202acf7.png
---
I'm currently working with the maintainer of the beautiful [`sql-studio`](https://github.com/frectonz/sql-studio) to make it possible to [serve the application from a subpath](https://github.com/frectonz/sql-studio/issues/16), which would allow me to add support for [running `sql-studio` as dependency-management-data's web interface](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/588).

As part of some investigations around this, I wanted to make a [somewhat cursed attempt](https://github.com/frectonz/sql-studio/issues/16#issuecomment-2189870808) to perform some server-side rewriting so I could make changes like:

```diff
-    <script type="module" crossorigin src="/assets/index-Dc4xAj-D.js"></script>
-    <link rel="stylesheet" crossorigin href="/assets/index-DpDMcPjm.css">
+    <script type="module" crossorigin src="/sql-studio/assets/index-Dc4xAj-D.js"></script>
+    <link rel="stylesheet" crossorigin href="/sql-studio/assets/index-DpDMcPjm.css">
   </head>
```

To do this, I've been using [`httputil.ReverseProxy`](https://pkg.go.dev/net/http/httputil#ReverseProxy), with an initial setup like:

```go
package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

func main() {
	u, err := url.Parse("http://localhost:3030")
	if err != nil {
		log.Fatal(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(u)
	mux := http.NewServeMux()
	mux.Handle("/sql-studio", proxy)
	log.Fatal(http.ListenAndServe(":8080", mux))
}
```

To perform the rewriting inside the proxy, I found [this StackOverflow](https://stackoverflow.com/a/45648370/2257038) which indicated that I should use the `ModifyResponse` function, which allows us to do something like:

```go
package main

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
)

func main() {
	u, err := url.Parse("http://localhost:3030")
	if err != nil {
		log.Fatal(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(u)
	proxy.ModifyResponse = func(r *http.Response) error {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			return err
		}
		defer r.Body.Close()
		// this is pretty hacky, but as a step towards https://github.com/frectonz/sql-studio/issues/16 in the meantime of upstream support
		b = bytes.Replace(b,
			[]byte("href=\"/"),
			[]byte("href=\"/sql-studio/"),
			-1)
		// ...

		// make sure we set the body, and the relevant headers for well-formed clients to respect
		r.Body = io.NopCloser(bytes.NewReader(b))
		r.ContentLength = int64(len(b))
		r.Header.Set("Content-Length", strconv.Itoa(len(b)))
		return nil
	}

	mux := http.NewServeMux()
	mux.Handle("/sql-studio/", proxy)
	log.Fatal(http.ListenAndServe(":8080", mux))
}
```

Et voila, we can now rewrite the response body from the proxied service, but before it goes back to the caller.
