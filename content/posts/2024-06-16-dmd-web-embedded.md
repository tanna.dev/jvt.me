---
title: Dependency Management Data's web application can now be deployed as a single static binary
description: "Announcing dependency-management-data's embedded SQL browser interface."
tags:
- dependency-management-data
- sql
date: 2024-06-16T14:40:55+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: dmd-web-embedded
image: https://media.jvt.me/0ce655e6df.png
---
Since the first iteration of the [dependency-management-data](https://dmd.tanna.dev) web application, I've offloaded the experience of writing and querying SQL querying to the excellent [Datesette](https://datasette.io) tool.

I'm a huge fan of Datasette, and recommend it as a great web-based SQLite database browser for querying and exploring your SQLite databases.

However, it makes deployments of the [`dmd-web`](https://dmd.tanna.dev/web/) CLI a little more difficult, as `dmd-web` is a static Go binary, but to run the Datasette application we need the Python runtime.

For organisations looking to reduce the CVE attack surface as well as simplifying their deployment runtime, this can make the `dmd-web` application a little less appealing.

As of Friday's [v0.97.0 release](https://gitlab.com/tanna.dev/dependency-management-data/-/releases/v0.97.0), it's now possible to omit the dependency on Datasette, and instead use an embedded database browser.

This means that for a query such as:

```sql
select
  distinct
  renovate.platform,
  renovate.organisation,
  renovate.repo,
  owner
from
  renovate left join owners
    on  renovate.platform = owners.platform
    and renovate.organisation = owners.organisation
    and renovate.repo = owners.repo
;
```

We can then compare what this looks like [using Datasette](https://dependency-management-data-example.fly.dev/datasette/dmd?sql=select+%0D%0Adistinct%0D%0Arenovate.platform%2C%0D%0Arenovate.organisation%2C%0D%0Arenovate.repo%2C%0D%0Aowner%0D%0Afrom+renovate+left+join+owners%0D%0Aon++renovate.platform+%3D+owners.platform%0D%0Aand+renovate.organisation+%3D+owners.organisation%0D%0Aand+renovate.repo+%3D+owners.repo%0D%0A%3B):

![A rendering of the above link, showing the result of the Datasette SQL browser querying a list of repositories and owners, in a rendering on a light background](https://media.jvt.me/e7528cb86b.png)

And then the same query [using `dmd-web`'s embedded database browser](https://dependency-management-data-example.fly.dev/browser/?encoded=c2VsZWN0IApkaXN0aW5jdApyZW5vdmF0ZS5wbGF0Zm9ybSwKcmVub3ZhdGUub3JnYW5pc2F0aW9uLApyZW5vdmF0ZS5yZXBvLApvd25lcgpmcm9tIHJlbm92YXRlIGxlZnQgam9pbiBvd25lcnMKb24gIHJlbm92YXRlLnBsYXRmb3JtID0gb3duZXJzLnBsYXRmb3JtCmFuZCByZW5vdmF0ZS5vcmdhbmlzYXRpb24gPSBvd25lcnMub3JnYW5pc2F0aW9uCmFuZCByZW5vdmF0ZS5yZXBvID0gb3duZXJzLnJlcG8KOw):

![A rendering of the above link, showing the result of the embedded dependency-management-data SQL browser querying a list of repositories and owners, on a dark background](https://media.jvt.me/c79958ee3a.png)

Note that the embedded web browser for `dmd-web` isn't as fully featured as Datasette, which is an active design decision. The expectation is that `dmd-web`'s embedded SQL browser is meant to be used for known queries, but for exploratory use-cases, it's still recommended to use Datasette.

In the future, [the embedded browser will be made more user friendly](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/574) to give a setup similar to [my preferred local workflow](https://www.jvt.me/posts/2024/06/07/sql-workflow/), providing autocompletion for the database's fields.

If you're already using `dmd-web` and disable the Datasette functionality, you'll land on fallback pages on `dmd-web`, which will handle queries, but not browsing to tables.

Finally, the embedded browser follows the same model as Datasette by embedding query information in the URL to make it easier to share queries with others, but unlike Datasette, `dmd-web` uses base64 URL encodes the query parameter, which allows avoiding issues with i.e. Cloudflare and other protection systems that try to avoid SQL injection, even though this is an explicit feature 😅 And if you provide an unencoded format i.e. `?sql=select...`, `dmd-web` will still respond to that, too.

I'm looking to continue investing in the embedded query browser, and would be interested to hear feedback!
