---
title: 'Product requirements for a "fund your dependency tree" service'
description: 'Thinking about what functionality I would want as a funder, or a fundee, for a "fund your dependency tree" service.'
date: 2025-02-20T17:55:11+0000
tags:
- "open-source"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: funding-oss-product
---
While at [State of Open Con](https://stateofopencon.com/), I had a great evening out with <span class="h-card"><a class="u-url" href="https://andy.xyz">Andy Williams</a></span>, and one of the topics we discussed at length was Open Source software funding.

Realising that I actually had a number of ideas around how to approach this - in particular from a product point of view - I thought it's worth writing up my thoughts, and get some wider feedback.

This isn't something new for me to be thinking about - a couple of years back I wrote [_Idea for Open Source/Startup: monetising the supply chain_](https://www.jvt.me/posts/2022/06/01/idea-supply-chain-monetisation/), which fed into the creation of [dependency-management-data](https://dmd.tanna.dev), and this topic has been something "bubbling away" in the back of my mind for a bit.

My thoughts have been informed through discussions I had previously at Deliveroo around Open Source funding (the answer being "no money, but _maybe_ you can have time to work on projects"), my work on [dependency-management-data](https://dmd.tanna.dev) and discovering metadata around projects you depend on and insights into whether they require funding, my time in Elastic's Open Source Working Group (pre-Open Source Program Office), and my time working with other Open Source Program Offices under the TODO Group at the Linux Foundation, as well as being [a maintainer](/open-source/) myself.

I've also done some testing (without actually spending any of my own money - boo!) through tools like [StackAid](https://www.stackaid.us/), [Thanks.dev](https://thanks.dev/), and am a maintainer for [Tidelift](https://tidelift.com), so also have some insight from both sides of the funding topic - as both a funder and fundee.

I've been wanting to catch up on [the Funding DevRoom at FOSDEM](https://fosdem.org/2025/schedule/track/funding/) from a couple of weeks ago, but wanted to share my thoughts first, so they're not skewed by the excellent folks' thoughts. I'll likely have some differing thoughts after watching the talks, so maybe there'll be a follow-up post.

The below thoughts are semi-structured, with some having some thinking about how it may be feasibly implemented, but some of them are more considerations to take.

I'll also note that although I'm using "Open Source" as the term below, this does also include "open source", aka non-OSI-approved projects.

## Meet maintainers where they are, financially

Firstly, if the platform requires a maintainer to sign up to receive funds, then they're already introducing friction.

I, as a maintainer, am set up on several platforms (GitHub Sponsors, OpenCollective, Tidelift, to name a few) but am also keeping an eye on platforms like StackAid or Thanks.dev, which I would need to sign up to when my package(s) start receiving funding there.

I understand that it's likely a mix of trying to build a platform maintainers want to use, as well as likely some regulatory needs to "know your customer" for the maintainers who are being funded, but it also means that maintainers are having to sign up to many different services to get their funding, each of which can require different levels of identity documents, financial and tax documents, and are generally an awkward barrier to deal with.

I would love if there was a way to orchestrate the funding through other means - notably where maintainers _already are_ instead of _another_ platform to onboard to, go through legal and financial requirements, then start receiving funding.

This may be a case of prioritising funding through i.e. GitHub Sponsors or OpenCollective if they're present, and then having a fallback for using the in-platform funding needs.

## Don't stop at supporting "standard" package manifests

I'm the co-maintainer of the Go project [`oapi-codegen`](https://github.com/oapi-codegen/oapi-codegen), which is primarily used as a command-line tool.

This means that it's not _usually_ found in the "standard" package manifest of a `go.mod`, but may be found in a `Makefile` or some other non-standard package files, which isn't be parsed by _any_ of the funding tools I've used.

I've suggested a couple of times to Tidelift that they should add support for this use-case, especially given I want to increase the number of organisations who sponsor me through Tidelift, but so far I've not seen any progress.

This isn't, however, a problem that is only seen with Tidelift - as I mentioned above, the other platforms I've looked at using focus on the "standard" package manifests like a `go.mod` or a `build.gradle.kts`, not the myriad of other places that a dependency may be found.

I absolutely get that supporting "standard" package manifests are important, but these are table stakes, and we (as funders and maintainers) should be pushing for platforms to support all the other ways that dependencies are declared, so we have a much more honest view of the dependencies we have, so we can better fund our projects.

Yes, this may end up being a rather complicated to determine dependency trees, but supporting more things will make it _much_ more rich and realistic to get the right people compensated for the work they're doing.

This is also why I heavily prefer using tools like [`renovate-graph`](https://gitlab.com/tanna.dev/renovate-graph) (which is a lightweight wrapped over [Renovate](https://docs.renovatebot.com/)), which gives much greater support for different package types, and [you can even add support for non-standard files ](https://www.jvt.me/posts/2024/04/12/use-renovate/#adding-support-for-additional-ecosystems), compared to vendor-built tools that don't have the wide support or ability to add your own.

## How do you want to handle company-backed projects?

There are a number of Open Source projects that are supported by for-profit companies. This is _generally_ good, because these companies fund full-time staff to work on the project, but this does lead to a slightly difficult question with respect to funding.

For instance, if your organisation heavily uses React, do you want to fund Meta?

What if you work at Deliveroo, and you heavily use Open Source from Uber, one of your competitors? Would there be any "optics" that could come from funding a project of a competitor?

This is something you may want more control over, rather than funding everything regardless of who's behind it.

## How do you want to handle foundations?

In a similar vein, how do you want to handle the funding of a project that's under a foundation?

If you're a member of the foundation itself, you're already paying, and the _hope_ is that some of that money goes towards the projects.

Also, does the project accept funding separately to being a member project? If so, how do you want to handle that? Is there a lower weighted percentage of the funding that you want to give to it, based on how much your foundation membership fee is?

## How do you want to handle internal dependencies?

There are a number of dependencies you'll have that are owned by your organisation, which you shouldn't be funding.

For instance, at Elastic, we don't want to send funding to our _own_ Open Source projects, so need to be able to filter these out.

## Creating a pot for "other" usage

Let's build on the examples above, and pretend that we've got an algorithm we're using to distribute funds, which has an equal weighting across each of your direct dependencies. In those dependencies, some of these are company-backed Open Source, and as above you've decided not to send any money to a company-backed project. This does, however, lead to money being "left on the table", as you'll have some money that should've been allocated to these projects, that now aren't.

One option you've got is to redistribute that money across the remaining dependencies, but there may be something else you can do instead.

For instance, let's say that you would be paying $1000/mo to Meta for your dependencies on React. You could instead put this money towards a "React community" pot, which doesn't have to go towards Meta the company, but instead be used to help _the community around the project_.

By looking at it in this way, it ensures that we remember there's a community around the project, it's not a project that's only for the company behind it, and that there are tonnes of people doing valuable work that isn't necessarily pushing commits to the project.

By providing a community pot, you'd be able to "pay it forward" by supporting community members who would be able to be sponsored to attend events, who could get paid for contributions, or sent a small "thank you" for the great work they're doing.

In the past, I'd spoken to a maintainer of a large Java project which is company-backed, but who [could get paid a non-negligible monthly budget by Tidelift](https://tidelift.com/lifter/packages-with-income), and suggested this as an option. This wasn't something they'd considered before, but I feel there will be a number of projects out there who may be similar.

This is something that could either be managed by a company-backed/foundation-backed project, where they indicate that "if you want to sponsor us, it instead goes into this pot", or it could be that you as the funders get to override the funding, and instead distribute it some other way.

## More control over team-level or company-level funding

Depending on the size and structure of your company, you may want to give more control to different parts of the organisation.

For instance, when I worked at Capital One, the UK was one of the many Lines of Business, each of which had their own significant reporting structure, control over operations, and I could see as it being useful to have some level of control over Open Source funding, on top of some company-wide management.

This may be that there's the ability for different parts of the organisation to prioritise different things, or it could mean the overall funds that each part of the organisation has.

If this is something that's being done, I'd love to be able to see a banner of "this dependency is getting $xxx/mo from other parts of the organisation" to allow putting the dependency into wider context.

## Give me insight into maintainer (teams) that I depend on

I'd also find it very interesting to get insights into the (teams of) maintainers that we more heavily rely upon.

There are a number of folks in different ecosystems who do maintain a lot more dependencies than others, and it's important to repeatedly humanise the people we rely upon, giving insights into i.e. "you're sponsoring ... who maintains 10% of your NPM dependencies!", and possibly looking at trying to look at building a better relationship with that maintainer, and working to make their life better!

This could _potentially_ be used to feed into higher percentages of funding to these maintainers (on top of them getting a higher share due to having more packages) - due to the additional context switching and work required across different packages - while also making sure that we don't make it something that can be gamed very easily.

This is something that's [planned in dependency-management-data](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/615) to provide that level of insight, and is something I've previously made some efforts to do with the data we have right now, and it can be very interesting (and worrying!) to see how there are a few folks who maintain a lot of your supply chain.

## How to handle different types of dependencies

There may be differences in how you want to manage things like development-only dependencies or dependencies for testing, where there is a possible distinction in your package ecosystem.

You may want a slightly different percentage to be given, or you may be happy with an equal split.

## Transitive dependencies

Additionally, how does the algorithm work for splitting overall funding between direct/transitive dependencies work?

I.e. is it 80% to direct, and 20% to transitive dependencies?

Do you, as a funder, want more control over that?

## Ecosystems with more dependencies

What about in ecosystems where there are more dependencies than fewer? For instance, the NPM ecosystem generally has more dependencies than a JVM- or Go-based project.

Do you want to weigh the funding in addition to take that into account?

## How deep in the dependency tree?

Where possible, how deep shall we go in the dependency tree?

Do we want to make sure that we can handle a third or fourth layer? What if it surfaces that, actually, your organisation _incredibly heavily_ depends on a transitive dependency that ends up being possibly more pivotal than a lot of your first-level dependencies?

## Prioritise certain dependencies

To misquote _Animal Farm_ by George Orwell:

> All dependencies are equal, but some dependencies are more equal than others

Although we rely upon a number of dependencies to get anything done, there are some dependencies that _are_ more important than others.

For instance, if you're in a Javascript/Typescript project, you'll use a number of dependencies. But should each of these dependencies be weighed as highly as the Node.JS runtime itself?

Alternatively, if your platform _heavily_ relies upon PostgreSQL, maybe you should be funding that less equally than other parts of your software stack?

This is also an interesting thought experiment to go through and consider whether there are any dependencies you consider more highly than others.

## Prioritise certain source projects

In a similar vein, there are projects in your organisation that are of higher priority than others, therefore dependencies in that project may want to be funded more highly.

We should be able to feed in information such as "business criticality", so you're prioritising funding dependencies that power your internal business-critical monolith more highly than dependencies on the dozen sample repos that have been abandoned.

## Highlighting maintainers who may need a hand

Something that's coming out of things like the [OpenSSF Scorecard](https://scorecard.dev/) is that more organisations are starting to look at their dependencies and getting a view of "this doesn't look maintained" or "there are supply chain security risks here".

It's possible that these maintainers may be more likely to be able to maintain the project or improve security posture, as part of starting to receive regular funding.

For instance, [Tidelift's 2024 maintainer impact report](https://tidelift.com/the-2024-tidelift-maintainer-impact-report) does indicate that more maintainers are doing (supply chain) security improvements when they're being paid for it.

It's very much an "it depends", but it _could_ be used as an indicator of areas that need improvement, but could also be something that's possible to game.

Creating an intersection of "this package isn't looking actively maintained" with "but the maintainers are actively seeking funding, which may help" provides some interesting insights, and it's something I've used dependency-management-data for in the past.

## Feeding in CHAOSS metrics

Relatedly, it may be possible to pull in [CHAOSS Metrics](https://chaoss.community/) that can again help to prioritise some dependencies more than other.

This would again be something that we don't want to game.

## Putting my money where my mouth is?

If I've got quite a few very specific wants, and a fairly strong idea of how I'd implement it, why don't I go and build the thing?

![A snippet of the _Avengers: Age of Ultron_, in which Thanos, in his glorious golden armor and an Infinity Gauntlet (with no Infinity Stones) proclaiming "Fine. I'll do it myself", very menancingly](https://media1.tenor.com/m/vCqpL7x4sPUAAAAd/thanos-fine-ill-do-it-myself.gif)

The trouble is that I don't _directly_ want to go and build this, and I'd love if someone had built this platform for me.

But I'm definitely going to be keeping this in mind a key feature that could be embedded into [deps.fyi](https://deps.fyi), which will already have deep insights into organisations' dependency trees, so may be a natural add-on.

I will definitely be making sure that a percentage of the bill for using [deps.fyi](https://deps.fyi) is going to Open Source funding, on top of committing to putting a percentage of the company's overall budget to dependencies, so if the platform itself also gives the ability to distribute that percentage in a reasonable way, that'd be awesome.
