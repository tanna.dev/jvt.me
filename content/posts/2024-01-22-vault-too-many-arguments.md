---
title: "Why am I getting `Too many arguments` with `vault`?"
description: Why you may be getting `Too many arguments` from the `vault` CLI, and how to fix it.
tags:
- blogumentation
- command-line
- vault
date: 2024-01-22T11:04:37+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: vault-too-many-arguments
---
Earlier today I received a slightly unhelpful message from the `vault` CLI, which I'd actually received last week, but after a 3 day weekend, I couldn't quite remember what was up with it.

With the following shell script snippet:

```sh
# exported elsewhere
VAULT_PATH=kv/path/somewhere
# and now looked up
vault kv get $VAULT_PATH -format=json
```

I received the error:

```
Too many arguments (expected 1, got 2)
```

Can you spot what the issue was here?

It turns out that `vault` wants the `-format` flag to be `before` any vault paths, so you need to instead do:

```diff
-vault kv get $VAULT_PATH -format=json
+vault kv get -format=json $VAULT_PATH
```
