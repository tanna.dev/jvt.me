---
title: "I'm on Go Time!"
description: "Announcing my first podcast appearance on Go Time, talking about OpenAPI, `oapi-codegen`, versioning, and some fun Unpopular Opinions."
date: 2024-08-08T16:02:24+0100
tags:
- podcast
- openapi
- oapi-codegen
- go
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: https://media.jvt.me/8f3a27d9e0.png
slug: go-time-openapi
---
I'm very excited to announce I'm on my first episode of the ever awesome [Go Time podcast](https://changelog.com/gotime), on today's episode, [OpenAPI + API Design](https://changelog.com/gotime/328).

As I mentioned in [_Learning a new language, or how I gained familiarity with Go_](https://www.jvt.me/posts/2022/08/12/learning-new-language-go/) I started regularly listening to Go Time as I started to learn Go when I joined Deliveroo, and it's been part of my regular rotation each week.

Digging through my listening history it turns out that actually the first episode I listened to was [about Gobot with Ron Evans in 2017](https://www.jvt.me/mf2/2017/03/gpzg4/), which I believe was after I met Ron at FOSDEM [a month earlier](https://www.jvt.me/posts/2017/02/09/fosdem-2017/). This was a lot earlier than I thought I'd been listening, especially as a non-Gopher, but it's a sign of the great content the Go Time folks put out.

Go Time is a great podcast, and I had a great time chatting with <span class="h-card"><a class="u-url" href="https://skriptble.me/">Kris Brandow</a></span> and <span class="h-card"><a class="u-url" href="https://www.jboursiquot.com/">Johnny Boursiquot</a></span> about OpenAPI, [`oapi-codegen`](https://github.com/oapi-codegen/oapi-codegen/), the burdens of Open Source maintenance, versioning, and a few other things.

Like [my first appearance on _Changelog + Friends_](https://www.jvt.me/posts/2024/02/17/changelog-friends/), I thought I should shout out the person who requested me as a guest - me!

❓❓❓ Yeah, I self-requested myself 😅 How's that for (ab)using my privilege as a white-ish man? 😹

Go Time hadn't had an episode on OpenAPI since [late 2017](https://changelog.com/gotime/60), and as I've recently been on [Cup o' Go to talk `oapi-codegen` and OpenAPI](https://www.jvt.me/posts/2024/05/10/cupogo-oapi-codegen/), I thought it may be a good time to have another one. On top of that, I know Kris has some [great thoughts](https://changelog.com/friends/24) about OpenAPI, Hypermedia, HATEOAS and API design in general, and we'd be able to have a good episode, and it turns out that the Go Time folks agreed!

I'm also _very_ eager to hear how unpopular [my Unpopular Opinions](https://changelog.com/gotime/328#t=4043) are 😄

Check it out and let me know what you think! I was also very up for doing a Changelog++ section, so hope y'all enjoy it.

<audio data-theme="night" data-src="https://changelog.com/gotime/328/embed" src="https://op3.dev/e/https://cdn.changelog.com/uploads/gotime/328/go-time-328.mp3" preload="none" class="changelog-episode" controls></audio><p><a href="https://changelog.com/gotime/328">Go Time 328: OpenAPI & API Design</a> – Listen on <a href="https://changelog.com/">Changelog.com</a></p><script async src="//cdn.changelog.com/embed.js"></script>

For some fun behind-the-scenes info, we started recording late as I'd been fighting Linux user woes - turns out being 830+40 dependencies behind on my Arch Linux install was a bit too much. As I joined the recording - on time, after a rushed dinner (rushed due to my own poor planning) - was told that my Chromium (the recommendation for the recording) needed an update for Riverside, which all of the Changelog records on.

So I set to update just Chromium - not wanting to perform ~900 updates - and when it was done, it then wouldn't load, due to a `libicu` error. Never heard of it, and so I'm trying to very quickly work out what's going wrong.

That then leads to me falling back to Firefox (my preferred browser) which _also_ wouldn't start as `libicu` was missing.

Trying to partially downgrade `libicu` failed too, so I  to get an old version (via the AUR) running but it was far more stress than I needed before starting - you don't wanna be up on adrenaline before recording something 😅 At least, any more than the usual amount before public speaking!

But I hope that doesn't come across, and y'all enjoy!
