---
title: "oapi-codegen is moving to its own org"
description: "Announcing `oapi-codegen`'s move to its own GitHub org, and a history lesson about the project."
tags:
- open-source
- oapi-codegen
date: 2024-05-09T08:58:35+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: oapi-codegen-org
image: https://media.jvt.me/b41202acf7.png
---
Note that this is a copy of [the announcement on GitHub](https://github.com/deepmap/oapi-codegen/discussions/1605).

> [!IMPORTANT]
> TL;DR: We're moving `oapi-codegen` to a new GitHub organisation. Nothing is changing as part of this move, aside from our module import path.
>
> The goal is to find a long term home for `oapi-codegen` because Deepmap no longer exists as a business and repos under the Deepmap org have an uncertain future.
>
> The move will occur at some point during May, _after_ the release of v2.2.0, which will use the previous module import path. Then, for future releases, there will be action required to update your import path to use the new location.

This is a good opportunity to share a bit of history of how the project has grown over time.

## Project genesis

In [February 2019](https://github.com/deepmap/oapi-codegen/commit/e7757a98520dc622499143d9b56bd2046ef1b55e), <span class="h-card"><a class="u-url" href="https://github.com/mromaszewicz">Marcin Romaszewicz</a></span> (aka [@deepmap-marcinr](https://github.com/deepmap-marcinr)) started the `oapi-codegen` project.

Marcin had worked on iterations of this project over the years at different companies - having used Go since v1.1! - but as he joined Deepmap and felt himself reaching for writing it again, he discussed with the company about making this an Open Source solution, of which they were happy to oblige.

Over the years, the project has grown in usage and favourability within the Go ecosystem:

[![Star History Chart](https://api.star-history.com/svg?repos=deepmap/oapi-codegen&type=Date)](https://star-history.com/#deepmap/oapi-codegen&Date)

We've become - as far as we're aware - the most popular Go-based OpenAPI-to-Go code generator (so excludes the usage of [openapi-generator.tech](https://openapi-generator.tech/)) which is pretty awesome!

As it was used by a number of teams within Deepmap, Marcin worked on a number of features and fixes to support work being done internally, with support from a couple of Deepmap colleagues.

As the project started growing in usage outside of Deepmap, there were more community contributions, which we love to see, but often Marcin wouldn't be able to get to them during working hours, so it would bleed into his free time, like many Open Source projects do.

As can be expected, Marcin's time to support things has gone up and down as life stuff as ["life happens"](https://indieweb.org/life_happens) and it's been best efforts, but he's strived to do keep on top of things, and I'm ever grateful for the work he's done in the past, and continues to do!

As mentioned in [_Creating a more sustainable model for `oapi-codegen` in the future_](https://www.jvt.me/posts/2024/05/09/oapi-codegen-sustainable/), community management, triage and code review take a good chunk of time of the maintainers - it's great to have community contributions and we really appreciate the work y'all put in, but it can be tough to balance that as well as also wanting to put work towards tackling issues on the backlog, implementing features _we_ want to use, or having an opportunity to write code that may not be possible to do so during the course of our work.

## I join as co-maintainer

In 2022, I ([@jamietanna](https://github.com/jamietanna)) joined Deliveroo as my team were looking to build a new API, and as a big proponent of OpenAPI, I nudged us towards building it in a design-first approach. Although I was able to convince the team of the benefits, I ended up then needing to shop around to find a code generator as [I didn't really know Go](https://www.jvt.me/posts/2022/08/12/learning-new-language-go/).

I found `oapi-codegen`, and it fit a number of the needs we had, but it didn't support the `gorilla/mux` router that we were standardising on at the company. In time, I ended up [adding support for `gorilla/mux`](https://github.com/deepmap/oapi-codegen/pull/585) which closed the loop on our requirements, and a bit later I [blogged on Deliveroo's engineering blog](https://deliveroo.engineering/2022/06/27/openapi-design-first.html) about how we used `oapi-codegen` and OpenAPI to ship more quickly.

In May 2022 I made [my first PR](https://github.com/deepmap/oapi-codegen/pull/576), and in the following months I'd ended up contributing fixes, documentation changes and features to support our usage at Deliveroo.

Marcin was super responsive with changes (at least compared to some of the other Open Source projects I was using at the time) which helped build confidence at Deliveroo with the project, as well as making it easier to get our changes upstream'd 👏

At this time, ~~Marcin reached out to me~~ I reached out to Marcin to see if there was anything more I could be doing to support. I found in particular that it'd be useful to label or close issues when I'd answered them, and there were some PRs that seemed trivial enough that I could merge them.

We started with my Triage access, and just before Hacktoberfest (which we didn't actively participate in) I got my Write access 👏

## Since then

Having a second maintainer has definitely helped over the last ~2 years while Marcin's been busy at NVIDIA, and in that time I've been able to help put together 17 releases, with a huge number of community contributions, as well as some key features and bug fixes from me.

As you may have seen from [v1.13.0 introduces lots of transitive dependencies to client library](https://github.com/deepmap/oapi-codegen/issues/1142), we ended up releasing some changes that had an unintended side effect of a large dependency tree.

While considering our [proposed solution](https://github.com/deepmap/oapi-codegen/discussions/1309) for how to use Go's module system to help alleviate this issue, we started to consider how this would look, for instance "do we call it `deepmap/oapi-codegen-runtime`"? Or would we have a multi-module project, i.e. `deepmap/oapi-codegen/runtime`, which would introduce overheard for releasing?

Being under Deepmap was a great starter home for the project, which has stayed true post-NVIDIA acquisition of Deepmap, and we're very appreciative of all the support they've given.

However, splitting into multiple packages was an opportunity for us to discuss whether this makes sense to be the long-term home. Since October 2023, Marcin's worked with NVIDIA to get the project released into its own organisation, which will hold all of the officially supported `oapi-codegen` projects.

## The new org

Therefore, we're going to be moving `deepmap/oapi-codegen` to `oapi-codegen/oapi-codegen` _after_ the release of v2.2.0, which should be in May.

We realise this will require work to update on your side when the v2.3.0 release lands, but through tools like [Renovate](https://docs.renovatebot.com/) it should be possible for them to auto-migrate you to the new module import path. Note that we will **not** be bumping the major version for the module, only changing the import path.

(Aside: for those who had hoped the v2.2.0 release would've already landed - [so did I](https://github.com/deepmap/oapi-codegen/discussions/1494) 😹 )

As noted in [_Creating a more sustainable model for `oapi-codegen` in the future_](https://www.jvt.me/posts/2024/05/09/oapi-codegen-sustainable/), the move to the new organisation _also_ enables us to get onto things like GitHub Sponsors, as well as making it clear that `oapi-codegen` is something community run, and that we're just volunteers working on this project, for the good of the community. Please read the linked for more details.


This also gives us more control in the future around repository permissions, as under the Deepmap organisation, the access that I as co-maintainer could get wasn't quite as high as I may have wanted.

Additionally, we had [legal concerns around publishing binaries](https://github.com/deepmap/oapi-codegen/pull/888#issuecomment-1579490856) that were relied upon, which now we will be released from, as a community run project, separate to the NVIDIA/Deepmap companies.

In the long-run, this will also lead to us rethinking and organising project governance and building a sustainable stewardship for the future.

We'd love to have more folks maintaining the project, but there's a few steps to get there, and before we can put the time in to get things ready for that, we need to work our way through the backlog of issues (to triage, and to address/close) as well as PRs raised by the community.

We also want to make sure that we are putting the right guardrails in place to avoid a maintainer backdoor like seen with [the xz backdoor](https://en.wikipedia.org/wiki/XZ_Utils_backdoor).

We'll work towards having some more structure in place governance wise towards the end of the year 🤞

## Here's to the next 5 years?

We've recently celebrated 5 years since the project launched, and I'd love to see the project grow over the next 5 years, in a more sustainable manner.

I'm also excited to see what happens with [OpenAPI 4.0 (aka Moonwalk)](https://www.openapis.org/blog/2023/12/06/openapi-moonwalk-2024) and what the ecosystem looks like when OpenAPI v4 gains widespread adoption.

We've had ~250 individual contributors over the years, and are very appreciative of the hard work everyone has put in to support the project. As well as those who have contributed feedback, docs, bug reports and feature enhancements, we also value our users. Thank you very much!

In the last 2 years, I've thoroughly enjoyed working with the project and the community, learning how different folks are using it and working to better the project for everyone. It's not the first Open Source project [I've been rather involved in maintaining](https://www.jvt.me/open-source/) in, but it's the one that I've spent the most time on in recent years.

Got any thoughts? Marcin and I would love to hear them [on GitHub](https://github.com/deepmap/oapi-codegen/discussions/1605).
