---
title: Getting symlinks to work with a `git clone` on Mac
description: How to restore symlinks in a previously cloned repository, where `core.symlinks=false`.
tags:
- blogumentation
- git
- mac
image: https://media.jvt.me/53239026de.png
date: 2024-10-01T09:01:39+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: mac-symlinks-git
---
Yesterday I was working with one of my colleagues, who had an issue in one of their Git repos where a symbolic link (symlink) wasn't resolving correctly.

## The problem

This turned out to be due to how MacOSX will by default set `core.symlinks=false`, as a security measure ([via](https://stackoverflow.com/questions/37168832/git-on-osx-is-cloning-symlinks-as-plaintext-files)).

This means that when a symbolic link is part of a cloned / pull'd repo, then instead of seeing i.e.:

```sh
# expected behaviour
$ ls -al bin/eksctl
lrwxrwxrwx 1 jamie jamie 19 Sep 30 13:18 bin/eksctl -> .eksctl-0.190.0.pkg
```

Then we will instead see:

```sh
# actual behaviour
$ ls -al bin/eksctl
-rw-r--r-- 1 jamie jamie 19 Oct  1 08:07 bin/eksctl
$ cat bin/eksctl
.eksctl-0.190.0.pkg
```

## Solution

So how do we unpick this?

First, we need to make sure Git creates symlinks:

```sh
$ git config core.symlinks true
```

Once we run this, we then see that Git has detected there's a "type change":

```sh
$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	typechange: bin/eksctl
```

However, we're still seeing no symlinks:

```sh
$ ls -al bin/eksctl
-rw-r--r-- 1 jamie jamie 19 Oct  1 08:07 bin/eksctl
$ cat bin/eksctl
.eksctl-0.190.0.pkg
```

To resolve this, we can then use `git checkout` i.e.

```sh
git checkout bin/eksctl
```

This then forces Git to re-checkout that file, with the correct type:

```sh
$ ls -al bin/eksctl
lrwxrwxrwx 1 jamie jamie 19 Oct  1 09:00 bin/eksctl -> .eksctl-0.190.0.pkg
```

And now our symlinks will start working again 👏🏼

This will need to be done for any other `typechange`s that Git detects.
