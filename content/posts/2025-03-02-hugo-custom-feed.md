---
title: "Creating single-purpose RSS feeds with Hugo"
description: "How to create a new RSS feed in Hugo for a single page's updates."
tags:
- blogumentation
- hugo
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
date: 2025-03-02T19:46:22+0000
slug: hugo-custom-feed
image: https://media.jvt.me/445ff8a8fc.png
---
Tonight I've added a new couple of RSS feeds to my website - one for my [/salary/ page](/salary/) and one for my [/now page](/now/) - for two pages that folks may want to keep an eye on more easily.

Because it was a little awkward to set up the feeds, I ended up wanting to [write it up as a form of blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/).

To start with, we need to modify our Hugo config to indicate we've got a new content type:

```toml
[outputs]
home = ["...", "salaryRSS"]

[outputFormats]
[outputFormats.salaryRSS]
mediatype = "application/rss"
baseName = "salary"
```

This indicates to Hugo that this is an RSS feed, and to create a file called `/salary.xml`.

Then, we need to wire in the feed itself, by creating `layouts/index.salaryrss.xml`:

```xml
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Jamie Tanna's salary page</title>
    <link>{{ .Site.Params.LiveSiteUrl }}/salary/</link>
    <description>Updates to Jamie Tanna's salary page</description>
    <!-- ... -->
    <lastBuildDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 -0700" | safeHTML }}</lastBuildDate>{{ end }}
    <!-- ... -->
    {{ with .GetPage "salary" }}
    <item>
      <title>{{ .Title }}</title>
      <link>{{ .Site.Params.LiveSiteUrl }}{{ .Permalink }}?utm_medium=rss&amp;utm_source=rss</link>
      <pubDate>{{ .GitInfo.AuthorDate.Format "Mon, 02 Jan 2006 15:04:05 -0700" | safeHTML }}</pubDate>

      <!-- ... ->
    </item>
    {{ end }}
  </channel>
</rss>
```

Notice that the template is `layouts/index.salaryrss.xml`, with a _lowercase_ filename, as Hugo appears to require it be case sensitive.

Because we've wired in the updated date (via Git metadata) this means that each time there is an update to the page, the feed will be updated, and followers will see there's a new update on the page.
