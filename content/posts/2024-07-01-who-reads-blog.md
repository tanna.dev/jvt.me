---
title: "Reader survey: Who reads my blog?"
description: "Wondering how my readers read my blog, and why."
tags:
- blogging
- www.jvt.me
date: 2024-07-01T11:08:35+0100
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: who-reads-blog
---
I think it's been sufficiently long since <span class="h-card"><a class="u-url" href="https://shkspr.mobi">Terence</a></span> wrote [his blog post]( https://shkspr.mobi/blog/2023/09/who-reads-my-blog/) that I can not-so-shamelessly steal the idea.

(Aside: I was originally going to post this the same week, but never got around to it)

Similar to Terence, I'd love to hear a bit more about you, the readers - I have analytics on my blog (as you can see with [my annual Site in Review posts](https://www.jvt.me/site-in-review/)) and so have some information on the traffic I receive, but I haven't (yet?) set up any insights into the access logs that I get via CloudFront/S3 to get an indication of how many folks read i.e. my RSS feed.

I get emails every so often from readers (in particular, that's reminded me of your emails Jordan and Wouter - promise I'll get to them this week!) which I really do appreciate, and it's always great to hear thoughts or questions, but [my ADHD](https://www.jvt.me/posts/2022/10/04/adhd/) as well as feeling like forever drowning in my TODO list may mean I don't get back to you so quickly, sorry in advance!

So, dear reader, I'd love to hear a bit more about you (feel free to answer any or all of the below questions, or send any other thoughts my way):

- How are you following me? RSS/Microformats2/other feed formats? Fediverse? LinkedIn? Infrequently checking?
- How/why did you start following me?
- Do you follow for a particular topic? Are you more interested in the tech or not-as-tech topics?
- Do you enjoy the variable time between posts?
- Is there anything you'd like me to write about?
- Is there anything you keep coming back to read on my site?

Feel free to reply through the channel you read this in - if you're able to, or via email or comment via Comment Parade at the bottom of this post.
