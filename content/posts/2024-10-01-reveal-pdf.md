---
title: "Converting a Reveal.js slide deck to PDF"
description: "How to convert a Reveal.js slide deck to a PDF, using Decktape."
tags:
- blogumentation
- revealjs
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
date: 2024-10-01T22:10:39+0100
slug: reveal-pdf
image: https://media.jvt.me/01aa6cf83f.png
---
Something I have to do every so often is convert [one of my talks](https://talks.jvt.me) from their web form in [Reveal.js](https://github.com/hakimel/reveal.js/) to a PDF.

Ahead of speaking at [DTX London tomorrow](https://www.dtxevents.io/london/), I'm preparing a PDF as well as the web URL, and I've _yet again_ forgotten how to do it, as it's just-infrequent-enough that I've not got it in my memory banks.

This is the perfect opportunity to [write it as a form of blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/), so next time I don't have to try and remember!

We can use the excellent [Decktape project](https://github.com/astefanutti/decktape) to do the hard work.

To do so, we need to install `decktape`, which I prefer to do in a temporary directory, as it's so infrequently used it doesn't make sense to be a globally installed tool.

From there, we can run:

```sh
# NOTE that the URL and output filename may want to change
npm exec decktape reveal --fragments http://localhost:1313/dmd-dtx/slides/ dmd-dtx.pdf
```

Note that `--fragments` is required to handle Reveal.js' fragments (aka in-slide transitions) and will create a new page on the PDF per fragment.
