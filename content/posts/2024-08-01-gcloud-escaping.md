---
title: "Why can't I use a comma with `gcloud`?"
description: "How to resolve commas being ignored when interacting with `gcloud`, and how to escape them."
tags:
- blogumentation
- google-cloud
date: 2024-08-01T12:48:16+0100
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
slug: gcloud-escaping
---
I've been trying to call a Google Cloud Run Job with the following arguments:

```
[[{"platform":"...","organisation":"...","repo":"..."}]]
```

I've done this with the following invocation:

```sh
job_name="..."
region="..."
project="..."
event='[[{"platform":"...","organisation":"...","repo":"..."}]]'
gcloud run jobs execute $job_name --region $region --project=$project --args "$event"
```

However, each time this made its way up to Google Cloud, the Job failed to run, as the input was being remapped to:

```
[[{"platform":"..." "organisation":"..." "repo":"..."}]]
                   ^                    ^
                   ^   missing commas   ^
```

After some awkward searching around, [this StackOverflow](https://stackoverflow.com/a/57972511) answer led me to [the `gcloud` docs for escaping](https://cloud.google.com/sdk/gcloud/reference/topic/escaping) which seems a bit complicated for what I wanted to do, but it seems reasonable for some of the more complex use-cases the docs shared.

This is required as the [`gcloud run jobs execute` docs](https://cloud.google.com/sdk/gcloud/reference/run/jobs/execute) note that `--args` requires `Comma-separated arguments`.

This results in the following tweak to our script:

```sh
event='[[{"platform":"...","organisation":"...","repo":"..."}]]'
# use the escaping
gcloud run jobs execute $job_name --region $region --project=$project --args "^#^$event"
#                                                                             ^^^
#                                                                             ^^^
#                                                                             ^^^
```

This makes sure that we delimit based on the `#`, which should never exist in the event we're processing.
