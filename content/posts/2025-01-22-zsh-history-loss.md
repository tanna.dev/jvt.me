---
title: "Why is zsh no longer storing history?"
description: "Spoiler: you may be missing both `HISTSIZE` and `SAVEHIST` variables."
date: 2025-01-22T20:56:27+0000
tags:
- "blogumentation"
- "zsh"
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
slug: zsh-history-loss
---
On my first day back in the office, I noticed something odd with my `zsh` setup, where new terminal sessions weren't having their history saved 🤔

Interestingly, my dotfiles [specify the `HISTSIZE`](https://gitlab.com/jamietanna/dotfiles-arch/-/blob/6d76e08a1d8d78b696d82203ad17a72ddc70eb52/terminal/home/.zshrc.local#L202) variable to a reasonably high amount, but it seems to not be working:

```sh
export HISTSIZE=30000
```

I've also since reproduced it on my other machines, so I think there's been a recent behaviour change to `zsh` behaviour recently, rather than somehow simultaneously hitting 30000 history items on _all my machines_.

It turns out that [via](https://unix.stackexchange.com/questions/389881/history-isnt-preserved-in-zsh) you should specify both `HISTSIZE` and `SAVEHIST`:

```sh
export HISTSIZE=30000
export SAVEHIST=30000
```

With these set, then `zsh` will correctly save everything.

I'm unsure why it's worked for several years, but at least I've fixed it now!
