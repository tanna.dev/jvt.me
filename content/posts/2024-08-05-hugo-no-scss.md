---
title: "Why isn't Hugo regenerating my SCSS files?"
description: "How to ensure you're using the right Hugo version to build SCSS files."
tags:
- blogumentation
- hugo
license_code: Apache-2.0
license_prose: CC-BY-NC-SA-4.0
date: 2024-08-05T08:34:49+0100
slug: hugo-no-scss
image: https://media.jvt.me/445ff8a8fc.png
---
Over the last couple of days I've been - after a long hiatus - making some tweaks to my Hugo site's theme. I've hit an issue on both my laptop and (newly fresh installed Arch) PC, so it's worth [writing it as a form of blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/).

I was finding that modifications to any SCSS files didn't trigger a rebuild of them 🤔

One thing very weird, is that even with my local setup modified to point to my local theme:

```diff
diff --git config.toml config.toml
index 39fef45f41..003150d38b 100644
--- config.toml
+++ config.toml
@@ -1,5 +1,7 @@
 baseURL = ""

+ignoreFiles = ['content/mf2']
+
 title = "Jamie Tanna | Software Engineer"

 languageCode = "en-GB"
@@ -287,4 +289,4 @@ slug = 2024
   path = '/week-notes/2024/**'

 [[module.imports]]
-path = "gitlab.com/jamietanna/www.jvt.me-theme"
+path = "./www.jvt.me-theme"
```

But when making changes to my local theme's SCSS, Hugo detected that there _were_ changes, but didn't regenerate the styling.

It turns out it's because I was using Hugo, not Hugo (Extended), which adds the SCSS support.

As soon as I started using the Extended release, my builds now started allowing me to modify SCSS.
