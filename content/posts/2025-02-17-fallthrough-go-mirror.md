---
title: "I'm on Fallthrough: Patching Problems with Persnickety Proxies Purveyed by Paternalistic Princes"
description: "Announcing my appearance as a guest host on Fallthrough, discussing the Go module proxy, pondering 'is Go dead?' and whether the Go team at Google have our best interests at heart."
date: 2025-02-17T13:40:46+0000
tags:
- podcast
- go
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: https://media.jvt.me/212aeb2c53.png
slug: fallthrough-go-mirror
---
Last week I was invited to join the wonderful [Fallthrough podcast](https://fallthrough.fm) as a guest host, on a more time sensitive episode discussing [the recent backdoor discovered in the Go module proxy](https://arstechnica.com/security/2025/02/backdoored-package-in-go-mirror-site-went-unnoticed-for-3-years/), and [the episode has just shipped](https://fallthrough.transistor.fm/episodes/patching-problems-with-persnickety-proxies-purveyed-by-paternalistic-princes), with a very good title!

This was actually my second episode with Fallthrough, but the first one that's published - there's an episode around API Design that'll be landing at some point in the future - and I enjoyed hanging with the rest of the panel.

Joining the panel was both a great experience generally, as I've been enjoying the differing thoughts of the folks on Fallthrough, but also because there's a lot of interesting background that the others were able to share, which I'd not had insight into.

As <span class="h-card"><a class="u-url" href="https://skriptble.me/">Kris</a></span> wrote [on the socials, when announcing this episode](https://bsky.app/profile/skriptble.me/post/3lietqyglpc26):

> I hope the nuance that we all intended came across in this episode. It’s really tough to be critical of the things you love, but if you don’t speak up at some point, I think things just wind up getting worse and worse.

This was an incredibly nuanced episode, and although we started in the vein of supply chain security (which I missed a chance to mention [dependency-management-data](https://dmd.tanna.dev)!), we ended up digging into questions like "is Go dead?" or "does Google's Go team have our best interests at heart?" as well as some of the usual levels of tangents folks will be familiar of with Fallthrough 🤓

As someone who [didn't learn Go until 2022](https://www.jvt.me/posts/2022/08/12/learning-new-language-go/), and who hasn't been as heavily interacting in the Go language development, there was a lot of background info I learned through the recording, and it's helped feed into giving me a more balanced view of Go's development over the years, albeit I don't think I have yet decided where I land on some of the questions.

I'm thinking this may be a pretty polarising episode, and I'm very much looking forward to seeing the reception of the episode!
