---
title: "You can now parse repo-level Renovate configuration with `renovate-graph`"
description: "Announcing a new release of `renovate-graph` which now parses repo-level Renovate configuration."
date: 2024-07-28T14:56:36+0100
tags:
- "renovate"
- dependency-management-data
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: "https://media.jvt.me/6657d4ff89.jpeg"
slug: renovate-graph-repo-config
---
Almost 2 years ago (!) I built [`renovate-graph`](https://www.jvt.me/posts/2022/11/01/renovate-dependency-graph/), a tool to extract the dependency trees for a given repository, which under the hood uses [Renovate](https://docs.renovatebot.com/). I've been getting tonnes of value from it as part of how it fits into the wider [dependency-management-data](https://dmd.tanna.dev) ecosystem, and providing more actionable data for understanding how you use internal and external dependencies in your projects.

I built `renovate-graph` as a lightweight shim over Renovate as a way to surface the dependencies in use in a project, but as it uses Renovate under-the-hood, it means that it needs to follow how Renovate works itself.

One of the many powerful features that Renovate has over other tools is its configurability is how you can [add support for other ecosystems or files that aren't understood out-of-the-box with the Custom Regex Manager](https://www.jvt.me/posts/2024/04/12/use-renovate/#adding-support-for-additional-ecosystems), which is hugely useful for data collection.

The other incredibly useful thing that teams can do with their configuration is _disable_ Renovate from updating specific dependencies, i.e. if you only care about a subset of the dependencies that your repository has.

Therefore when `renovate-graph` runs, it makes sure to ignore the Renovate configuration in the given repository, so we don't risk disabling the collection of any interesting data.

This is because `renovate-graph` should collect the maximal data available for a given repository, regardless of whether it's a production dependency, dev/test dependency, or i.e. the version of Ruby recommended to use in the documentation.

When previous versions of `renovate-graph` run, the `renovate.json` in a given repo is explicitly ignored, which then has the risk of Custom Regex Manager rules being ignored, and missing out on interesting data it can surface.

The difficulty is how to selectively enable the configuration, which fortunately from the newly released `renovate-graph` v0.20.1, has now been solved, [by `force`ing the `packageRules` and `enabledManagers` to a no-op](https://gitlab.com/tanna.dev/renovate-graph/-/merge_requests/23).

Now you can run `renovate-graph` and collect any repo-specific data, as well as following any other `extends` or other presets that may be useful 🚀
