---
title: "I'm on Cup o' Go!"
description: "Announcing my appearance on Cup o' Go, talking about `oapi-codegen`, OpenAPI, working on Open Source and blogging."
date: 2024-05-10T17:27:32+0100
tags:
- openapi
- oapi-codegen
- podcast
- adhd
- blogging
- public-speaking
- go
license_code: "Apache-2.0"
license_prose: "CC-BY-NC-SA-4.0"
image: https://media.jvt.me/eb5b1f6b5a.jpeg
slug: cupogo-oapi-codegen
---
This morning I was a guest on [Cup o' Go](https://cupogo.dev), ahead of today's episode, which is [now live](https://cupogo.dev/episodes/a-quick-tour-of-some-proposals-and-a-long-chat-about-openapi-with-jamie-tanna).

![A screenshot of the Riverside recording studio showing Shay, Jonathan and Jamie smiling at the camera. Shay is holding up his water bottle, branded Cup o' Go's mascot Brewster, and Jonathan is holding up a Cup o' Go mug. Jamie unfortunately doesn't have any swag (yet) but is wearing his dependency-management-data t-shirt, which he thinks still kinda counts](https://media.jvt.me/09ff2ecd4c.png)

Cup o' Go is a great podcast, which I've listened to since the first episode was released last January, and I very much recommend a listen - not just to my episode, but others! - as there's a great split between "here's ~15 minutes of recent happenings in the community" followed by an interview with someone of interest. Even if you don't have time to listen to the whole episode, just the ~15 minutes of news is a great way to stay abreast of the goings on in the community.

I ~~was invited to speak~~ was a little cheeky and invited myself to speak by using the "suggest a guest" featured to mention myself 🫣 But <span class="h-card"><a class="u-url" href="https://www.mrnice.dev/">Shay</a></span> and <span class="h-card"><a class="u-url" href="https://boldlygo.tech/">Jonathan</a></span> were happy to talk to me.

When suggesting myself, it was largely around the yet-to-be-announced changes for `oapi-codegen`, and we managed to time it _very_ well with us being able to green light the announcements just in time to have it posted before being able to talk about it on the podcast!

This was a really fun interview, and was glad we were able to do it!

We spoke about OpenAPI, how it can be useful, and then talked about my posts yesterday about [looking to make `oapi-codegen` more sustainable](https://www.jvt.me/posts/2024/05/09/oapi-codegen-sustainable/) and [the move to the new org](https://www.jvt.me/posts/2024/05/09/oapi-codegen-org/).

We also got into conversations about good API design, [retrofitting OpenAPI docs to Rails](https://www.jvt.me/posts/2022/10/20/rails-openapi-contract-test-retrofit/) and [content-type negotiation](https://www.jvt.me/posts/2021/01/05/why-content-negotiation/) (and my [yet-to-be-written](https://gitlab.com/tanna.dev/jvt.me/-/issues/1218) post on why content negotiation is hard.

We also, naturally delved into my blog, and why I find it useful to write:

- [Why should you blog?](https://www.jvt.me/posts/2023/10/07/why-blog/)
- [How blogging has affected me, as a neurodiverse person](https://www.jvt.me/posts/2023/10/04/blogging-neurodiversity/)
- [Writing as a form of blogumentation](https://www.jvt.me/posts/2017/06/25/blogumentation/).

We ended up with a conversation on [how I started to learn Go](https://www.jvt.me/posts/2022/08/12/learning-new-language-go/) and what surprised me - the answer may surprise you, too 😹

This also reminded me of my _actual_ first time playing with Go, after the [Digital Lincoln meetup "Five reasons why you should use Golang"](https://www.meetup.com/digitallincoln/events/281382077/), which I'd not mentioned in my post about learning Go, presumably as I'd forgotten it.

I had a great time, and it was really fun to chat with Shay and Jonathan, as well as be part of a podcast I love to hear!

And trust me, even with _many_ listens under my belt and therefore knowing that a dire pun is coming from Shay to start off the interview, it _still_ took me out 😅

Hope you enjoy!
