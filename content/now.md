---
title: /now
aliases:
- /about/
---
This page is inspired by <span class="h-card"><a class="p-name u-url" href="https://sivers.org">Derek Sivers</a></span>'s post [_The /now page movement_](https://sivers.org/nowff) and [the Now Movement](https://nownownow.com/about). The purpose of this page is to share what I'm currently focussed on, and will be updated as things change.

This is both for longer-lived interests and shorter-lived "what I'm currently doing".

What am I up to right now?

- Living with my partner <span class="h-card"><a class="p-name u-url" href="https://annadodson.co.uk">Anna Dodson</a></span> and our beautiful terror of a cat [Morph](/tags/morph/) and our dog [Cookie](/tags/cookie/)
- I'm currently at Elastic as a Senior Software Engineer, working in the Platform Engineering Productivity organisation, on the DevFlow ("everything left of merge to main") team
  - I'm currently pushing towards my next promotion, to a Principal Software Engineer
- I'm actively blogging about technical and personal things
- I'm stepping into the IndieWeb movement, one [#IndieWeb](/tags/indieweb) post at a time
- I'm largely focussing on my Open Source projects [dependency-management-data](https://dmd.tanna.dev) and [oapi-codegen](https://github.com/deepmap/oapi-codegen), but also [a number of others I maintain](/open-source/)
- I'm building [deps.fyi](https://deps.fyi)
- I'm co-organising the OpenUK Digital meetup with <span class="h-card"><a class="u-url" href="https://lornajane.net/">Lorna</a></span>
- I'm co-organising [Batch Bunch](https://www.batchbunch.dev/) with <span class="h-card"><a class="u-url" href="https://www.rizwanakhan.com/">Riz</a></span>

What are my interests?

- Blogging
- Decentralisation
- GNU/Linux
- Free Software Movement
- Self-hosting services
- Software Testing
- Automation
- Infrastructure-as-code
- DevOps
- Tech Meetups

What are my Operating System preferences?

- On Mobile, I'm an Android user
- On Laptop/Desktop, I'm an Arch Linux user
- On the server, I'm a Debian Linux user

My primary programming languages at this time are Go, Ruby and TypeScript.

My daily drivers for work and play are:

- My phone is a Pixel 6 Pro
- My laptop is an XPS 13 9305 (Tiger Lake)
- My desktop is a custom build with:
  - Intel i7-6900k
  - NVIDIA TITAN X

I currently use an Elgato Wave:3 as my microphone, and a Logitech C922 as my webcam.

My current colourscheme is [Srcery](https://srcery.sh).

I'm an Open Source maintainer for [quite a few projects](/open-source/).
