---
title: Typography
---
This is a page to illustrate different pieces of content.

This is a long paragraph of text to illustrate how different characters look:

Lorem ipsum dolor sit amet, **consectetur adipisicing elit**, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <a href="#">aliquip ex ea commodo consequat</a>. _Duis aute irure dolor_ in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, `sunt in culpa` qui officia deserunt mollit anim ~~id est laborum~~.

- A list
- ... that is not
- ... ordered

1. A list
1. ... that is
1. ... ordered

> Sometimes, there may be sentences that are quoted

> And sometimes
> that quote
> may be on multiple
> lines (in Markdown form)

There's a paragraph between these multi-line blocks

> And sometimes
> that quote
>
> may be on multiple
> lines
>
> (with explicit linebreaks)

```
A code block, but no syntax detected
```

A code block, with `go` syntax highlighting:

```go
package main

func main() {
}
```

An image (at a `200px` height):

<img src="/img/profile.png" alt="My profile picture" style="height: 200px;">

<details>

<summary>This is the summary of the &lt;details&gt; block</summary>

And this is the content

```
Which


can

be


quite






long
```


</details>

This is an Asciicast:

{{< asciicast id="tz" src="/casts/tz.json" >}}

<table>
  <tr>
    <th>Date</th>
    <th>Note</th>
  </tr>

  <tr>
    <td>2024-08-14</td>
    <td>Today</td>
  </tr>

  <tr>
    <td>2024-01-01</td>
    <td>New Year's Day</td>
  </tr>

  <tr>
    <td>XXXX-XX-XX</td>
    <td><code>Code snippet</code></td>
  </tr>
</table>

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6
