{
  "type" : [ "h-entry" ],
  "properties" : {
    "name" : [ "My workflow for writing SQL(ite) queries (2024 edition)" ],
    "summary" : [ "Writing about my recent workflow for writing, executing, and sharing SQL queries with others." ],
    "category" : [ "blogumentation", "sql", "sqlite", "neovim", "datasette", "lsp" ],
    "featured" : [ {
      "value" : "https://media.jvt.me/f1d9375fc8.png",
      "alt" : "Featured image for sharing metadata for article"
    } ],
    "url" : [ "https://www.jvt.me/posts/2024/06/07/sql-workflow/" ],
    "uid" : [ "https://www.jvt.me/posts/2024/06/07/sql-workflow/" ],
    "published" : [ "2024-06-07T10:53:14+01:00" ],
    "updated" : [ "2024-06-09T19:50:24+01:00" ],
    "content" : [ {
      "html" : "<p>In the last ~18 months I've been writing <em>a lot</em> of SQL due to my work on <a href=\"https://dmd.tanna.dev\">dependency-management-data</a>.</p><p>As <a href=\"https://www.jvt.me/posts/2024/02/02/dmd-birthday/\">I wrote about on dependency-management-data's birthday</a>, I've been learning quite a few things about SQL and becoming much more fluent with writing it.</p><p>I'll end up writing an SQL query against the dependency-management-data data at least once per day, and so I want to make sure that my workflow works for me.</p><p>I've recently made some improvements to the local workflow I've been using for authoring SQL and thought it would be useful to share.</p><p>Note: I've called this \"2024 edition\" but I've never written one of these before. However, I am expecting that in future years this process will be improved and I'll add new posts.</p><h2 id=\"initial-setup\">Initial setup</h2><p>My original setup was that I would write queries directly into the <code>sqlite3</code>, which were often copied from my Neovim editor where at least I had syntax highlighting and could easily create multi-line queries.</p><p>Not long after, I learned about the excellent <a href=\"https://datasette.io\">Datasette</a> database browser, and started using that as my main interface for the queries I was writing.</p><p>But in the recent months, I've been finding that I'll be writing a query, and then need to join across one of a couple of tables. Writing this by hand got so repetitive that I've now got a snippet in my Neovim which completes to:</p><div class=\"highlight\"><pre tabindex=\"0\" class=\"chroma\"><code class=\"language-sql\" data-lang=\"sql\"><span class=\"line\"><span class=\"cl\"><span class=\"k\">left</span><span class=\"w\"> </span><span class=\"k\">join</span><span class=\"w\"> </span><span class=\"n\">owners</span><span class=\"w\"> </span><span class=\"k\">on</span><span class=\"w\"> </span><span class=\"n\">renovate</span><span class=\"p\">.</span><span class=\"n\">platform</span><span class=\"w\"> </span><span class=\"o\">=</span><span class=\"w\"> </span><span class=\"n\">owners</span><span class=\"p\">.</span><span class=\"n\">platform</span><span class=\"w\">\n</span></span></span><span class=\"line\"><span class=\"cl\"><span class=\"w\"></span><span class=\"k\">and</span><span class=\"w\"> </span><span class=\"n\">renovate</span><span class=\"p\">.</span><span class=\"n\">organisation</span><span class=\"w\"> </span><span class=\"o\">=</span><span class=\"w\"> </span><span class=\"n\">owners</span><span class=\"p\">.</span><span class=\"n\">organisation</span><span class=\"w\">\n</span></span></span><span class=\"line\"><span class=\"cl\"><span class=\"w\"></span><span class=\"k\">and</span><span class=\"w\"> </span><span class=\"n\">renovate</span><span class=\"p\">.</span><span class=\"n\">repo</span><span class=\"w\"> </span><span class=\"o\">=</span><span class=\"w\"> </span><span class=\"n\">owners</span><span class=\"p\">.</span><span class=\"n\">repo</span><span class=\"w\">\n</span></span></span></code></pre></div><p>However, this ends up with me starting to write the query in Datasette, then copy-pasting the query I've written so far into Neovim to add the <code>owners</code> snippet and merge that in, then copy that back into Datasette.</p><p>This was getting a little awkward, especially in the case that I'm sharing my screen with folks, so there's an awkward \"BRB\" moment.</p><p>I was also looking at whether I could get Datasette to provide autocomplete when writing queries, but it <a href=\"https://github.com/simonw/datasette/issues/1614\">seems like it's not yet complete</a>.</p><p>I'd started to consider looking at trying out <a href=\"https://github.com/glacambre/firenvim\">firenvim</a> again, which is best for screen sharing, but I found a few rough edges when I was using it last (in 2023?).</p><p>Instead, I remembered that it's been a while since I've tried to follow the Language Server Protocol (LSP) setup for SQL, and so thought I'd give that a go.</p><h2 id=\"my-new-pure-neovim-setup\">My new, pure Neovim, setup</h2><p>I tried <a href=\"https://github.com/joe-re/sql-language-server\">sql-language-server</a> and <a href=\"https://github.com/sqls-server/sqls\">sqls</a> and found that sqls works <em>super nicely</em> for what I want, and it's now my daily driver.</p><p>It's got to the point where I'm now barely using Datasette as my interface, and instead using sqls and a local copy of the database, and then only moving to Datasette if I want to share the query URLs with other people.</p><p>Not only does sqls give me full autocomplete and hover comments based on columns, it also has the ability to execute the queries, so I can completely stay in Neovim.</p><p>For instance, this is what it looks like trying to write a query, then using the LSP functionality to execute that query:</p><p><asciinema-player src=\"https://www.jvt.me/casts/sql-workflow-2024/workflow.json\"></asciinema-player></p><p>(Note: there doesn't appear to be any colours shown in the Neovim editor due to <a href=\"https://discourse.asciinema.org/t/colors-in-neovim-not-shown/363/3\">a known issue</a>, but I promise there is, I'm not a monster who doesn't use syntax highlighting 🥲)</p><p>This setup is largely helped by the fact that the databases I'm working with are SQLite databases that I can run locally, and that I have tooling to make it easier to sync those local copies of the database from where they're produced.</p><p>You'll notice that we can use the inbuilt LSP functionality to execute queries, or in my case I've bound this to <code>&lt;leader&gt;x</code> for easier execution.</p><p>But sometimes it's useful to be able to share a query with someone else, which as mentioned is usually deployed centrally via Datasette.</p><p>For instance, for a given query I can call the <code>:Datasette</code> function (bound to <code>&lt;leader&gt;D</code>) and it'll construct the URL I need to access it on a given Datasette URL 👏 like so:</p><p><asciinema-player src=\"https://www.jvt.me/casts/sql-workflow-2024/datasette.json\"></asciinema-player></p><h2 id=\"does-it-work-for-me\">Does it work for me?</h2><p>This new setup gives me all the benefits I want out of being able to author and execute SQL:</p><ul><li>I can write queries with syntax highlighting</li><li>I can write queries with autocomplete for tables/columns</li><li>I can execute queries</li><li>I can construct shareable URLs</li><li>I don't have to switch between editors to achieve writing a single query</li></ul><p>I'm loving my new setup and it's making it a really nice experience for all the SQL I'm doing!</p>",
      "value" : "In the last ~18 months I've been writing a lot of SQL due to my work on dependency-management-data.\nAs I wrote about on dependency-management-data's birthday, I've been learning quite a few things about SQL and becoming much more fluent with writing it.\nI'll end up writing an SQL query against the dependency-management-data data at least once per day, and so I want to make sure that my workflow works for me.\nI've recently made some improvements to the local workflow I've been using for authoring SQL and thought it would be useful to share.\nNote: I've called this \"2024 edition\" but I've never written one of these before. However, I am expecting that in future years this process will be improved and I'll add new posts.Initial setup\nMy original setup was that I would write queries directly into the sqlite3, which were often copied from my Neovim editor where at least I had syntax highlighting and could easily create multi-line queries.\nNot long after, I learned about the excellent Datasette database browser, and started using that as my main interface for the queries I was writing.\nBut in the recent months, I've been finding that I'll be writing a query, and then need to join across one of a couple of tables. Writing this by hand got so repetitive that I've now got a snippet in my Neovim which completes to:left join owners on renovate.platform = owners.platform and renovate.organisation = owners.organisation and renovate.repo = owners.repo\nHowever, this ends up with me starting to write the query in Datasette, then copy-pasting the query I've written so far into Neovim to add the owners snippet and merge that in, then copy that back into Datasette.\nThis was getting a little awkward, especially in the case that I'm sharing my screen with folks, so there's an awkward \"BRB\" moment.\nI was also looking at whether I could get Datasette to provide autocomplete when writing queries, but it seems like it's not yet complete.\nI'd started to consider looking at trying out firenvim again, which is best for screen sharing, but I found a few rough edges when I was using it last (in 2023?).\nInstead, I remembered that it's been a while since I've tried to follow the Language Server Protocol (LSP) setup for SQL, and so thought I'd give that a go.My new, pure Neovim, setup\nI tried sql-language-server and sqls and found that sqls works super nicely for what I want, and it's now my daily driver.\nIt's got to the point where I'm now barely using Datasette as my interface, and instead using sqls and a local copy of the database, and then only moving to Datasette if I want to share the query URLs with other people.\nNot only does sqls give me full autocomplete and hover comments based on columns, it also has the ability to execute the queries, so I can completely stay in Neovim.\nFor instance, this is what it looks like trying to write a query, then using the LSP functionality to execute that query:\n\n(Note: there doesn't appear to be any colours shown in the Neovim editor due to a known issue, but I promise there is, I'm not a monster who doesn't use syntax highlighting 🥲)\nThis setup is largely helped by the fact that the databases I'm working with are SQLite databases that I can run locally, and that I have tooling to make it easier to sync those local copies of the database from where they're produced.\nYou'll notice that we can use the inbuilt LSP functionality to execute queries, or in my case I've bound this to <leader>x for easier execution.\nBut sometimes it's useful to be able to share a query with someone else, which as mentioned is usually deployed centrally via Datasette.\nFor instance, for a given query I can call the :Datasette function (bound to <leader>D) and it'll construct the URL I need to access it on a given Datasette URL 👏 like so:\nDoes it work for me?\nThis new setup gives me all the benefits I want out of being able to author and execute SQL:I can write queries with syntax highlightingI can write queries with autocomplete for tables/columnsI can execute queriesI can construct shareable URLsI don't have to switch between editors to achieve writing a single query\nI'm loving my new setup and it's making it a really nice experience for all the SQL I'm doing!",
      "lang" : "en-gb"
    } ],
    "author" : [ {
      "type" : [ "h-card" ],
      "properties" : {
        "name" : [ "Jamie Tanna" ],
        "photo" : [ {
          "value" : "https://www.jvt.me/img/profile.png",
          "alt" : "Jamie Tanna's profile image"
        } ],
        "url" : [ "https://www.jvt.me" ]
      },
      "lang" : "en-gb",
      "value" : "Jamie Tanna"
    } ]
  },
  "lang" : "en-gb"
}
